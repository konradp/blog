+++
title = "Redirect"
draft = false
+++
<html>
<head>
    <meta http-equiv="refresh" content="0; url=/post" />
    <link rel="canonical" href="/blog/post">
</head>
<body>
    <p>If you're not redirected automatically, <a href="/post">click here</a>.</p>
</body>
</html>
