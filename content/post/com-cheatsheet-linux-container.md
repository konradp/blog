---
title: "COM: Linux container (arachsys)"
subtitle: 
date: 2019-11-11
categories: wip
---

{{<toc>}}

# Types
Types:
- lxc

Attach bridge interface to VM.
```
virsh attach-interface VMNAME bridge em1brNUM --model virtio --config
```
Note: `--config` makes it permanent


# Config

type | path
--- | ---
lxc | `/etc/libvirt/lxc.conf`

# cgroup and network iptables
https://www.nova-labs.net/using-cgroups-and-net_cls-to-assign-specific-iptables-rules/  
https://www.evolware.org/?p=369  
Reference:  
https://www.kernel.org/doc/html/latest/admin-guide/cgroup-v1/net_cls.html

# arachsys containers
```
# Install containers
git clone https://github.com/arachsys/containers.git
cd containers
make
sudo make install

# Get alpine img
cd ..
wget http://dl-cdn.alpinelinux.org/alpine/v3.12/releases/armhf/alpine-minirootfs-3.12.0-armhf.tar.gz
mkdir alpine
tar -xf alpine-minirootfs-3.12.0-armhf.tar.gz -C alpine
~/containers/pseudo tar -xvf ../alpine-minirootfs-3.11.6-armhf.tar.gz
cd alpine

# Startup script
cat << EOF >> sbin/openrc
#!/bin/sh
ln -sf /dev/null /dev/tty1
ln -sf /dev/null /dev/tty2
ln -sf /dev/null /dev/tty3
ln -sf /dev/null /dev/tty4
ln -sf /dev/null /dev/tty5
ln -sf /dev/null /dev/tty6
EOF
chmod +x sbin/openrc

# Run container
contain . /sbin/init & echo $! > pid
PID=$(< pid)
inject $PID /bin/sh
```

At this stage, network won't work:
```
/ # ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8): 56 data bytes
ping: sendto: Network unreachable
/ # ip a
1: lo: <LOOPBACK> mtu 65536 qdisc noop state DOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
```

Networking
```
ROUTE=tutorial_route
HOSTIP=192.168.0.100
sudo mkdir /sys/fs/cgroup/net_cls/$ROUTE
# Note: dir gets auto populated
cd /sys/fs/cgroup/net_cls/$ROUTE
sudo sh -c 'echo 0x00110011 > net_cls.classid'
sudo iptables -t mangle -A OUTPUT -m cgroup --cgroup 0x00110011 -j MARK --set-mark 11
sudo sh -c "echo 11 $ROUTE >> /etc/iproute2/rt_tables" # just once!
sudo ip rule add fwmark 11 table $ROUTE
sudo ip route add default via $HOSTIP table $ROUTE
```
you will get
```
konrad@bananapipro:/sys/fs/cgroup/net_cls/tutorial_route$ sudo ip route add default via 10.0.10.58 table $ROUTE
Error: Nexthop has invalid gateway.
```

|https://rancher.com/learning-paths/introduction-to-container-networking/  
https://blog.scottlowe.org/2013/09/04/introducing-linux-network-namespaces/

```
NS=ns1
NET0=veth0
NET1=veth1
# NET0 is host-side, NET1 is container-side
ln -s /proc/$PID/ns/net /var/run/netns/$NS
#Equivalent: ip netns add $NS
ip link add veth0 type veth peer name veth1
ip link set veth1 netns $NS
ip netns exec $NS ip link list
ip netns exec $NS ip addr add 10.1.1.1/24 dev veth1
ip netns exec $NS ip link set dev veth1 up
```


