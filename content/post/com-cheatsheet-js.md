---
title: "javascript"
subtitle: 
date: 2022-12-10
tags: [ "com", "cheatsheets" ]
categories: cheatsheets
---

My js cheatsheet, to use for quicker lookup.
{{<toc>}}

## general
- style guide: https://google.github.io/styleguide/jsguide.html
- javascript vs. TypeScript vs. ECMAScript
    - javascript
    - TypeScript: Microsoft's javascript with types
    - ECMAScript: javascript standard

## various
- copy array: `let new = [...old]`
- detect mobile  
{{<highlight js>}}
// do this on window 'resize' event
if (navigator.userAgent.includes('Mobile') 
  || 'ontouchstart' in document.documentElement) {
{{</highlight>}}
- merge two objects, spread operator  
{{<highlight js>}}
let employee = {
    ...person,
    ...job
};
{{</highlight>}}
- **addEventListener()** [[ref]](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener)  
  {{<highlight js>}}
div.addEventListener('click', (e) => {
  e.stopPropagation(); // Avoid parent onclick
  let t = e.target;
})
// or
div.addEventListener('change', changeFn);
div.addEventListener('change', changeFn, true);
{{</highlight>}}
  signature
  ```
  addEventListener(type, listener)
  addEventListener(type, listener, options)
  addEventListener(type, listener, useCapture)
  ```  
  **Note:** **`useCapture`** bool: should this listener get the events before other elements beneath it in the DOM tree. (defaults to false)
- set dom element attribute
{{<highlight js>}}
el.setAttribute(name, value);
{{</highlight>}}
- input/label grid: https://stackoverflow.com/questions/9686538/align-labels-in-form-next-to-input
- range (slider input): on 'change' vs on 'input'
- timer  
  {{<highlight js>}}
setTimeout(() => {
  console.log('Delayed for 1 second.');
}, 1000);
setTimeout(someFunction, 1000);
{{</highlight>}}
  **Note:** Clear with `clearTimeout(timer_id)`, where `timer_id` is returned by `setTimeout()`
- data attributes  
{{<highlight js>}}
el.dataset['attrname'] = 'value';
// becomes attr data-attrname
{{</highlight>}}
- TODO: See https://stackoverflow.com/questions/23567825/how-to-set-an-event-listener-callback-to-a-member-function and calling class member functions from class member callbacks, https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener
- `parseInt('2')`
- sorttable: https://www.kryogenix.org/code/browser/sorttable/
- for...of ([ref](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of))
{{<highlight js>}}
for (const el of ['a','b','c']) {
  console.log(el);
}
{{</highlight>}}
- gitlab table of content: https://gist.github.com/nisests/d3f5c970be8161617031bcdeaa27b3a2

## DOM: getting elements
- [**`document.getElementById()`**](https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById)
- `document.getElementsByClassName()` (or on any element)
- **`querySelector()`**: first element  
{{<highlight js>}}
document.querySelector("select[name='waveform']");
document.querySelector("div[data-attr='val'][data-attr2='val2']");
{{</highlight>}}
- **`querySelectorAll()`**  
{{<highlight js>}}
document.querySelectorAll('.classname');
{{</highlight>}}
- **`fetch()`**  
{{<highlight js>}}
fetch('page.html')
  .then((response => response.text()))
  .then(data => {
    // do something with data
    // also, instead of .text()
    // we can have .json()
  })
{{</highlight>}}
- TODO: `getElementsByName()`
- TODO: `getElementsByTagName()`

## DOM: setting elements
- `el.setAttribute('attr', 'value')`
- innerHTML, innerText, textContent. TODO: See MDN for advice
- TODO: check out https://stackoverflow.com/questions/19030742/difference-between-innertext-innerhtml-and-value

## libraries
### canvas
- references:
    - [MDN: Canvas tutorial](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial)
    - [MDN: strokeRect()](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/strokeRect)
    - [MDN: draw images](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images#drawing_images)
- basic example  
{{<highlight js>}}
// <canvas id='canvas'></canvas>
const canvas = _getId(canvas_id);
const c = canvas.getContext('2d');
// Rectangles and circles
c.clearRect(x, y, width, height);
// fillRect(x, y, w, h);
// strokeRect(x, y, w, h);
// arc(x, y, rad, startAng, endAng, counterclockwise);
// arcTo(x1, y1, x2, y2, radius);
// CIRCLE
// arc(x, y, rad, 0, 2*Math.PI);
// Lines
c.beginPath();
c.moveTo(10, 20);
c.lineTo(20, 30);
c.stroke();
// Text
let text = c.measureText('some text');
// text.width and text.height
c.font = "50px serif";
c.fillText('some text', x, y);
// Styling
c.lineWidth = 5;
c.fillStyle = 'red';
c.strokeStyle = 'blue';
{{</highlight>}}
- `canvas.width, canvas.height` and `window.innerWidth, window.innerHeight`
- normalising mouse position inside canvas  
{{<highlight js>}}
var rect = this.canvas.getBoundingClientRect();
const mouseX = (e.clientX - rect.left)*(canvas.width/canvas.clientWidth);
const mouseY = (e.clientY - rect.top)*(canvas.height/canvas.clientHeight);
{{</highlight>}}

### web audio API
- oscillators are not re-usable: recreate it after stopping it (see [ref](https://stackoverflow.com/questions/37710236/restart-oscillator-in-javascript))
- get note frequency  
  {{<highlight js>}}
function noteToFreq(note, octave) {
  // note = e.g. 'C' or 'C#'
  // octave 0, 1, 2, 3, 4. The C4 is the middle C
  //  ref: https://pages.mtu.edu/~suits/NoteFreqCalcs.html
  //  f_n = f_0 * (a)^n
  //  where
  //   f_0: freq of A4 = 440
  //   n: # of half-steps from f_0
  //   a: (2)^(1/12)
  const notes = [ 'C', 'C#', 'D', 'D#', 'E',
    'F', 'F#', 'G', 'G#', 'A', 'A#', 'B' ];
  const n = notes.indexOf(note) - 9 + 12*(octave - 4);
  const a = Math.pow(2, 1/12);
  return 440 * Math.pow(a, n);
}
{{</highlight>}}


## helper functions
{{<highlight js>}}
function _new(tag) {
  return document.createElement(tag);
}
function _getId(id) {
  return document.getElementById(id);
}
{{</highlight>}}

## TODO
Unanswered questions:

- `document.addEventListener('DOMContentLoaded', ...)` vs. window... 'load'
