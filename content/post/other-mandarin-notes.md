---
title: 'ZH: Mandarin notes'
subtitle: 
date: 2022-04-13
tags: [ 'other' ]
categories: other
---
{{<toc>}}

<script>
function Load() {
  let words = [
    ['樓梯', 'lou2 ti1', 'stairs'],
    [ '希慣', 'xi2 guan4', 'n. habit / custom / v. accustomed to' ],
    ['放下', 'fang4 xia4', 'put down'],
    ['站住', 'zhan4 zhu4', 'stop / halt'],
    ['種', 'zhong3', 'species / race / branch / kind / type / seed'],
    ['聽', 'ting1', 'listen / hear'],
    ['生氣', 'sheng qi', 'v. take offence / get angry'],
    ['建康', 'jian kang', 'health / healthy'],
    ['不行', 'bu4 xing2', 'be allowed'],
    ['遠', 'yuan3', 'far away'],
    ['壓力', 'ya1 li4', 'pressure'],
    ['空間', 'kong1 jian1', 'space / room'],
    ['找', 'zhao3', 'look for'],
    ['走路', 'zou3 lu4', 'walk'],
    ['電梯', 'dian4 ti1', 'lift'],
    ['肘', 'zhou3', 'meat'],
    ['已經', 'yi3 jing1', 'already'],
    ['假日', 'jia4 ri4', 'holiday'],
  ];

  // SENTENCES
  let sentences = [
    [
      '我每個月都要向老板匯報工作',
      'wo3 mei3 ge4 yue4 dou1 yao4 xiang4 lao3 ban3 hui4 bao4 gong1 zuo4',
      'I have to report to my boss on my work every month',
    ], [
      '我跟朋友在周末見面的時候，會喝啤酒',
      'wo3 gen1 peng2 you3 zai4 zhou1 mo4 jian4 mian4 de shi2 hou4, hui4 he1 pi2 jiu3',
      'When my friends and I meet up on weekends, we drink beer',
    ], [
      '這個電影是關於什麼的?',
      'zhe4 ge4 dian4 ying3 shi4 guan1 yu2 shen2 me de?',
      'What is this movie about?',
    ], [
      '你看見了我給你發的合同嗎?',
      'ni3 kan4 le wo3 gei3 ni3 fa1 de he2 tong2 ma?',
      'Have you read the contract I sent you?',
    ], [
      '現在越來越冷冬天來了',
      'xian4 zai4 yue4 lai2 yue4 leng3 dong1 tian1 lai2 le',
      "It's now getting colder, winter is here",
    ], [
      '王老是住在倫敦哪兒?',
      'wang2 lao3 shi1 zhu4 zai4 lun2 dun1 na3 er2?',
      'Where in London does teacher Wang live?',
    ], [
      '這個項目終於結束了!',
      'zhe4 ge4 xiang4 mu4 zhong1 yu2 jie2 shu4 le!',
      'This project has finally come to an end!',
    ], [
      '這家酒店的房間不太乾淨',
      '...fang2 jian1...gan1 jing4...',
      'The rooms in this hotel are not very clean',
    ], [
      '我已經看完了這本書',
      '...yi3 jing1 kan4 wan2...',
      'I have already finished reading this book',
    ], [
      '我能說流利的西班牙語',
      '...liu2 li4...',
      'I can speak fluent Spanish',
    ], [
      '下個月我要去北京現在得買飛機',
      'I am going to Beijing next month, I need to buy the plane ticket now',
    ], [
      '為什麼你從來不來看我們?',
      'Why do you never come to see us?',
    ], [
      '我昨天寫了一個晚上的報告',
      'Yesterday I spent the whole night writing the report',
    ], [
      '我的家周圍有很多樹',
      'There are many trees around my house',
    ], [
      '現在的天氣很乾燥也很涼快',
      'The weather now is dry ans also cool',
    ], [
      '她很飽所以吃得非常少',
      'She is full, so she eats very little',
    ], [
      '這裡不乾淨請打掃一下',
      "It's not clean here, please do some cleaning up",
    ], [
      '感謝大家對我的幫助',
      'Thank you everyone for helping me',
    ], [
      '我的姐姐不要留在家裡',
      'My older sister does not want to stay at home',
    ], [
      '你知道有什麼好的應用麼?',
      'Do you know any good apps?',
    ]
  ];

  for (w of words) {
    let ul = document.getElementById('list_words');
    let li = document.createElement('li'),
        ti = document.createElement('span'),
        de = document.createElement('span');
    ti.className = 'term';
    de.className = 'definition';
    ti.innerHTML = w[0];
    de.innerHTML = w[1] + '<br>' + w[2];
    ti.appendChild(de);
    li.appendChild(ti);
    ul.appendChild(li);
  }

  for (w of sentences) {
    let ul = document.getElementById('list_sentences');
    let li = document.createElement('li'),
        ti = document.createElement('span'),
        de = document.createElement('span');
    ti.className = 'term';
    de.className = 'definition';
    ti.innerHTML = w[0];
    if (w.length == 3) {
      de.innerHTML = w[1] + '<br>' + w[2];
    } else {
      de.innerHTML = w[1];
    }
    ti.appendChild(de);
    li.appendChild(ti);
    ul.appendChild(li);
  }
}
</script>
<script>
  window.addEventListener("load", Load, false);
</script>





## Words
<ul id='list_words'></ul>

## Sentences
<ul id='list_sentences'></ul>




## Linking words
Once being able to say simple sentences such as 'I like this', 'I think this', 'This is...', we can combine these into multi-clause sentences, e.g.:

- if ..., then
- although ..., ...
- despite ..., ...

List:
- {{<t>}}和{{<d>}}and {{<br>}}ㄏㄜˊ{{</d>}}{{</t>}}
- {{<t>}} 如果{{<d>}}if{{<br>}}ㄖㄨˊ ㄍㄨㄛˇ{{</d>}}{{</t>}}
  - {{<t>}}如果你没有钱，我不能卖这个{{<d>}}If you don't have money, I can't sell you this{{</d>}}{{</t>}}
- {{<t>}} 如果。。。就。。。{{<d>}}if...then{{<br>}}ㄖㄨˊ ㄍㄨㄛˇ 。。。ㄐㄧㄡˋ{{</d>}}{{</t>}}
  - {{<t>}}如果週末下雪，我們就在家看電影{{<d>}}如果周末下雪，我们就在家看电影{{<br>}}If snows on the weekend, then we watch a movie at home{{</d>}}{{</t>}}
- {{<t>}} 雖然 。。。但是 。。。{{<d>}}although... but... {{<br>}}虽然 。。。但是 。。。{{<br>}}ㄙㄨㄟ ㄖㄢˊ 。。。 ㄉㄢˋㄕˋ 。。。{{</d>}}{{</t>}}
  - {{<t>}}雖然那條褲子很便宜，但是不舒服{{<d>}}Although that pair of pants is cheap, (but) they are not comfortable{{</d>}}{{</t>}}
- {{<t>}} 因為{{<d>}}because {{<br>}}因为 {{<br>}}ㄧㄣ˙ㄨㄟˋ{{</d>}}{{</t>}}
- {{<t>}} 先 。。。然後 。。。最後{{<d>}}first...then...finally {{<br>}}先。。。然后。。。最后{{</d>}}{{</t>}}


## Phrases
Useful phrases, for conversations.

### Helpers
- What does X mean?
- How do you say X?
- How do you pronounce this character?
- Can you please repeat?
- My Mandarin is not too great

### Directions
- How do I get to X?
- Is X far from here by tube?
- Which tube station is for X?

### General
- Can you drive?
- I can't drive
- I don't like cars

### Sports
- Can you ice-skate?
- I can ice-skate a bit, but I'm not very good at stopping
- Is there an ice-skate rink here?
- I like to play squash
- How much is the squash court session?

### Work
- What do you do for work?
- Do you like your work?

### Dinner
- Two people
- Can I have the menu please?
- What would you recommend today?
- I don't mind spicy

### Music
- What music do you like to listen to?
- Do you play any instruments?
- I play guitar/piano
- I'm not very good at piano
- I'm an amateur
- I play to relax


## Rooms
{{<t>}}夢想家園
{{<d>}}mèngxiǎng jiāyuán<br>dream home{{</d>}}{{</t>}}

rooms/places:

- {{<t>}}臥室
  {{<d>}}wòshì<br>bedroom{{</d>}}{{</t>}}
- {{<t>}}浴室
  {{<d>}}yùshì<br>bathroom{{</d>}}{{</t>}}
- {{<t>}}儲藏室
  {{<d>}}chúcáng shì<br>storage room{{</d>}}{{</t>}}
- {{<t>}}地下室
  {{<d>}}dìxiàshì<br>basement{{</d>}}{{</t>}}
- {{<t>}}廚房
  {{<d>}}chúfáng<br>kitchen{{</d>}}{{</t>}}
- {{<t>}}花園
  {{<d>}}huāyuán<br>garden{{</d>}}{{</t>}}
- {{<t>}}客廳
  {{<d>}}kètīng<br>living room{{</d>}}{{</t>}}
- {{<t>}}陽台
  {{<d>}}yángtái<br>balcony{{</d>}}{{</t>}}
- {{<t>}}車庫
  {{<d>}}chēkù<br>garage{{</d>}}{{</t>}}

items:

- {{<t>}}洗衣機
  {{<d>}}xǐyījī<br>washing machine{{</d>}}{{</t>}}
- {{<t>}}窗戶
  {{<d>}}chuānghù<br>window{{</d>}}{{</t>}}
- {{<t>}}灰色
  {{<d>}}huīsè<br>gray{{</d>}}{{</t>}}
- {{<t>}}鏡子
  {{<d>}}jìngzi<br>mirror{{</d>}}{{</t>}}
- {{<t>}}明亮
  {{<d>}}míngliàng<br>bright{{</d>}}{{</t>}}
- {{<t>}}木工
  {{<d>}}mùgōng<br>woodwork{{</d>}}{{</t>}}


## Resources
### mandarinspot.com
This site has some great features:

- [Annotate](https://mandarinspot.com/annotate):
  Paste Mandarin text and press 'Annotate'. You can then hover over the
  characters to see their translation.
- [Bookmarklet](https://mandarinspot.com/bookmark): Add Annotate to any website.
- [API](https://mandarinspot.com/api): Add Annotate to your own
  blog/website/program.
