---
title: "COM: Build FM synth with csound"
subtitle: 
date: 2022-06-14
tags: [ 'wip' ]
math: true
categories: wip
---
Here I attempt to build an FM synth. First, I learn how FM synthesis works, guided by the article here (https://blog.landr.com/fm-synthesis/). Then I will try out some of the ideas from the article. The ultimate goal is an FM synth which I can control by by MIDI controller (including certain parameters through the knobs on the MIDI controller).

**Aims:**
- FM synth, like Yamaha PSS-780
- MIDI controlled
- params such as ADSR are controlled by MIDI controller

Final aim features:
- ? how many operators? different algorithms? 
- filters?
- effects? reverb?

**Contents:**
{{< toc >}}

<style>
  canvas {
    border: 1px solid;
  }
</style>

<canvas id='ex1'></canvas>
<script src='/media/com-csound-fm-synth/js/ex1.js'></script>

## TODO
- (x) js osciloscope
- (x) js spectrum analyse, spectrogram
- (x) PM model/equation  
  write maths with latex, e.g. $x = {-b \pm \sqrt{b^2-4ac} \over 2a}$
- (x) is there a model for FM?

## Yamaha PSS-780
The operator configuration is printed on the front of Yamaha Portasound PSS-780.
<center>

![pss-780 operator arrangement](/media/com-csound-fm-synth/yamaha_pss-780.svg)
</center>

It has two operators, OPERATOR 1 (modulator) and OPERATOR 2 (carrier). The envelope generators for the operators are not the full ADSR, but rather an AD (attack and decay only).

**Note:** The envelope generator is AD, why? Wiki says that AD envelope can be used for oscillator sync. What is it, and is it something that is used here? (https://en.wikipedia.org/wiki/Envelope_(music)#Other_envelopes, https://en.wikipedia.org/wiki/Oscillator_sync).



## csound study
**Single operator**

<table>
<th>name/source</th>
<th>example</th>
<th>desc</th>
<tr>
  <td><a href='/media/com-csound-fm-synth/csound/01sine_wave.txt'>
    sine wave
    </a></td>
  <td>
    {{< player "com-csound-fm-synth/1sine.ogg" >}}</td>
  <td>
    This works. I can press keys on the MIDI controller,<br>
    and a sine wave is played (also, polyphonic). However,<br>
    because there is no envelope generator, the audio includes<br>
    clicking sounds.</td>
</tr>
<tr>
  <td><a href='/media/com-csound-fm-synth/csound/01sine_wave.txt'>
    with ADSR
    </a></td>
  <td>
    {{< player "com-csound-fm-synth/2sine_adsr.ogg" >}}
  </td>
  <td>Includes ADSR (the `madsr` opcode).</td>
</tr>
<tr>
  <td><a href='/media/com-csound-fm-synth/csound/03sine_wave_adsr_midi.txt'>
    with MIDI control
    </a></td>
  <td>
    {{< player "com-csound-fm-synth/3sine_adsr.ogg" >}}
  </td>
  <td>ADSR is controlled with MIDI controller knobs</td>
</tr>
</table>

**Two operators: sine and LFO**  
Here, the LFO modulates the sine wave. At low frequency the effect is a vibrato, and it becomes FM after around 30Hz.
<center>

![spectrogram 1](/media/com-csound-fm-synth/spectrogram1.png)<br>
{{< player "com-csound-fm-synth/4two_operators.ogg" >}}
</center>

**Note:** peak at 370 Hz, and other two peaks at 290Hz, 460Hz. Towards the end, when the note is held, the modulator modulates at 81Hz, however that 81Hz is not seen as a peak on the spectrogram.


## References
### Sites

todo:
- https://www.cs.cmu.edu/~music/icm-online/readings/fm-synthesis/fm_synthesis.pdf
- https://ccrma.stanford.edu/software/snd/snd/fm.html
- https://web.njit.edu/~gilhc/ECE489/ece489-IV.htm
- https://multimed.org/student/eim/en/04-FM.pdf

practical:
- https://blog.landr.com/fm-synthesis/

csound:
- [envelopes](http://www.csounds.com/journal/issue11/csoundEnvelopes.html)
- [FM synthesis Signal Generators](https://csound.com/docs/manual/SiggenFmsynth.html)
- [csound FM](https://flossmanual.csound.com/sound-synthesis/frequency-modulation)

other:
- https://www.soundonsound.com/techniques/introduction-frequency-modulation
- https://www.sfu.ca/~truax/Frequency_Modulation.html
- https://www.sfu.ca/~truax/fmtut.html
- https://www.allaboutcircuits.com/textbook/radio-frequency-analysis-design/radio-frequency-modulation/frequency-modulation-theory-time-domain-frequency-domain/
- https://www.allaboutcircuits.com/textbook/radio-frequency-analysis-design/radio-frequency-modulation/the-many-types-of-rf-modulation-radio-frequency/
- http://www.evalidate.in/lab2/pages/FMMod/FM/FM_T.html
- https://www.montana.edu/aolson/eele445/lecture_notes/EELE44514_L30-32.pdf
- https://docs.zhinst.com/hf2_user_manual/tutorial_frequency_modulation.html
- https://vlab.amrita.edu/index.php?sub=59&brch=163&sim=261&cnt=1
- https://unacademy.com/content/jee/study-material/physics/frequency-modulation-equations/
- https://man.fas.org/dod-101/navy/docs/es310/FM.htm
- https://www.elprocus.com/frequency-modulation-and-its-applications/
- https://user.eng.umd.edu/~tretter/commlab/c6713slides/ch8.pdf
- https://www.sciencedirect.com/topics/earth-and-planetary-sciences/frequency-modulation

### Data
- [Arturia minilab mkII knobs](/media/com-csound-fm-synth/arturia_ctrl.txt)
- approximating square wave with sine waves (I think this is from a DX7 book), table [here](/media/com-csound-fm-synth/sines_square.txt)
- save csound file: `csound -o out.ogg --ogg`
