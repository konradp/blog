---
title: "mysql"
date: 2022-11-08
tags: [ "com", "cheatsheets" ]
categories: cheatsheets
---

Create user and grant permissions
```
create user user@hostname identified by 'somepassword';
-- allowed from any host
create user user@'%' identified by 'somepassword';

grant all privileges on db.table to user@host;
grant all privileges on *.* to user@host;
grant all privileges on db.* to user@host;

flush privileges;
```

Create db and tables
```
drop database db_name;
create database db_name;
use db_name;

create table tbl_name (
  col_one varchar(20),
  col_two varchar(30)
);
create table tbl_name (
  id int not null auto_increment,
  col_one varchar(20),
  col_two varchar(30),
  primary key (id)
);
-- TODO: foreign key here

describe tbl_name;
show tables;
```

Dump database and recreate it.
```
sudo mysql -e 'drop database db_name'
sudo mysql -e 'create database db_name'
sudo mysqldump --compact db_name > db_name.sql
sudo mysqldump --compact --no-data db_name > db_name.sql
sudo mysql --force db_name < db_name.sql
```

Data manipulation
```
select * from tbl_name;
insert into tbl_name (col1, col2) values ('one', 'two');
insert into tbl_name (col1, col2) values ('one', 'two'), ('three', 'four');
delete from tbl_name where col1 = 'value';
UPDATE tbl_name SET column1 = value1, column2 = value2 WHERE id=100;
```

Table manipulation
```
alter table tbl_name add col_name int;
```

Views
```
CREATE VIEW stocks_perc_increase AS
SELECT
  s.ticker,
  i.name industry,
  round(100*(q.close-q.open)/q.close, 1) perc
FROM stocks s
JOIN industries i
  ON s.industry_id = i.id
JOIN quotes q
  ON s.ticker = q.ticker;
MariaDB [fintools]> select * from stocks_perc_increase limit 10;
+--------+----------------------------+------+
| ticker | industry                   | perc |
+--------+----------------------------+------+
| AACI   | N/A                        |  0.0 |
| AADI   | Biotechnology              | -2.2 |
| AAL    | Airlines                   | -0.8 |
| AAME   | Insurance                  |  4.6 |
| AAOI   | Communications             |  3.9 |
| AAON   | Building                   | -0.2 |
| AAPL   | Technology                 |  2.6 |
| AATC   | Electrical Equipment       |  0.8 |
| AAWW   | Logistics & Transportation | -0.4 |
| ABCB   | Banking                    | -0.4 |
+--------+----------------------------+------+
```


Data types

TYPE      | note        | example
--- | --- | ---
TINYINT   | -128 to 127               |  
SMALLINT  | -32768 to 32767           |
MEDIUMINT | -8388608 to 8388607       |
INT       | -2147483648 to 2147483647 |
DECIMAL   | decimal numbers           | DECIMAL(4)
FLOAT     |
DATE      | YYYY-MM-DD                | 2022-11-14

DECIMAL[(M[,D])], default: DECIMAL(10, 0)  
DECIMAL(1,2)  
DECIMAL(2)

- M: precision (total number of digits)
- D: scale (number of digits after decimal point)
- if D=0, no decimal part, numbers rounded
- max(M)=65
- max(D)=38

**Note:** see reference for full list, the types listed here are just my favourite.

# Reference
- https://mariadb.com/kb/en/create-user/
- https://mariadb.com/kb/en/grant/
- https://mariadb.com/kb/en/flush/
- https://mariadb.com/kb/en/create-database/
- https://mariadb.com/kb/en/create-table/
- https://mariadb.com/kb/en/mariadb-dumpmysqldump/
- https://mariadb.com/kb/en/backup-and-restore-overview/
- `man mysql`
- https://mariadb.com/kb/en/insert/
- https://mariadb.com/kb/en/foreign-keys/
- https://mariadb.com/kb/en/delete/
- https://mariadb.com/kb/en/update/
- https://mariadb.com/kb/en/alter-table/
- https://mariadb.com/kb/en/data-types/
- https://mariadb.com/kb/en/create-view/
