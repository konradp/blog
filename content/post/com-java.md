---
title: "COM: java: learning"
subtitle: 
date: 2023-06-12
categories: wip
---
{{<toc>}}

## TODO
general:
- compiled, curly braces
- java SE, SDK, JRE, ME, EE, JVM
- hello world
- testing and mocking in java
- basic examples, snippets, printing lines, import libraries

stack, setup and plugins:
- dev stack: IDEs, build systems, gradle, maven, testing, coverage, linting etc
- dev setup for java, gradle?
- lombok gradle plugin

what is:
- jvm
- java beans
- `final`
- `@Slf4j` simple logging facade?
- `@Autowired`
- jpa entity graph for querying DB
- `.stream().filter()`
- null

## Intro

### Paradigms
- generic (like templates in C++)
- object-oriented (class-based)
- functional
- imperative: uses statements that change program's state, describe how exactly operates. Compare to e.g. SQL (declarative)
- reflective: `typeOf()`, `eval` (e.g. C is not, go is through the 'reflect' pkg)
- concurrent: in general: multitasking, time-charing, processes, threads, coroutines, futures/promises, channels, in java: thread class or Runnable interface


### History
First appeared: 1995.05.23
TODO: Licensing: it doesn't look simple


### Syntax
- like C/C++
- strong on classes
- HelloWorldApp.java contains public HelloWorldApp class 


## Learning resources
- get a book or a course
- [docs.oracle.com: tutorials](https://docs.oracle.com/javase/tutorial/java/index.html)
- [wikibooks.org: Java Programming](https://en.m.wikibooks.org/wiki/Java_Programming)
- [codejava.net: 4 Best Free Java E-Books for Beginners](https://www.codejava.net/books/4-best-free-java-e-books-for-beginners)
- Java SE Specifications: [docs.oracle.com](https://docs.oracle.com/javase/specs/index.html)

## dev tools: IDEs and editors
TODO, consider:
- {{<x>}} IntelliJ Fleet <- lightweight (but it took like a 1GB of more of space on my laptop). Looks nice, but definitely doesn't open in one second, so removed it. And you install it through a 'jetbrains-toolbox' app which itself takes ages to load and feels like an ad. And I didn't reclaim all the disk space after uninstalling it
- VS Code: Takes a few seconds to load
- Eclipse
- jGRASP
- Cursor

### IntelliJ IDEA
- ref: [jetbrains.com: resources](https://www.jetbrains.com/idea/resources/)
- ref: [jetbrains.com: keyboard shortcuts](https://www.jetbrains.com/help/idea/mastering-keyboard-shortcuts.html)

Tasks:

- 1. power through the 'Getting started' guide

## Basics: compile and run
ref: [docs.oracle.com](https://docs.oracle.com/javase/tutorial/getStarted/cupojava/unix.html)

Edit file
```
vi HelloWorld.java
```
with content
```java
/**
 * The HelloWorldApp class implements an application that
 * simply prints "Hello World!" to standard output.
 */
class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World!"); // Display the string.
    }
}
```

Compile and run (compilation gives `HelloWorld.java`)
```
javac HelloWorld.java
java HelloWorld
```

## Deployment: jar files
refs:
- [docs.oracle.com: Trail: Deployment](https://docs.oracle.com/javase/tutorial/deployment/index.html)  
- [docs.oracle.com: Packaging programs in JAR files](https://docs.oracle.com/javase/tutorial/deployment/jar/index.html)

Notes:
- JAR: Java Archive, use `jar` tool

Commands:
- **`java -jar app.jar`**  
  run JAR-packaged app          
- **`jar cf OUT.jar IN_FILES`**  
  create JAR (**C**reate **F**ile), can be dirs also, opts:
  - `v`: verbose
  - `0`: no compression
  - `M`: don't create default manifest
  - `m`: use existing manifest file (see [modifying manifest file](https://docs.oracle.com/javase/tutorial/deployment/jar/modman.html)
  - `-C`: change dir
- **`jar tf FILE`**  
  view JAR (**T**able **F**ile), extra opt `v` for verbose
- **`jar xf FILE_JAR`**  
  extract JAR                   
- **`jar xf FILE_JAR FILES_ARCHIVED`**  
  extract files form JAR        
- **`jar uf FILE_JAR FILES_TO_ADD`**  
  update (add) files to a JAR file, opt `-C`


## Running java
More detailed


### applications
If there is a `HelloWorld.class` file:
```
java HelloWorld
```

If there is a `FILE_JAR`, e.g. `HelloWorld.jar`
```
java -jar FILE_JAR
```

### applets
invoke applet
```
<applet code=TicTacToe.class 
        width="120" height="120">
</applet>
```

invoke applet packaged as JAR:
```
<applet code=AppletClassName.class
        archive="JarFileName.jar"
        width="120" height="120">
</applet>
```



## References
- https://en.wikipedia.org/wiki/Java_(programming_language)
- https://en.wikipedia.org/wiki/Java_(software_platform)































