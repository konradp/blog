---
title: "Build Ardour from source"
subtitle: 
date: 2022-11-03
categories: computers
---

My steps for building Ardour from source.

Find out the latest stable version from https://github.com/Ardour/ardour/tags.  
In this example we are downloading version 7.1.

**Note:** First, uninstall the previous version in `~/Apps` dir with `sudo ./waf uninstall`.

```
VERSION=7.1
cd ~/Apps
git clone --depth 1 --branch $VERSION \
  git://git.ardour.org/ardour/ardour.git \
  Ardour-$VERSION
cd Ardour-$VERSION
./waf configure
./waf
./waf install
./waf clean
ardour7
```

# Notes
- compilation takes two hours

# Reference
- https://ardour.org/building_linux.html
