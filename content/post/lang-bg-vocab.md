---
title: "LANG: Bulgarian vocabulary"
date: 2023-04-02
tags: [ 'wip' ]
categories: wip
---

**{{<hv съдържание "table of contents">}}**
{{<toc>}}

## {{<hv Речник Vocabulary>}}
### TODO: Categorise
- {{<hv добър good>}}, {{<hv по-добър better>}}, {{<hv Най-добър best>}}
- {{<hv лош bad>}}, {{<hv по-лош worse>}}, {{<hv най-лош worst>}}


### {{<hv "Време и броене" "Time and counting">}}
- {{<hv първо first>}}, {{<hv второ second>}}, {{<hv трето third>}}, {{<hv четвърто fourth>}}, ...
- {{<hv предишен previous>}}, {{<hv текущ current>}}, {{<hv следващо next>}}, {{<hv последно last>}}
- **{{<hv "единици за време" "time units">}}:** {{<hv секунда second>}}, {{<hv минута minute>}}, {{<hv час hour>}}, {{<hv ден day>}}, {{<hv седмица week>}}, {{<hv месец month>}}, {{<hv година year>}}
- **{{<hv ден day>}}:** {{<hv сутрин morning>}}, {{<hv следобед afternoon>}}, {{<hv обед midday>}}, {{<hv вечер evening>}}, {{<hv нощ night>}}
- **{{<hv "относително време" "relative time">}}:**
    - {{<hv вчера yesterday>}}, {{<hv днес today>}}, {{<hv утре tomorrow>}}, {{<hv завчера "day before yesterday">}}
    - {{<hv "миналия месец" "last month">}}, {{<hv "следващия месец" "next month">}}
    - {{<hv "миналата година" "last year">}}, {{<hv "следващата година" "next year">}}
- {{<hv преди before>}}, {{<hv след after>}}, {{<hv "по време" during>}}, {{<hv "по средата на" "in the middle of">}}

### Joining words
- {{<hv от from>}}, {{<hv до to>}}
- {{<hv но but>}}, {{<hv или or>}}
- {{<hv "макар че" although>}}
- {{<hv ако if>}}, {{<hv тогава then>}}

### Tenses
- {{<hv беше was>}}, {{<hv ще will>}}

### Grammar
#### who, what
- {{<hv aз me>}}, {{<hv ти you>}}, {{<hv той he>}}/{{<hv тя she>}}/{{<hv то it>}}, {{<hv ние we>}}/{{<hv вие you>}}/{{<hv те they>}}
- my, yours, his/hers, theirs
- {{<hv мой "мой моя мое my">}}, {{<hv твой "твой твоя твое yours">}}
- this, that, these, those
- questions:
    - what, who, whose, when, where

#### where
- on, at, below, above, to the left/right of

#### when

### General
- inside/outside
- in/out

### Directions
- left/right
- front/back/straight, in front/behind
- here, there


### Restaurant
- bill/check/receipt
- card, cash

### Geography
- place, road, city, country

### Transport
- car, bus, bicycle


## Sentences
