---
title: "MUS: digital piano/synth"
subtitle: 
date: 2023-07-24
categories: wip
---

A digital piano with both acoustic piano sound and a synth capabilities.

{{<toc>}}


## ideal spec
- 88 keys
- graded hammer action
- MIDI out
- pitch wheel + mod wheel
- knobs: e.g. attack
- mic in + vocoder effects
- furniture-like stand

## Features

<center>
<img src='/media/mus-piano-digital/overview.png' style='width:100%'></img>
</center>

**Note:** None of these have aftertouch

<center>
<img src='/media/mus-piano-digital/features2.png' style='width:30%'></img>
</center>

## List

### winner: ROLAND FANTOM 8
<center>
<img src='/media/mus-piano-digital/roland_fantom8.png' style='width:100%'></img>
</center>

### ROLAND FANTOM 08
<center>
<img src='/media/mus-piano-digital/roland_fantom08.jpg' style='width:100%'></img>
</center>

### ROLAND FP-E50
<center>
<img src='/media/mus-piano-digital/roland_fpe50.jpg' style='width:100%'></img>
</center>

### ROLAND JUNO-DS88
<center>
<img src='/media/mus-piano-digital/roland_juno_ds88.jpg' style='width:100%'></img>
</center>

### ROLAND RD-2000
<center>
<img src='/media/mus-piano-digital/roland_rd2000.jpg' style='width:100%'></img>
</center>

### YAMAHA MODX8+
<center>
<img src='/media/mus-piano-digital/yamaha_modx8+.jpg' style='width:100%'></img>
</center>

### YAMAHA CP300
<center>
<img src='/media/mus-piano-digital/yamaha_cp300.jpg' style='width:100%'></img>
</center>


## Demos

### winner: Roland FANTOM 8
main page: [roland.com](https://www.roland.com/ca/products/fantom_series/features/)


<center>
piano sound #1 + other patches
<iframe width="853" height="480"
  src="https://www.youtube.com/embed/rzaq5Cls0zE"
  title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

<center>
piano sound #2
<iframe width="853" height="480"
  src="https://www.youtube.com/embed/G7xWE0ydjXQ"
  title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>


<center>
demo: youngr
<iframe width="853" height="480"
  src="https://www.youtube.com/embed/ROTFgD1LTRg"
  title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

<center>
superNATURAL engine demo
<iframe width="853" height="480"
  src="https://www.youtube.com/embed/LRXEWjCbwl8"
  title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

<center>
sequencing
<iframe width="853" height="480"
  src="https://www.youtube.com/embed/16sxYVdYYMg"
  title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

<center>
youtube video
<iframe width="853" height="480"
  src="https://www.youtube.com/embed/nUuMoe3Tc4g"
  title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>


### Roland FP-E50
<center>
<iframe width="853" height="480"
  src="https://www.youtube.com/embed/zyzWi3F1Pnw"
  title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

### Roland JUNO-DS88
<center>
<iframe width="853" height="480"
  src="https://www.youtube.com/embed/9SD3EzyPpXk"
  title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

### Roland RD 2000
<center>
<iframe width="853" height="480"
  src="https://www.youtube.com/embed/IrPH7H6-efk"
  title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

<center>
<iframe width="853" height="480"
  src="https://www.youtube.com/embed/Gvu162XleCM"
  title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

<center>
<iframe width="853" height="480"
  src="https://www.youtube.com/embed/E1yT1l7wyYs"
  title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

### YAMAHA MODX8+
<center>
<iframe width="853" height="480"
  src="https://www.youtube.com/embed/Ft5qaYLQIZ8"
  title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>
