---
title: "AWS CDK with Typescript"
date: 2023-11-13
categories: computers
---
My notes on using AWS CDK with TypeScript.

TABLE OF CONTENTS:
{{<toc>}}

## Notes
Following the doc: https://docs.aws.amazon.com/cdk/v2/guide/work-with-cdk-typescript.html

~/.aws/config
```
[default]
aws_account_id = <get_from_iam>
output = json
region = eu-central-1

[profile prod]
region = eu-central-1
role_arn = <get_from_iam>
source_profile = default

[profile test]
region = eu-central-1
role_arn = <get_from_iam>
source_profile = default
```
