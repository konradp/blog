---
title: "COM: javascript: smallsynth"
subtitle: 
date: 2023-05-12
tags: [ "wip" ]
categories: wip
---
A MIDI synth in the browser.

Table of contents:
{{<toc>}}


## Plan
Features:
- receives MIDI messages
- ADSR envelope
- wave types: sine, square, sawtooth

Features, extra:
- can play samples (e.g. piano, bass)
- load own sample packs
- fm synth
- additive synth
- subtractive synth
- oscilloscope

Features, low priority:
- micro-tuning individual notes, e.g. non-well-tempered tuning systems
features
- draw a keyboard

## Research
- in [Simple synth][mdn2] and piano-practice, we have one oscillator for each note that we can play.


## References
- my piano-practice repo
- MDN:
  - [Creating and sequencing audio][mdn1]
  - [Simple synth][mdn2]:
    - [demo][mdn2_demo]
- [Computer music programming][cmp]:
  - [Envelopes][cmp_envelopes]

<!-- REFERENCES -->
[mdn1]: https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API/Advanced_techniques
[mdn2]: https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API/Simple_synth
[mdn2_demo]: https://yari-demos.prod.mdn.mozit.cloud/en-US/docs/Web/API/Web_Audio_API/Simple_synth/_sample_.the_video_keyboard.html
[cmp]:  https://dobrian.github.io/cmp/topics.html
[cmp_envelopes]: https://dobrian.github.io/cmp/topics/building-a-synthesizer-with-web-audio-api/4.envelopes.html
