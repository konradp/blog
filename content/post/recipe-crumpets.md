---
title: "RECIPE: Crumpets"
date: 2024-05-09
tags: [ 'other' ]
categories: other
---

{{<toc>}}
<!--more-->

This recipe comes from https://www.recipetineats.com/crumpet-recipe/#wprm-recipe-container-48368. Apparently it's the recipe released by Warburtons

- prep time: 5min
- cook time: 20min
- makes: 6 crumpets

## Ingredients
- 150g (1 cup) white flour, plain / all purpose
- 200ml (3/4 cup + 1 tbsp) warm water, tap water (200g) (Note 1)
- 1/2 tsp salt, cooking/kosher salt (1/4 tsp table salt)
- 1/2 tsp white sugar
- 1 tsp baking powder

Yeast Mixture
- 1 tsp yeast, instant/rapid rise OR dry active yeast (Note 2)
- 1 tbsp warm water(just tap water)

## Instructions
Crumpet Batter:
- 1. Place flour, water and salt in a bowl and whisk for 2 minutes (electric beater 1 minute onspeed 5).
- 2. Yeast Mixture - Dissolve Yeast into 1 tbsp warm water in a small bowl.
- 3. Add Yeast Mixture, sugar and baking powder into bowl, then whisk for 30 seconds (or 15 sec speed 5).
- 4. Cover with cling wrap or plate, then place in a very warm place for 15 to 30 minutes until the surface gets nice and foamy. It will only increase in volume by ~10 - 15%.

Cooking Crumpets:
- 1. Grease 2 or 3 rings with butter (approx 9 cm / 3.5" wide, though any ring or metal shaper will do, Note 3) (TIP: Non stick rings - brush with melted butter. Everything else - smear with butter)
- 2. Brush non stick skillet lightly with melted butter then place rings in the skillet.
- 3. Turn stove on medium high (medium for strong stoves) and bring to heat (Note 4 for "sizzle test")
- 4. Pour 1/4 cup batter into the rings (65ml), about 1cm / 2/5" deep (will rise ~60%).
- 5. Cook for 1 1/2 minutes - bubbles should start appearing on the surface (but not popping yet).
- 6. Turn heat down to medium, cook for 1 minute - some bubbles should pop around the edges.
- 7. Turn heat down to medium low, cook for a further 2 1/2 to 4 minutes, until the surface is "set" and it's clear there will be no more bubbles popping! (At this stage you can help the final bubbles pop with a skewer!)
- 8. Remove rings (you might need to run knife around to loosen).
- 9. Then flip and cook the other side for 20 to 30 seconds for a blush of colour.
- 10. Transfer to write rack (golden side down) and fully cool.
- 11. Can be eaten once cool, but it's even better the next day (Note 5).

Notes
- Warm water - just tap water, warm enough that you'd want to take a bubble bath in it, not so hot that you'd scorch yourself.
- 200ml = 200g (handy so you can just pour straight in rather than measuring out separately!)
- Yeast - original recipe calls for normal active dried yeast. Works exactly the same with instant / rapid rise yeast - tried with both, no difference.
- Fresh yeast - Haven't tried but see no reason why it wouldn't work using the standard conversion of 7.75g / 0.275 ounces fresh yeast per 1 teaspoon of dry yeast. Crumble into the warm water with the 1/2 tsp sugar per recipe, and follow recipe as written.
- Rings - anything round like biscuit cutters, egg rings or even a cleaned empty tuna can (remove top and bottom, clean thoroughly and use labels, grease well). Though why restrict yourself to round?? Any cookie cutter will work here!
- Pan heat - the batter needs to sizzle gently when it hits the pan, otherwise it's not hot enough to get the bubbles happening. But if too hot, the crumpets will burn!
- TEST by putting a dab of batter on the end of a butter knife and pressing it on the skillet. Sizzle = hot enough. There should not be wisps of smoke coming from the pan at this stage (too hot).
- COOKING TIP: Heat control is key to crumpet success! You need stronger heat at the begin to get the holes bubbling, then lower heat so the crumpet cooks through without burning the base BUT still strong enough to make the bubbles "pop". The temps provided in the recipe are for a standard stove - if yours is extra strong (like the portable one I use for videos), dial it down a bit.
- Bubbles will start to pop around the edges first, then in the centre. There can be some wisps of smoke from the butter around the rings, but if it gets quite smokey, it means the skillet is too hot. If this happens, remove skillet from stove to cool it down a bit, then return it to the stove.
- Texture of crumpets really becomes just like store bought if you leave them overnight, more of that signature "rubbery" texture (I realise that sounds totally off-putting but I don't know how else to describe it!).
- Crumpet height / size - makes 6 crumpets using 9cm / 3.5" rings that are about 1.7cm / 2/3" high (store bought height). If you want to go a bit trendy-bistro style and make thicker ones, use a heaped 1/4 cup (about 1/4 cup + 1 tbsp) for each ring - you will get slightly less holes on the surface (thicker = less holes) but can make them about 2.2cm / just shy of 1" thick which looks very puffy and impressive - some trendy bistros charge upwards of $20 for house made thick crumpets!
- Different cup sizes - cups and tablespoons differ slightly between countries (with the US having the greatest variance to the rest of the world). It's best to make this recipe with the provided weights if you can, for absolute accuracy. But I did make it using US cups mixed with Aussie tablespoons and it works just fine. So it seems to be a pretty forgiving batter - it's the stove cook temp that makes the most difference
- Store in an airtight container in the fridge for 4 days, or freeze 3 months.
