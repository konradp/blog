---
title: "COM: Ansible tutorial"
date: 2018-10-20
tags: [ 'wip' ]
categories: wip
---

A practical Ansible tutorial using Docker Compose.
<!--more-->

The following packages are required for this tutorial.

- Docker (`sudo apt-get install docker`)
- Docker Compose (`sudo apt-get install docker-compose`)

Clone the tutorial repository and follow the `README.md`.

    git clone https://gitlab.com/konradp/ansible-tutorial.git
    cd ansible-tutorial
    cat README.md

In the tutorial we provision a test infrastructure with Docker Compose, which consists of the Ansible server, and Ansible nodes.

The tutorial covers:

- Creating a basic playbook
- Using inventory
- DOING: Using roles
- TODO: Using handlers
- TODO: Writing custom modules
