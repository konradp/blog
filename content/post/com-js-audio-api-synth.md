---
title: "javascript: Web audio API synth"
subtitle: 
date: 2023-01-02
categories: computers
---


Inspired by article https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API/Simple_synth

This is to create a simple synth which I can use in the trumpetjs project.

{{<toc>}}

## Components
Things I need:

- {{<cross>}} trumpet range
- {{<cross>}} notes table: from trumpet's low F# (concert E) to high C (concert Bb).
- {{<cross>}} notes drop-down: F#, G, G#, A, ...
- {{<cross>}} how to use oscillator (use the guide)
- {{<cross>}} sawtooth oscillator (for trumpet-like sound)
- {{<cross>}} timer to stop playing sound after 1 second
- {{<cross>}} a 'play' button

optional:

- {{<cross>}} ADSR envelope: adjust the attack rate for a less-harsh sound

### Trumpet range
According to [wiki](https://en.wikipedia.org/wiki/Trumpet#Range), written range:

- low: F#, below middle C (middle C: C4, has pitch 261.63 Hz)
- high: high C, two octaves above C4, that is, C6 (1046.402 Hz)

but, these are written, not concert pitches. The written C on trumpet sounds as Bb, a tone lower than what is written. So, the range in concert pitch is:

- low: E3, below middle C4
- high: Bb, just below C6

full table:

trumpet | concert | freq
:-- | :-- | :--
F# | E3  | 164.814
G  | F3  | 174.614


