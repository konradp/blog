---
title: "aws_cli"
date: 2022-11-29
tags: [ "com", "cheatsheets" ]
categories: cheatsheets
---
My cheatsheet and aliases.  

TODO:

- security group mgmt for EC2, firewalls, allow ssh from my IP
- creating roles, template the policy document, and specify just the Statement.Principal.Service=ec2.amazonaws.com (or codedeploy.amazonaws.com or other)
- EC2 shutdown
- EC2 tagging
- https://repost.aws/knowledge-center/lambda-function-retry-timeout-sdk

{{<toc>}}

## IAM
alias | cmd | example
--- | --- | ---
`a iamlr`      | `list-roles`<br>`--query "Roles[].[RoleName]"`<br>`--output text`
`a iamlp`      | `list-policies --query "Policies[].[PolicyName, Arn]"`
`a iamlarp`    | `list-attached-role-policies`<br>`--role-name NAME`
`a iamlip`     | `list-instance-profiles \|`<br>`jq -r '.InstanceProfiles[].InstanceProfileName`
`a iamlipfr`   | `list-instance-profiles-for-role`<br>`--role-name NAME`
`a iamgr`      | `get-role`<br>`--role-name NAME`
`a iamgip`     | `get-instance-profile`<br>`--instance-profile-name NAME`
**`a iamcr`**  | `create-role` <br>`--role-name ROLE`<br>`--assume-role-policy-document file://file.json` | `a iamcr`<br>`EC2InstanceRole`<br>`iam-ec2-policy.json`
`a iamcip`     | `create-instance-profile`<br>`--instance-profile-name NAME`
`a iamarp`     | `attach-role-policy`<br>`--role-name NAME`<br>`--policy-arn ARN`
`a iamartip`   | `add-role-to-instance-profile`<br>`--instance-profile-name NAME`<br>`--role-name NAME`
`a iamdrp`     | `detach-role-policy`<br>`--role-name NAME`<br>`--policy-arn ARN`
`a iamrrfip`   | `remove-role-from-instance-profile`<br>`--instance-profile-name NAME`<br>`--role-name NAME`
`a iamdr`      | `delete-role`<br>`--role-name NAME`
`a iamdip`     | `delete-instance-profile`<br>`--instance-profile-name NAME`

### Create a role

- iamcr
- iamarp: attach policy
- iamcip: create instance profile
- iamartip: attach instance profile

## EC2
cmd: `aws ec2 ...`


alias    | cmd
---      | ---
`a ec`**`ls`** | `describe-instances`<br>`--query`<br>`"Reservations[].Instances[].[InstanceId,PublicDnsName,State.Name]`<hr>`describe-instances --instance-ids ID`
`a ec`**`ri`** | `run-instances`<br>**`--image-id AMI-XXXXX`**<br>`--count 1`<br>`--instance-type t2.micro`<br>**`--key-name KEYPAIR`**<br>**`--security-groups NAME`**
`a ec`**`rm`** | `terminate-instances`<br>`--instance-ids ID`

### Prerequisites: images, keys, security groups, tags

alias          | cmd
:--            | :--
`a ec`**`amihvm`** | `describe-images [...] amzn2-ami-hvm-*-x86_64-gp2`
`a ec`**`amikrn`** | `describe-images [...] amzn2-ami-kernel-*-x86_64-gp2`
`a ec`**`amidot`** | `describe-images [...] amzn2-x86_64-MATEDE_DOTNET-*`
keypair-list    | `describe-key-pairs`
keypair-del     | `delete-key-pair --key-name NAME`
keypair import  | `import-key-pair`<br>`--key-name NAME`<br>`--public-key-material PUBKEY`<br>**Note:** PUBKEY is base64 enoded
`a ec`**`dsg`** | `describe-security-groups`<br>`--query "SecurityGroups[*].[GroupName]"`<br>`--output text`<hr>`describe-security-groups`<br>`--group-names NAME`
`a ec`**`ct`**  | `create-tags`<br>`--resources ID`<br>`--tags "Key=string,Value=string"`

### Create instance
- `a ecamihvm`
- `a ec2 describe-key-pairs`
- `a ecri`

Notes:

- `--subnet-id`: we omit this as we don't specify a network interface
- security groups: there is a 'default', and 'launch-wizard-1' from Console


### Free tier AMIs
name | id
---  | ---
`amzn2-ami-kernel-5.10-hvm-2.0.20221103.3-x86_64-gp2` | `ami-0beaa649c482330f7`
`amzn2-ami-hvm-2.0.20221103.3-x86_64-gp2` | `ami-0185a6f76b69a1870`
`amzn2-x86_64-MATEDE_DOTNET-2022.08.31` | `ami-08778753ef37aa408`

So we search by name:

- amzn2-ami-kernel-*-x86_64-gp2
- amzn2-ami-hvm-*-x86_64-gp2
- amzn2-x86_64-MATEDE_DOTNET-*

example:
```
aws ec2 describe-images \
  --owners amazon \
  --filters \
    "Name=name,Values=amzn2-ami-kernel-*-x86_64-gp2" \
  --query "reverse(sort_by(Images, &Name))[:1].[Name, ImageId]" \
```
giving rise to thre aliases:

- `a ecamikrn`
- `a ecamihvm`
- `a ecamidot`



## S3
cmd: `aws s3 ...`  
[ref](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/s3/index.html#available-commands)

title  | cmd | note
---    | --- | ---
`ls`   | `{{<t>}} ls [TARGET] {{<d>}}
                  ls s3://bucket {{</d>}}{{</t>}}`
`mb`   |         `mb s3://bucket` | make/create
`rb`   | `{{<t>}} rb s3://bucket {{<d>}}
                  rb s3://bucket --force {{</d>}}{{</t>}}` | remove/delete
`rm`   | `{{<t>}} rm s3://bucket/file {{<d>}}
                  rm s3://bucket/dir --recursive {{</d>}}{{</t>}}` | delete S3 object, e.g. file
`mv`   | `{{<t>}} mv SRC TARGET {{<d>}}
                  mv s3://bucket1/dir s3://bucket2/ <br>
                  mv local.txt s3://bucket <br>
                  mv s3://bucket/file.txt ./ {{</d>}}{{</t>}}`
`cp`   | `{{<t>}} cp SRC TARGET {{<d>}}
                  cp(stdin): cp - s3://bucket <br>
                  cp (cat):  cp s3://bucket - {{</d>}}{{</t>}}`
`sync` | `{{<t>}} sync SRC TARGET {{<d>}}
                  sync s3://bucket1 s3://bucket2 <br>
                  sync . s3://bucket <br>
                  sync . s3://bucket --delete {{</d>}}{{</t>}}`

### S3 API
The CLI provides a way to interact more closely with the S3 API. This is needed for example for enabling versioning on a bucket.  ([ref](https://docs.aws.amazon.com/cli/latest/reference/s3api/index.html#cli-aws-s3api))  
cmd: `aws s3api ...`  

cmd | notes
--- | ---
`get-bucket-versioning --bucket NAME` | returns none if not enabled
`put-bucket-versioning`<br>`--bucket NAME`<br>`--versioning-configuration Status=Enabled` | or `Suspended`
`list-object-versions` | 

## CC: CodeCommit
`aws codecommit ...`

alias | cmd
--- | ---
`cc`**`ls`** | `list-repositories`
`cc`**`lr`** | `get-repository --repository-name NAME`
`cc`**`rm`** | `delete-repository --repository-name NAME`
`cc`**`mr`** | `create-repository --repository-name NAME`


## CD: CodeDeploy
[ref](https://docs.aws.amazon.com/cli/latest/reference/deploy/index.html)  
cmd: `a deploy ...`  
alias: `a cd...`

alias | cmd
--- | ---
`cdla` | `list-applications`
`cdca` | `create-application --application-name NAME`
`cdga` | `get-application --application-name NAME`
`cdda` | `delete-application --application-name NAME`
`cdldg` | `list-deployment-groups --application-name NAME`
`cdcdg` | `create-deployment-group`<br>`--deployment-group-name NAME`<br>`--application-name NAME`<br>`--service-role-arn ARN`
`cdgdg` | `get-deployment-group`<br>`--application-name NAME`<br>`--deployment-group-name NAME`

### Notes
DeploymentGroup.deploymentConfigName:

  - CodeDeployDefault.OneAtATime
  - CodeDeployDefault.HalfAtATime
  - CodeDeployDefault.AllAtOnce

## CP: CodePipeline
[ref](https://docs.aws.amazon.com/cli/latest/reference/codepipeline/index.html)  
cmd: `a codepipeline ...`  
cmd: `a cp...`

alias | cmd
--- | ---
`cpcp` | `create-pipeline ` <hr>`create-pipeline --generate-cli-structure

Note:

- `create-pipeline --generate-cli-skeleton`

## TODO
### CLI syntax
The IAM CLI commands are very difficult to memorise. Consider wrapping them as below. Study the REST API first to see in case the API is already structured more like this.

cmd | meaning
--- | ---
role                               | list
role NAME                          | get
role NAME create FILE              | create
role NAME rm                       | delete
role NAME instance-profile         | list
role NAME instance-profile NAME rm | detach
instance-profile                   | list
instance-profile NAME              | get
instance-profile NAME create       | create
instance-profile NAME rm           | delete

### Default roles?
What are these roles? Did I create them?
```
$ a iamlr
AWSServiceRoleForAmazonSSM
AWSServiceRoleForSupport
AWSServiceRoleForTrustedAdvisor
```

## Notes
### General
I have `alias a='aws'` in my bashrc. Combined with the `~/.aws/cli/alias`, I can write this
```
a iamlr
```
instead of this
```
aws iam list-roles --query "Roles[].RoleName"
```

### Remember

- prefer use of `--query` and `--filter` instead of `| jq '....'` for quicker API responses with server-side filtering

## References
- https://docs.aws.amazon.com/cli/latest/userguide/cli-usage-alias.html
- S3:
    - [UG: S3](https://docs.aws.amazon.com/cli/latest/userguide/cli-services-s3-commands.html)
    - [MAN: S3](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/s3/index.html#available-commands)
- EC2:
    - [CLI ref](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/ec2/index.html)  
    - [filters/queries](https://docs.aws.amazon.com/cli/latest/userguide/cli-usage-filter.html)
    - [finding AMI](https://stackoverflow.com/questions/28168420/choose-a-free-tier-amazon-machine-image-ami-using-ec2-command-line-tools)
    - [finding AMI](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/finding-an-ami.html)
    - [CLI and EC2](https://docs.aws.amazon.com/cli/latest/userguide/cli-services-ec2-instances.html)
- IAM:
    - [CLI ref](https://docs.aws.amazon.com/cli/latest/reference/iam/)
