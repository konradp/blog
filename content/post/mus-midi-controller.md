---
title: "MUS: MIDI controller"
subtitle: 
date: 2023-10-05
categories: music
---

Currently I have Arturia Minilab mkii, but it has no MIDI out, and my setup is hardware-only.

{{<toc>}}


## ideal spec
- MIDI out
- knobs
- sliders
- pads
- small number of keys, or even no keys

legend:
- A?: from musicworld
- B?: from muziker

contestants:
- A1: ARTURIA MINILAB 3
- A2: AKAI MPK MINI PLUS
- A3: M-AUDIO AXIOM AIR 25
- A4: NATIVE INSTRUMENTS KOMPLETE KONTROL M32
- A5: NATIVE INSTRUMENTS KOMPLETE KONTROL A25
- B1: Arturia beatstep pro
- B2: Arturia beatstep


table

dev | priceA | priceB | MIDI    | knobs   | sliders | keys    | pads
--- | ---    | ---    | ---     | ---     | ---     | ---     | ---
A1  | 199    | 169/185| {{<v>}} | 8       | 4       | 25      | 8
A2  | 349    | 319    | {{<v>}} | 9       | {{<x>}} | {{<v>}} | 4
B1  |        | 519    | {{<v>}}
B2  |        | 224    | {{<v>}} | {{<v>}} | {{<x>}} | {{<x>}} |
    |        |        |         |         |         |         | 
    |        |        |         |         |         |         | 
    |        |        |         |         |         |         | 
    |        |        |         |         |         |         | 
    |        |        |         |         |         |         | 
    |        |        |         |         |         |         | 
