---
title: 'COM: app: Tuning'
date: 2023-09-01
tags: [ 'wip' ]
categories: wip
math: true
---
<link rel="stylesheet" href="/media/com-app-tuning/style.css">
<script src="https://konradp.gitlab.io/tuner/graph.js"></script>

An app for creating custom tunings by ear, microtuning. This is for my study of equal temperament and just intonation.

{{<toc>}}

## UI mockup
Premise:

- there are slides which correspond to frequencies
- on the right there is a 'chart' which shows these frequencies relative to some other frequencies (e.g. standard tuning).
- there is an oscillator for each slide
- each slide corresponds to a MIDI key
- the slides can be moved with a MIDI controller
- there should be a way of adding more sliders

Mockup
<iframe width="100%" onload="this.style.height=(this.contentWindow.document.body.scrollHeight+20)+'px';" src="/com-app-tuning-mockup.html"></iframe>

From this, we make a repo and a demo:

- https://gitlab.com/konradp/tuner
- https://konradp.gitlab.io/tuner


## App notes
### Improvement: imprecise slider
Some blog articles:

- [1](https://css-tricks.com/sliding-nightmare-understanding-range-input/)
- [2](https://ux.stackexchange.com/questions/105997/improving-the-sensitivity-of-range-sliders)
- [3](https://www.nngroup.com/articles/gui-slider-controls/)

something like this instead, but vertical
<img src="/media/com-app-tuning/toopreciseslider.png"></img>

A workaround alternative is to allow the range slider respond to mouse wheel actions, which move the slider in smaller increments. This has a problem in that it's not mobile friendly

### Other TODO
- **menus:**
  - {{<v>}} add settings menu
  - {{<v>}} add action menu
  - {{<v>}} add help menu
  - {{<x>}} action menu: note/slider: add/remove last
  - {{<x>}} action menu: add up to two overlays: e.g. chromatic scale, A/B/C/D... min/maj
  - {{<x>}} action menu: init well-tempered tuning, or old tunings Amin, Amaj, Bmin, Bmaj, ....
  - {{<x>}} slider menu: delete slider
  - {{<x>}} slider menu: move slider left/right
  - {{<x>}} slider menu: init pre-set frequency, e.g. concert A4
  - {{<x>}} settings menu: number of notes
  - {{<x>}} slider menu
  - {{<x>}} decide on the app page:
    - {{<x>}} should there be a main page for the project (emphasise the 'library' aspect of it, to be used in others' projects?)
    - {{<x>}} documentation
    - {{<x>}} or should it be just the app? I'd actually like to use it myself in interactive blog articles
  - {{<x>}} standalone mode, but with graph and 'play' buttons for each note and 'play' button for entire scale
  - {{<x>}} allow triggering notes with keyboard instead of midi keyboard, qwerty
- **MIDI:**
  - {{<v>}} trigger note with MIDI
  - {{<x>}} 'learn' MIDI note to slider assignment
  - {{<x>}} tweak frequency with MIDI
- **graph functionality:**
  - {{<v>}} zoom in/out with wheel
  - {{<v>}} move left/right with mouse drag
  - {{<v>}} horizontal lines
  - {{<x>}} hover tooltips showing the exact frequency
  - {{<x>}} fix canvas size (update canvas pixel size on resize)
  - {{<x>}} standalone graph
    - {{<x>}} hide the sliders
  - {{<x>}} if graph is in standalone, add cogwheel icon in top right
  - {{<x>}} if graph is in standalone, auto map MIDI keyboard keys
  - {{<x>}} highlight notes being played
  - {{<x>}} squeeze text into the margin space, avoid spillover
    - idea: specify font and margin on 'new Graph()'
- **synth functionality:**
  - {{<x>}} add piano samples
  - {{<x>}} add synth controls: ADSR
- **others:**
  - {{<x>}} fine tune
  - {{<x>}} rename user notes to 1,2,3,4,5,... by default
  - {{<x>}} move notes by dragging them left/right by the menu button
  - {{<x>}} add sequencer: but note, if MIDI support is there, can use hw/sw sequencers!
  - {{<x>}} youtube video: equal temperament vs. just intonation

### Bugfixes
- {{<x>}} A4 is not aligned on start on graph

## Thoughts

- how perceivable is just intonation vs equal temperament?  
  e.g. frusciante detuned string allegory (in californication?)
- **dynamic intonation**  
  we have equal temperament vs. just intonation. But with digital instruments, can we have something in between? Say, we call it 'dynamic intonation'. E.g. a song in Amaj that uses just intonation, but modulates into another key over time? And in the new key, the notes will be 'just' for that new key, e.g. the notes will shift tuning over time. Question: Would it even be perceivable?
- understand just intonation vs. equal temperament on guitar

### Constructing a just intonation: method 1
I don't quite get the description from [wiki](https://en.wikipedia.org/wiki/Just_intonation), or it's not entirely intuitive for me. So I try my own method and see if it's equivalent.

We try with a formula:
<center>
  $freq = fi/j$
</center>

where:

- $f$: fundamental note (e.g. 440Hz)
- $i,j \in \mathbb{N}$
- $440 <= freq <= 880$: we only include frequencies between the fundamental and the next octave

First few examples, manually calculated
{{<table sortable>}}
a | b | degree | include
--- | --- | --- | ---
1  | 440 * 1/1   = 440.00 |1  | yes
2  | 440 * 2/2   = 440.00 |   | no
3  | 440 * 2/1   = 880.00 |13 | yes
4  | 440 * 3/3   = 440.00 |   | no
5  | 440 * 3/2   = 660.00 |9  | yes
6  | 440 * 4/4   = 440.00 |   | no
7  | 440 * 4/3   = 586.67 |7  | yes
{{</table>}}

With a generator
<script src="/media/com-app-tuning/ex1-generator.js"></script>
<div id="example1">
  <center>
  fundamental:
  <input id="fundamental" type="number" value="440"></input><br>
  notes:
  <input id="notes" type="number" value="11"></input><br>
  <button id="generate">generate</button>
  </center>
  <br>
  <pre id="output">press 'generate'</pre>
</div>
These i/j look like numbers that have no common divisor other than 1.  

Table:
<table class="sortable">
  <thead>
    <th>k</th>
    <th>i</th>
    <th>j</th>
    <th>freq</th>
  </thead>
  <tbody id="ex1-tbl"></tbody>
</table>

TODO:

- Add a graph (maybe from tuning app) which shows these frequencies against an equal temperament, against a just intonation frequencies from wiki
- What if we take f=880? Will the generated notes for this octave be the octave-up from the notes of the previous octave?
- What does it sound like?

Graph:


<center>
<script src="/media/com-app-tuning/ex2-graph.js"></script>
<div id="example2">
  notes:
  <input id="ex2-notes" type="number" min="2" max=52" value="2"></input><br><br>
  <canvas id="ex2-canvas" style="border:1px solid"></canvas>
</div>

</center>


### Constructing a just intonation: method 2
The same formula:
<center>
  $freq = fi/j$
</center>

but here, $j$ is a power of $2$ because dividing a frequency by two moves it an octave lower.



### Another intonation
What about:

- 440 -> 880
- 440 + 440/2 = 440 + 220 = 660
- 440 + (660-440)/2 = 440 + 220/2 = 440 + 110 = 550
- 440 + (550-440)/2 = 440 + 110/2 = 440 + 55 = 495


### Another intonation
- 880-440 = 440
- 440/11 = 40
- 440 + 0*40 = 440
- 440 + 1*40 = 480
- 440 + 2*40 = 440 + 80 = 520
- ...

## Resources
- [note freqs](https://www.inspiredacoustics.com/en/MIDI_note_numbers_and_center_frequencies)
