---
title: "javascript: Move Canvas elements with mouse"
subtitle: 
date: 2023-02-02
categories: computers
---
<script src='/media/com-js-canvas-move/example7.js'></script>
<script src='/media/com-js-canvas-move/example8.js'></script>

Draw a Canvas JS **`arcTo()`** shape, and move its points with the mouse.

{{<toc>}}

## Motivation
In my article [COM: saxjs](http://konradp.gitlab.io/blog/post/com-saxjs/), I needed to use the **`arcTo()`** method to connect two lines with a round edge. There is a great example of this at [MDN's reference](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/arcTo) but it's a static image and I'd like to instead be able to click on the individual points and move them with the mouse to get a better feel for how this shape behaves.

## Plan
I'm going to take the [example from MDN](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/arcTo), which looks like this:

<center>
<img src='/media/com-js-canvas-move/mdn_arc.png'/>
</center>

and adapt it so that we can drag the red and blue dots with the mouse. For the mouse interactions I'll study a snippet from [this site](http://www.java2s.com/ref/javascript/html-canvas-mouse-drag-and-drop-shapes.html).

## Example study
I first study the example from [this site](http://www.java2s.com/ref/javascript/html-canvas-mouse-drag-and-drop-shapes.html). Observations:

- **touch events: desktop vs. mobile**  
  The example uses bool flags to track the states of the Mouse Events (`mouseOver=false, mouseMove, mouseDown, mouseUp`). For mobile, there are Touch Events such as `touchStart, touchMove, touchEnd`. I see that the big advantage of Touch Events is the ability to touch multiple points at the same time but I'll try to avoid using these, and use the Mouse Events instead  
  [ref: MDN: Touch Events](https://developer.mozilla.org/en-US/docs/Web/API/Touch_events)
- **mouse event listeners** (e.g. `mousedown`)  
  are attached to the entire **`canvas`**, not to the individual canvas 'shapes'
- **`context.isPointInPath(x, y)`**  
  is used for detecting if mouse is over a canvas path/shape
- **onclick mouse and dragging offset**  
  We can click anywhere on the shape to start dragging it so we can't just set its position to the current mouse coordinates - we need to take into account an offset
- **this vs that**  
  There is an usage of `this, that` in callbacks, and I don't yet fully have this at my fingertips, so I include it here:
  {{<highlight js>}}
listen() {
    // listen() is a member of a class Events
    const that = this;
    if (this.drawStage !== undefined) {
        this.drawStage();
    }
    this.canvas.addEventListener("mousedown", evt => {
        that.mouseDown = true;
        that.reset(evt);
    }, false);
{{</highlight>}}
  I remember that the other option is to use the **`bind()`** in this situation. Anyway, the **`this`** refers to the `Events` class, but becomes something else inside the `eventListener` callback, and so we introduce the **`that`** to be able to refer to class members inside the callbacks.  
  As a side note, I know that the last **`false`** param of the **`addEventListener`** is unnecessary here since it's `false` by default.
- **OOP, class usage**  
  The example has an `Events` class. It's similar to my typical Canvas classes in that the `canvas` ID is passed to the constructor, which then gets the canvas element with `document.getElementById(canvasId)`. One thing that is different from how I've done this before, is the **`setDrawStage`** member function which takes a drawing function as a parameter. This way, the main drawing function is defined outside of the class. I'm not yet sure if I will use that or not, but it's quite nice
- **`addRegionEventListener('mousedown')`**  
  It's a custom function of the `Events` class which I don't quite get:  
{{<highlight js>}}
addRegionEventListener(type, func) {
    let event = (type.indexOf('touch') == -1) ? `on${type}` : type;
    this.currentRegion[event] = func;
}
{{</highlight>}}
  There is an object called **`currentRegion`** which can look like this **`currentRegion = { onmouseover: func(), onmousemove: func2() }`**. And the interesting thing is that these functions can then be called like **`this.currentRegion.onmousedown()`**. This technique is new to me, and I also don't quite get **why** it's done like this. Perhaps it's to handle both the desktop and mobile (mouse vs. touch) events with the same code? Or it's to allow for the code to be extended to being able to move more than one shape?


## Prototype 1: Moving a single point
First we draw a single point which can be clicked on and moved with the mouse.  
([source](https://gitlab.com/konradp/snippets/-/blob/master/js/libs/html5canvas/public/example7.js))

<center>
<canvas id="example7" style="border: 1px solid; display:block;"/>
</center>

Main points:
- **`addEventListener()`** for the entire canvas, capture these MouseEvents:
    - `mousedown`
    - `mousemove`
    - `mouseup`
- **`context.isPointInPath(x, y)`** for detecting if mouse is over a canvas path/shape ([ref: MDN](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/isPointInPath))


### Mouse position relative to canvas
As I started writing this, I realised that when capturing the mouse position, we get a `MouseEvent` with these coordinates:

- `clientX, clientY`
- `screenX, screenY`

These are local DOM coordinates and global screen coordinates ([ref MDN: MouseEvent](https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent), but I require canvas-relative coordinates. Returning back to the example, I see something like this:
{{<highlight js>}}
    setMousePosition(evt) {
        const mouseX = evt.clientX - this.getCanvasPos().left
          + window.pageXOffset;
        const mouseY = evt.clientY - this.getCanvasPos().top
          + window.pageYOffset;
        this.mousePos = {
            x: mouseX,
            y: mouseY
        };
    }
{{</highlight>}}
where the `mouseX` and `mouseY` get calculated from the canvas position within the screen. I found a slightly simpler way on [stack overflow](https://stackoverflow.com/questions/17130395/real-mouse-position-in-canvas) which uses the **`getBoundingClientRect()`**:
{{<highlight js>}}
function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}
{{</highlight>}}

and after googling it further, this was the best that I could find, and so I will use it. I also note that some JS drawing libraries might provide a method for this, e.g. [Konva.js](https://konvajs.org/docs/sandbox/Relative_Pointer_Position.html)


### Getting reference to the path for **`isPointInPath()`**
Looking at the [MDN reference](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/isPointInPath), the `isPointInPath()` is to be used either while drawing a current path (between the `beginPath()` and `stroke()/fill()`), or can be invoked as **`isPointInPath(path, x, y)`** where the `path` is a [`Path2D` object](https://developer.mozilla.org/en-US/docs/Web/API/Path2D/Path2D). I'll use the `Path2D` for this.


## Final version: Multiple points and shapes
Building on the previous example, we can now draw the arc with movable points.  
([source](https://gitlab.com/konradp/snippets/-/blob/master/js/libs/html5canvas/public/example8.js))

<center>
<canvas id="example8" style="border: 1px solid; display:block;"></canvas>
<br>
<span>radius:</span>
<input id="radius" type="range" value="40"></input>
<span id="label">40</span>
</center>


## Conclusion: What I've learned
I've learned a few things which I didn't know before:

- how the **`arcTo()`** behaves
- MouseEvents vs TouchEvents
- **`isPointInPath()`** canvas method
- use **`getBoundingClientRect()`** to get canvas-relative mouse position
- use **`Path2D`** for storing a reference to a canvas path, e.g. for use in **`isPointInPath()`**

Next step, for the [COM: saxjs article](http://konradp.gitlab.io/blog/post/com-saxjs/), is to find out how to calculate the radius and control point coordinates for an **`arcTo()`** which can smootly connect/join two line segments.

<!-- INCLUDE EXAMPLES -->
<script>
  var e = new Example7('example7');
  var e8 = new Example8('example8', 'radius', 'label');
</script>
