---
title: "AWS DevOps exam"
date: 2022-11-17
categories: computers
---

Exam info:

- AWS Certified DevOps Engineer - Professional (DOP-C01)
- 180min, $300
- 75 questions: multiple choice + multiple response, Pearson VUE
- score: 100 - 1000, 750 to pass
- use us-east-2 for personal testing

Contents:

{{<toc>}}

## Progress: Study material
- {{<tick>}} [Course site](https://aws.amazon.com/certification/certified-devops-engineer-professional/)
- {{<tick>}} [Exam guide](https://d1.awsstatic.com/training-and-certification/docs-devops-pro/AWS-Certified-DevOps-Engineer-Professional_Exam-Guide.pdf)
- {{<tick>}} [Training: Exam Prep](https://explore.skillbuilder.aws/learn/course/external/view/elearning/9313/exam-prep-aws-certified-sysops-administrator-associate?devops=sec&sec=prep): expected: ??, completed in: 2hr48min
- {{<tick>}} [Training: Exam readiness](https://explore.skillbuilder.aws/learn/course/external/view/elearning/74/exam-readiness-aws-certified-devops-engineer-professional?devops=sec&sec=prep): expected: 7hr, completed in: 6hr
- {{<cross>}} [Sample questions](https://d1.awsstatic.com/training-and-certification/docs-devops-pro/AWS-Certified-DevOps-Engineer-Professional_Sample-Questions.pdf)
    - {{<tick>}} get links to study from answers
    - {{<cross>}} attempt all questions
    - {{<cross>}} all questions correct
- {{<cross>}} Research, user guides: https://docs.aws.amazon.com/index.html
    - {{<cross>}} IAM
    - {{<cross>}} CC: CodeCommit
    - {{<cross>}} CB: CodeBuild
    - {{<cross>}} CP: CodePipeline
    - {{<cross>}} CloudWatch
        - {{<cross>}} CloudWatch
        - {{<cross>}} CloudWatch Events
        - {{<cross>}} CloudWatch Logs
    - {{<cross>}} CloudFormation (including template guide)
    - {{<cross>}} AWS Config
    - {{<cross>}} AWS Systems Manager
    - {{<cross>}} AWS Service Catalog: admin guide
    - {{<cross>}} AWS Elastic Beanstalk
    - {{<cross>}} RDS
    - {{<cross>}} SNS dev guide
    - {{<cross>}} route53 developer guide
    - {{<cross>}} S3
- {{<cross>}} Research, whitepapers: https://aws.amazon.com/whitepapers/
    - [blue/green](https://d1.awsstatic.com/whitepapers/AWS_Blue_Green_Deployments.pdf)
    - [CD on AWS](https://docs.aws.amazon.com/whitepapers/latest/practicing-continuous-integration-continuous-delivery/)
- {{<cross>}} Research, FAQs: https://aws.amazon.com/rds/faqs/
- {{<cross>}} Practice?
- {{<cross>}} [Practice question set](https://explore.skillbuilder.aws/learn/course/external/view/elearning/12467/aws-certified-devops-engineer-professional-official-practice-question-set-dop-c01-english?devops=sec&sec=prep)
- {{<cross>}} **aws.training**
- {{<cross>}} aws.amazon.com/training (classroom training)
- {{<cross>}} aws.amazon.com/certification
- {{<cross>}} **http://aws.amazon.com/training/course-descriptions/devops-engineering/**

## Exam content
section | % of exam
--- | ---
1: SDLC Automation | 22%
2: Configuration Management and Infrastructure as Code | 19%
3: Monitoring and Logging | 15%
4: Policies and Standards Automation | 10%
5: Incident and Event Response | 18%
6: High Availability, Fault Tolerance, and Disaster Recovery | 16%

ref: Exam guide

## Glossary

acronym | meaning
--- | ---
AMI         | Amazon Machine Image
EB          | AWS Elastic Beanstalk
CP          | CodePipeline
CC          | CodeCommit
CB          | CodeBuild
CF          | CloudFormation
CT          | CloudTrail
CW          | CloudWatch
ECR         | Amazon Elastic Container Registry
ECS         | Amazon Elastic Container ServiceA
EKS         | Amazon Elastic Container Service for Kubernetes
IAM         | AWS Identity and Access Management
MFA         | Multi-factor authentication
RDS         | Amazon Relational Database Service
RPO         | Recovery Point Objective
S3          | Simple Storage Service
SDLC        | Software Developement Life Cycle
SES         | Amazon Simple Email Service
SNS         | Amazon Simple Notification Service
VPC         | Amazon Virtual Private Cloud
WAF         | Web Application Firewall

## Training notes: Exam Prep: AWS Certified SysOps Administrator - Associate

Domains:

- 1: Monitoring/Logging/Remediation
- 2: Reliability/Business Continuity
- 3: Deployment/provisioning/automation
- 4: Security/compliance
- 5: Networking and Content Delivery
- 6: Cost and Performance Optimization


### 1: Monitoring/Logging/Remediation

- Amazon CW:
    - CW Logs
    - CW Logs Insights
    - CW agent
    - CW dashboards
    - AWS CloudTrail Logs
    - CW alarms
- SNS
- SES
- Service Quotas
- AWS Health events
- remediation:
    - AWS EventBridge
    - AWS Systems Manager Automation
    - AWS Config

### 2: Reliability/Business Continuity:
scale rapidly (in/out), operate independently, balance traffic, tolerate failure, recover and restore

- **SUB1: implement scalability and elasticity**
    - auto scaling
    - caching:
        - Amazon ElastiCache
            - redis vs. memcached
      - Amazon CloudFront:
          - object expiration
          - DAX
          - DynamoDB Accelerator
    - read replicas (for RDS and Aurora): which DB support multi-region
    - loose coupling
- **SUB2: implement HA and resilient environments**
    - health checks (for LB and route53)
    - multi-az deployments
    - fault tolerance:
        - SQS, SNS, dynamoDB, EFS, S3
    - route53 routing:
        - failover
        - latency-based routing
- **SUB3: implement backup and restore strategies**
    - snapshots and backups:
        - RTO vs. RPO
        - Amazon Data Lifecycle Manager
        - how to set up RDS snapshots
    - DB restore;
        - promoting read replicas to primary DB
        - point-in-time restores
    - versioning
    - lifecycle rules
    - cross-region replication
    - disaster recovery (DR) procedures:
        - pilot light
        - warm standby architectures
- QUESTION: latency-based routing
- UNDERSTANDNG:
    - EC2 Autoscaling
    - horizontal vs. vertical
    - Amazon SQS (Simple Queue Service)

### 3: Deployment, provisioning, automation

- **SUB1: Provision and maintain cloud resources**
    - create and manage AMIs: EC2 Image Builder
    - use CF proficiently: create, manage, troubleshoot CF
    - provision acrocc multiple Regions and accounts: AWS Recource Access Manager, CF StackSets, IAM cross-account roles
    - select deployment scenarios and services: blue/green deployments, rolling environments, canary environments
    - identify and remediate deployment issues: permission issues, service quotas, VPC sizing, subnet sizing, CF errors, AWS OpsWorks errors
- **SUB2: Automate manual or repeatable processes**:
    - Automate deployments: OpsWorks or CF, EB, AWS Systems Manager
    - automate patching
    - schedule automated tasks: EventBridge, AWS Config
- QUESTION: metadata service: curl 168.254.169.254 to get pub IP
- CF
- AWS Systems Manager

### 4: Security and Compliance
- scope: user management, network controls, data classification methodologies, encryption (at rest, in transit), encryption key mgmt, storing secrets
- **SUB1: Implement and manage security and compliance policies**
    - IAM:
        - password policies
        - MFA
        - users, groups, roles, policy documents, resource policies (incl. conditions)
        - able to look at JSON of IAM policies
    - validating policies (and permissions), troubleshooting and auditing access issues (CT, but also loops in objects like IAM Access Analyzer or IAM Policy Simulator)
    - Trusted Advisor: how it works, which security controls it observes
    - Multi-account strategies: AWS Control Tower, AWS Organizations
    - Multi-region strategies (for compliance): validate region and service selection
- **SUB2: Implement data and infrastructure protection strategies**
    - Data classification: which data needs to be locked down, which public
    - Encryption at rest: managing enc keys with native options in S3, and other services, how KMS, and ClourHSM work
    - Encryption in transit: enc termination options, how to leverage VPN and AWS Certificate Manager
    - Secrets management:
        - AWS Secrets Manager
        - Systems Manager Parameter Store
    - Observation and reporting: AWS Config, GuardDuty, Security Hub, Inspector, Amazon Macie
- QUESTION: Move 500TB data to AWS Cloud S3, time/cost/performance. How?
    - rsync to S3 bucket
    - AWS Snowball, Edge compute-optimized appliances
    - AWS Snowball, Edge storage-optimised appliances: RIGHT ANSWER
    - AWS Storage Gateway (file gateway type), facilitate on-premises copy using the file gateway mount point
- UNDERSTANDING:
    - IAM
        - does everyone need direct console access?
        - if so, roles and federation
        - perm: launch/terminate resources in prod: consider enforced change management proces
        - automation tool to handle changes in the environment
        - almost no one to access the prod account
    - AWS CT
        - audit access to accounts and services
        - activities performed by users/roles/services/API activity
        - connect it to Amazon CW
    - Amazon Cloudwatch: enforce compliance, set up alerts
    - exam: go for the more secure solution always

### 5: Networking and Content Delivery
- Area 1: networking features and connectivity:
    - Amazon VPC: config various connection types to the VPC, managing subnets, route tables, network ACLs, security groups, NAT and internet gateways
    - network ACLs: stateless, work at subnet level
    - security groups: stateful, work at EC2 level
    - network ACLs: allow all by default, can use allow rules and block rules
    - security groups: block all inbound, allow all outbound by default, use only allow rules
    - route tables: attach to subnets
    - AWS Systems Manager Session Manager
    - VPC endpoints
    - VPC peering
    - VPN
    - AWS WAF
    - AWS Shield
- Area 2: config of domains, DNS, content delivery:
    - route53
    - hosted zones and records
    - route53 routing policies
    - general DNS configurations
    - Amazon CloudFront
    - Amazon S3
- Area3: Troubleshooting network connectivity issues:
    - evaluate VPC config: subnets, route tables, network ACLs, security groups
    - collect and evaluate logs for networking and content delivery:
        - VPC Flow logs
        - Elastic Load Balancer access logs
        - AWS WAF
        - web ACL logs
        - CloudFront access logs
    - troubleshoot content delivery services directly (CloudFront and S3)
    - troubleshoot route53 and connectivity issues, hybrid/private connectinos to VPCs
- Focus on:
    - Implementing networking features and types of connectivity
    - Configuring domains, DNS services, and content delivery
    - Troubleshooting networking and connectivity issues
- QUESTION: VPN connection to Amazon VPC, virtual private gateway, customer gateway, connecting to EC2 instance. Modify the route table in Amazon VPC subnet by doing:  Add the route for the virtual private gateway.
- UNDERSTANDING:
    - Amazon VPC: instead of making the EC2 instances public, set up a VPN to the VPC
    - Amazon API Gateway: use AWS WAF to protect against SQL injections, block users
    - AWS Shield: against DDoS: always-on detection, automatic inline mitigations

### 6: Cost and performance optimization
- Cost optimizations: where is spending going, find services that are underutilized
- Understand:
    - AWS Trusted Advisor
    - Cost Explorer
    - AWS Budgets
- Use tagging for cost allocation
- Consider:
    - AWS Compute Optimizer
    - Amazon EC Spot Instances
    - services with reserved capacity: EC2, RDS, DynamoDB etc.
- Know about managed services:
    - RDS
    - AWS Fargate
    - EFS
- EC2 optimizations:
    - EC2 instance types and configs
    - enhanced networking and placement groups
    - Amazon EBS metrics to modify EC2 configs
- S3 performance features:
    - S3 Transfer Acceleration
    - multipart uploads
- RDS metrics, Amazon RDS performance optimizations, Performance Insights, RDS Proxy
- QUESTION: file gateway to copy users' home drives to S3, files not accessed after 45 days, two methods to reduce storage costs and still provide some level of access to files
    - ANSWER: lifecycle policy: move files older than 45 days to S3 Glacier storage class
    - ANSWER: lifecycle policy: move files older than 45 days to Infrequent Access storage class
- UNDERSTANDING:
    - Amazon EC2 Spot Instances

## Training notes: Exam Readiness: AWS Certified DevOps Engineer - Professional
### SDLC Automation
- overview: cicd pipeline, source control, testing, artifacts, deployment/delivery (A/B, blue/green, canary, red/black)
- CI/CD: source > build > test > deploy > monitor
    - continuous integration
    - continuous delivery
    - continuous deployment
- CI goals:
    - new release when code checked in
    - build and test code in consistent&repeatable env
    - always have an artifact ready for deployment
    - close feedback loop when build fails
- CD goals:
    - deploy new changes to staging for testing
    - deploy to prod
    - increase deployment freq, reduce change lead time and failure rate
- immutable infrastructure
- AWS Code Services:
    - AWS CP:
        - source: CC
        - build: CB
        - test: CB
        - deploy: AWS CodeDeploy
    - monitor: Amazon CW
- detail:
    - CC:
        - managed, host private git repos
        - store anything
        - IAM integration
        - infra is HA
        - data is durable
        - code push > CC > CW Event > CB
    - CB:
        - managed build service, build from CodeCommit, S3, Bitbuchet, Github
        - build and test code, debug locally with CB agent
        - continuous scaling
        - pay as you go
        - build spec YAML file
    - CodeDeploy:
        - automates deployment to any instance
        - CodeDeploy agent installed on instances
        - doesn't do system config
        - AppSpec YAML config file
        - deployment config:
            - OneAtAtime
            - HalfAtATime
            - AllAtOnce
            - custom
        - lambda
    - CP:
        - actions and transitions
        - builds, tests, deploys code on every change
        - pipeline:
            - source control: CC
            - build, run unit tests: CB
            - deploy staging, run integration, performance, load tests:
                - CodeDeploy
                - CF
                - EB
                - OpsWorks
            - deploy prod
                - CodeDeploy
                - CF
                - EB
                - OpsWorks
        - Actions:
            - source: S3, CC, Github
            - build: CB, Jenkins, TeamCity
            - test: CB, Jenkins, Ghost Inspector
            - deploy: CF, CodeDeploy
            - invoke: custom function: Lambda
            - approval: Publish SNS topic for manual approval: SNS
        - cross-account pipeline with AWS KMS key, account policies/roles
- Automated testing:
    - overview:
        - unit tests
        - service/integration/component
        - performance/compliance
        - UI tests
    - continuous testing with IaC:
        - build stage: unit tests, static analysis, mocked dependencie and environments, vulnerability image scans
        - test stages: integration tests against real dependencies and real environments, load testing, penetration testing, monitoring to test impact of deployments on environment
    - Testing in CI/CD pipeline:
        - Development: unit tests, static code analysis
        - Build: integration/component/regression testing
        - Stage: system/performance/load/compliance/UAT testing
        - Production: A/B, canary
    - TDD: Test-driven development (unit tests): unit tests on every commit, independend unit tests, one failing unit doesn't affect others, test small parts of code with distinct functions, coverage, run fast fail fast
    - Integration vs unit:
        - integration:
            - more complex, dependent on multiple modules
            - shouldn't expose bugs in business logic
            - should expose bugs in interaction between modules
        - unit tests:
            - small units, easy to test
            - bugs in business logic
    - Fault tolerance testing:
        - take component down on purpose, e.g. terminate EC2 instance from CLI to see if EC2 AutoScaling provisions a new one
- Deployment strategies:
    - overview:
        - in-place: outage
        - rolling updates: few instances at a time, or canary (single resource)
        - blue/green deployment (A/B deployment) with route53 to allocate traffic 50%, Amazon RDS Multiple Availability Zones in the backend
        - red/black deployment???? how is it different from A/B?
        - immutable/disposable upgrades: throw away old, re-do entire architecture, use Elastic Load Balancing
    - AllAtOnce
        - impact: downtime
        - deploy time: 1
        - rollback: re-deploy
        - deployed to: existing instances
    - Rolling
        - impact: single instance/batch out of service
        - deploy time: 2
        - no downtime
        - rollback: re-deploy
        - deployed to: existing instances
    - Rolling with additional batch: same as above, but longer, and we have new instances too
    - Immutable
        - impact: minimal
        - deploy time: 4
        - zero downtime
        - rollback: terminate new instanes
        - deployed to: new instances
    - blue/green (with two envs):
        - impact: minimal
        - deploy time: 4
        - zero downtime
        - rollback: swap URL
        - deployed to: new instances

Questions:

- how to avoid storing creds in code repo:
    - git-secrets as pre-commit hook
- review security of build environments (CB), ensure only authed user can access service, and build artifacts are securely stored:
    - role with AWSCodeBuildAdminAccess policy
    - post-build command to push artifact to S3 bucket with default encryption enabled
- reduce cost of Jenkins/build environment
    - install EC2 plugin to Jenkins: auto launch and terminate EC2 instances based on builds
- AWS CodeStar, test nodejs project template, created CC repo, deployed the webapp using git cmds. Deployment cals lambda function called 'start-hello-world'. How to change the resource name?
    - CodeStar has template.yml file inside the repo
- buildspec.yml file to create docker image using CB. need to login to Amazon ECR and set repo URI in pre-build stage, but what options to use for Build and Post-build stages:
    - build with docker build, and push with docker push

### Configuration Management and Infrastructure as Code
- CF:
    - infra as code, templates, dev, CI/CD, automated testing for CI/CD env
    - components:
        - CF Template: parameters, mappings, conditions, resources, outputs
        - Architecture template
        - CF engine
        - architecture stack
    - **change sets: updating stacks**
        - no interruption, some interruption, replacement (new physical id generated)
        - updating CF metrics vs. resizing EC2 instance vs. update database backend
        - update policy applied to auto-scaling group:
            - MaxBatchSize
            - MinInstancesInService
            - PauseTime
            - WaitOnResourceSignals
            - SuspendProcesses
            - MinSuccessfulInstancesPercent
        - layering with nested stacks: chaining and nesting
        - depends on vs WaitCondition and WaitConditionHandle
        - /opt/aws/bin/cfn-signal
        - CreationPolicy: EC2 and autoscaling components
        - stack policies: updates are open by default but once created, needs allowing
        - Custom resource workflow
    - StackSets: accounts and regions from single template
    - CF helper scripts
        - **cfn-init**: bootstrap script
        - **cfn-hup**: /etc/cfn/cfn-hup.conf
        - cfn-signal
        - cfn-get-metadata
- EB
    - automated infra mgmt and code deployment for applications
    - load balancing, health monitoring, auto scaling, app platform mgmt, code deployment
    - components:
        - environment:
            - tiers: web server, worker
            - type: single-instance, auto scaling
            - one app per env, can have multiple envs
        - application versions:
            - app code, stored in S3
            - deploy different versions to different envs
            - multiple versions available to support rollbacks
            - support for rolling updates
        - configuration
            - config individual services used by EB
            - install additional yum packages, supply own daemon config files
    - versions:
        - in-place update: `eb deploy --version 1.1`
        - environment swap:
          ```
          eb clone -n myapp-v11
          eb deploy myapp-v11
          eb swap myapp-v10 --destination-name myapp-v11
          ```
    - customize: .eb config files
    - containers: ECS docker environments
- AWS OpsWorks
    - App infra mgmt: puppet or chef
        - OpsWorks for Chef Automate
        - OpsWorks for Puppet Enterprise
        - OpsWorks Stacks
    - OpsWorks Stacks: load balancer, app layer, db layer
    - chef recipes: passing metadata
- Containers
    - mgmt: ECS or EKS
    - hosting: EC2 or Fargate
    - image registry: ECR
    - CICD pipeline for containers
- Serverless: AWS Lambda
    - nodejs, java, python, C#, go, ruby
- Amazon API Gateway: REST and WebSocket
- choosing right solution:
    - high-level: Fargate, EB, OpsWorks
    - do it yourself: CF, ECS, EC2
- what to know:
    - functions of AWS CF in-depth
    - when and how to use CF, EB, OpsWorks
    - CI/CD pipeline of docker applications onto ECS
    - for routing 'portions of users' always chose Route 53

QUESTIONS:

- Q: update resources in CF stack, fails, on rolling back, stack enters UPDATE_ROLLBACK_FAILED state. Why?
    - A: stack resources were changed outside CF
- Q: blue/green deployment, blue: autoscaling group behind single LB, need a quick roll back over next 24 hours in case bugs emerge, how?
    - A: new Launch Configuration, attach to new AutoScaling group, register it behind a new LoadBalancer, create weighted route53 public record set, slowly transition traffic from old env to new env
- Q: CF stacks, cleanup delete stacks, delete failing:
    - A: VPC has EC2 instances created outside CF, CF stacks have termination protection enabled
- Q: migrate on-prem app to cloud, EB and SQS:
    - A: docker container that includes code, reploy it to EB worker env
- Q: LB in front of autoscaling group of web servers, new AMI created for new app ver. Move to A/B deployment for new versions, and migrate portion of users for min 24 hours to test the new version before moving rest:
    - A: route53 weighted round robin
- Q: use CF to push app changes, worked at first, but next deployments take 15 minutes. lower deployment time?
    - A: cfn-hup.conf, lower interval value

### Monitoring and Logging
- Amazon CW: collect, monitor(alarms+dashboards), act(autoscaling+events), analyze, compliance
    - 15 months
    - Metrics:
        - LB: ELB 5xx, HTTPCode_ELB5xx, HTTPCode_ELB_4xx, Latency, HTTPCode_Backend, **SurgeQueueLength**, **SpilloverCount**
        - EC2: CPUUtilization, StatusCheckFailed, CPUCreditUsage, CPUCreditBalance
        - network: NetworkIn, NetworkOut
        - I/O: DiskRead, DiskWrite
        - custom: app-specific
        - autoscaling group: min/max group size, desired capacity, stage of instances (InService, Pending, Standby, Terminating), Total instances, Common error codes 400 (AccessDeniedException, InvalidClientTokenId, InvalidParameterValue), 500 (Internal Failure, ServiceUnavailable), 400 (Handle error in application, 500 Retry operation
    - CW Logs: ELB access log file: every 5 min
    - CW alarms: build an alarm
    - CW events: console sign-in, autoscaling state change, EC2 instance state change, EBS volume creation, any API call
    - CW rules: match events to targets
    - CW targets: EC instances, lambda, etc
- Additional logging:
    - AWS CT:
        - user activity + API usage
        - console events
        - who made request, when, from where, what was requested, what was response
        - best practices: enable in all regions, enable log file validation, encrypted logs, integrate with CW Logs, centralize logs from all accounts, create additional trails
    - AWS X-Ray
        - performance bottlenecks, errors
        - pinpoint services
        - identify impact
        - visualise service call graph
    - Monitor CI/CD:
        - CC: triggers to sent SNS notifications or invoke lambdas
        - CB: notifications on build state change
        - CodeDeploy: triggers can send SNS notifications before, during, after deployment
        - CP: notifications on pipeline/state/action status change
- Logging
    - inform AWS Config, CW agent installed
    - VPC Flow Logs
    - Amazon Kinesis: gathering and processing logging data
    - best practices: monitor everything: metrics, logs, events, traces
- Tagging:
    - manage, search, filter

QUESTIONS:

- Q: prep for yearly audit, access to app, system, AWS API logs, how?
    - A: CW, CW logs, CT
- Q: website, autoscaling enabled, high response times during peak hours. edit auto scaling polity to reduce, how?
    - A: more number of servers in autoscaling group, custom CW metric for no of requests, autoscaling to use its alarm
- Q: 16-18 days ago, periods of 1 minute latency on web app, how to NOT find out why?
    - A: CF logs don't show scaling events, CT logs don't show ELB latency rates
- Q: recent deployment, spike in 404s, build real-time monitoring to catch it in future, how?
    - A: install CW logs agent and point it to logs, filter on the log group for 404 errors, create CW alarm for elevated rates
- Q: stateless web app, 3 LB app servers on EC2, 1 RDS DB with IOPS storage. avg response time s increasing, 95% CPU usage on app servers, min mem usage, 20%CPU usage on DB with minimal disk IO, how to improve?
    - A: different EC2 instance (bigger), autoscaling for more app servers

### Policies and Standards Automation

- Securing infrastructure
    - IAM: user, group, role, policy document
    - use roles over permanent security creds (users)
    - steps to create cross-account access
    - trust relationship
    - how to define in trust policy which users are allowed to assume it
    - individual user must be given policy document to be able to assume it
    - IAM policy document: action, condition, resource. condition: e.g. IP range, or multifactor auth present. doesn't work when attached to users, need to be attached to temp credentials
    - data protection: in transit: TLS, IPSEC VPN, AWS Cert Manager
    - data protection: data-in-flight: ssl terminatino at ELB, certs in IAM, multiple SSL certs per LB
    - Amazon CloudFront
    - network perimeter controls: Security groups, NACLs, host firewalls
    - familiar with general VPC architecture
    - data protection: at rest:
        - S3 server-side encryption, supports:
            - customer managed keys
            - S3-managed keys
            - KMS master keys
        - amazon EBS: server side or host
        - Amazon Glacier: encryption is by default
        - Amazon EFS supports encryption only through AWS KMS
        - AWS KMS: managed AWS service for encryption keys
- Amazon GuardDuty: network intrusion detection
    - managed service, protect AWS accounts, workloads, monitor AWS env, threat lists and trusted IPs: analyzes data from CT, VPC Flow Logs, DNS Logs
- Amazon Inspector
    - security of OSs and applications
    - rules, templates, custom rule sets
- AWS Systems Manager
    - agent, software inventory, apply OS patches, linux, windows, create system images, config windows and linux
    - features:
        - **run command**:
        - state manager
        - inventory
        - maintenance window
        - **patch manager**
        - automation
        - **parameter store**
            - store passwords, EC2 instances will pull them (like Vault)
        - session manager
    - Security credential storage options:
        - AWS System Manager Parameter Store
        - AWS Secrets Manager: strictly for creds, encrypted via AWS KMS, auto cred rotation: additional charge
        - AWS License Manager: app license storage, license use tracking, with custom enforced usage rules: windows licence, office suite
- AWS Config:
    - e.g. all EBS volumes need to be encrypted
- Cost optimization:
    - AWS Trusted Advsor
    - tagging: aws ec2 describe-instaces -filters Name=tag:Environment,Values=Development
- Governance and compliance:
    - AWS Service Catalog:
        - catalogs of approved IT services, limit access to underlying AWS services, governance, compliance
        - how to use with other services, how access is managed and controlled, how to use it to restrict
- EC2 instance compliance:
    - customize AMIs to be used by users, bootstrap them with custom user data scripts
    - config mgmt: puppet, chef, ansible, AWS OpsWorks
- Summary:
    - AWS Config, AWS Inspector
    - access control to all services, auth methods
    - encryption in transit, at rest
    - AWS Systems Manager features in depth

QUESTIONS

- Q: enc at rest+in transit, LB, tored on EBS volumes for procesing, results stored in S3 with AWS SDK
    - A: tcp LB on LB, SSL term on EC2, os-level disk enc on EBS vols, S3 with server-side enc
    - A: SSL termination on LB, SSL listener on EC2, EBS enc on EBS volumes containing PHI, S3 with server-side enc
- Q: admin left company, root user, personal IAM admin account
    - A: change pass, add MFA to root
    - A: rotate keys and change passes on IAM users
    - A: delete admin's IAM user
- Q: manage EC2 instances, patch within 7 days of patch release, list of instances in and out of compliance. How to automate instead of logging into each host and updating it
    - A: patch baseline in AWS Systems Manager Patch Manager
- Q: improve build process of app code. CI pipeline to build AMIs, how?
    - A: AWS Systems Manager Automation document to generate the AMI, create a Jenkins job to run it every time there's change in app code

### Incident and Event Response
- Troubleshooting tips:
    - centralize log storage
    - log as much as you can
    - keep logs
    - CW + X-Ray
- Discern patterns by analyzing data: utilization patterns, maximum, minimum, averages, ELB access logs, aggregate metrics into CW through Amazon EMR
    - notify on access keys from root user used for API calls:
        - CT push logs to CW
        - CW Logs metric filter
        - CW Alarm to SNS email notification
- Configuration management:
    - OpsWorks
    - EB
    - CF: cfn-init, cloud-init
- Automated healing, differences between products:
    - CF: define infra, use demplates to deploy collections of resources
    - EB: deploy code to cloud, quickly redeploy entire stack in few clicks
    - OpsWorks: manage infra, auto host replacement, combine with CF in the recovery phase
- Auto scaling and lifecycle hooks
    - scaling processes
    - detach instance: move from one autostaling group to another, testing purposes
    - standby state: remove instance from service, troubleshoot or make changes, put back into service
- Suspending autoscaling services
    - launch: disrupts other processes
    - terminate: disrupts other processes
    - AlarmNotifications: notify from CW alarms
    - ReplaceUnhealthy; terminate unhealthy and create new instances to replace them
    - HealthCheck
    - AZRebalance: balance across AvailabilityZones in the region
    - AddToLoadBalancer
- Autoscaling termination policies:
    - default: instances span AZs
    - oldest instance: upgrade to a new EC2 instance type
    - newest instance: test new config but don't want to keep it in prod, can be customized
    - AllocationStrategy: replace on-demand instances of lower priority with higher priority
- EC2 auto scaling group lifecycle hooks
    - put actions on hold, trigger other actions at specified stages, e.g. while scaling out or scaling in, remove logs, or tie into 3rd party services, e.g. add/remove to monitoring solution
    - Autoscaling summary:
        - combine multiple types of autoscaling
        - Step Scaling
        - scaling on two or more metrics (not just CPU)
        - scale out early and fast, scale in slowly
        - use lifecycle hooks: to trigger autoscaling launches, what can be suspended or terminate instances
- Suspending load balancing services
    - disable a specific AZ
    - configure slow start
    - target groups: deregister targets, delete target group
    - delete LB (note: deletion protection might be enabled)
- AWS CodeDeploy (and autoscaling)
    - agent on the instance
    - events we can control: ApplicationStop, BeforeInstall, AfterInstall, ApplicationStart, ValidateService
    - events we can't control: Start, DownloadBundle, Install, End
- Summary:
    - understand autoscaling and lifecycle hooks
    - logging architectures
    - various methods for automation for recovery
    - suspending load balancing and auto scaling processes

QUESTIONS:

- Q: enforce tagging structure actoss EC2, for later audit. how to terminate badly tagged EC2 instances asap?
    - A: config a rule in AWS Config to look for non-compliant EC2 instances. Then CW Event to run a custom Lanbda to terminate it
- Q: autoscaling group of instances, processing messages form SQS queue. scales based on queue depth. CPU-intensive, long-running job, can't be decomposed. When group scales in, instances are terminated while they are processing. How to reduce number of incomplete process attempts?
    - A: put instance into Auto Scaling Standby while it's processing msg, return to InService once done
- Q: code repo with S3 as data store. maintain integrity of data in S3 bucket, and securely deploy code from S3 to apps running on EC2 in VPC instance. How to mitigate?
    - A: S3 bucket policy, require MFA to delete, enable bucket versioning. Create IAM role with authorizatino to access S3 bucket. All app EC2 instances should use this role
- Q: issues with recent deployment of app. CodeDeploy to deploy code to EC2 instances in autoscaling group. EC2 comes online, code deployed, EC2 attached behind LB. EC2 then fails the ELB health check and terminates, what to do? 2:
    - A: Suspend ReplaceUnhealy autoscaling process to let dev to sign in and troubleshoot before it's terminated. Then, add ValidateCode event to CodeDeploy to validate before it's put into service

### High Availability, Fault Tolerance, and Disaster Recovery
- objectives:
    - multi-Avalability Zone vs. multi-region
    - implement: HA, scalability, fault tolerance
    - right services for business needs: RTO/RPO, cost
    - disaster recovery strategies
    - evaluate deployment for points of failure
- HA:
    - route53 for multi-region
    - RDS, DynamoDB, S3, EBS
    - multi-az setup for RDS
    - replication for EBS: custom AMIs, EBS snapshots
- key points:
    - HA factors: scalability, fault tolerance, recoverability
    - multi-regions for services
    - multi AZ architestures
    - enabling elasticity
- Scalability
    - Lambda@Edge
    - Middle-tier scaling architecture
        - SQS (queue for each direction), SNS
        - fan-out processing architectures: fan out to SQS, SQS queue subscribed to SNS topic
    - data storage scaling architecture
        - static content to S3
        - scale S3 and EBS
    - DB scaling: resize quickly, extend in read replicas, cache common requests (ElastiCache in front of DB)
    - scaling DynamoDB: scale read/write capacity units
    - cache with DynamoDB Accelerator (DAX)
- Disaster recovery:
    - RPO vs. RTO: how much can lose vs. how long app can be unavailable:
        - backup and restore
        - pilot light
        - warm standby
        - hot standby (with multi-site)
- Failover

QUESTIONS:

- Q: web server on EC2 behing a LB, using RDS. how to self-healing architecture (cost-effective)?
    - A: autoscaling group for web server that uses EC2 CPU utilization CW metric
- Q: web app no EC2, behing ELB. slow customer page load time. how to ensure that load time is not affected by too many requests per second?
    - A: re-deploy in CF, EB and autoscaling. Scale off number of request per second and the customer load time
- Q: auto scaling group of EC2 resources, two S3 buckets. EC2 downloads user-created img from 1st S3 bucket, applying watermark, then upload to second S3 bucket. Autoscaling group mn size of 25, max size of 50. suddenly 50 instances are being used and not much is being processed, where it used to be 40 and all good. What's wrong?
    - A: analyze app logs

### Wrap up

## Sample questions

pass1    | score       | pass2 | score
---      | ---         | ---   | ---
1. D     | {{<cross>}} | B     | {{<tick>}}
2. AE    | {{<cross>}} | AD    | {{<tick>}}
3. C     | {{<tick>}}  | C     | {{<tick>}}
4. BDF   | {{<cross>}} | BDE   | {{<tick>}}
5. BCF   | {{<cross>}} | ADE   | {{<tick>}}

## User guides

### UG: CP
https://docs.aws.amazon.com/codepipeline/latest/userguide/welcome.html

- deploy
    - to EC2, using:
        - CodeDeploy
        - EB
        - OpsWorks Stacks
    - container-based apps by using:
        - ECS

Action: types and providers:

action type | providers
--- | ---
**source**   | S3 (versioned), ECR, CC, CodeStarSourceConnection (Bitbucket, GitHub)
**build**    | CB, Custom Jenkins (install and config CP plugin on every Jenkins instance and allow CP to HTTPS/SSL to it.<br> If Jenkins is on EC2, provide AWS creds by installing AWS CLi on each instance,<br> then config AWS profile on these instances), Custom TeamCity, Custom CloudBees
**test**     | CB, AWS Device Farm, Custom Jenkins, ...
**deploy**   | S3, CF, CF StackSets, CodeDeploy, ECS, CodeDeployToECS (for blue/green), EB, AppConfig, OpsWorks, ServiceCatalog, Alexa
**approval** | Manual, SNS (publish to a topic)
**invoke**   |  Lambda, Step Functions (state machine)

Statuses

object | statuses
--- | ---
pipeline          | InProgress, Stopping, Stopped, Succeeded, **Superseded**, Failed
stage             | InProgress, Stopping, Stopped, Succeeded, Failed
action            | InProgress, **Abandoned**, Succeeded, Failed
inbound execution | InProgress, Stopped, Failed

Stop execution: pipeline visualisation page, or CLI stop-pipeline-execution:

- Stop and wait: can retry later: recommended
- Stop and abandon: can retry later: can result in out-of-sequece tasks. can be used when we have custom actions that don't need to finish

Manage pipeline flow:

- transitions: can be enabled/disabled
- approval action
- failure

Pipeline structure:

- stage for each epp env, AWS region or AZ
- test, deploy and approval in single stage: DeployTestEnv = Deploy + Test

Inbound execution: next stage locked by another execution, or transition is disabled: check in CLI: get-pipeline-state

IO artifacts:

- S3 artifact bucket we chose
- CP zips and transfers the files
- output art of one stage is the input art for the next stage
- CP creates S3 bucket for the arts. For new pipelines, CP will create new folders in the same bucket, name: codepipeline-region-12345EXAMPLE
- art bucket needs to be in the same AWS region as the pipeline
- out arts need unique names

Getting started:

- attach **AWSCodePipeline_FullAccess** IAM managed policy to the IAM user. Note: this is fullaccess!

Integrations with CP:

- CW
- CW Events
- Cloud9
- CT
- CodeStar Notifications
- KMS: for cross-account resources, customer-managed KMS key

## AWS Technical Essentials:
First covers: intro, compute, networking

### Storage
- block vs object:
  - block storage: split file
  - object storage: single unit
  - static data: WORM: write once, read many model <- object storage is fine
  - frequently updated data, high transaction rates <- block storage
- EFS vs FSx
- block storage: EC2 Instance Store vs Amazon EBS
  - EC2 Instance Store: internal, ephemeral, tied to EC2 instance
  - EBS volumes: external, network-attached, persistent, Elastic Block Store
    - SSD vs HDD
  - backup, EBS snapshots
- object storage: S3
  - S3 Bucket Policies

### Databases
- RDS: relational, charges per hour
- Amazon DynamoDB: NoSQL, charges by usage
- Amazon DocumentDB: with MongoDB compatibility
- Amazon Neptune: graph database
- Amazon QLDB: immutable ledger database
- Amazon MemoryDB: redis-compatible
- Amazon Keyspaces: for Apache Cassandra
- Amazon Timestream: timeseries for IoT


### ELB: Elastic Load Balancing
- ALB: Application Load Balancer: layer 7, HTTP/HTTPS, routing based on content, supports auth
  - listener: port+protocol, e.g. 443/http
  - target group: e.g. EC2 instances, IP addresses, lambda, ALB
  - rule: condition (source IP addr), action (which target grou pto send to)
- NLB: Network Load Balancer: layer 4, TCP/UDP, routing based on IP
  - target groups: IP, EC2, ALB
  - listeners: TCP/UDP/TLS
- GLB: Gateway Load Balancer: layer 3+4: IP, for third-party appliances
  - target group: IP, EC2
  - listeners: IP


### EC2 Auto Scaling
- launch template/launch configuration, EC2 Auto Scaling group, scaling policies
- simple scaling
- step scaling


#### Tutorials
- See https://konradp.gitlab.io/blog/post/com-aws-ug-cp/
- AWS Technical Essentials: did at work: https://explore.skillbuilder.aws/learn/course/1851/play/85986/aws-technical-essentials
- AWS Observability: did at work: https://explore.skillbuilder.aws/learn/course/internal/view/elearning/14688/aws-observability
- things to read:
  - https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/working_with_metrics.html
  - https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/metrics-collected-by-CloudWatch-agent.html
  - https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/Statistics-definitions.html
  - https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/MonitoringLogData.html
  - https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/cloudwatch_concepts.html
  - https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-state.html
  - https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/installing-cloudwatch-agent-ssm.html
  - https://docs.aws.amazon.com/systems-manager/latest/userguide/sysman-compliance-about.html
  - https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/create-cloudwatch-agent-configuration-file-wizard.html
  - https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/CloudWatch-Agent-Configuration-File-Details.html
  - https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/metrics-collected-by-CloudWatch-agent.html
  - https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/WhatIsCloudWatchLogs.html
  - https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/CloudWatch_Dashboards.html
  - https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/CloudWatch_Anomaly_Detection.html
  - https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/Create_Composite_Alarm.html
  - https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/AlarmThatSendsEmail.html
  - https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/Create_Anomaly_Detection_Alarm.html
  - https://docs.aws.amazon.com/awscloudtrail/latest/userguide/monitor-cloudtrail-log-files-with-cloudwatch-logs.html
  - https://docs.aws.amazon.com/awscloudtrail/latest/userguide/cloudtrail-user-guide.html
  - https://docs.aws.amazon.com/awscloudtrail/latest/userguide/cloudtrail-event-reference-aws-console-sign-in-events.html


## Study log

date                 | dur      | note
---                  | ---      | ---
20221117 17:00-17:30 | 30min    | Read course prep material
20221117 17:36-18:28 | 1hr      | Exam prep - sysops admin
20221117 21:10-21:40 | 30min    | Exam prep - sysops admin
20221117 21:50-22:07 | 17min    | Exam prep - sysops admin
20221117 22:10-22:44 | 34min    | Exam prep - sysops admin
20221117 23:30-23:57 | 27min    | Exam prep - sysops admin - done
20221118 16:05-17:26 | 1hr20min | Exam readiness - devops
20221121 11:25-12:00 | 25min    | Exam readiness - devops
20221121 12:07-13:05 | 1hr      | Exam readiness - devops
20221121 14:45-15:41 | 1hr      | Exam readiness - devops
20221121 16:02-17:15 | 1hr15min | Exam readiness - devops
20221122 12:32-13:25 | 40min    | UG: CodePipeline
20221123 10:40-11:44 | 1hr      | UG: CodePipeline
20221123 14:30-15:19 | 50min    | UG: CodePipeline
20221123 16:15-17:00 | 45min    | UG: CodePipeline
20221127 22:15-22:49 | 25min    | UG: CodePipeline
20221128 10:20-11:00 | 40min    | UG: CodePipeline
