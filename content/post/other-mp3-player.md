---
title: "OTHER: MP3 player"
date: 2023-11-03
categories: other
---

{{<toc>}}

I have a collection of mp3s and wavs (30GB+). I store these on OneDrive and they take up a lot of space. I'd like to go over them in my spare time (e.g. when walking), and mark music I don't like for deletion to free up some space. Also, currently I use Spotify for music, and I've noticed music sometimes disappears from there. Also, Spotify got involved in cancel culture recently, so having an mp3 player would ensure that I keep my music forever.

Here I compare various mp3 players so I can choose the one I like.

list

name                         | price | bat(hr) | disk(GB)
---                          | ---   | ---     | ---
Astell&Kern A&norma SR25 mkII|       | 20      | 64
Astell&Kern AK Jr            |       | 9       | 64
Astell&Kern A&ultima SP2000T |       | 9       | 256
Cowon Plenue D3              | 1079  | 45      | 64
FiiO M11S                    |       | 14      | 32
Sony NW-A306                 |       | 4       | 16
Sony NW 155L                 |       | 45      | 16
Sony NW-E394LB               |       | 34      | 8
Onkyo DP-X1A                 |       | 16      | 64


Notes:
- Korg Nature 6P1 tubes in Shanling M30 32 GB, also in overdrive by Nu:Tekt OD-S

## References
- [2023 techradar.com](https://www.techradar.com/news/best-mp3-players-techradars-guide-to-the-best-portable-music-players): Initial ideas
