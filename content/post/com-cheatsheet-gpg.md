---
title: "gpg"
subtitle: 
date: 2024-01-08
tags: [ "com", "cheatsheets" ]
categories: cheatsheets
---
## Info
list keys
```
gpg --list-keys
```

get info about key file
```
gpg --list-packets pubkey.asc
```
- RSA: algo 1, algo 2, algo 3
- DSA: algo 17

## Create, delete
create key pair
```
gpg --full-generate-key
```
delete pub and secret key
```
gpg --delete-key "User Name"
gpg --delete-secret-key "User Name"
```

## Export/import

export pub key
```
gpg --export -a "User Name"
```
export priv key
```
gpg --export-secret-key -a "User Name"
```

## Encrypt / decrypt
encrypt
```
gpg --encrypt --output FILE_OUT.gpg --recipient example@example.com FILE_IN
```
decrypt
```
gpg --decrypt --output FILE_OUT FILE_IN.gpg
```

## Reference
- http://irtfweb.ifa.hawaii.edu/~lockhart/gpg/
- https://www.gnupg.org/gph/en/manual/x110.html
