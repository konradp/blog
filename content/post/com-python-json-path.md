---
title: "COM: Python XPath for JSON"
date: 2023-02-05
tags: [ 'wip' ]
categories: wip
---

List:

- jsonpath: https://pypi.org/project/jsonpath/
- jsonpath-ng: https://pypi.org/project/jsonpath-ng/
- jsonpath-rw: https://pypi.org/project/jsonpath-rw/
- dpath: https://pypi.org/project/dpath/
- jmespath: https://pypi.org/project/jmespath/
- jq: https://pypi.org/project/jq/
- pyjq: https://pypi.org/project/pyjq/

# Comparison

{{<table sortable>}}
lib          | stars | forks | open_issues | open_PRs
---          | --:   | --:   | --:         | --:
jsonpath     | ?     | ?
jsonpath-ng  | 386   | 69    | 67          | 17
jsonpath-rw  | 579   | 181   | 36          | 6
dpath        | 555   | 86    | 15          | 6
jmespath     | 1770  | 168   | 41          | 12
jq           | 258   | 44    | 8           | 6
pyjq         | 184   | 27    | 13          | 0
{{</table>}}

winners:

- jmespath
- jsonpath-rw
