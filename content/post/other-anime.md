---
title: 'OTHER: Anime'
date: 2021-05-23
tags: [ 'other' ]
categories: other
---
<!--<script src='/media/other-anime/data.js'></script>-->
<script src='/media/other-anime/core.js'></script>

<body onload='main();'></body>
<table style='border:0px solid #000000'>
  <th>columns</th>
<!--  <div style='float: left;'>
    <label for='cols'>Show/hide column:</label><br>-->
  <th>show</th>
  <tr>
    <td style='border:0'>
    <select name='cols' id='cols' onclick='showHideCol(value);'></select>
    <!--<label for='show-watched-unwatched'>Show:</label><br>-->
    </td>
    <td style='border:0'>
    <select onChange='flipShowHideWatched()'>
      <option value='watched'>watched</option>
      <option value='unwatched'>unwatched</option>
    </select>
    </td>
  </tr>
<br>

<table id='t-watched' class='sortable'>
  <thead><tr id='t-watched-head'></tr></thead>
  <tbody id='t-watched-body'></tbody>
</table>

# Legend
Where unspecified, the value is a score of 0-10

- **title**
- **score**  
  How much I like the anime
- **audience-age**  
  0:childish, 5:teen, 10:adult  
  'seriousness' of the anime
- **available**  
  [ 'yes', 'no' ]  
  Is available on Netflix
- **episodes** null,
- **fanservice** null,
- **genre** null,
- **gore** null,
- **music** null,
- **music-theme** null,
- **released** null,
- **series** null,
