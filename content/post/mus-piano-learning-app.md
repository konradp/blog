---
title: 'MUS: Piano learning app: Exercises'
subtitle: 
date: 2022-04-13
tags: [ 'wip' ]
categories: wip
---

Earlier, I wrote [synth](https://gitlab.com/konradp/synth) ([code here](https://gitlab.com/konradp/synth)), which reads notes from a MIDI keyboard, and plots the average note velocity on a line graph. This could be used as an exercise in practicing playing uniform trills. This post builds on that example, and lists ideas for other exercises which I can think of, ordered by difficulty.

**Note**: The 'synth' would read MIDI messages, but if we could also use pitch detector, we could use an acoustic piano and a mobile phone microphone.

# Exercises
- Scales:
  - major
  - minor
- Scale degrees: 1, 2, 3, 4, 5, at random
- Chords I:
  - Given a scale, play all 'natural' chords, moving up: I, ii, III etc.
    - scale: major, minor, or both
  - Given a scale, play chord I, II, or III:
    - scale: major, minor, or both
    - the chords will be 'natural' for the scale, e.g. I, ii, III etc. (the 'nice-sounding chords)
- Chords II:
  - Given a scale, play chord I, i, II, ii
- Chords III: diminished chords
- Chords IV: seventh chords
  - for each scale, major and minor
- Chord progressions:
  - Given a scale, play progressions, e.g. ii-V-I (jazz progression), progressions:
    - 50s progression: 
