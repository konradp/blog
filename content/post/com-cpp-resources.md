---
title: "C++: Resources"
subtitle: 
date: 2022-11-03
categories: computers
---
- https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines
- https://caiorss.github.io/C-Cpp-Notes/
- https://learn.microsoft.com/en-us/cpp/cpp/cpp-language-reference?view=msvc-170
- http://www.umich.edu/~eecs381/handouts/handouts.html
- style guides:
  - http://www.yolinux.com/TUTORIALS/LinuxTutorialC++CodingStyle.html
  - https://google.github.io/styleguide/cppguide.html
  - http://www.doc.ic.ac.uk/lab/cplus/c++.rules/
