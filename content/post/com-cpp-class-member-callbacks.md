---
title: "C++: Class member functions as callbacks"
subtitle: 
date: 2022-10-26
categories: computers
---

C libraries often use function pointer callbacks. When supplying using a non-static class member function as a callback, we get an error.
```
error: invalid use of non-static member function
```
In this post I attempt to understand why this happens, and to note down the solution.

{{< toc >}}


# A working "C" program with callbacks
This is not really a C program, but it shows the callback working fine when not being used within a C++ class. The `start_timer(...)` simulates an external library function and it accepts two parameters: 

- a callback function (the `lib_timer_cb`, a pointer to a function),
- number of seconds to wait

The `start_timer(...)` waits for a given number of seconds, then executes the given callback function. The callback itself also prints out the given parameter (`int number`).

{{<highlight cpp "linenos=table,hl_lines=8 16 21">}}
#include <iostream>
#include <unistd.h> // for usleep
using std::cout;
using std::endl;

// Library
typedef void (*lib_timer_cb)(int number);
void start_timer(lib_timer_cb cb, int seconds) {
  int ms = 1000000; // second in microseconds
  usleep(seconds*ms);
  cb(seconds);
};


// Main program
void my_callback(int number) {
  cout << "Passed to callback: " << number << endl;
}

int main() {
  start_timer(my_callback, 1);
  start_timer(*my_callback, 2);
  start_timer(&my_callback, 2);
}
{{</highlight>}}

Compiling and running it works fine.
```bash
$ c++ 0callback.c -o 0callback
$ ./main_c_callback 
Passed to callback: 1
Passed to callback: 2
Passed to callback: 2
```
**Note:** We defined a new type for the callback:
{{<highlight cpp>}}
typedef void (*lib_timer_cb)(int number);
{{</highlight>}}

which simplifies the `start_timer` definition:
{{<highlight cpp>}}
void start_timer(lib_timer_cb cb, int number) {
{{</highlight>}}

For more on this, see:

- [riptutorial](https://riptutorial.com/c/example/31818/typedef-for-function-pointers)
- [libuv](https://github.com/libuv/libuv/blob/c83b9bd9c6876c0b6b605f38c92a92c2f630e0bd/include/uv.h#L321)
- [learncpp](https://www.learncpp.com/cpp-tutorial/function-pointers/)
- [stackoverflow](https://stackoverflow.com/questions/4295432/typedef-function-pointer)


# Class member as a callback
Here, we use the same library, but the callback is a member of the `MyClass` class: `MyClass::MyCallback(...)`.

{{<highlight cpp "linenos=table,hl_lines=22">}}
#include <iostream>
#include <unistd.h> // for usleep
using std::cout;
using std::endl;

// Library
typedef void (*lib_timer_cb)(int number);
void start_timer(lib_timer_cb cb, int seconds) {
  int ms = 1000000; // second in microseconds
  usleep(seconds*ms);
  cb(seconds);
};

// My program
class MyClass {
  public:
  MyClass() {
    start_timer(MyCallback, 2);
  };

  private:
  void MyCallback(int number) {
    cout << "My number is: " << number << endl;
  };
}; // end MyClass

int main() {
  MyClass c = MyClass();
}
{{</highlight>}}

the compilation fails with an **`error: invalid use of non-static member function...`**.

```
$ c++ 1class_member_cb.cc -o 1class_member_cb
1class_member_cb.cc: In constructor ‘MyClass::MyClass()’:
1class_member_cb.cc:18:30: error: invalid use of non-static member function ‘void MyClass::MyCallback(int)’
     start_timer(MyCallback, 2);
                              ^
1class_member_cb.cc:22:8: note: declared here
   void MyCallback(int number) {
        ^~~~~~~~~~
make: *** [Makefile:8: 1] Error 1
```

**TODO: Why is this a problem?**

# Make callback a static member
A simple solution is to make the callback a *static method*, by replacing 

{{<highlight cpp>}}
  private:
  void MyCallback(int number) {
{{</highlight>}}

with

{{<highlight cpp>}}
  private:
  static void MyCallback(int number) {
{{</highlight>}}

and it works fine

```
$ c++ 2static_member.cc -o 2static_member
$ ./2static_member 
My number is: 2
```

**TODO: If we do this, might we not just as well move the callback outside of the class? Especially if it cannot access class members, as seen below?**

## Problem: Accessing members from the callback
However, the static class member callback will fail as soon as it needs to access another member, e.g. the `int member` below:

{{<highlight cpp "linenos=table,hl_lines=6 11">}}
class MyClass {
  public:
  MyClass() {
    start_timer(MyCallback, 2);
  };
  int member;

  private:
  static void MyCallback(int number) {
    cout << "My number is: " << number << endl;
    member = number;
  };
}; // end MyClass
{{</highlight>}}

where the compilation gives an **`error: invalid use of member [...] in static member function`**:

```
c++ 3static_member_fail.cc -o 3static_member_fail
3static_member_fail.cc: In static member function ‘static void MyClass::MyCallback(int)’:
3static_member_fail.cc:25:5: error: invalid use of member ‘MyClass::member’ in static member function
     member = number;
     ^~~~~~
3static_member_fail.cc:20:7: note: declared here
   int member;
       ^~~~~~
make: *** [Makefile:14: 3] Error 1
```

**TODO: Why? Is there another solution? Search for this question.**

# Wrapper static function with a cast

Using the libuv library. Note that the `uv_handle_t*` has a `data` field, in which we store the pointer to the `MyClass` instance, **`this`**.
{{<highlight cpp "linenos=table,hl_lines=15 21-22 30-34">}}
#include <iostream>
#include <unistd.h> // for usleep
#include <uv.h>
using std::cout;
using std::endl;
uv_loop_t *loop;
uv_timer_t timer;


// My program
class MyClass {
  public:
  struct TimerData {
    int number;
    MyClass* instance;
  };
  MyClass() {
    loop = uv_default_loop();
    uv_timer_init(loop, &timer);
    uv_handle_t* h = (uv_handle_t*) &timer;
    uv_handle_set_data(h, new TimerData { 1, this });
    uv_timer_start(&timer, _MyCallback, 1000, 2000);

    uv_run(loop, UV_RUN_DEFAULT);
    uv_loop_close(loop);
  };
  int member = 4;

  private:
  static void _MyCallback(uv_timer_t* timer) {
    TimerData *td = (TimerData*)timer->data;
    MyClass *c = (MyClass*)td->instance;
    c->MyCallback(td->number);
  };
  void MyCallback(int timer_number) {
    cout << "User data number: " << timer_number << endl;
    cout << "Class member data: " << member << endl;
    member = 5;
  };
}; // end MyClass

int main() {
  MyClass c = MyClass();
}
{{</highlight>}}


Reference:

- reinterpret_cast: [stack_overflow](https://stackoverflow.com/questions/67463464/invalid-use-of-a-non-static-member-function-how-to-pass-a-function-pointer-of)
- [google groups](https://groups.google.com/g/fltkgeneral/c/pGheoD9Rg4I)


# Reference
- cast, could be good: [blog vishnumaiea](https://vishnumaiea.in/articles/computer/sending-member-functions-as-callbacks-to-global-functions/)
- hints for boost, some explanations, other answers: [stackoverflow](https://stackoverflow.com/questions/400257/how-can-i-pass-a-class-member-function-as-a-callback)
- [blog mbedded.ninja](https://blog.mbedded.ninja/programming/languages/c-plus-plus/callbacks/)
- [blog embeddedartistry](https://embeddedartistry.com/blog/2017/07/10/using-a-c-objects-member-function-with-c-style-callbacks/)
- [blog codeproject](https://www.codeproject.com/Questions/5296927/How-to-implement-a-callback-function-from-one-clas)
- [github gist](https://gist.github.com/mihids/64588b533063e412b9c0)
- [blog avout libuv](https://misfra.me/2016/02/24/libuv-and-cpp/)
