---
title: "deb_pkg_mgmt"
subtitle: 
date: 2022-11-01
tags: [ "com" ]
categories: cheatsheets
---
This article is the result of me finally taking the time to read the relevant sections of the Debian manual which relate to package management and querying. The result is a cheatsheet of tools/queries that I use most often.

{{<toc>}}

## Cheatsheet

cmd | desc
:-- | :--
**`apt install PKG`**           | 
**`apt remove PKG`**            | 
|
**`dpkg -l`**<br>`dpkg --list`  | list installed pkgs
**`dpkg -L PKG`**<br>`dpkg --listfiles PKG` | list pkg contents
**`dpkg -S file_name_pattern`** | which pkg contains the file
**`apt-mark showmanual`**       | list manually installed pkgs
|
**`apt show PKG`**              | pkg info
**`apt purge PKG`**             | remove pkg and also  pkg config files
**`apt clean`**                 | local repo: remove pkg files (/var/cache/apt/archives)
**`apt autoclean`**             | local repo: remove pkgs and pkg files
**`apt autoremove`**            | remove no longer required autoinstalled pkgs
**`apt-cache search regex`**    | Nicer output than `apt search`


## Notes
### Tools
- **apt**: use for pkg mgmt: wrapper of apt-get and apt-cache, **not for scripts**
- **apt-get, apt-cache**: use in scripts
- **aptitude**: manage installed pkgs and search available. interactive, most versatile
- **dpkg**, uses dpkg-query and dpkg-deb

ref: https://wiki.debian.org/PackageManagementTools


### Upgrade
- update: **`apt update`**
- upgrade: **`apt full-upgrade`**

**Note:** `apt upgrade` and `apt full-upgrade` are similar, but `apt full-upgrade` removes the packages thata are not needed.


### Notes
- pkg docs: /usr/share/doc/pkg_name
- do not use **aptitude** for sys upgrades
- history: see /var/log/dpkg.log for package updates. TODO: Is there another way?

TODO: How to deal with this?
sudo apt full-upgrade gave:
```
W: APT had planned for dpkg to do more than it reported back (6528 vs 6533).
   Affected packages: texlive-latex-base:amd64
```

## References
- [debref,ch2: Debian package management](https://www.debian.org/doc/manuals/debian-reference/ch02.en.html)
- [deb-faq,ch7: Basics of the Debian package management system](https://www.debian.org/doc/manuals/debian-faq/pkg-basics.en.html)
- [deb-faq,ch8: The Debian package management tools](https://www.debian.org/doc/manuals/debian-faq/pkgtools.en.html)
- [deb-faq,ch9: Keeping your Debian system up-to-date](https://www.debian.org/doc/manuals/debian-faq/uptodate.en.html)
- [deb-wiki: PackageManagementTools](https://wiki.debian.org/PackageManagementTools)
