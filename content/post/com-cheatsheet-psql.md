---
title: "psql"
subtitle: 
date: 2023-09-26
tags: [ "com", "cheatsheets" ]
categories: cheatsheets
---

- `psql -h HOSTNAME -U USER -d DBNAME`
- `\dt`: list tables
- `\d TABLE`: describe table
- `\d+ TABLE`: describe table
