---
title: "Audio and C++: Portaudio and libsndfile"
subtitle: 
date: 2019-10-17
categories: computers
---
# Portaudio
Get prerequisites.
```
apt-get install portaudio19-dev
apt-get install libportaudiocpp0
```
Include header.
```
#include <portaudio.h>
```

Study examples and tutorials.  
http://portaudio.com/docs/v19-doxydocs/tutorial_start.html  
http://portaudio.com/docs/v19-doxydocs/group__examples__src.html  

Compile the `examples/paex_sine_c++.cpp` from Portaudio's tarball.
```
g++ paex_sine_c++.cpp -lportaudio -o paex_sine_c++
```

My tweaking of the example  
https://gitlab.com/konradp/snippets/tree/master/cpp/audio/portaudio/sine

## Portaudio callback
All the processing happens in the callback you supply to [`Pa_OpenStream`](http://portaudio.com/docs/v19-doxydocs/portaudio_8h.html#a443ad16338191af364e3be988014cbbe) function.  
See also the documentation for [`PaStreamCallback`](http://portaudio.com/docs/v19-doxydocs/portaudio_8h.html#a8a60fb2a5ec9cbade3f54a9c978e2710) type.

A simple pass-through callback can read frames from the file opened with `libsndfile` and will write them to Portaudio output buffer (see https://github.com/hosackm/wavplayer).  
The callback has to return an indicator of whether processing is finished (reached end of stream or there's more to process).  
These return constants are defined here:  
http://portaudio.com/docs/v19-doxydocs/portaudio_8h_source.html#l00753
i.e.
```
typedef enum PaStreamCallbackResult
{
    paContinue=0,   
    paComplete=1,   
    paAbort=2       
} PaStreamCallbackResult;
```

# libsndfile
For playing audio files, use libsndfile library at http://www.mega-nerd.com/libsndfile/
Get lib headers
```
sudo apt-get install libsndfile1-dev
```
Include
```
#include <sndfile.h>
```
Compile
```
g++ file.cc -lsndfile -o sndfilehandle
```

# Combining portaudio and libsndfile
See https://github.com/hosackm/wavplayer/blob/master/main.c
