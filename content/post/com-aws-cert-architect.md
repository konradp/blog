---
title: "AWS cert: Solutions Architect - Professional"
date: 2024-08-24
categories: computers
---

{{<toc>}}

- main page: https://aws.amazon.com/certification/certified-solutions-architect-professional/: this has:
  - exam guide
  - practice
  - digital training
- course code: SAP-C02
- exam: AWS Certified Solutions Architect - Professional exam (SAP-C02)
- exam guide: https://d1.awsstatic.com/training-and-certification/docs-sa-pro/AWS-Certified-Solutions-Architect-Professional_Exam-Guide.pdf


Compute
• Cost management
• Database
• Disaster recovery
• High availability
• Management and governance
• Microservices and component decoupling
• Migration and data transfer
• Networking, connectivity, and content delivery
• Security
• Serverless design principles
• Storage

## Other links
https://scalefactory.com/blog/2022/08/31/how-i-passed-the-aws-solutions-architect-professional-exam/
- youtube playlists:
  - https://www.youtube.com/playlist?list=PL7GozF-qZ4KfaRAFdZ-U5ElWPaxXOi7XO
  - https://www.youtube.com/playlist?list=PLzde74P_a04f55gl4COVFH1u5uKGub0To

AWS Certified Solutions Architect - Professional exam (SAP-C02):
- https://aws.amazon.com/certification/certified-solutions-architect-professional/
- [exam guide](https://d1.awsstatic.com/training-and-certification/docs-sa-pro/AWS-Certified-Solutions-Architect-Professional_Exam-Guide.pdf)


Components:

- Compute
  - [dg: AppRunner](https://docs.aws.amazon.com/apprunner/latest/dg)
  - [dg: AppStream 2.0](https://docs.aws.amazon.com/appstream2/latest/developerguide/)
  - [ug: EC2](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/)
  - [dg: ECS](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/)
  - [dg: ECS/Fargate](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/AWS_Fargate.html)
  - [ug: EKS](https://docs.aws.amazon.com/eks/latest/userguide/)
  - [dg: Lambda](https://docs.aws.amazon.com/lambda/latest/dg/)
  - [dg: Step Functions](https://docs.aws.amazon.com/step-functions/latest/dg/)
  - [ag: WorkSpaces](https://docs.aws.amazon.com/workspaces/latest/adminguide/)
- Database
  - [ug: Aurora](https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/)
  - [dg: DocumentDB](https://docs.aws.amazon.com/documentdb/latest/developerguide/)
  - [dg: DynamoDB](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/)
  - [ug: RDS](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/)
  - [dg: Timestream](https://docs.aws.amazon.com/timestream/latest/developerguide/)
- Disaster recovery
  - https://docs.aws.amazon.com/wellarchitected/latest/reliability-pillar/plan-for-disaster-recovery-dr.html
- High availability
- IAM:
- Management and governance
  - [ug: Organizations](https://docs.aws.amazon.com/organizations/latest/userguide/)
- Microservices and component decoupling
- Monitoring
  - [ug: Athena](https://docs.aws.amazon.com/athena/latest/ug/)
  - [ug: CloudWatch](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/)
  - [dg: Route53](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/)
- Migration and data transfer
  - [wp: Snowball Edge Data Migration](https://d1.awsstatic.com/whitepapers/snowball-edge-data-migration-guide.pdf)
- Networking, connectivity, and content delivery
  - [ug: Outposts](https://docs.aws.amazon.com/outposts/latest/userguide/)
  - [ug: VPC](https://docs.aws.amazon.com/vpc/latest/userguide/)
  - [ug: VPC/PrivateLink](https://docs.aws.amazon.com/vpc/latest/privatelink/)
  - [wp: VPC/PrivateLink](https://docs.aws.amazon.com/whitepapers/latest/building-scalable-secure-multi-vpc-network-infrastructure/aws-privatelink.html)
- Security
  - [ug: IAM](https://docs.aws.amazon.com/IAM/latest/UserGuide/)
  - [ug: Macie](https://docs.aws.amazon.com/macie/latest/user/)
  - [ug: SystemsManager](https://docs.aws.amazon.com/systems-manager/latest/userguide/)
- Serverless design principles
- Storage: S3:
  - [ug: S3](https://docs.aws.amazon.com/AmazonS3/latest/userguide)
  - [ug: S3 outposts](https://docs.aws.amazon.com/AmazonS3/latest/s3-outposts/)

Other:
- https://aws.amazon.com/premiumsupport/knowledge-center/iam-policy-service-control-policy/
- Cost management
  - https://aws.amazon.com/aws-cost-management/aws-budgets/
  - https://aws.amazon.com/ec2/pricing/
  - https://aws.amazon.com/lambda/pricing/
