---
title: "AWStools: JSON generator helper"
date: 2023-01-08
categories: computers
---

This is a project journal for **awstools**.

**awstools** is a webpage for generating JSON payloads for the AWS CLI tool. For example, the IAM role policy document, or CodePipeline pipeline definition.

{{<toc>}}

## Motivation
While practicing using the CLI with the UserGuide tutorials for CodePipeline (the [blog article](/post/com-aws-ug-cp)), I ended up having to handcraft a JSON file for the pipeline structure. I decided that it would be helpful to have a tool which would generate this for me.

**Note:** I realise that I would probably normally use Terraform for these kind of tasks, instead of AWS CLI, but I think this project should still be fun and worth doing.

## Planning
General decisions:

- {{<v>}} use my usual node/express + js/html stack
- {{<v>}} UI:
    - {{<v>}} two column view: JSON on the right, buttons on the left
- {{<x>}} consider: instead of two column view, could we edit JSON 'directly'?
- {{<x>}} decide: draw boxes with canvas API or DIVs?

Functionality

- {{<x>}} IAM role
- {{<x>}} CodePipeline pipeline generator
- {{<x>}} templates:
    - {{<x>}} what templates?
- {{<x>}} buttons:
    - {{<x>}} add stage
    - {{<x>}} remove stage
    - {{<x>}} copy to clipboard
- {{<x>}} tool switcher: IAM, CodePipeline, etc
- {{<x>}} color coding
- {{<x>}} JSON is not editable
- {{<x>}} move stages? with mouse or buttons?
- {{<x>}} validity checker?

## Prototype 1
- must start with template, take the one from the UserGuide: https://docs.aws.amazon.com/cli/latest/reference/codepipeline/create-pipeline.html#examples

### Problem: mapping the UI elements to JSON
An issue I encountered is mapping the input elements to the JSON structure. In js, it seems to be hard to pass elements by reference to functions, for example, the below will not work.
```
function update(target, value) {
  target = value;
}
update(gJson.pipeline.stages[0].name, 'NewName');
```
I also looked at converting a string to an object, a method suggested in https://stackoverflow.com/questions/10934664/convert-string-in-dot-notation-to-get-the-object-reference but that didn't work either because of the `[0]` selectors.

#### Solution 1: custom paths
Not an ideal one, but I solve this by giving DOM objects an id such as:

- `pipeline-stages-0-actions-0-name`

This creates some constraints for the names of the stages:

- name cannot be an integer
- name cannot contain the hyphen char "**`-`**"

#### Solution 2: UUIDs
Another option I could think of, is to assign an UUID to every object which can be edited. This would mean adding metadata to the JSON data, which is then stripped for display. The objects would have an `_id` field, and string fields, such as `name`, would have an id as part of the string. This is for bonus points, might consider it later, but the JSON would look something like:
```
{
 "pipeline": {
  "name": "UUID:MySecondPipeline",
  "roleArn": "UUID:arn:aws:iam::111111111111:role/AWS-CodePipeline-Service",
  "stages": [
    {
      "_id": "UUID",
      "name": "UUID:Source",
      "actions": [
        {
          "name": "UUID:Source",
          "inputArtifacts": [],
          "actionTypeId": {
            "category": "UUID:Source",
            "owner": "UUID:AWS",
            "version": "UUID:1",
            "provider": "UUID:S3"
          },
[...]
```

#### Solution 3: object-oriented storage
This might be the best idea. Instead of storing the pipeline as JSON, store it as objects/classes, e.g. there would be a **`Pipeline`** class, a **`Stage`** class, an **`Action`** class. The **`Pipeline`** class would have a **`generateJSON()`** method. Or even all classes would have one. Now, this feels most involved, but it might be the most correct solution.

For this, we need:

- parse JSON into classes
- write classes
