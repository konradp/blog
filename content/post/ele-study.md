---
title: "ELEC: Study electronics"
date: 2023-07-28
tags: [ 'other' ]
categories: other
---

Table of contents:
{{<toc>}}

Books:
- http://en.wikibooks.org/wiki/Practical_Electronics
- http://en.wikibooks.org/wiki/Electronics
- http://en.wikibooks.org/wiki/Digital_Circuits

## Notation
- **voltage (V: volt)**  
  V = W/Q  
  work per unit charge (joule per coulomb)
- **current (I: ampere)**  
  I = Q/t, ampere=charge/time  
  1A is 1 Coulomb per second
- **power (P: Watt or Volt Amp VA),**  
  `watt=voltage*ampere`  
  work done over time, product of voltage and current  
  P = VI = W/Q * Q/t = W/t = E
- **resistance R**
  voltage across the conductor divided by current flowing  
  Ohm (omega letter)  
  R = V/I, resistance = voltage/ampere

## Ohm's law
- **`V = IR`**

TODO:
- electrostatic force
- electrodynamic force
- charge & electric force
- charge & magnetic force
- electromagnetic force
- Coulomb's law
- Ampere's law
- Lorentz's law
