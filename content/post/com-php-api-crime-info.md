---
title: "Crime info: Write a PHP API app"
subtitle: Subtitle
date: 2019-01-25
categories: computers
---

A PHP app CI/CD with Docker, RPM package, and Ansible
<!--more-->


# Overview

Inspired by blog posts:

- [Dockerise your PHP application with Nginx and PHP7-FPM][blog-docker1]
- [A docker-compose php environment from scratch][blog-docker2]

Overview:

- Application:
  - PHP REST API
  - no authentication
  - GET requests
  - nginx server
- Docker
- RPM package
- Tests
- Deploy: GitLab CI/CD + Ansible


# Workflow

- develop
- test
- package
- test
- deploy

# Project layout
Repository: https://gitlab.com/konradp/crime-info    
The project has the following layout.
```
  $ git ls-tree -r master --name-only
  README.md
  ansible/
  docker/docker-compose.yml
  pkg/buildrpm/php-nginx-example.spec
  src/index.php
```

# App

Inspired by [Easy APIs Without Authentication][blog-apis] blog post, we chose the [UK Police API][policeapi] to use in out app, specifically the 'API->Crimes at location' endpoint.


> Example: [https://data.police.uk/api/crimes-at-location?date=2018-03&lat=51.521376&lng=-0.620965][example-police-api]

## App objective: List crimes for a date/location
#### Overview

- Input:
  - date (YYYY-MM)
  - postcode
- Output:
  - list of crimes
  - resolution status (outcome)

#### Examples
Endpoint


[http://addr/crimes/POSTCODE/DATE][example-obj1]  
[http://addr/crimes/AB1 2CD/YYYY-MM][example-obj2]

Output
```
  [
    {
      'crime': {
        'date': YYYY-MM,
        'type': burglary
      },
      'outcome' : {
        'date': YYYY-MM,
        'type': sentenced in court
      }
    }
  ]
```  

**Note**: We could have encoded our variables in the url (urlencode), but this is not very REST, and is best avoided.  
[http://addr/crimes?date=YYYY-MM&postcode=AAA BBB][example-obj1]

Note that the Police API above requires latitude/longitude input, but we would like to instead use the postcode, which is more user-friendly.

## Postcode to geolocation: external API

We convert postcode to latitude/longitude using [Postcodes.io][postcodes] API.

Spec

- Input
  - SL1 4PN
- Output
  - latitude: 51.521376
  - longitude: -0.620965

> Example: [http://postcodes.io/postcodes/SL1 4PN][example-postcode-api]  

## Application code

```
  ./src/index.php
```
with contents: https://gitlab.com/konradp/php-nginx-example/blob/master/src/index.php

## Dev deploy application
We can use the built-in php server for early development.
```
  cd src
  php -S localhost:8000
```
Example: [http://localhost:8000/health][example-local1]

We can use Docker Compose to containerise the application to make it easier to share our project with others than if we used the built-in php server.  

**`docker/docker-compose.yml`**
```
version: "3.3"

services:
  nginx:
    image: nginx:latest
    ports:
        - "8000:80"
    volumes:
      - ../src:/var/www
      - ./nginx/conf.d/default.conf:/etc/nginx/conf.d/default.conf
    links:
      - php-fpm
  php-fpm:
    image: php:7-fpm
    volumes:
      - ../src:/var/www
```
with the Nginx config as below.  
https://gitlab.com/konradp/php-nginx-example/blob/master/docker/nginx/conf.d/default.conf

Run Docker Compose.
```
  cd docker
  docker-compose up
```
Example: [http://localhost:8000/health][example-local1]  
Example: [http://localhost:8000/crimes/SL1 4PN/2018-03][example-local2]

## Manual API tests

- web browser
- curl with jq
- Postman

Job done? Could deploy with `git clone` or copy files over with config management.  
Nope.  

# Package: deb, RPM

## RPM
We need:

- Source tarball (`.tar.gz`)
- SPEC file
- Docker

```
cd pkg/buildrpm
docker-compose build
docker-compose up
```

https://gitlab.com/konradp/php-nginx-example/-/jobs/artifacts/master/raw/artifacts/php-nginx-example-0.1-1.el7.centos.noarch.rpm?job=rpm

### Nginx
The default nginx config will look slightly different between Debian and CentOS. This initially made me think that on CentOS I have to edit the main `nginx.conf` file instead of linking a separate config block. However, we can use the convenient config blocks on CentOS also, and so there should be no need to touch the main config file (see reference).

Reference: https://www.digitalocean.com/community/tutorials/how-to-set-up-nginx-server-blocks-on-centos-7

## Develop test

Test driven development.  
We are used to quick and smooth local deployment.

```bash
  git clone https://gitlab.com/konradp/fincharts.git
  cd fincharts
  npm install
  npm start
```

## Benefits
- AB testing
- Clear ownership
- Separate responsibility

<!--Reference-->
[blog-docker1]: http://geekyplatypus.com/dockerise-your-php-application-with-nginx-and-php7-fpm/
[blog-docker2]: https://x-team.com/blog/docker-compose-php-environment-from-scratch/
[blog-apis]: https://shkspr.mobi/blog/2016/05/easy-apis-without-authentication/  
[policeapi]: https://data.police.uk/
[postcodes]: https://postcodes.io/
[example-postcode-api]: http://api.postcodes.io/postcodes/SL1%204PN
[example-police-api]: https://data.police.uk/api/crimes-at-location?date=2018-03&lat=51.521376&lng=-0.620965
[example-obj1]: http://addr/crimes/POSTCODE/YYYY-MM
[example-obj2]: http://addr/crimes/AAA%20BBB/YYYY-MM
[example-local1]: http://localhost:8000/health
[example-local2]: http://localhost:8000/crimes/SL1%204PN/2018-03
