---
title: "MUS: New mixer"
subtitle: 
date: 2023-07-02
tags: [ 'wip' ]
categories: wip
---
{{< toc >}}

## Current mixer
Behringer XENYX QX 1002 USB:
- inputs: 10: 4 stereo + 2 mono
- outputs: 2

## New mixer
requirements:
- can record directly, e.g. to USB drive, no DAW needed
- multitrack output over USB
- multitrack record
- at least one stereo input
- 10 inputs:
  - 4 x drums
  - 1 x bass synth
  - 1 x guitar
  - 1 x mic
  - 1 x piano
  - 1 x chords synth
  - total: 9

## Candidates
legend:
- p1: price @ thomann.de (BGN)
- p2: price @ muziker.bg (BGN)
- rec: can record
- IN: number of inputs
- IN rec: how many tracks it can record
- OUT: how many usb outs
- FX: number of effects

mixer                   | IN  | IN rec | out | rec    | FX | p1  | p2
---                     | --- | ---    | --- | ---    | ---| --- | ---
Allen & Heath Qu-16     | 16  |        |     | {{<v>}}|    |     | 4569
Boss BR-800             |     | 4
Presonus StudioLive ARc |
Soundcraft MTK12        |     |        |     | {{<x>}}
Tascam DP-24SD          | 24  | 8      |     | {{<v>}}|    |     | 1219
Tascam model 16         | 16  | 14     |     | {{<v>}}
Yamaha MG10XU           | 10  |        |     | {{<v>}}| 24 |     | 579
Yamaha MG10XUF          | 10  |        |     | {{<v>}}| 24 |     | 646
Zoom R16                |
Zoom L-12               | 12  |        | 4   | {{<v>}}| 16 | ?   | 1139

Notes:
- Zoom L-12:
  - 14in/4out usb
  - 12 channels: 8mono + 2stereo
  - 14 track recording
  - 12 track playback
- Yamaha MX10XUF: same as the other but with faders
- Zoom R20: skipped
  - 8 ins, price 884, has only two TRS inputs, rest is XLR

## References
blog articles:
- [strongsounds.com: Best mixers with multitrack usb recording](https://strongsounds.com/dj/best-mixers-with-multitrack-usb-recording/)
