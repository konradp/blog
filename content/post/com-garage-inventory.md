---
title: 'garage-inventory'
date: 2023-11-06
tags: [ 'com' ]
categories: computers
---
My project diary for my garage inventory app.

{{<toc>}}

## Overview

## Plan/status
- {{<v>}} create git repo and init npm+js setup as per [cheatsheet: node](https://konradp.gitlab.io/blog/post/com-cheatsheet-node/)
- {{<v>}} minimal page: hello world: works locally (+documentation)
- {{<v>}} minimal page: hello world (+gitlab pages): works remotely
- {{<x>}} read google drive js api doc: https://developers.google.com/drive/api/quickstart/js
- {{<x>}} decide on data storage structure
- {{<x>}} basic google drive interaction client-side: read a file
- {{<x>}} figure out gooogle oauth
- {{<x>}} js access to android camera
- {{<x>}} create app in google developer portal

## Preparation
Reading https://developers.google.com/drive/api/quickstart/js, we first need to create a Google Cloud project, then enable the Google Drive API for it, then configure the OAuth consent screen. Then, authorise credentials for a web application (create credentials (OAuth client ID). Also need to create an API key
