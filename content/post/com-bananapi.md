---
title: "Bananapi set-up"
subtitle: 
date: 2020-11-15
categories: computers
---
{{<toc>}}

## Intro
My banana pi model: BPI-M1: http://www.banana-pi.org/m1.html  
Armbian image from: https://www.armbian.com/bananapi/

```
# Important: zero the SD card first, otherwise you will get stuck loading with 'Loading: T T T T'!
$ sudo dd bs=4M if=/dev/zero of=/dev/mmcblk0 status=progress
$ xz -d Armbian_20.08.1_Bananapi_focal_current_5.8.5.img.xz 
$ ls
Armbian_20.08.1_Bananapi_focal_current_5.8.5.img
sudo dd if=Armbian_20.08.1_Bananapi_focal_current_5.8.5.img of=/dev/mmcblk0 status=progress
```
Notes:
- dd zero takes about 30 mins
- dd zero with bs not specified runs at 17.5-18 MB/s, takes 1:30hr, bs=4M runs at 50MB/s and takes 20min, no significant improvement with bs=8M
- boot takes about 40s

Then

- SSH to the device: `root/1234`
- Change root pass, add konrad user via the guide
- reboot

## General
```
sudo apt update
sudo apt upgrade
armbian-config
ssh-keygen
echo 'nameserver 8.8.8.8' >> /etc/resolv.conf
```

## Speeding up boot time
TODO

## Personal setup
Install packages
```
sudo apt install vim
```

Config
```
mkdir -p ~/.vim/tmp
vi ~/.vimrc
```
https://gitlab.com/konradp/dotfiles/-/raw/master/config/vimrc

Aliases
```
vi ~/.bash_aliases
```
with
```
alias g='git'
alias s='sudo'
alias v='vim'
```
