---
title: 'COM: app: Piano sampler'
date: 2023-09-09
tags: [ 'wip' ]
categories: wip
math: true
---

<script src="https://konradp.gitlab.io/piano-sampler/js/piano-sampler.js"></script>

A piano sampler for use in my apps [app tuning](http://konradp.gitlab.io/blog/post/com-app-tuning/), [piano learning](http://konradp.gitlab.io/blog/post/mus-piano-learning-app/).  

The outcome is here:

- homepage: https://konradp.gitlab.io/piano-sampler/
- code: https://gitlab.com/konradp/piano-sampler

**TABLE OF CONTENTS**:

{{<toc>}}


## TODO

- {{<v>}} play sample, note on
- {{<v>}} load samples
- {{<x>}} different ways of specifying notes:  
  - {{<v>}} <code>play(60)</code>
  - {{<v>}} <code>play('60')</code>
  - {{<x>}} <code>play({midi: 60})</code>
  - {{<x>}} <code>play({note: 'C#4')</code>
  - {{<x>}} <code>play({freq: 440})</code>
- {{<x>}} use pitch shift when playing specific frequencies
- {{<x>}} MIDI
- {{<x>}} play sample from MIDI
- {{<x>}} ADSR, ramp down on release
- {{<x>}} velocity sensitive
- {{<x>}} sustain pedal
- {{<x>}} detune individual notes
- {{<x>}} overcome CORS

### Things to check

- {{<x>}} check: https://blog.logrocket.com/exploring-web-audio-api-web-midi-api/
- {{<x>}} check: https://www.toptal.com/web/creating-browser-based-audio-applications-controlled-by-midi-hardware
- {{<x>}} check: https://zpl.fi/pitch-shifting-in-web-audio-api/ : check also other articles here
- {{<x>}} https://stackoverflow.com/questions/28230845/communication-between-tabs-or-windows
- {{<x>}} https://blog.bitsrc.io/4-ways-to-communicate-across-browser-tabs-in-realtime-e4f5f6cbedca
- {{<x>}} https://swarajpure.medium.com/postmessage-communication-between-browser-tabs-85216a4b2046
- {{<x>}} https://blog.landr.com/best-online-daw/
- {{<x>}} https://filmora.wondershare.com/audio/top-free-daw-online.html

## Prepare data
First, I get the 'Upright piano multicamples' [sound pack](https://freesound.org/people/beskhu/packs/17088/) by beskhu from freesound (88 piano keys). These are all files in .aiff format, and I don't need such quality, so I convert these to (ogg/mp3).

```
mkdir ../ogg
for I in *.aiff; do sox $I ../ogg/${I%%.*}.ogg; done
```

An example sound of size 4.3M reduces to 310K. The entire sound pack shrinks from 257M to 19M.

Also, the files are named strangely:
```
277060__beskhu__37-c2.ogg
277061__beskhu__38-d2.ogg
277062__beskhu__31-g1.ogg
277063__beskhu__32-g1.ogg
277064__beskhu__29-f1.ogg
277065__beskhu__30-f1.ogg
277066__beskhu__35-b1.ogg
277067__beskhu__36-c2.ogg
...
```

I only care about this part of the filename:

```
277060__beskhu__37-c2.ogg
                ^^
```

so we rename the files

```
for I in *; do
  NEW=$(echo $I \
    | cut -d'_' -f5 \
    | cut -d'-' -f1
  )
  mv $I $NEW.ogg
done
```

to get

```
09.ogg
10.ogg
...
95.ogg
96.ogg
```

The names correspond to MIDI note numbers. So 09 is MIDI note 09. The 21 is MIDI note 21, which is piano note 1, A0, and so on: 96 is MIDI note 96, piano note 76, the C7 note.

- ([ref](https://www.inspiredacoustics.com/en/MIDI_note_numbers_and_center_frequencies))

## Milestones

### 1: Playing sounds

References:

- [MDN: Using Web Audio API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API/Using_Web_Audio_API)
- [tinydrum](https://gitlab.com/konradp/tinydrum)
- [drum](https://gitlab.com/konradp/drum)

<script src="/media/com-app-piano-sampler/piano1.js"></script>
<script src="/media/com-app-piano-sampler/ex1.js"></script>

<button id="ex1-btn">play</button>


### 2: MIDI support
This is not going to be part of piano-sampler lib, but I include it here nonetheless.




## Resources
- [note freqs](https://www.inspiredacoustics.com/en/MIDI_note_numbers_and_center_frequencies)
