---
title: "vim error: BufWinEnter Autocommands: Unknown option: termmode="
subtitle: 
date: 2023-01-23
categories: computers
---
Recently I started occasionally seeing a new error when opening files in vim. I enjoyed learning new things while fixing it, and so I document the journey in this post.

{{<toc>}}

## Problem
When opening one of my files, the below error is shown **occasionally**.
```
"~/code/blog/content/post/com-python-cheatsheet.md" 66L, 1738B
Error detected while processing BufWinEnter Autocommands for "*.*"..script /home/konrad/.vim/view/~=+code=+blog=+c
ontent=+post=+com-python-cheatsheet.md=:
line  107:
E518: Unknown option: termmode=
Press ENTER or type command to continue
```

## Troubleshooting
**Note:** In hindsight, the error message tells me where to look (the **`~/.vim/view/LONG_FILENAME.md=`** file, line 107), and so it would have been most logical to look there, but it wouldn't have helped me anyway, because, as we will see later, the offending line disappears from the 'view' file as soon as we open one of my files in vim. And, funnily enough, that's not how I proceeded from the error message.

### What does it do: vimrc: views and folding
I recognised the `BufWinEnter` as something I have in my `.vimrc` file, which reads:
```
[...]
" Remember folds
set foldmethod=indent
set foldnestmax=10
set nofoldenable
set foldlevel=2
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview 
```
It's likely that I found these in some blog article, maybe one of these: [1](https://til.hashrocket.com/posts/17c44eda91), [2](https://vim.fandom.com/wiki/Make_views_automatic), [3](https://vi.stackexchange.com/questions/9494/how-to-save-a-view-with-folds-into-the-file), [4](https://vi.stackexchange.com/questions/17526/excluding-line-numbering-from-mkview-loadview)

Now, the **`BufWinEnter`** and **`BufWinLeave`** are documented in vim [Automatic commands](https://vimhelp.org/autocmd.txt.html):

- [BufWinEnter](https://vimhelp.org/autocmd.txt.html#BufWinEnter): triggered after a buffer is displayed in a window
- [BufWinLeave](https://vimhelp.org/autocmd.txt.html#BufWinLeave): triggered before a buffer is removed from a window

so these automatic commands trigger the `silent loadview` and `mkview` when we open and leave a file, respectively. The `mkview` and `loadview` appear in the context of saving and restoring folds in vim by using views, reference [here](https://vimhelp.org/usr_28.txt.html#28.4) for folds, and [here](https://neo.vimhelp.org/usr_21.txt.html#21.5) for description of views. There is also the **`viewdir`**, which is my `~/.vim/view`.

So, each time I open and close a file, its 'view' is loaded and then saved, and, among other things, the folds are preserved.

As per the documentation, "You might want to delete old views now and then.", but I think I might actually disable the views altogether as I don't really care about preserving the folds. Also, I doubt I'm going to remember cleaning up the dir, and so should be able to reclaim some disk space:
```
$ du -sh ~/.vim/view
11M     /home/konrad/.vim/view
```

### Cause: Vim upgrade?
I recently upgraded my OS. I suspect something changed in vim, and the `termmode` option is no longer used.

Now, I see the `termmode` present in most view files:
```
$ ls -1 ~/.vim/view/ | wc -l
2511
$ grep -lv termmode ~/.vim/view/* | sort | uniq | wc -l
2509
```
that is, out of 2511 files, 2509 contain the offending line. It looks like this:
```
setlocal termmode=
```
and let's get some times and dates with a beautiful and highly readable one-liner
```
for FILE in *; do GREP=$(grep termmode "$FILE"); DATE=$(stat --format="%y" "$FILE"); echo "$DATE $GREP"; done | sort > /tmp/out
```
this outputs a file with last modified date followed by 'setlocal termmode=' or '' depending on whether the offending string is present in the file or not.
```
2018-12-09 23:46:53 
2018-12-10 12:04:47 
[...]
2021-01-20 21:12:26 
2021-01-21 20:39:18 setlocal termmode=
2021-01-21 20:57:41 setlocal termmode=
[...]
2023-01-19 16:59:07 setlocal termmode=
2023-01-19 17:22:26 setlocal termmode=
2023-01-19 19:05:09 
2023-01-19 19:18:28 
```
Here, we see that this option used to be absent originally, was introduced before 2021-01-21 20:39, and disappeared again on 2023-01-19 between 17:22 and 19:05.

Now, I see there was an upgrade to the vim package
```
$ grep "upgrade vim:" /var/log/dpkg.log
2023-01-19 16:55:15 upgrade vim:amd64 2:8.1.0875-5+deb10u2 2:8.1.0875-5+deb10u4
2023-01-19 18:29:25 upgrade vim:amd64 2:8.1.0875-5+deb10u4 2:8.2.2434-3+deb11u1
```
so vim was upgraded on 2023-01-19 18:29:

note | deb_ver                  | vim_ver
--- | --- | ---
from | `2:8.1.0875-5+deb10u4`   | `8.1.0875`
to   |   `2:8.2.2434-3+deb11u1` | `8.2.2434`

Now, the removal of `setlocal termmode=` could be something that has happened in vim itself, or could have been separate.

**Note:** The `setlocal` is a vim-specific option, documented [here](https://vimhelp.org/options.txt.html#%3Asetlocal) and is for setting local variables/options. For the `termmode` option itself, I cannot see it here: https://vimhelp.org/quickref.txt.html#option-list, but I can see other options starting with `term*`.

#### Git repo search
I think I'll be able to find it by looking at the git repo of the documentation, the `quickref.txt` page. I have two vim versions that I want to compare, but I don't want to download the git repo and do it in the shell. Luckily, github seems to have the option I need:

- [github: Comparing commits](https://docs.github.com/en/pull-requests/committing-changes-to-your-project/viewing-and-comparing-commits/comparing-commits)
- [github: Comparing releases](https://docs.github.com/en/repositories/releasing-projects-on-github/comparing-releases)

giving the url:

- https://github.com/vim/vim/compare/v8.1.0875...v8.2.2434

**Note:** The order matters here, and with `v8.2.2434...v8.1.0875` we will get "There isn't anything to compare".

Anyway, in the huge diff of 'Files changed', I found the `quickref.txt` eventually, but no mention of `termmode`.

So I did something that I should have done earlier, just search for 'termmode' in the entire github repo: https://github.com/vim/vim/search?q=termmode&type=issues. There is nothing in commits, but there is one pull request:

- [Proposal: ":terminal" option about winpty/conpty #3905](https://github.com/vim/vim/pull/3905)

Digging through that pull request, I find a commit:

- https://github.com/h-east/vim/commit/e176dbce9d5ca61fcc86b7be3fb4f818edad478b

which has an update to the `options.txt` but not `quickref.txt`. Anyway, this PR shows that the **`termmode`** was replaced with **`termwintype`** and we also see a comment
```
Updated patch (rebased on v8.1.0880)
```
which suggests that we have found the right thing. And also, going back to the comparison between the tags, I find [the patch](https://github.com/vim/vim/commit/c6ddce3f2cf6daa3a545405373b661f8a9bccad9):
```
patch 8.1.0880: MS-Windows: inconsistent selection of winpty/conpty
Problem:    MS-Windows: inconsistent selection of winpty/conpty.
Solution:   Name option 'termwintype', use ++type argument and "term_pty" for
            term_start(). (Hirohito Higashi, closes #3915)
```

## Summary
The vim package was updated from `v8.1.0875` to `v8.2.2434`. In between these versions, the `termmode` option was replaced with the `termwintype` in [patch 8.1.0880: MS-Windows: inconsistent selection of winpty/conpty](https://github.com/vim/vim/commit/c6ddce3f2cf6daa3a545405373b661f8a9bccad9) as per [issue #3905](https://github.com/vim/vim/pull/3905) and [issue #3915](https://github.com/vim/vim/pull/3915). The error message would appear once for every file which had a view file present from before the upgrade.

## Followups
This was a great fun to do and gave me ideas for further questions and things to learn, mostly to do with vim release process:

- Does vim have release notes?
- Does the vim pkg have release notes or changelog? Could I simply have looked at the changelog between packages?
- What's the github.com/h-east/vim ? Is it to do with the vim release process?
- Why is there no edit to the `quickref.txt` in **#3905**? Was it forgotten and done later? When?
- At some point I found a reference to **`vimtutor`**. It looks fun, I want to try it at some point.
- How to relate an issue/pull request to which release it went into? Is it only mentioned in the comment?
- Added to my Debian pkg mgmt cheatsheet: /var/log/dpkg.log for package update history

## Conclusion
As I don't need to preserve the folds, I removed these two lines from my `~/.vimrc`:
```
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview 
```
and removed the dir entirely:
```
$ rm -rf ~/.vim/view
```
This was fun, now I can think of creative ways to use these 11MB which I've successfully reclaimed :)
