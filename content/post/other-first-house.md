---
title: "OTHER: First house"
date: 2024-12-22
tags: ['wip']
categories: wip
---

{{<toc>}}


TODO: Data sources:
- Numbeo
- BIS (Bank for International Settlements) Residential Property Prices
  https://data.bis.org/topics/RPP
- MDPI Research
- International Housing Observatory (Dallas Fed)  
  https://www.dallasfed.org/research/international/houseprice  
  This uses HPI
- housing observatory: https://int.housing-observatory.com/resources
- Global Property Guide  
  https://www.globalpropertyguide.com/
- OECT Housing Prices
- European Central Bank (ECT)
- DataHub.io
- what is real vs nominal house price

## Budget
legend:
- p10: price assuming 10% deposit
- p20: price assuming 20% deposit
- prices in thousands (k)

{{<table sortable>}}
country     | currency | cash | p10   | p20
---         |---       | --:  | --:   | --:
Australia   | AUD      | 83   | 830   | 410
Bulgaria    | BGN      | 100  | 1 000 | 500
Croatia     | EUR      | 50   | 500   | 250
Czechia     | CZK      | 1256 | 12560 | 6280
Greece      | EUR      | 50   | 500   | 250
New Zealand | NZD      | 92   | 920   | 460
Poland      | PLN      | 213  | 2 130 | 1 065
Portugal    | EUR      | 50   | 500   | 250
Spain       | EUR      | 50   | 500   | 250
UK          | GBP      | 42   | 420   | 210
{{</table>}}


## Prices and mortgage
Per country/city, answers from Perplexity, for apartments in the centre 100m2, prices in EUR (thousands)
- price-median is a median calculated from price-low and price-high
- mrate%: mortgage rate

{{<table sortable>}}
city         | country     | price-low | price-high | price-median | mrate%
---          | ---         | ---       | ---        | ---          | --:
Melbourne    | Australia   | 450       | 550        | 500
Sofia        | Bulgaria    | 180       | 330        | 255          | 2.60
Split        | Croatia     | 365       | 780        | 572
Prague       | Czechia     | 300       | 400        | 350
Athens       | Greece      |
Thessaloniki | Greece      |
Auckland     | New Zealand |
Krakow       | Poland      | 370       | 609        | 490          | 7.90-9.00
Wroclaw      | Poland      | 200       | 300        | 250          | 7.90-9.00
Warsaw       | Poland      | 331       | 664        | 498          | 7.90-9.00
Lisbon       | Portugal    | 320       | 1000       | 660
Valencia     | Spain       | 214       | 303        | 259          | 3.99
Bristol      | UK          | 667       |            |              | 5.23
Reading      | UK          | 667       |            |              | 5.23
?            | US          |           |            |              | 6.72
{{</table>}}


## Websites
- Australia
  - realestate.com.au
  - domain.com.au
  - homely.com.au
  - allhomes.com.au
- Bulgaria
  - imoti.net
  - Bulgaria Property?
  - Properties in Bulgaria?
  - property.bg [link](https://www.property.bg/search/index.php?&country_id=1&stown=4732&mquart[]=468&mquart[]=856&maxprice_curr=NaN&spredlog=innear&action=1&csel=&searchform=1&sadx=1&hmsf=1&srtby=3&page=2)
  - bulgarianproperties.com:  
    Not many in center, for foreigners
- Croatia
  - Croatian Real Estate
  - Njuškalo
  - Real Estate Croatia
- Czechia
  - sreality.cz
  - bezrealitky.cz
  - reality.cz
- Greece
  - Spitogatos.gr: The most visited real estate website in Greece, featuring a wide range of property listings including homes, apartments, and commercial properties. It also offers a property pricing index and mobile apps for easy access 13.
  - Xe.gr: A classifieds platform that includes real estate listings for properties available for sale and rent across Greece. It also features listings for vehicles and other products 15.
  - Tospitimou.gr: This site provides diverse property listings, including residential, commercial properties, and land for sale. It caters to various user needs, including student accommodation 13.
  - Indomio.gr: A growing real estate platform that offers a variety of property listings across Greece 35.
  - Plot.gr: One of the largest property classifieds websites in Greece with over 150,000 listings for residential and commercial properties 13.
  - Goldenhome.gr: A prominent site for real estate listings in Greece that focuses on both residential and commercial properties 3.
  - Green-acres.gr: This platform specializes in Greek properties, showcasing a variety of houses and apartments for sale throughout the country 4.
  - Ktimatoemporiki.gr: With over 13,000 properties listed, this site is known for its extensive portfolio and experience in the Greek real estate market 2.
  - Spiti24.gr: An online marketplace that provides listings across various property types in Greece, part of the Spitogatos network 15.
  - Landea.gr: Specializes in auction properties in Greece, offering updated listings daily for those interested in bidding on real estate
- New Zealand
  - Trade Me Property: A major platform for buying and renting residential properties.
  - Realestate.co.nz: Offers a wide range of property listings across New Zealand.
  - Homes.co.nz: Provides property listings along with market insights and valuations.
- Poland
  - otodom.pl
  - nieruchomosci-online.pl
  - morizon.pl
  - domiporta.pl
  - adresowo.pl
  - regiodom.pl
  - szybko.pl
- Portugal
  - idealista.pt: A leading real estate portal in Portugal with numerous listings.
  - OLX.pt: A classifieds site that includes a section for real estate sales.
  - casa.sapo.pt: Offers a variety of properties available for sale or rent throughout Portugal.
- Spain
  - idealista.com: One of the most popular sites for real estate in Spain, featuring extensive listings.
  - fotocasa.es
  - habitaclia.com
- UK: 
  - rightmove.co.uk: can't filter by m2
  - zoopla.co.uk
  - onthemarket.com

## Like

location        | price      | ppm2 | area | link
---             | ---        | ---  | ---  | ---
Poland, Wroclaw | 2.1mln PLN | 9k   | 234 | [otodom](https://www.otodom.pl/pl/oferta/pietro-kamienicy-w-calosci-234m2-7pokoi-4balkony-ID4sDvF)
Poland, Wroclaw | 1.8mln PLN | 12k  | 148 | [otodom](https://www.otodom.pl/pl/oferta/sprzedam-mieszkanie-w-kamienicy-we-wroclawiu-ID4tYdb)
