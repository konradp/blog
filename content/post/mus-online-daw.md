---
title: 'MUS: Online DAWs'
date: 2023-10-19
tags: [ 'wip' ]
categories: wip
---
Reviewing online DAWs

## Initial look
name          | look further? | free?  | notes
---           | :-:           | :-:    | ---
soundtrap     | {{<x>}}       | {{<w>}}| Can be used for free, but has intrusive paywall ads
amped studio  | {{<v>}}       | {{<v>}}| nice, some premium ads, but not as obtrusive
soundation    | {{<x>}}       | {{<x>}}| sort of free, 3 projects, 1GB space for free. Looks very nice though
autiotool     | {{<x>}}       | {{<v>}}| can't see how it works, doesn't look like a daw
audio sauna   | {{<x>}}       | ?      | requires flash
bandlab       | {{<v>}}       | ?      | great and simple
soundbridge   | {{<x>}}       | {{<w>}}| 10 tracks, but can't even sign up on site
internet daw  |
drumbot       |
viktor nv-1   |

## Other to check
- tracktion's t7 <- now called waveform 12
- cakewalk by bandlab

## Details

## Soundtrap
soundtrap.com

## Amped studio
ampedstudio.com

## Soundation
soundation.com

## Audiotool
audiotool.com

## Audio sauna
audiosauna.com

## BandLab

## SoundBridge
soundbridge.io

## Internet DAW
thedawstudio.com/internet-daw

## Drumbot
drumbot.com

## viktor NV-1
nicroto.github.io/viktor



## References
- https://blog.landr.com/best-online-daw/
- https://filmora.wondershare.com/audio/top-free-daw-online.html
