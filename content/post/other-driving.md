---
title: "OTHER: learn driving from youtube"
date: 2023-09-05
categories: other
---

{{<toc>}}

I decided to learn to drive (again). Before taking up lessons with an instructor, let's learn everything that I need from youtube.

## Plan
- how the car works:
  - gearbox and pedals
  - braking
- learn signs
- learn manouvres (parallel parking etc)
- know what exam will look like
- book driving instructor

Note: Include UK vs rest of the world differences

## Software
- City Car Driving: https://store.steampowered.com/app/493490/City_Car_Driving/
- BeamNG.drive: https://en.wikipedia.org/wiki/BeamNG.drive

## Hardware
https://www.technopolis.bg/en//PC-peripherials/PC-Gaming-Accessories/Wheels/c/P11010907

CEX:
- logitech G27, C £135
- logitech G27, B £150
- PXN V9, B £120

## Signs

## Manouvres

## Exam
