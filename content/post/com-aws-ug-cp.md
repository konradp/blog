---
title: "AWS user guide tutorials: code pipeline"
date: 2023-01-05
categories: computers
---

These are my condensed notes from the CodePipeline UserGuide tutorials.  
Use these with the [AWS CLI cheatsheet](http://konradp.gitlab.io/blog/post/com-aws-cli-cheatsheet/).

## Tutorial: a simple pipeline (S3 bucket)
https://docs.aws.amazon.com/codepipeline/latest/userguide/tutorials-simple-s3.html

- S3: create bucket:
    - name: testDATE
    - enable versioning
    - upload [SampleApp_Linux.zip](https://docs.aws.amazon.com/codepipeline/latest/userguide/samples/SampleApp_Linux.zip)
- IAM: create role for EC2:
    - name: EC2InstanceRole (arbitrary)
    - use case: EC2
    - policy: AmazonEC2RoleforAWSCodeDeploy
    - policy: AmazonSSMManagedInstanceCore
- EC2: create instance
    - name: CPKonrad
    - AMI: Amazon Linux
    - type: t2.micro
    - security group: default
- IAM: create role for CodeDeploy:
    - name: CodeDeployRole (arbitrary)
    - use case: CodeDeploy
    - policy: AWSCodeDeployRole
- CodeDeploy:
    - application:
        - name: MyDemoApplication
        - platform: EC2/On-premises (other options: Lambda, ECS)
    - deployment group:
        - name: MyDemoDeploymentGroup
        - deploymentStyle:
            - deploymentType: IN_PLACE  
            - deploymentOption: WITHOUT_TRAFFIC_CONTROL (no load balancer)
          (other option: blue/green)
        - deploymentConfigName: CodeDeployDefault.OneAtATime
        - **env config: EC2 instances (other option: EC2 autoscaling groups, on-prem)**  
          **TODO: Is this controllable via CLI?**
- CP: create pipeline
    - name: kppipeline
    - service role: KPService
    - first stage:
        - name: Source
        - source: S3, CloudWatch Events (other option: AWS CodePipeline)
    - second stage:
        - name: Prod
        - deployment group: CPProd
        - action name: Deploy-Second-Deployment
        - cli: aws codepipeline start-pipeline-execution --name MyFirstPipeline
        - cli: aws codepipeline get-pipeline --name "MyFirstPipeline"
        - cli: aws codepipeline update-pipeline --cli-input-json file://pipeline.json

## Tutorial: simple pipeline (CC repo)
https://docs.aws.amazon.com/codepipeline/latest/userguide/tutorials-simple-codecommit.html

- CC repo:
    - name: MyDemoRepo
    - clone repo
    - upload files from: [SampleApp_Linux.zip](https://docs.aws.amazon.com/codepipeline/latest/userguide/samples/SampleApp_Linux.zip)
- EC2 instance with CodeDeploy agent
    - create role (EC2):
        - name: EC2InstanceRole
        - policies:
            - AmazonEC2RoleforAWSCodeDeploy
            - AmazonSSMManagedInstanceCore
    - instance:
        - name: MyCodePipelineDemo (tag Key = Name, tag Value = MyCodePipelineDemo)
        - OS: AMI
        - type: t2.micro
        - add ssh key pair
        - network: auto-assign public IP
        - create new security group
        - ssh source type: My IP
        - add security group, HTTP, source type: My IP
        - IAM instance profile: EC2InstanceRoleTwo (the one just created)
