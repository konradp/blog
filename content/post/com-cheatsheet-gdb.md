---
title: "gdb"
date: 2022-12-15
tags: [ "com", "cheatsheets" ]
categories: cheatsheets
---
My GDB cheatsheet.

short | long      | example         | desc
---   | ---       | ---             | ---
r     |  run      |                 |
b     |  break    | b 20            | break at
      |           | b 20            |
      |           | b function      |
      |           | b file:function |
i b   |  info b   | i b             | list breakpoints
d     |  disable  | d 20            | disable break at 20
      |           | d               | disable ALL breaks
e     | enable    | e 20            | enable break at 20
      |           | e               | enable ALL breaks
d     | delete    | d 20            | delete break at 20
      |           | d               | delete ALL breaks
cond  | condition | cond 20 a==3    | break at this breakpoint only<br>if a condition is met
------|-----------|-----------------|--------
c     | continue  |                 | continue until next breakpoint
s     | step      |                 | step INTO
n     | next      |                 | next line (step OVER)
------|-----------|-----------------|--------
l     | list      | l               | print source
      |           | l 20            |
bt    | backtrace |                 | display program stack
p     | print     | p 2<0 : false   | print value of expression
sho   | show      |                 | show variables


