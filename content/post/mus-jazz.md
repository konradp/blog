---
title: 'MUS: Jazz musicians'
subtitle: 
date: 2021-10-27
tags: [ 'wip' ]
categories: wip
---

Table of contents:
{{<toc>}}

## Musicians
<script src='/media/mus-jazz/core.js'></script>
<body onload='main();'></body>

Score is from 0 to 10, from worst to best.
<table style='border:0px solid #000000'>
  <th>columns</th>
<!--  <div style='float: left;'>
    <label for='cols'>Show/hide column:</label><br>-->
  <th>show</th>
  <tr>
    <td style='border:0'>
    <select name='cols' id='cols' onclick='showHideCol(value);'></select>
    </td>
    <td style='border:0'>
    <select onChange='flipShowHideWatched()'>
      <option value='watched'>heard</option>
      <option value='unwatched'>unheard</option>
    </select>
    </td>
  </tr>
<br>

<table id='t-watched' class='sortable'>
  <thead><tr id='t-watched-head'></tr></thead>
  <tbody id='t-watched-body'></tbody>
</table>

## Periods

### The Teens (1910 - 1920)
This period is also known as 'Early Jazz'.  
Related genres:

- early jazz
- Chicago jzz
- New Orleans jazz

#### Artists
Key artists:

- The Original Dixieland Jass Band
  - James Reese Europe - Castle House Rag ( 1914)
- Kid Ory
- Clarence Williams

Other artists:

- Papa Celestin (trumpet: 1884-1954)
- Will Marion Cook (composer, violin: 1869-1944)
- The Eagle Band (1895-1929)
- Bunk Johnson (1879–1949)
- Sara Martin (1884-1955)
- New Orleans Rhythm Kings (1922
- Original Creole Orchestra

#### Summary/conclusion
Key questions:

- **Q: Why are key artists the key artists?**  
  **A:** ??
- Do I like others better?
- Do I like the music of this time?
- Who is my favourite musician?
- Any other musicians in this era who are not in the book?
