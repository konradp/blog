---
title: "Cleaning up disk space"
subtitle: 
date: 2023-01-20
categories: computers
---
This is a list of ideas for cleaning disk space on my personal laptop. The outcome will be a script which will do this for me.

{{<toc>}}

## General ideas for the app
- Do I want an UI?
    - I kind of ignored all the other already-existing apps out there as they are all UI-based and I'd like to create my own so that I know exactly what it does (while also learning new things)
- script/functionality to automate?
- script to alert when the disk can be cleaned?
- review newly installed packages, e.g. a week after they've been installed, some UI prompts:
    - keep indefinitely?
    - give some more time to test the app?
    - remove?

## Packages
[ref: cheatsheet: Debian package management](http://konradp.gitlab.io/blog/post/com-debian-pkg-mgmt/)

General notes:

- what happens to app cache after I uninstall a package?
- Is this dependend on .deb pre/post scripts?
- Do the packages clean up after themselves?
- How do I find these?
- Should I look at the .dot dirs in my home dir one by one?
- how big is my system if I freshly reinstall it?
- Can I even do it?

### General maintenance, clean cache
```
apt autoremove
apt clean
apt autoclean
```

### Package info
I want to find:

- package size
    - does it include cache files?
- largest packages
- rarely used packages

#### Package sizes

```
$ apt show vim | grep Size
Installed-Size: 3,365 kB
Download-Size: 1,494 kB

$ apt-cache show vim | grep Size
Installed-Size: 3286
Size: 1494176

$ dpkg --status vim | grep Size
Installed-Size: 3286

$ dpkg-query --show --showformat '${Package} ${Installed-Size} ${binary:Synopsis}\n' | grep vim
vim 3286 Vi IMproved - enhanced vi editor
[...]
```

Solution:
```
$ dpkg-query --show \
  --showformat \
  '${Installed-Size} \t ${Package} ${binary:Synopsis}\n' \
  | sort -n \
  | tail -n 20
114026   firmware-iwlwifi Binary firmware for Intel Wireless cards
119604   libqt5webenginecore5 Web content engine library for Qt - Core
132921   libreoffice-core office productivity suite -- arch-dependent files
138246   libboost1.74-dev Boost C++ Libraries development files
145169   fluid-soundfont-gm Fluid (R3) General MIDI SoundFont (GM)
145616   docker-ce-cli Docker CLI: the open-source application container engine
167160   openjdk-11-jre-headless OpenJDK Java runtime, using Hotspot JIT (headless)
184108   discord Chat for Communities and Friends
198790   ansible Configuration management, deployment, and task execution system
212698   carla audio plugin host
214359   chromium web browser
223530   hydrogen-drumkits drumkits for Hydrogen
223554   libwine Windows API implementation - library
226697   libwine Windows API implementation - library
263632   linux-image-4.19.0-13-amd64 Linux 4.19 for 64-bit PCs (signed)
264115   linux-image-4.19.0-23-amd64 Linux 4.19 for 64-bit PCs (signed)
307517   google-chrome-stable The web browser from Google
310248   linux-image-5.10.0-20-amd64 Linux 5.10 for 64-bit PCs (signed)
314913   grafana Grafana
510797   zoom Zoom Cloud Meetings 
```

## OS files
  - /var
  - /lib
  - /var/log
  - /var/cache
  - /var/tmp
  - /usr/lib
  - /usr/share
  - /usr/local
  - /usr/lib
  - old kernels

### Old kernels
```
$ dpkg-query --show --showformat '${Package} ${Installed-Size} ${binary:Synopsis}\n' | grep linux-image
linux-image-4.19.0-13-amd64 263632 Linux 4.19 for 64-bit PCs (signed)
linux-image-4.19.0-23-amd64 264115 Linux 4.19 for 64-bit PCs (signed)
linux-image-5.10.0-20-amd64 310248 Linux 5.10 for 64-bit PCs (signed)
linux-image-amd64 13 Linux for 64-bit PCs (meta-package)
```
and
```
$ uname -r
5.10.0-20-amd64
```
and
```
sudo apt-get --purge remove linux-image-4.19.0-13-amd64
sudo apt-get autoremove
```


## Application files


### Ardour
https://manual.ardour.org/working-with-sessions/cleaning_up/


### Chrome / Chromium
```
rm -r ~/.cache/chromium/Default/Cache/Cache_Data/*
rm -r ~/.cache/google-chrome/Default/Cache/Cache_Data/*
rm -r ~/.cache/google-chrome/Default/Code\ Cache/*
```


### Docker
images
```
docker rmi $(docker images -aq)
```
volumes
```
docker volume ls
docker volume prune
```
gives
```
Total reclaimed space: 373.8MB
```

```
docker system prune -a
```
gives
```
$ docker system prune -a                                                                
WARNING! This will remove:                                                              
  - all stopped containers                                                              
  - all networks not used by at least one container                                     
  - all images without at least one container associated to them                        
  - all build cache                                                                     

Are you sure you want to continue? [y/N] y                                              
Deleted Containers:                                                                     
[...]

Deleted Networks:                                                                       
[...]

Deleted Images:                                                                         
[...]

Total reclaimed space: 1.606GB
```

Images
```
docker images -a
```
gives
```
docker ima$ docker images -a
REPOSITORY          TAG         IMAGE ID       CREATED        SIZE
fintools-ib_bin     latest      acbb767c0d21   29 hours ago   1.23GB
<none>              <none>      5d8acff98c75   29 hours ago   925MB
<none>              <none>      3c8d7b933efb   29 hours ago   925MB
fintools-ib_ib_gw   latest      da8c0ea9a7b2   29 hours ago   379MB
<none>              <none>      44cedb24c95a   29 hours ago   379MB
<none>              <none>      032be9e9314e   29 hours ago   367MB
<none>              <none>      241224a96c4c   29 hours ago   367MB
<none>              <none>      af61632d5351   29 hours ago   356MB
<none>              <none>      0416e176ac9d   29 hours ago   356MB
python              latest      33ce09363487   13 days ago    925MB
eclipse-temurin     11-alpine   be00c8675b69   4 weeks ago    352MB
```

### Terraform
.terraform dir. What happens if we delete it?


## Other
- clean up disk space:
  - convert wavs to ogg or mp3
  - clean up audacity files
  - remove packages that I don't need, e.g.:
    - svn
    - xine
  - remove files for apps that no longer exist, e.g. ~/.mozilla, ~/.tintin, ~/.npm, ~/.local
      - but watch out: bookmarks

