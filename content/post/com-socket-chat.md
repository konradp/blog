---
title: "COM: Chat server using network sockets"
subtitle: 
date: 2020-05-28
tags: [ "wip" ]
categories: wip
---
# Abstract
We develop a C/C++ chat program which uses Network Sockets and uses the `fork()` method to spawn a separate process for each client. Typically, such application implies I/O multiplexing, and `select/poll/epoll` methods are encouraged for managing multiple connections instead of `fork()`. Here however, we try the `fork()` method, combined with an additional Unix Domain Socket.

# Motivation
We write a simple C++ chat application which uses Linux [Network sockets](https://en.wikipedia.org/wiki/Network_socket). Many network socket programming tutorials describe creating simple server/client programs, with a typical example of an echo server, where the server repeats/echoes back to the client the messages sent by the client. These examples usually use the `fork()` method which spawns a separate process for each client connection. However, this means that these processes are separate from the main program (forked), and there is no longer an obvious way for the main process to be able to e.g. receive the message from client A and send it to client B.

To address this, we further employ another type of socket, an [Unix domain socket](https://en.wikipedia.org/wiki/Unix_domain_socket). Domain sockets are simpler compared to network sockets and can be used for [Inter-Process Communication](https://en.wikipedia.org/wiki/Inter-process_communication). This way, all the server processes connect to a single Unix domain socket, call it C. Now, whenever client A sends a message to the server, the corresponding forked server process (call it A') sends the data to socket C. The server process which is handling the client B, call it B', also watches the socket C, reads the message from client A using `recv()`, and sends it to the client B.

For select/poll/epoll, additionally to the respective manual pages, see also:

- https://medium.com/@copyconstruct/nonblocking-i-o-99948ad7c957
- https://jvns.ca/blog/2017/06/03/async-io-on-linux--select--poll--and-epoll/
- https://devarea.com/linux-io-multiplexing-select-vs-poll-vs-epoll/#.XtRMkaeYW00
- https://medium.com/@copyconstruct/the-method-to-epolls-madness-d9d2d6378642

# Network sockets
We use my tinysocket library from https://gitlab.com/konradp/tinysocketlib. To set-up a network socket, tinysocket provides GetAddr/Socket/Bind/Listen/Accept methods, which are wrappers for the network socket methods:

- GetAddr: [`getaddrinfo`](https://www.man7.org/linux/man-pages/man3/gai_strerror.3.html)`(..., &servinfo)`
- Socket: `sockfd = ` [`socket`](https://www.man7.org/linux/man-pages/man2/socket.2.html)`(...)` and `ok_sockopt = ` [`setsockopt`](https://www.man7.org/linux/man-pages/man3/setsockopt.3p.html)`(...)`
- Bind: [`bind`](https://www.man7.org/linux/man-pages/man2/bind.2.html)`(sockfd, servinfo->...)`
- Listen: [`listen`](https://www.man7.org/linux/man-pages/man2/listen.2.html)`(sockfd, ...)`
- Accept: in a loop: `int filedesc = ` [`accept`](https://man7.org/linux/man-pages/man2/accept.2.html)`(sockfd, &remote_addr, ...)`

Once connection is accepted (`accept()` returns non-zero), do `fork(...)` to spawn a new process, and inside the new process you can do `send(sockfd, ...)` and `recv(sockfd, ...)` to send and receive data, respectively.

Now, the problem is due to `fork()`. After forking, the file descriptor of the underlying socket is the same (e.g. integer such as 4, 5, 6 etc.), but it doesn't describe the same socket.

# Unix domain sockets
Sockets offer two-way communication. Network sockets communicate in the network domain. Here, we look at the Unix domain sockets, which communicate within the Unix domain, i.e. on the same Unix system, for example between two processes (in our case, two forked processes spawned by our server program).

Steps for server:

- Socket: `sockfd = socket(AF_UNIX, SOCK_STREAM, 0)`
- Bind: `bind(sockfd, &local, ...)`, where `local` describes the socket file address and type
- Listen: `listen(sockfd, maxconnlimit)`
- Accept: in a loop: `accept(sockfd, ...)`

# Plan
In the server code, before fork, set up a domain socket server, fork it. When client connections are accepted, and forked, connect them to domain socket as a client.

# References
Beej's Guide to Unix Interprocess Communication: https://beej.us/guide/bgipc/  
Beej's Guide to Network Programming: Using Internet Sockets: http://beej.us/guide/bgnet/

