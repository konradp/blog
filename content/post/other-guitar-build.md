---
title: 'OTHER: Build a guitar body'
subtitle: 
date: 2023-08-31
tags: [ 'other' ]
categories: other
---

table of contents:
{{<toc>}}

This is project page for a guitar body build, to fit an existing neck. Also, will refret the neck perhaps.


## Planning

We will look at:
- tools, e.g. router
- parts, e.g. pickups, bridge
- preparation


### Tools
For the body:
- straightedge/long notched metal ruler
- drill guide for straight drilling
- through-body bridge - hardtail
- make a ferrule bar

For the neck:


#### Router
To cut the body blank into a guitar shape and rout cavities, we typically use a router.

TODO: routers are a bit expensive and so I decided to hand-carve the body instead. TODO: Link the youtube video where I saw this.

guides:
- [stewmac: routing neck pocket](https://www.stewmac.com/video-and-ideas/tool-demo-videos/luthier-tools-and-supplies-videos/how-to-rout-a-fender-neck-pocket/)
- https://www.stewmac.com/video-and-ideas/online-resources/routing-and-templates/using-ball-bearing-cutter-bits-for-guitar-routing/
- https://www.youtube.com/watch?v=rW_PArgRqho


## Parts:
- {{<x>}} hardtail bridge:
  - 37.13 BGN [link](https://www.musicworld.bg/en/c_605/i_81680/Fender_Hardtail_Strat_Bridge_Assembly_Chrome.html)
  - 53.80 BGN [link](https://www.musicworld.bg/en/c_626/i_69281/Fender_6_Saddle_Standard_Series_Bridge_Assembly.html)


## Preparation
Preparation:
- get a template:
  - get it from an existing body or from strat?
  - draw it myself from strat
  - TODO: get measurements of harrier
  - TODO: get measurements of fender, e.g. thickness
  - inkscape
  - consider making it smaller
    - but I'd like it smaller, can I work out the measurements?
    - but I'd like it smaller, should the body be thicker? How thick is my blank?
    - I'd like it smaller, how much would it require? e.g. smaller pickguard plate (so needs to be custom)

videos on templates and bridge position:
- https://www.youtube.com/watch?v=ZGxnYFmq4qo
- https://www.youtube.com/watch?v=pCM_C7z72LY
- https://www.youtube.com/watch?v=HX-7qX3OALg
- https://www.youtube.com/watch?v=UyQMktFSemA
- https://www.youtube.com/@BradAngove/videos
- https://www.youtube.com/watch?v=NGCO4pZ7ls8
- https://www.youtube.com/watch?v=mMk0jhqlono
- https://theelectricluthier.com/how-to-determine-guitar-bridge-placement-on-any-guitar/


## Execution
- build body:
  - route shape
  - route neck socket
  - route cavities:
    - pickup cavity
    - jack socket
  - bridge holes
- finish: transparent, dark wood paint, oil?
