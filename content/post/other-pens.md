---
title: "OTHER: pens"
date: 2024-01-10
categories: other
---

{{<toc>}}

<style>
img {
  width: 80%;
}
</style>

## 001 Pelikan
<center>
<img src="/media/other-pens/001pelikan01.jpg"></img>
<img src="/media/other-pens/001pelikan02.jpg"></img>
</center>

## 002 Parker P-51 reissue
<center>
<img src="/media/other-pens/002parker01.jpg"></img>
<img src="/media/other-pens/002parker02.jpg"></img>
<img src="/media/other-pens/002parker03.jpg"></img>
</center>

## 003: "51" youth
colour: green
<center>
<img src="/media/other-pens/003youth01.jpg"></img>
<img src="/media/other-pens/003youth02.jpg"></img>
<img src="/media/other-pens/003youth03.jpg"></img>
</center>

## 004: Pelikan 30 rolled gold
- cap marking: PELIKAN 30 ROLLED GOLD - GERMANY
- colour: navy blue
<center>
<img src="/media/other-pens/004pelikan01.jpg"></img>
<img src="/media/other-pens/004pelikan02.jpg"></img>
</center>

## 005: S * N
- nib marking: TOKER 60
- colour: black
<center>
<img src="/media/other-pens/005sn01.jpg"></img>
<img src="/media/other-pens/005sn02.jpg"></img>
</center>

## 006 WING SUNG
- cap marking: WING SUNG MADE IN CHINA
- colour: grey
<center>
<img src="/media/other-pens/006wingsung01.jpg"></img>
<img src="/media/other-pens/006wingsung02.jpg"></img>
</center>

## 007 iridium point
- nib marking: IRIDIUM POINT GERMANY
- colour: black
<center>
<img src="/media/other-pens/007ip01.jpg"></img>
<img src="/media/other-pens/007ip02.jpg"></img>
</center>
