---
title: "COM: Password managers"
subtitle: 
date: 2021-09-13
tags: [ 'wip' ]
categories: wip
---
Requirements:

- open source
- cloud based
- Android support
- actively developed

Considered:

- KeePass: https://keepass.info
- clippers: https://clipperz.is

Winner:

- ?

# Considerations
## KeePass
- one

# WIP
- {{< icon tick >}}
- {{< icon cross >}}
- {{< icon question >}}
- {{< icon warning >}}
