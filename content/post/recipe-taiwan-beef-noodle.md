---
title: "RECIPE: Taiwan beef noodle soup"
date: 2021-04-06
tags: [ 'other' ]
categories: other
---

![beef_noodle_soup](/media/recipe-taiwan-beef-noodle-soup/2.jpg)
<!--more-->

# Notes
I always add too much salt here.  
Go easy on salt and soy sauce. Test test

# Ingredients
bowl 1:

- **beef, 400g**

bowl 2: (dry spices)

- **star anise**, 3
- **sichuan pepper**, 2tsp
- bay leaf, 2
- cloves, 3
- cinnamon, 1/2 stick
- cumin, 1tsp
- (use 5 spice powder here? 1tsp? also coriander seeds, fennel seeds?)

bowl 3, saute:

- **onion**, sliced (+white part of green onion)
- **ginger**, thin slices
- tomato

bowl 4, saute:

- **garlic**, 3 cloves
- red chillies
- tomato paste, 1 teaspoon
- green onion

other:

- **shaohshing rice wine**, 1/4 cup
- soy sauce, light, 3 tbsp
- soy sauce, dark, 1 tbsp
- fish sauce, 2 tbsp (or soy sauce)
- sugar, dark, 1 tsp

serve with:

- **noodle**
- **pak choi**
- sesame seeds


# Recipe

## Toast spices in instant pot

- bowl 2 (dry spices)
- 2.5 min toast (or until smell nice)
- remove all

## Browning the beef
wait until pot is hot, use browning meat option  
10 min, or until water from the beef has disappeared, and beef is browned

- salt, 1 pinch
- oil, 1 tbsp

Remove

## Saute onion/tomato/ginger, then garlic/chillies/spices
Saute for 5 minutes:

- oil
- bowl 3 (onion/tomato/ginger)

Saute for 1 min:

- bowl 4 (garlic/chilli/tomato paste/green onion)
- spices


## Deglaze pot with rice wine
- add rice wine, 1/4 cup (consider less here)
- deglaze (scrub) and mix


## Cook the soup and leave overnight

- browned beef
- 3 tbsp soy sauce
- dark pepper, 1 tsp (consider 1/2 tsp)
- sugar, 1 tsp
- 1625ml cold water

Mix together and cook 45 minutes.  
Then, cook 30 minutes natural release.
Leave overnight.

## Reheat and serve
Cook again 30 minutes, natural release.  
Cook noodles with pak choi.  
Serve.
