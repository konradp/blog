---
title: "COM: Segmenting Mandaring text using CC-CEDICT"
subtitle: 
date: 2021-11-02
tags: [ 'wip' ]
categories: wip
---

# Abstract
This is my own attempt at segmenting Mandarin text (traditional), without having first studied any other projects which do it. We do this in javascript, and compare for accuracy against [this tool][cc-cedict-word-dict1]. We find that...TODO.

# Problem statement
Consider a sentence *上週我看了一部台灣電影 (Shàng zhōu wǒ kànle yī bù táiwān diànyǐng) (Last week I watched a Taiwanese film)*. Although the text is written without any spaces, it contains some polysyllabic words, e.g. *電影 (diànyǐng) (movie)*, where each character has its own literal meaning:

- 電 diàn: lightning, elecricity, electric (bound form), ...
- 影 yǐng: picture, image, film, movie

and so we can interpret the *電影* word per-character as 'electric picture', or simply as what it is accepted to mean: 'a movie/film'. A dictionary such as CC-CEDICT will have both individual character entries, as well as the resulting word:

- `電 电 [dian4] /lightning/electricity/electric (bound form)/to get (or give) an electric shock/phone call or telegram etc/to send via telephone or telegram etc/`
- `影 影 [ying3] /picture/image/film/movie/photograph/reflection/shadow/trace/`
- `電影 电影 [dian4 ying3] /movie/film/CL:部[bu4],片[pian4],幕[mu4],場|场[chang3]/`

and so (for the purpose of translating) it's useful to split the above text into segments:

- 上: last
- 週: week
- 我: I
- 看: see
- 了: past tense (see : saw)
- 一: one
- 部: measure word for 'film'
- 台灣: Taiwan
- 電影: film

**Question:** Given a dictionary which contains both the characters, as well as multi-character words - can we (in a simple way) **segment/partition a Mandarin text** in a way which characters are clustered into words?

# Algorithm 1: Idea
It goes as follows, we process one char at a time:

- 1: Get a character (char1). Store location
- 2: Get all dict entries which start with char1 (also store the exact entry): how many of these?
  - If only one (only exact entry): This is a word. Store this word, move to next char (step1)
  - If more than one:
      - Get next char, how many of the entries in dict that start with these two?
      - If only one: This is a word.... (recursive here, back to step1)

In this way, we first get many dict entries which start with a given char. We process next chars, until we reach the full word.

The program which processes the dictionary will require a function:

- `def get_entries_start_with(str char)`  
   Return list of dictionary entries which start with given character

It is fair to assume that this list will be small. If it's of length one (one dict entry found), then we are done. If the length is more than one, we process the next character, and so reduce the list of potential words.

## Problems
### Is it possible to have two possible entries and be stuck?
The Algorithm 1 has a potential problem.  
Example: dict words:

- 12A
- 12B

and consider a sentence:

- 12C

so there is no dict entry for '12' on its own, which is what we would have wanted to pick.
Which one to pick?

**Answer:** The last best entry, i.e. at each step don't just pick all the entries that 'start with', but also store the one which is 'exactly as specified'. If we get no word matches when we consider the next char, then the last 'exact' match is our match.
So in the algorithm we need the step 'Store the exact entry too'

### Chunking: AB C vs. A BC
Consider a sentence:

- ABC

with a dictionary:

- A
- B
- C
- AB
- BC
- ABC

How should such text be chunked? Options:

- A B C
- **A BC**
- **AB C**
- ABC

The algorithm should capture all four possibilities. So, after processing the first three characters and finding the A, AB, ABC, on the next iteration, we do not move to the character after C, but we move to the second character, B.

Consider now a sentence with four characters (ABCD), and options (8):

```
- A B C D (000)
- A B CD  (001)
- A BC D  (010)
- A BCD   (011)
- AB C D  (100)
- AB CD   (101)
- ABC D   (110)
- ABCD    (111)
```

# Interlude: A dictionary parser
At this stage we can start trying out ideas. We define how our dictionary is going to work, and how we parse it. For full details, refer to [CC-CEDICT format][cc-cedict-syntax], which is in short:
```
Traditional Simplified [pin1 yin1] /English equivalent 1/equivalent 2/
```
for example
```
中國 中国 [Zhong1 guo2] /China/Middle Kingdom/
```
and the dictionary parser will need to be able to:

- get entries which start with X char
- split and store these as:
  - entry: meaning

Note: Interestingly, for our purposes (for now, at least), we do not need a method which gives an exact match for given char/word, only the 'starts with'.

# Algorithm 1: Implementation
There is a gotcha in 'Algorithm 1' (see 'Problems' section). Here we see if we can solve it.

# Algorithm 1: Test cases
Dict:
```
```


# Displaying tooltips
## Algorithm 1
The translator outputs an arary of translated text.
```
[
  {char: A, meanings: [...],},
  {char: BC, meanings: [...],}
]
```

## Algorithm 2
The translator outputs an array of translated text.
```
[
  { char: "中", options: [...] },
  { char: "國", options: [...] },
]
```
in more detail:
```
[
  {
    char: "中",
    options: [
      {trad: "中", pinyin: "Zhong1", meanings: ["China", "Chinese"]},
      {trad: "中", pinyin: "zhong1", meanings: ["within", "among"]},
      {trad: "中", pinyin: "zhong4", meanings: ["to hit (the mark)"]},
      {trad: "中國", pinyin: "Zhong1 guo2", meanings: ["China"]},
    ],
  },
  {
    char: "國",
    options: [
      {trad: "國", pinyin: "Guo2", meanings: ["surname Guo"]},
      {trad: "國", pinyin: "guo2",meanings: ["country", "nation"]},
    ]
  }
]
```
From this data we can create a tooltip over the first character (中) which will contain all the 'options' and their meanings for this character (中, 中, 中, 中國), with the longest one at the top. It will also highlight this longest option in the text. The next tooltip will be over the character 國 and will contain two options.

## Drawing tooltip
Questions:

- How to represent tooltip: floating div with a solid background
- Where to store translated data:
  - inside html (span element)
  - leave it inside the data, but assign indexes to the span objects for lookup
- How to highlight the text:
  - algorithm 1: `span style=background-color: rgb()`
  - algorithm 2: as above, but highlight the longest word (neighbouring spans)

The HTML will consist of `<span>` objects for each text character, e.g. `<span>中國</span>`
<span>

```

<!--Reference-->
[cc-cedict-word-dict1]: https://www.mdbg.net/chinese/dictionary?page=worddict&wdrst=0&wdqtm=2&wdqcham=0&wdqt=%EF%BC%88%E5%8F%B0%E7%81%A3%E8%8B%B1%E6%96%87%E6%96%B0%E8%81%9E%EF%BC%8F+%E6%94%BF%E6%B2%BB%E7%B5%84+%E7%B6%9C%E5%90%88%E5%A0%B1%E5%B0%8E%EF%BC%89%E7%AA%81%E7%A0%B4%E5%A4%96%E4%BA%A4%E9%87%8C%E7%A8%8B%E7%A2%91%EF%BC%81%E6%AD%90%E6%B4%B2%E8%AD%B0%E6%9C%83%E3%80%8C%E5%A4%96%E5%9C%8B%E5%B9%B2%E9%A0%90%E6%AD%90%E7%9B%9F%E6%B0%91%E4%B8%BB%E7%A8%8B%E5%BA%8F%E7%89%B9%E5%88%A5%E5%A7%94%E5%93%A1%E6%9C%83%E3%80%8D7%E5%90%8D%E8%AD%B0%E5%93%A1%E6%98%8E%EF%BC%883%EF%BC%89%E6%97%A5%E6%8A%B5%E9%81%94%E5%8F%B0%E7%81%A3%E8%A8%AA%E5%95%8F%EF%BC%8C%E5%85%B6%E4%B8%AD%E4%B8%80%E5%90%8D%E8%AD%B0%E5%93%A1%E5%9C%A8%E6%8E%A8%E7%89%B9%E4%B8%8A%E8%AD%89%E5%AF%A6%EF%BC%8C%E5%B7%B2%E6%90%AD%E4%B8%8A%E4%BE%86%E5%8F%B0%E7%8F%AD%E6%A9%9F%E3%80%82&dpohm=0
[cc-cedict-syntax]: https://cc-cedict.org/wiki/format:syntax

