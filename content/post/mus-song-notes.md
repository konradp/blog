---
title: "MUS: Song notes"
subtitle: 
date: 2022-09-11
tags: [ 'music' ]
categories: music
---
My notes for my own songs.
{{< toc >}}

## Songs

### Old clothes

instr          | voice           | tune | mix vol  | mix pan
---            | ---             | --:  | ---      | --- |
**kick**       | **04 RAPKICK**  |   3  | 99       | <3  |
**snare**      |   41 AMBIENTP   |  13  | 99       | 3>  |
**closed hat** |   20 CLOSEDHH   |  -7  | 99       | 3>  |
mid hat        |   21 MIDHH      |   0  | 99       | <>  |
**---**        |   ---           | ---  | ---      | --- |
open hat       |   22 OPENHH     |   1  | 99       | <>  |
**claps**      |   36 GATECLAP   |  -5  | 99       | 3>  |
perc 3         |   35 TAMBOURI   |  -4  | 99       | 3>  |
perc 4         |   39 PIPE       |  -5  | 67       | 3>  |
**---**        |   ---           | ---  | ---      | --- |
tom 1          |   16 FATSNARE   |   0  | 99       | 3>  |
tom 2          |   11 MIDPICSN   |  -1  | 85       | <>  |
tom 3          |   11 MIDPICSN   |   1  | 85       | 3>  |
tom 4          |   11 MIDPICSN   |   3  | 90       | 1>  |
**---**        |   ---           | ---  | ---      | --- |
ride           |   11 MIDPICSN   |  -5  | 90       | <1  |
crash          |   28 CRASHCYM   |   7  | 85       | 2>  |
perc 1         |   39 PIPE       |  11  | 78       | 3>  |
perc 2         |   39 PIPE       |   4  | 74       | 3>  |
