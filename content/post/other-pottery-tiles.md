---
title: "OTHER: Pottery project: Tiles and glazes"
subtitle: 
date: 2021-07-12
tags: [ 'wip' ]
categories: wip
---

{{< toc >}}

# Intro
TODO: This is about making tiles to test glazes and a first firing.

Two batches:

- single fire: glaze included
- bisque firing, then glaze

# TODO
- buy tongs
- find or get clay photos from eariler stages
- photos of fire pit
  - dry
  - hot
- photo of gunky clay
- remove the gunk from top of the clay
- buy cheap jeans
- buy spray bottles (or other bottles to store glazes)
- prepare clay: remove water: jeans
- wooden guides: thin pieces of wood
- buy glaze materials: vitamins, mineral supplements: iron etc
- buy thermocouple probes: k-type, 2,000*C (install it, if applicable)
- make a tiny version of this post, e.g.
  - How to fire clay in a fire pit
  - Pottery glazes from vitamin supplements

# Objectives
I intend to confirm the below.

- is this even clay
- will it break when drying
- will it break when firing
- single firing or bisque+glaze firing
- can I make glazes from Tesco mineral supplements
- how hot does a washing machine drum firepit get
- do I need to add sand or grog to filtered clay

## Clay: Gather and filter
I dug out some clay from my back garden. I dissolved it in water, and then passed through various sieves. I've let it stand for weeks, and poured water off from the top.


## Jeans treatment
TODO: Photos here

## Mark tiles
ABC

where:

- A: is sand added
  - 0: no sand added
  - 1: sand added
- B: single or double fire
  - 0: single fired  
    glaze applied on dry clay, then fired once
- C: glaze number
  - 0: no glaze

Example:

010: No sand, twice-fired, no glaze
