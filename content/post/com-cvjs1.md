---
title: 'cvjs: Interactive CV'
date: 2022-12-07
categories: computers
---
<head><link rel='stylesheet' href='/media/com-cvjs1/style.css'></head>
<script src='https://cdnjs.cloudflare.com/ajax/libs/js-yaml/4.1.0/js-yaml.min.js'></script>
<script src='/media/com-cvjs1/app.js'></script>
<body onload='OnLoad()'></body>

Project diary for the [cvjs app](https://gitlab.com/konradp/cvjs).

Contents:

{{<toc>}}

## Motivation
As I have many projects in GitLab, snippets and various cheatsheets, I would like to supplement my CV with an interactive/clickable CV which would list my skills and interests. I would like to write it in JavaScript, served as a static page.

## Prototype 1: render YAML as HTML UL tree
The prototype is a clickable tree of UL/LI elements, as below:
<center>
<div id='content' style='height: 200px; border: 1px solid; width: 50%; text-align: left; overflow-y: scroll'><h2>First Last</h2></div>
[demo](https://konradp.gitlab.io/cvjs-old/)/[code](https://gitlab.com/konradp/cvjs-old)
</center>

The HTML is built from a YAML file, parsed with js-yaml:
```yaml
bio blurb and interests: text
skills:
  cloud plaftorms:
  - aws
  config management:
  - overview
  - ansible
  - puppet
  databases:
    mysql: trolo
  linux:
  programming:
    general:
      programming patterns:
      - one
      - two
```

While this worked to an extent, the YAML file started growing very quickly. In the meantime I also got an idea to have a section on the right which would display the pages selected from the tree, something like:
<center><img src='/media/com-cvjs1/cvjs.png' style='width: 60%'/></center>

This would make it somewhat similar to a Confluence space, a documentation page (see Ansible docs).

### What I learned
What I learned from this prototype:

- HTML/CSS tricks:
    - `list-style-type: none`: hide ul/li bullet points
    - `flex` and `flex-flow` for document layout
    - `overflow-x: hidden; overflow-y: scroll;`: scrollbar on a div
- Additions to the [NodeJS cheatsheet](http://localhost:1313/blog/post/com-node-cheatsheet/)

CSS flex for two-column view:
```
.container {
  display: flex;
  flex-flow: column;
  height: 100%;
}
.header {
  flex: 0 1 auto;
}
.content {
  flex: 1 1 auto;
  overflow-y: hidden;
  display: flex;
}
.footer {
  flex: 0 1 10px;
  text-align: right;
}
```
reference: https://developer.mozilla.org/en-US/docs/Web/CSS/flex

## Prototype 2: dir structure and markdown to html
For the second prototype, the ideas were:

- instead of the tree structure as YAML, use a dir structure
- write documents as .md files, convert them to html

For this, I wrote two scripts:

- [dirs2json](https://gitlab.com/konradp/snippets/-/blob/master/js/other/dirs2json/main.js)
- [markdown2html](https://gitlab.com/konradp/snippets/-/blob/master/js/libs/markdown2html/build.js)

### dirs2json
The script takes a dir structure, e.g.:
```
skills/
  cloud platforms/
    overview.html
    aws.html
skills/
  cloud platforms/
    overview.html
    aws.html
```
and turns it into a JSON file:
```
[
  {
    "name": "skills",
    "type": "dir",
    "children": [
      {
        "name": "cloud platforms",
        "type": "dir",
        "children": [
          {
            "name": "overview.html",
            "type": "file"
          },
          {
            "name": "aws.html",
            "type": "file"
[...]
```

### markdown2html
This script takes a dir called 'content', and recreates its structure under a dir called 'public', but with all the .md files converted into .html files. It uses the **showdown** JS library.

### Thoughts
- the JSON file will also contain the URLs of the pages.
- ordering of documents: metadata file in the dir?
- combine the two scripts

### idea: custom dir and file titles
In the first prototype, one of the dirs was called **'computers/programming'**. However, the '/' char is not allowed in UNIX dirs. One idea is to include a metadata file in the dir structure, e.g. called **`.meta`**, which will contain the dir display-name.

For the files, there is a functionality in `showdown` which allows including metadata in the markdown files. TODO: How, where?

### idea: update URLs on a single page
The design is that of a single page app. After clicking on a file in the left pane, it will be fetched and loaded in the right-hand pane. It would be nice to change the URLs in the browser, to allow sharing these URLs. But I can't think of a nice way to do this while preserving the tree on the left and keeping the page static.

Consider:

- https://stackoverflow.com/questions/824349/how-do-i-modify-the-url-without-reloading-the-page
- https://stackoverflow.com/questions/3338642/updating-address-bar-with-new-url-without-hash-or-reloading-the-page
- https://www.30secondsofcode.org/articles/s/javascript-modify-url-without-reload


## Prototype 3: final version
For this, I combined the two scripts `dirs2json` and `markdown2html` into a single script which can be executed with `npm run build`.

### UI
- dirs have '+' or '-' in front
- files don't have '+' or '-' in front

<center>
<img src='/media/com-cvjs1/cvjs2.png' style='width:80%'/>
</center>
The CVJS is now available at https://konradp.gitlab.io/cvjs
