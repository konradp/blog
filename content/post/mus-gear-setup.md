---
title: "MUS: Recording gear setup"
subtitle: 
date: 2022-05-24
tags: [ 'wip' ]
categories: wip
---
{{< toc >}}

## Gear
acoustic:

- guitar (+electric)
- piano
- trumpet
- harmonica
- flute
- violin

studio:

- audio interface: Behringer U-PHORIA UMC202HD
- synth (mono): Behringer CRAVE
- sequencer: KORG SQ-64
- drum machine: Alesis HR-16:B
- mic: Rode NT1-A, 2 x Shure SM57
- controller: Arturia MINILAB mkII

## Audio connections
**Note:** HR-16:B has four outputs, and USB interface has only two inputs. Need a mixer?

<img src="/media/mus-gear-setup/connections_audio.png"></img>

## MIDI connections
Both MIDI-producing devices (sq64 and arturia) send USB MIDI to the laptop. The laptop then controls the Crave via USB MIDI, and controls the HR-16 through the USB-to-MIDI cable.

<img src="/media/mus-gear-setup/connections_midi.png"></img>

SQ-64 and Arturia can both control the below (with MIDI channels):

- 1: HR-16
- 2: CRAVE
- 3: csound (chords)
- 4: LinuxSampler (samples)

## Live scenarios
Again, these are limited by the audio interface two-input problem:

### Scenario 1: guitar, drums, chords, samples (no CRAVE)
- lead (guitar and/or csound): can do looping here?
- drums (HR-16): SQ64 and/or Arturia
- chords (csound): SQ64 and/or Arturia
- samples (LinuxSampler): SQ64 and/or Arturia

Drums is a must-have, and the other audio input of the interface is taken by the guitar. I can't use the CRAVE here because it has no USB audio and I only have a two-input audio interface.

Guitar effects can be controlled with Arturia.

Pros:

- lead can switch
- SQ64 has four tracks, so it can control:
  - csound (chords and melodies)
  - LinuxSampler (chords, melodies, fx)
- Arturia can do MIDI CC, so it can control:
  - csound params (ADSR, fx params, etc): for solo or chords
  - csound effect params

Cons:

- audio interface is no mixer, so only two audio channels
- software mixer
- no CRAVE

### Scenario 2: CRAVE, drums, chords, samples
Audio interface inputs:

- CRAVE
- drums

Because the audio interface has only two inputs, and the SQ-64 has four tracks, so the SQ-64 (and Arturia) can control these:

- CRAVE
- csound
- drums
- LinuxSampler (samples or chords)

and any acoustic instruments (or electric guitar) need to be done with samples, or overdubbed afterwards.


## Questions

- I can tweak HR16 live (e.g. pitch for each instrument). Can these be tweaked by MIDI CC?

## Laptop setup
I have a dir in onedrive where all of my music is stored (21 GB). This dir is synced to an sd card on my laptop.

music dir:
```
~/music
```

#### SD card
- lexar vs samsung. I'm using Samsung, 64 GB card because it's faster
- the SD card is formatted using FAT
- mount /dev/mmcblk1p1 /music


#### Onedrive
Install onedrive client
```
sudo apt install onedrive
```

selective sync: https://github.com/abraunegg/onedrive/blob/master/docs/USAGE.md#performing-a-selective-sync-via-sync_list-file

ref: https://github.com/abraunegg/onedrive/blob/master/docs/USAGE.md
