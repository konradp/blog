---
title: "MUS: Piano ABRSM notes: 2021/2022"
subtitle: 
date: 2021-04-23
tags: [ "mus" ]
categories: music
---

Pieces I like

# Grade 2
One from each A,B,C list. So 3 pieces in total.  
Score out of 10.

A:

- 10 Attwood - Allegro (1st movt. Sonatina No.1 in G)
- 09 Kabalevsky - Galop/Hopping <- Learn in personal time
- 09 Mozart - Minuet in D, K.7
- 08 Carse - Rustic Danse
- 08 Haydn - Rondino
- 07 Goedicke - Etude in A minor, Op 33/13
- 05 L.Mozart - Burlesque
- 10 Gambarini - Minuet
- 02 Hassler - Ecossaise in G
- 02 Anon - Corranto

B:

- 10 Steibelt - Adagio, A minor (Sonatina in C)  
  video: https://www.youtube.com/watch?v=UhHM0bwz1TA  
  score: https://drive.google.com/file/d/1NWaXyMGnOj2BnEpuOoDg4pHgovllTGKK/view
- 10 Madden - The first flakes are falling
- 10 Bartok - Sorrow
- 10 Grechaninov - Farewell
- 10 Spindler - Walts, A minor
- 05 Pie - Le cant du patre
- 05 trad. eng. - Waly Waly, ar. Davies
- 05 Hammond - Raindrop reflections
- 05 Ravel - Pavane de la belle au bois dormant

C:

- 10 Watts - Postcard from Paris
- 10 Einaudi - The snow prelude no.3 <- nice
- 10 DUET Iles - Sweet pea
- 05 Chapple - March Hare
- 05 Gaudet - Angelfish <- too much pedal
- 05 Norton - Inter-City Stomp <- jazz
- 05 DUET Garscia - Brigand's dance
- 01 Sculthorpe - Singing sun
- 01 Swayne - Whistline tune <- BUT MAYBE
- 01 Crosland - In my spot <- jazz, really bad
- 01 Konecsni - Dinosaur, don't <- awful!!!!
