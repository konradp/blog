---
title: "MUS: Making a kaval"
subtitle: 
date: 2024-10-05
math: true
categories: wip
---

**Note:** All measurements are in millimeters.

TABLE OF CONTENTS:
{{< toc >}}


## Plan
- {{<x>}} measure kaval
- {{<x>}} study physics
- {{<x>}} question: given inner diameter X and length Y, what is the frequency of the note?
- {{<x>}} cut wood
- {{<x>}} dry wood
- {{<x>}} create a lathe jig


## Intuitive understanding
- create an open pipe and a closed pipe  
  variable length
- play it in different ways: like kaval, like ney, like 'carinet'


## Physics
- closed pipe: It's closed at one end, open at the other

From 'Concise Advanced Physics' by D. R. J. Wood (part I, p. 142):

<div style="background-color: silver">
<b>VIBRATION OF AIR COLUMNS</b><br>
<i>Closed Pipes</i><br>
If a disturbance is set up at the mouth of the pipe the progressive wave
travelling down the pipe may interfere with the wave reflected from the closed
end, producing, if the pipe has a length equal to an odd number of quarter
wavelengths, a stationary wave. In this condition the pipe is resonating
(Fig. 7.16).

When the air is resonating there must be a point of no vibration called a node,
at the closed end, and a point of maximum vibration called an anti-node at the
open end. Hence the fundamental note sounded by a closed pipe will be such that
$l = \frac{\lambda}{4}$ where $l$ is the length of the pipe and $\lambda$ the
wavelength of the note emitted (full curve).

Wavelengths such that $l = \frac{3\lambda'}{4}$ (broken curve):
$\frac{4\lambda''}{4} : \frac{7\lambda'''}{4}$ etc., may also be present. These
are called the odd harmonics, since they have frequencies an odd number of
times that of the fundamental.
</div>






### Kaval measurements

kaval in D:
- mouthpiece:
  - length: 255
  - thread: 27
  - inner diameter: 16.55, 16.31
- body:
  - length: 263
  - thread: 26
  - width: 27
- tail:
  - length: 175

kaval in C:
- mouthpiece:
  - length: 292
  - thread: 30
- body:
  - length: 300
  - thread: 29
  - width: 29
- tail:
  - length: 200

## Experiment 1
- mouthpiece:
  - inner diameter: 1.631 cm
  - freq (Hz): 551, 557 (once 539)
  - length: 25.4 cm = 0.254 m

From DRJW:

$l = \frac{f}{4}$, i.e. $f = 4l$

so

$f = 4 (25.4) = 101.6$

Also:
- f*2 = 203.2
- f*3 = 304.8
- f*4 = 406.4
- f*5 = 508.0
- f*6 = 609.6

so it doesn't make sense

## References
- https://www.youtube.com/watch?v=0BdExLqS5mU
