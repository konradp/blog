---
title: "MUS: A sampler for Ardour"
subtitle: 
date: 2024-09-11
tags: [ 'wip' ]
categories: wip
---

{{< toc >}}


From https://www.reddit.com/r/linuxaudio/comments/laqdwo/samplerdrum_sequencer_for_ardour/ we see

<center>
<img src='/media/mus-ardour-sampler/reddit1.png'></img>
</center>

to try:
- sfz banks: sfizz
- sf2 banks: fluidsynth
- samplv1
- TAL sampler
- redux
- drumGizmo
- **drumkv1**
- sitala
- speedrum
- linuxsampler
- samplv1: https://samplv1.sourceforge.io/


## drumkv1
Install
```
apt install drumkv1 drumkv1-common drumkv-lv1
```

## linuxsampler
https://linuxaudio.github.io/libremusicproduction/html/tutorials/linuxsampler-and-ardour-using-them-together.html


## References
- sample editing in Ardour: https://www.youtube.com/watch?v=iuUgnBQLJE4
