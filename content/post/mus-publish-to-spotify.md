---
title: "MUS: Publishing to spotify"
subtitle: 
date: 2024-04-21
tags: [ 'wip' ]
categories: wip
---
{{< toc >}}

It seems I can't just upload my music to spotify, but need to use third party sites for this. No surprise, it looks like they charge money.  
Quickread: I chose distrokid

## Notes
I uploaded the 'jarzebina' album to distrokid today (2024-04-21).

## Alternatives
Sites I know, do they upload to spotify?
- magnatune
- jamendo
- soundcloud
- bandcamp

## Traditional

List:
- [DistroKid](https://distrokid.com/): $20/year
- [TuneCore](https://www.tunecore.com/): $30 first year, $50/year after
- [CD Baby](https://cdbaby.com/): $5 per single, $20/album + 9% commission

Others:
- [ReverbNation](https://www.reverbnation.com/): $9.95/month, 19.95 full package
- [LANDR](https://www.landr.com/): $1/month, 12/year, $3/month
- [Amuse](https://amuse.io/)
- [ONErpm](https://onerpm.com/)
- [AWAL](https://www.awal.com/): 15% commission
- [Symphonic Distribution](https://symphonicdistribution.com/): $25 for sign up, $11 for release
- [FreshTunes](https://freshtunes.com/)
- [UnitedMasters](https://unitedmasters.com/)
- [Spinnup](https://spinnup.com/)
- [RouteNote](https://routenote.com/)
- [Horus Music](https://www.horusmusic.global/)
- [Ditto Music](https://www.dittomusic.com/): $19/year
- [Loudr](https://loudr.fm/)
- [Songtradr](https://www.songtradr.com/)
- [iMusician](https://www.imusiciandigital.com/)
- [Record Union](https://www.recordunion.com/)
- [Cinq Music](https://cinqmusic.com/)
- [FUGA](https://www.fuga.com/)
- [The Orchard](https://www.theorchard.com/)
- [Ingrooves](https://www.ingrooves.com/)
- [Believe](https://www.believemusic.com/)
- [Kobalt](https://www.kobaltmusic.com/)
- [stem](https://stem.is/): 5% commission


## Reference
- [site](https://www.productionmusiclive.com/blogs/news/top-10-music-distribution-services-cd-baby-vs-distrokid-vs-tunecore-vs-others)
