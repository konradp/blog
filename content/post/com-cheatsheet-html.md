---
title: "html"
subtitle: 
date: 2023-01-08
tags: [ "com", "cheatsheets" ]
categories: cheatsheets
---

For when I need to look-up the things that I commonly use.

{{<toc>}}

WIP: grid:

- grid-template-columns/grid-template-rows, short: grid/grid-template  
  `grid-template-columns: 2fr 1fr 1fr;`


## include CSS
```
<link rel='stylesheet' href='style.css'>
```

## font
```
body {
  margin: 0;
  font-family: sans-serif;
}
```


## flex
### layout: header, content, footer
```
.container {
  display: flex;
  flex-flow: column;
  height: 100%;
}
.header {
  flex: 0 1 auto;
}
.content {
  flex: 1 1 auto;
  overflow-y: hidden;
}
.footer {
  flex: 0 1 10px;
  text-align: right;
}
```

### layout: two columns
```
.content {
  display: flex;
}
.left {
  flex: 1 1 auto;
  overflow-y: auto;
  width: 40%;
}
.right {
  flex: 1 1 auto;
  overflow-y: scroll;
  width: 60%;
}
```
## table: border-collapse
{{<highlight css>}}
table {
  border-collapse: collapse;
}
table, td, th {
  border: 1px solid;
}
{{</highlight>}}


## HTML: input
ref: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input

- **type/usual: button, checkbox, date, text, range, number**
- type/extra: color, date, datetime-local, email, file, hidden, image, month, password, radio, reset, search, submit, tel, time, url, week
- attributes:
    - input/checkbox, input/radio: checked
    - min, max
    - name
    - step

## icons
https://iconduck.com/sets/genericons-neue

## HTML codes
https://www.toptal.com/designers/htmlarrows/punctuation/
