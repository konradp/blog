---
title: "COM: csound cheatsheet"
subtitle: 
date: 2022-05-24
tags: [ 'wip' ]
categories: wip
---
# Setup
Install csound.
```
sudo apt-get install csound
```
Run files
```
csound -+rtaudio=jack -Ma --daemon --output=/dev/null 1.csd
```

# Possibly useful
```
-+jack_client=[client_name]
The client name used by Csound, defaults to 'csound5'. If multiple instances of Csound connect to the JACK server, different client names need to be used to avoid name conflicts. (Linux and Mac OS X only)
-M DEVICE, --midi-device=DEVICE
Read MIDI events from device DEVICE. If using ALSA MIDI (-+rtmidi=alsa), devices are selected by name and not number. So, you need to use an option like -M hw:CARD,DEVICE where CARD and DEVICE are the card and device numbers (e.g. -M hw:1,0). In the case of CoreMIDI and MME, DEVICE should be a number, and if it is out of range, an error occurs and the valid device numbers are printed. With PortMIDI, DEVICE is either a number for single port input, 'a' for listening to all input ports, or 'm' for mapping ports to MIDI channels above 16. In this case, device 0 uses 1-16, device 1 uses 17-32, ... device N uses (N+1)*channel. The options 'a' and 'm' are also convenient when you don't have devices as they will not generate an error.
--midi-devices[=x]
list midi devices (x=out, output devices only; x=in, input; else input and output) and exit.
--midi-key=N
Route MIDI note on message key number to pfield N as MIDI value [0-127].
--midi-key-cps=N
Route MIDI note on message key number to pfield N as cycles per second.
--midi-key-oct=N
Route MIDI note on message key number to pfield N as linear octave.
--midi-key-pch=N
Route MIDI note on message key number to pfield N as oct.pch (pitch class).
--midi-velocity=N
Route MIDI note on message velocity number to pfield N as MIDI value [0-127].
--midi-velocity-amp=N
Route MIDI note on message velocity number to pfield N as amplitude [0-0dbFS].
--midioutfile=FILENAME
Save MIDI output to a file (Csound 5.00 and later only).

-o FILE, --output=FILE
Output soundfile name. If not a full pathname, the soundfile will be placed in the directory given by the environment variable SFDIR (if defined), else in the current directory. The name stdout will cause audio to be written to standard output, while null results in no sound output similarly to the -n flag. If no name is given, the default name will be test.

The name devaudio or dac (you can use -odac or -o dac) will request writing sound to the host audio output device. It is possible to select a device number by appending an integer value in the range 0 to 1023, or a device name separated by a : character (e.g. -odac3, -odac:hw:1,1). It depends on the host audio interface whether a device number or a name should be used. In the first case, an out of range number usually results in an error and listing the valid device numbers.

-Q DEVICE
Enables MIDI OUT operations to device id DEVICE. This flag allows parallel MIDI OUT and DAC performance. Unfortunately the real-time timing implemented in Csound is completely managed by DAC buffer sample flow. So MIDI OUT operations can present some time irregularities. These irregularities can be reduced by using a lower value for the -b flag.
If using ALSA MIDI (-+rtmidi=alsa), devices are selected by name and not number. So, you need to use an option like -Q hw:CARD,DEVICE where CARD and DEVICE are the card and device numbers (e.g. -Q hw:1,0). In the case of PortMIDI, CoreMIDI and MME, DEVICE should be a number, and if it is out of range, an error occurs and the valid device numbers are printed.

--realtime
realtime priority mode is switched on which the following effects:
all opcode audio file reading/writing is handled asynchronously by a separate thread.
all init-pass operations are also performed asynchronously.

-+rtaudio=string
(max. length = 20 characters) Real time audio module name. The default is PortAudio. Also available, depending on platform and build options: Linux: alsa, jack; Windows: mme; Mac OS X: CoreAudio. In addition, null can be used on all platforms, to disable the use of any real time audio plugin.

-+rtmidi=string
(max. length = 20 characters) Real time MIDI module name. Defaults to PortMIDI (if present). Other options (depending on platform and build) are Linux: alsa; MacOS: cmidi; Windows: mme, winmm. In addition, null can be used on all platforms, to disable the use of any real time MIDI plugin.
ALSA MIDI devices are selected by name and not number. So, you need to use an option like -M hw:CARD,DEVICE where CARD and DEVICE are the card and device numbers (e.g. -M hw:1,0). PortMIDI allows multiple port inputs with either -Ma or -Mm.
```

# MIDI
```
-+rtmidi=portmidi
-M999
-Ma
# input:2 output: 1
-+rtmidi=portmidi -M2 -Q1
# virtual keyboard
-+rtmidi=virtual -M0
```

# Reference
- csound command line reference:
  https://csound.com/docs/manual/CommandFlags.html
- Getting started:
  https://csound.com/get-started.html
- FLOSS manual:
  https://flossmanual.csound.com/introduction/preface
  - working with controllers: https://flossmanual.csound.com/midi/working-with-controllers
- The Canonical Csound Reference Manual:
  https://csound.com/docs/manual/index.html

# Other useful docs
- using csound in realtime (might be obsolete?):
  http://compmus.ime.usp.br/sbcm/1999/papers/sbcm-paper-1999-13.pdf
