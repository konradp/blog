---
title: "COM: Software synth for PI"
subtitle: 
date: 2023-12-12
tags: [ 'wip' ]
categories: wip
---
{{<toc>}}

## Chosing the synth
Synths to consider:
- yoshimi (zynaddsubfx)
- rtaudio + rtmidi
- bristol
- zyn-fusion (zynaddsubfx)
  - [freesoftwaremusic.wordpress.com: zynaddsubfx without gui](https://freesoftwaremusic.wordpress.com/2015/01/02/using-zynaddsubfx-without-the-gui/)
- fluidsynth

Here we will use Yoshimi synth. For Banana PI set-up, see [bananapi set-up](/post/com-bananapi)

TODO, a way that works without tmux:
```
mkfifo out
tail -f out | yoshimi -C
```
on another terminal
```
echo "list" > out
```


## yoshimi
Install yoshimi.
```
apt install yoshimi
#apt install yoshimi-data
```
Run
```
yoshimi -i
```
giving
```
root@bananapi:~# yoshimi -i
Yoshimi 2.2.3 is starting
[...]
Clientname: yoshimi
Audio: alsa -> 'default'
Midi: alsa -> 'Arturia MiniLab mkII:0'
Oscilsize: 512
Samplerate: 48000
Period size: 256

Yay! We're up and running :-)
Found 3 new banks in root /root/.local/share/yoshimi/found/yoshimi/banks
Found 24 new banks in root /usr/share/yoshimi/banks

Found 900 instruments in 28 banks
Root 5. Bank set to 5 "Arpeggios"
yoshimi> 
```

## Controlling synth with MIDI

### MIDI tools
List MIDI devices/ports to listen to:
- `cat /proc/asound/seq/clients`
- `aconnect -iol`: List Input and Output
- `aseqdump -l`
-aseqdump `amidi -l`: it doesn't list virtual keyboard
  - `amidi -d -p hw:2,0,0` for hex, get port with `amidi -l`: doesn't work, device or resource busy
- `pmidi -l`: Requires install


Dump MIDI messages
```
aseqdump -p 16:0
```

Send MIDI message
```
TODO
# send-hex
amidi -S
```

#### MIDI message examples
MIDI note on:
```
EVENT KEY VELOCITY
144   61  127
```
note off:
```
EVENT KEY VELOCITY
128   61  0
```
Program change: bank 001, preset 122
```
EVENT ?      BANK
176   0      1
EVENT PRESET
192   122
```
Program change: bank 002 preset 123
```
EVENT ?      BANK
176   0      2
EVENT PRESET
192   123
```
Notes:
- 176=bank select, 192=program change
- in hex: bank select='b0', program change='c0', 122=7a, 123=7b
- decimal to hex: `yourNumber.toString(16)`, e.g. `(176).toString(16)`
- hex to decimal: `parseInt(hexString, 16)`

so in hex:
```
# Bank 1
b0 0 1
# Preset 1
c0 1
# Bank 2
b0 0 2
# Preset 2
c0 2
```
Note: No need to change the bank each time, if not specfied, will use current bank

#### MIDI messages refresher
**```STATUS DATA1 DATA2```**

Notes:
- byte is 8 bits, i.e. two hex digits (0-255, 00-FF)
- MIDI message consists of a status byte followed by one or two data bytes:
  - message format:  
    **```STATUS DATA1 DATA2```**
  - example (decimal): 00 00 00
  - example (decimal): 128 40 127
- There are 16 MIDI channels, so counting in hex helps
- ranges:
  - STATUS: MSB is always set to binary 1, so available range is:
    - binary: 1000 0000 - 1111 1111
    - decimal: 128 - 255
    - hex: 80 - FF
  - DATA: MSB is always set to binary 0, so available range is:
    - binary: 0000 0000 - 0111 1111
    - decimal: 0 - 127
    - hex: 00 - 7F
- the status byte will indicate the channel, e.g.
  - channel 1, note 48, note on: STATUS=144
  - channel 2, note 48, note on: STATUS=145
  - ...
  - channel 16, note 48, note on: STATUS=159
  - hence, note on STATUS is 144 to 159
  - hence, note off STATUS is 128-143
- middle C is 60d

STATUS byte values for different types of messages:

dec       | hex     | abbr. hex | msg type
---       | ---     | ---       | ---
128 - 143 | 80 - 8F | 8n        | note off
144 - 159 | 90 - 9F | 9n        | note on
160 - 175 | A0 - AF | An        | poly key pressure (aftertouch)
176 - 191 | B0 - BF | Bn        | control change (CC)
192 - 207 | C0 - CF | Cn        | program change
208 - 223 | D0 - DF | Dn        | channel pressure
224 - 239 | E0 - EF | En        | pitch bend

**Control Change messages**  
**```STATUS CN CV```**  
Notes:
- message format:  
  **```STATUS CN CV```**  
  where:
  - CN: CONTROLLER_NUMBER
  - CV: CONTROL_VALUE
- 120 controller numbers: 0-119

controller number assignment per range

dec       | hex     | bin                   | purpose
---       | ---     | ---                   | ---
0-31      | 00 - 1F | 0000 0000 - 1000 0000 | MSB for most continuous controller data
32-63     | 20 - 3F | 0010 0000 - 0011 1111 | LSB for controllers 0 - 31
64 - 95   | 40 - 5F | 0100 0000 - 0101 1111 | single-byte controllers, e.g. attack, release and others
96 - 101  | 60 - 65 | 0110 0000 - 0110 0101 | increment/decrement and parameter numbers
102 - 119 | 66 - 77 | 0110 0110 - 0111 0111 | undefined single-byte controllers

Example: The MIDI message `176 1 70` is **control change (CC)** message for **controller 1**, which is the expression slide on my MIDI keyboard, and **70 is the value** (roughly in the middle between 0-127).

Notes:
- controllers 0-31 are for pedals, levers, wheels, etc
- controllers 32-63 are optional for use as LSB for controllers 0-31

**MSB and LSB: Fine-grained controls**  
Now, this is something that was very confusing to me before - all this LSB and MSB stuff. Yoshimi manual doesn't explain it (as it's actually part of the MIDI spec), but the MIDI 1.0 detailed specification explains it well (in references, page 12). And here it is, learned from the MIDI spec, but in my own words...

Consider the controller 7 (volume). It has 128 available steps/values (0-127). If this is not enough, and we need more **fine-grained control** (more possible values), then we can use these LSB controllers. For example, for controller 0 (MSB), the corresponding LSB controller is the controller 32; for controller 1, the corresponding LSB controller is the controller 33, and so on. In this example, for controller 7, the corresponding LSB is the controller 7+32 = 39 (39th controller). This gives us the resolution (available steps/values) of `128*128 = 16384`, instead of 128, giving us a more **fine-grained range**.

**Example**
How I think this works on an example, for MSB/LSB controller pair, 7 and 39.
```
176 7  70
176 39 20
```
Suppose that you send `176 7 70`, a CC message for **controller 7**, setting its value to 70. Then, if you follow it with a message `176 39 20`, (a CC message for **controller 39**, which is the LSB which corresponds to controller 7), this will set the value of the controller 7 to **not** the original value of 70, but to **70 + 20/128 = 70.16**, which is between 70 and 71.

Another thing that the MIDI spec explains, is that we don't need to send the addiotional LSB if 128 steps of resolution are sufficient for our needs, so this is fine:
```
176 7  70
176 7  60
```
And also, once we've sent the MSB, and then fine-grained it with the LSB, we can follow it with more LSB messages for further adjustments (and we don't need to send the MSB again). For example, this is valid:
```
176 7  70
176 39 20
176 39 30
```
In other words, we don't need to send that `176 7i ...` message twice for each fine-grained adjustment.


**RPN/NRPN**  
These are used e.g. for pitch bend sensitivity, master tuning (fine tuning, coarse tuning), etc. These are special types of CC messages, and they take two MIDI messages also.

controller numbers used:
- 6: data entry
- 96: data increment
- 97: data decrement
- 98: NRPN LSB
- 99 NRPN MSB
- 100: RPN LSB
- 101: RPN MSB

Usage:
- first select parameter number to be edited (controllers 98, 99, 100, 101)
- then, send/adjust the data for that parameter (controllers 6, 96, 97)

**channel mode messages**  
controller numbers 121 - 127, ignore

We also ignore sysex messages

## yoshimi: nrpn
Now, the below should make sense to me (from yoshimi advanced reference manual).
<center>
  <img src="/media/com-synth-sw/yoshimi-nrpn.png" style="border:1px solid"></img>
</center>

Now, the DATA MSB and DATA LSB are not NRPN MSB and NRPN LSB, they have their own CC numbers (see globals.h in yoshimi source):
- DATA MSB: controller 6  (0x6)
- DATA LSB: controller 38 (0x26)

**TODO:** What is the 8130 here?

So in MIDI messages, this will be:
```
# TODO: This is wrong
# Set NRPN MSB (CC 99) to 64
176 99 64
# Set NRPN LSB (CC 98) to 2 (8130)
176 98 2
# After that, send the DATA values
# Set DATA MSB (CC 6) to 7: master volume
176 6 7
# Set DATA LSB (CC 38) to 10: master volume = 10
176 38 10
```
or in hex
```
# Enable reading values
b0 63 40
b0 62 2
# Set master volume?
b0 6 7
b0 26 10
```

```
TODO: UPDATE THIS, it works now
which looks like this in midisnoop:
<center>
  <img src="/media/com-synth-sw/yoshimi-nrpn-example1.png" style="border:1px solid"></img>
</center>

With these settings in yoshimi
<center>
  <img src="/media/com-synth-sw/yoshimi-nrpn-example2.png" style="border:1px solid"></img>
</center>
I see in the terminal in which I opened yoshimi:
<center>
  <img src="/media/com-synth-sw/yoshimi-nrpn-example3.png" style="border:1px solid"></img>
</center>
...but somehow the master volume did not change. And how to actually print the values? Nothing gets displayed to the terminal.
```

How about the tidy close, from the manual:
<center>
  <img src="/media/com-synth-sw/yoshimi-nrpn-example4.png" style="border:1px solid"></img>
</center>

with messages
```
b0 63 44
b0 62 44
```
That worked!

Also here: https://www.freelists.org/post/yoshimi/New-idea-partly-done we have:
```
 DATA
MSB

100 LSB > 63 Send reports to Reports window, otherwise to stderr.

109 LSB x List the following:
Reports destination
Current Root path
Current Bank
CC to control Root path change
CC to control Bank change
Program change enabled/disabled
Set part active when program changed enabled/disabled
CC to control Extended Program change (instruments 127 to 159)
Number of active parts

110 LSB x List all root paths
111 LSB path List all banks in Root ID {path}

118 LSB parts Set number of active parts {parts = 16, 32 or 64}
119 LSB x Save all dynamic settings
```
the key is that the output is sent to stderr or to the Reports window. For me, that's stdout
cd



## Controlling the synth with tmux
### Setup
There seems to be no API exposed (e.g. IPC, UDS sockets or else) for yoshimi. So how do I drive it remotely? One way, described here, is to use **`tmux`** and its **`send-keys`** to send commands to a running yoshimi session/instance.

Ideas:
- {{<v>}} Run the process inside tmux or screen:
  - [serverfault.com: the other answer: screen and tmux](https://serverfault.com/questions/178457/can-i-send-some-text-to-the-stdin-of-an-active-process-running-in-a-screen-sessi)
  - [wiki: tmux](https://en.wikipedia.org/wiki/Tmux)
  - [stackoverflow: screen](https://stackoverflow.com/questions/54943240/run-a-command-from-a-script-directly-to-another-process)
  - [theterminallife.com: screen](https://theterminallife.com/sending-commands-into-a-screen-session/)
  - [unix.stackexchange.com: screen and tmux](https://unix.stackexchange.com/questions/13953/sending-text-input-to-a-detached-screen/13954#13954)
  - TLDR: use tmux instead of screen, as it's more modern
- {{<w>}} How does the gui do it?
- {{<x>}} Use named pipe as per [linuxquestions.org](https://www.linuxquestions.org/questions/linux-newbie-8/%5Bbash%5D-send-command-to-another-terminal-831313/)  
  more info: [linuxournal.com](https://www.linuxjournal.com/content/using-named-pipes-fifos-bash)
- {{<x>}} Write to /proc/PID/fd0: [serverfault.com](https://serverfault.com/questions/178457/can-i-send-some-text-to-the-stdin-of-an-active-process-running-in-a-screen-sessi)  
  doesn't seem to work, sends text, but not ENTER

To send commands to yoshimi, run the process inside tmux. Adapted from:
- [wiki: tmux](https://en.wikipedia.org/wiki/Tmux)
- [serverfault.com](https://serverfault.com/questions/178457/can-i-send-some-text-to-the-stdin-of-an-active-process-running-in-a-screen-sessi)
- [unix.stackexchange.com](https://unix.stackexchange.com/questions/13953/sending-text-input-to-a-detached-screen/13954#13954)

Start yoshimi in `tmux` session.
```
tmux new-session -s yosh yoshimi -i
```
**Note:** The `-s` sets the session name.  

Then, send commands from another terminal:
```
tmux send-keys -t yosh "list" Enter
```

The above doesn't receive the output. So let's tee the output of yoshimi to a file, clear it, then send command, then read the file.  

Again, run `yoshimi`, but this time `tee` the output to a file.
```
tmux new -n yosh 'yoshimi -i |&tee yosh_out'
```
**Note:**
- the `tee` is part of the command passed to `tmux`, not outside of it
- the `|&tee` instead of `|tee` gives us both `stdout` and `stderr`:, and so yoshimi will print the current cli context level after each command (e.g. `@ Top`)
- `tmux new` is shorthand for `tmux new-session`.

Then, on another terminal, clear the file, pass a command to yoshimi, and read the output.
```
echo -n > yosh_out
tmux send-keys -t yosh "list" Enter
cat yosh_out
```
and so we get
```
konrad@bananapi:~$ echo -n > yosh_out 
konrad@bananapi:~$ tmux send-keys -t yosh "list" Enter
konrad@bananapi:~$ cat yosh_out; echo
list
Lists:
  Roots                       all available root paths
  Banks [n]                   banks in root ID or current
  Instruments [n]             instruments in bank ID or current
  Group <s1> [s2]             instruments by type grouping ('Location' for extra details)
  Parts [s]                   parts with instruments installed ('More' for extra details)
  Vectors                     settings for all enabled vectors
  Tuning                      microtonal scale tunings
  Keymap                      microtonal scale keyboard map
  Config                      current configuration
  MLearn [s <n>]              midi learned controls ('@' n for full details on one line)
  SECtion [s]                 copy/paste section presets
  History [s]                 recent files (Patchsets, SCales, STates, Vectors, MLearn)
  Effects [s]                 effect types ('all' include preset numbers and names)
  PREsets                     all the presets for the currently selected effect

@ Top
yoshimi> 
```
and this is something we can parse, albeit not easily.

Questions:
- Is it fast enough?  
  Once we send the command, is the output immediately available, or do we need to wait?

### Demo
https://gitlab.com/konradp/snippets/-/tree/master/js/music/yoshimi-ui

Clone
```
sudo apt install nodejs npm yoshimi
git clone https://gitlab.com/konradp/snippets.git
cd snippets/js/music/yoshimi-ui
npm install
npm start
```
Run yoshimi
```
#tmux new -d -s yosh yoshimi -i
tmux new -d -s yosh 'yoshimi -i |&tee yosh.out'
```
**Note:** The `-d` option avoids attaching to the current terminal, and runs the process in the background.

## UI
### idea: web server
For this, ideas, we'll need:
- use node+express (instead of my usual Python Flask)
  - [blog: it's already in Express](https://www.scaler.com/topics/expressjs-tutorial/express-websocket/)
- use websockets
- run shell programs, e.g. tmux, or is there API, node bindings?
  - ~~python:~~
    - ~~[SO: subprocess vs. libtmux](https://stackoverflow.com/questions/49123717/python-how-to-invoke-tmux-in-background)~~
    - ~~[tmux-python/libtmux](https://github.com/tmux-python/libtmux) (note: libtmux uses `subprocess` itself)~~
  - node:
    - ~~[exec-sh](https://www.npmjs.com/package/exec-sh)~~
    - ~~[SO: `shell.exec` from `shelljs`](https://stackoverflow.com/questions/53943574/how-to-run-tmux-cmds-in-nodejs)~~
    - [SO: child_process.exec](https://stackoverflow.com/questions/12941083/execute-and-get-the-output-of-a-shell-command-in-node-js)
      - docs: [child_process.exec()](https://nodejs.org/docs/latest-v8.x/api/child_process.html#child_process_child_process_exec_command_options_callback)

### idea: TUI app
- a python TUI app
- urwid
- mido for MIDI



## Other notes
### Ideas that didn't work
#### direct access
```
A point to bear in mind is that eventually there will be no direct access to any synth controls.
It will all go via the ring buffers, but it should be possible to get current values from FLTK's
valuators as these should always be updated.
It's not an issue yet, as gui read and write aren't fully implemented. I can't instigate read
from the gui to the gui until I have made writes actually write!
```
ref: [github/yoshimi/pull/11](https://github.com/Yoshimi/yoshimi/pull/11#issuecomment-264514608)

#### eval
This doesn't work
```
mkfifo yoshpipe
eval $(cat yoshpipe)
```
Then on another terminal
```
echo 'yoshimi -i' > yoshpipe
```
ref: [linuxquestions.com](https://www.linuxquestions.org/questions/linux-newbie-8/%5Bbash%5D-send-command-to-another-terminal-831313/)

## Reference
- yoshimi
  - [yoshimi: help.html](http://yoshimi.github.io/help.html): see advanced reference for command-line interface
  - [yoshimi.sourceforge: user guide](https://yoshimi.sourceforge.io/docs/user-guide/yoshimi_cli.html)
  - [blog: adriangin/raspberry-yoshimi](https://adriangin.wordpress.com/2014/08/04/raspberry-pi-synth-yoshimi-zynaddsubfx-part-4/)
- yoshimi/MIDI: 
  - [dev_notes](https://github.com/Yoshimi/yoshimi/tree/master/dev_notes/Yoshimi%20Control%20Data/Sections): see main.html, bank.htm, midi.html
- MIDI:
  - https://mclarenlabs.com/blog/2018/07/03/linux-midi-cheatsheet/
  - connecting midi ports: https://alsa.opensrc.org/AlsaMidiOverview
  - https://www.midi.org/specifications-old/item/table-3-control-change-messages-data-bytes-2
  - [MIDI 1.0 spec: archive.org](https://archive.org/details/Complete_MIDI_1.0_Detailed_Specification_96-1-3/page/n7/mode/2up)
  - [same as above, direct link](https://ia601801.us.archive.org/5/items/Complete_MIDI_1.0_Detailed_Specification_96-1-3/Complete_MIDI_1.0_Detailed_Specification_96-1-3.pdf)

