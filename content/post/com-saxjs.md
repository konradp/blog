---
title: 'COM: saxjs'
date: 2023-01-26
tags: [ 'music' ]
categories: music
---
WORK IN PROGRESS:<br>Project diary for the [saxjs](https://konradp.gitlab.io/saxjs), a saxophone fingering chart app. ([source](https://gitlab.com/konradp/saxjs)).

{{<toc>}}

## Intro
Recently I started learning saxophone and I would like a javascript app which draws the sax fingering chart showing all notes, specific scales, alternative fingerings etc. This blog post is my project diary for it, documenting how I overcome challenges, solve problems, and summarising the new things I learned as part of the process.

### Saxophone keys: understand and draw by hand
First of all, let's understand the saxophone keys. A great resource for this is the page below:

- https://www.wfg.woodwind.org/sax/sax_fing.html

I also took a few photos of my own saxophone.

<center>
  <img src='/media/com-saxjs/sax1.jpg' style='width:5%'/>
  <img src='/media/com-saxjs/sax2.jpg' style='width:10%'/>
  <img src='/media/com-saxjs/sax3.jpg' style='width:10%'/>
  <img src='/media/com-saxjs/sax4.jpg' style='width:5%'/><br>
</center>

Then, from these, I drew an approximation in paint.  
**Note:** keys played with left hand above the blue line, and right hand below the blue line.

In order to represent the various fingerings as data (e.g. array of objects), and ultimately draw them with CanvasJS, let's group/organise the keys (I follow here the reference above):

<table>
<tr>
<td><img src='/media/com-saxjs/fingers.png' style='width:100%'/></td>
<td>

- **LH:**
    - octave key (top left, played with a thumb)
    - palm keys: Eb, D, F from top, are the ones shaped like a teardrop
    - main keys:
        - 1, 2, 3 keys (B, A/C, G)
        - extras: front F key (above 1), Bb key (between 1 and 2)
    - pinky keys: C#, G#, Bb, B (circle: top, left, right, bottom)
- **RH:**
    - main keys:
        - 1, 2, 3 keys (F, E, D)
        - F# alternate (between 2 and 3)
    - side keys:
        - E, C, Bb (from top)
        - f# (high F#, between 1 and 2)
    - pinky keys: Eb, C (from the top, bottom-left circle)
</td>
</tr>
</table>


### Initialise project: node, express, canvas, CICD
I first initialised a new project dir structure as per my cheatsheets for [node](http://konradp.gitlab.io/blog/post/com-node-cheatsheet/) and [javascript/canvas](http://konradp.gitlab.io/blog/post/com-js-cheatsheet/). Also, added a basic `README.md`, a `.gitlab-ci.yml` for CI/CD and hosting on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/). With these, after an `npm start`, I see the below in my browser, and the app is available at https://konradp.gitlab.io/saxjs.

<center>
<img src='/media/com-saxjs/app1.png' style='width:40%'/>
</center>

So let's get drawing!


### Plan: milestones, functionality
I like to write down **milestones** as I work through a project, to help me stay focused and motivated towards the next 'deliverable'. I constantly get creative ideas as I work, and it's sometimes easy to get carried away. With the milestones in sight, I can filter these ideas with a question:

<blockquote>
Does it get me closer to reaching the next milestone or should I, for now, write down this idea for one of the next stages?
</blockquote>

**Milestones**

- **1: draw sax, unpressed pads**
    - draw special key shapes (e.g. the pinky key)
    - draw all key groups: LH and RH
    - draw circle
- **2: draw pressed/unpressed pads**  
- **3: draw simplified diagram**
- **4: interactive**  
  A single diagram with some interactive features
    - onmouseenter: draw key names and descriptions
    - onmouseclick: close or open pads on mouse click
    - multiple fingerings  
      One note can sometimes be played in different ways.  
      How to address?
- **5: cards**  
    - draw multiple cards at once,  
      e.g. all the notes or the Cmaj scale side by side
- **6: scales**  
  Different scales
    - text: note name (a label on canvas?)
- **10: FINAL APP**
    - homepage: fingering chart + scales
    - homepage: fix links for prod, relative links?
    - extra: homepage: draw a favicon  
      ```
      GET http://localhost:8085/favicon.ico 404 (Not Found)
      ```
    - side page: sax-paint
    - tidy up blog and examples in the blog
    - extra: pentatonic
    - extra: highlight I, II, III notes etc (colours)
    - extra: clean up code, e.g. function naming and order
    - extra: bug: fix realistic diagram
    - extra: bug: mouse actions don't work if canvas are shrinked with e.g. 'width:50%'
    - extra: colours
    - extra: add tests
    - extra: better frontF key in realistic diagram
    - extra: better pinky keys in realistic diagram
    - extra: sheet music
    - extra: concert pitch note name
    - extra: mirror vertically
    - extra: synth: play notes as we click on them
    - extra: API documentation + examples
    - extra: add to CDNJS
    - extra: is this truly the sax range? or just basic range?
    - extra: swicher is hard to operate (too small) when cards are small
    - extra: transparent background for youtube video overlays


## Milestone 1: Draw unpressed keys
I start hands-on by drawing the saxophone pads using Canvas API. At the same time, I work on the 'Planning' section above. The outcome of this milestone (screenshot below), is a Canvas drawing with coordinate inputs which let me easily tweak the key coordinates live on-screen. It achieves the following:

- programmatically draw key contours with canvas
- work out coordinates for the keys
- get more familiar with the keys

<center>
  <img src='/media/com-saxjs/test1.png' style='width:40%'/>
</center>

Starting with circles, we have the 1,2,3 pads for both hands, which are the same size, and also the slightly smaller Bb key between 1 and 2. All three of the LH palm keys (teardrops) use the same shape. The side keys (E, C, Bb) are identical rectangles on top of each other. The octave key is an oval. The remaining special-shape keys are the front F key (triangle, to keep it simple but still inspired by my own sax); the pinky keys for LH and pinky keys for RH, and then high F# and F# alternate key.

The playbook will be as follows, e.g. for the round key:

- if key is pressed, draw shape and do **`fill()`**
- draw shape and do **`stroke()`**

This way, we can draw unpressed and pressed keys, and it will make it easier to extend the app with colours also (but for this milestone we only need to do the **`stroke()`**).


## Milestone 2: Draw pressed keys
For this milestone, we draw both the unpressed and pressed keys. The outcome is a diagram which presses (fills) each key one by one in a loop.

<center>
  <canvas id='example1-pressing-keys' style='border:1px solid;width:10%'></canvas>
</center>

The milestone achieves:

- draw unpressed and pressed keys: `strokeRect()` vs `fillRect()`
- divide canvas horizontally and vertically into 100 'units' to avoid using pixel coordinates for keys
- add `SetFingering()` method which accepts an array of booleans for each key (pressed or depressed): More on this later


## Milestone 3: Simplified diagram

There is a much more compact diagram used by fingering charts and youtube videoswhich I'd like to use.

<center>
  <img src="/media/com-saxjs/simplified1.png"/>
  <img src="/media/com-saxjs/simplified2.png"/>
  <img src="/media/com-saxjs/simplified3.png"/>
  <img src="/media/com-saxjs/simplified4.png"/>

  (source: [ref2](https://www.sgasd.org/cms/lib/PA01001732/Centricity/Domain/255/Alto%20Saxophone%20Fingering%20Chart.pdf))
</center>


The outcome for this milestone is to re-do the previous milestone, but using this simplified diagram. Note also how the side keys are not displayed unless they are pressed.

<center>
  <canvas id='example2-simplified-diagram' style='border:1px solid;width:10%'></canvas>
</center>


## Milestone 4: Interactive
The outcome of this milestone is to make the diagram interactive with mouseover and mousedown events, as well as providing a way to display multiple fingerings on a single card/diagram.

- example1: press on keys to press/depress
- example2: hover on the keys to update this (<span id='name'></span>)
- example3: press the left/right arrows (bottom right) to show different fingerings for A# note

<center>
  <canvas id='example3-interactive-press' style='border:1px solid'></canvas>
  <canvas id='example4-interactive-hover' style='border:1px solid'></canvas>
  <canvas id='example5-interactive-picker' style='border:1px solid'></canvas>
</center>


Recalling [my other blog post](https://konradp.gitlab.io/blog/post/com-js-canvas-move/), to highlight the pads on mouseover, I use the canvas `context.isPointInPath(path, x,y)`. I also store the individual pad paths using the `Path2D object`.

Also, the simplified diagram does not draw the palm keys or the side keys unless they are used. I add a setting which will force drawing all pads even if they're not pressed.
{{<highlight js>}}
const sax = new Sax('canvas', {
  isSimplified: true,
  isStrokeAll: true,
});
{{</highlight>}}


### Mouse callbacks, this vs. that
As the `Sax` class is meant to be only for displaying the saxophone diagram, I will give it an `addCanvasEventListener()` function, and it will be up to the user to decide what happens when the mouse is over individual paths/pads.

{{<highlight js>}}
sax.addCanvasEventListener('mousemove', (e) => {
  console.log(this);
});
{{</highlight>}}

In order for the 'this' to refer to the `Sax` class instance here, we use the `bind()` (see [MDN reference](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener#other_notes)).

{{<highlight js>}}
addCanvasEventListener(type, func) {
  this.canvas.addEventListener(type, func.bind(this));
}
{{</highlight>}}


### Data storage
As this project grew with each milestone, I now reached a point where it might be worth reconsidering how the various data points are stored. At first I only needed to store the coordinates for the pads:
{{<highlight js>}}
this.key_coordinates = {
  LH: {
    octave: { x: 14, y: 22 },
    frontF: { x: 40, y: 2 },
    [...]
{{</highlight>}}

which then introduced another variable when I added the simplified diagram, another one to store the references to Canvas Path2D objects, and yet another one for storing the fingerings (`this.fingering`).

I merge most of these (except for the 'coordinates' object) into a single **`this.state`** object:

{{<highlight js>}}
this.state = [
  {
    id: 0,
    name: 'octave',
    longName: 'octave',
    path: null, // the canvas path
    isDown: false,
  },
  { name: 'frontF', longName: 'F front' },
  { name: 'lh1', longName: 'B first' },
  { name: 'lhBb', longName: 'Bb' },
];
{{</highlight>}}

which should also make it easier to create various getting and setting methods, such as:

- pressPad: by id or name
- depressPad
- getPadById
- pressAllPads
- depressAllPads


### Multiple fingerings
Certain notes can have multiple fingerings available, for example there are three ways to play the A# note:

<center>
  <img src="/media/com-saxjs/multiple.png"/>

  ([image source: ref2](https://www.sgasd.org/cms/lib/PA01001732/Centricity/Domain/255/Alto%20Saxophone%20Fingering%20Chart.pdf))
</center>

For this, we add a picker widget in bottom right of the diagram:

<center>
  <img src="/media/com-saxjs/picker.png"/>
</center>

The picker will appear when more than one fingering has been supplied with `sax.SetFingering([[...], [...]])`, represented by an array of arrays.

We hook into the `SetFingering()` and first check if the supplied fingering is an array of arrays (multiple fingerings) or not (single fingering). For this, I was going to check just the first element, but I saw a solution on [StackOverflow](https://stackoverflow.com/questions/25189937/detect-array-of-arrays) which uses the `.every()` to check them all.

{{<highlight js>}}
let isMultipleFingering = arr => {
  return arr.every(e => {
    return Array.isArray(e);
  });
};
if (isMultipleFingering(fingering)) {
  // multiple fingering
}
{{</highlight>}}

or, writing more compactly (see [MDN reference](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/every)):

{{<highlight js>}}
if (fingering.every((e) => Array.isArray(e))) {
  // multiple fingerings
}
{{</highlight>}}


## Milestone 5: Multiple cards
In this milestone we finally draw multiple notes/cards on a single page, e.g. a chromatic scale, or a Cmaj scale. First, I input all the fingerings into a datafile (0=false/depressed, 1=true/pressed):
{{<highlight js>}}
const SAX_NOTES = [
  [ // A#
    0,0,    1,0,1,1,  0,0,0,  0,0,0,1,
    1,1,1,  0,0,0,    0,0,    0,1,
  ],
  [ // B
    0,0,    1,0,1,1,  0,0,0,  0,1,0,0,
    1,1,1,  0,0,0,    0,0,    0,1,
  ],
  [ // C
    0,0,    1,0,1,1,  0,0,0,  0,0,0,0,
    1,1,1,  0,0,0,    0,0,    0,1,
  ],
  [...]
{{</highlight>}}

and then, inside a 'for' loop, we can supply these to `sax.setFingering()`, to get the below (only the first four notes are shown here).

<center>
<div id='example6-multiple-cards'></div>
</center>

Here I noticed a bug where the mouse events are not working correctly if the cards are scaled with CSS, e.g. `width: 50%`. I add this to my notes as something to be addressed later.


## Milestone 6: Scales
Here we again draw multiple cards, but this time we include a drop-down for various notes (C, C#, D, D#, E, ...). Once we pick a note from the drop-down, only the notes from the corresponding major scale are be shown.

Side notes on scales (see [wiki](https://en.wikipedia.org/wiki/Major_scale)): The major scale can be built from the root note by a series of intervals (whole steps=2 and half steps=1). Similarly the minor scale:

- major: 2, 2, 1, 2, 2, 2, 1
- minor: 2, 1, 2, 2, 1, 2, 2

**Note:** We mean the 'natural minor' scale here.

Also, there are 32 notes available on the saxophone:

- A#, B,
- C, C#, D, D#, E, F, F#, G, G#, A, A#, B,
- C, C#, D, D#, E, F, F#, G, G#, A, A#, B,
- C, C#, D, D#, E, F

Using the above maj/min intervals we construct major and minor scales from the chosen key note, and we get the below.

<center>
<div style='border:1px solid'>
<strong>Instructions: Pick the scale root note and the scale type</strong>
<div id='example6-scales-controls'></div>
<div id='example6-scales-canvas'></div>
</div>
</center>

This milestone achieves:

- calculate various major and minor scales
- add labels to cards
- fix the picker scaling bug  
  Now, we can scale the cards with CSS, and we can still press the arrows on the fingering picker to show alternative fingerings


### Bugfix: scaling for alternative fingerings
TODO: Finish this section.

I looked at:

- https://webglfundamentals.org/webgl/lessons/webgl-anti-patterns.html
- https://stackoverflow.com/questions/45852075/getting-relative-mouse-position-on-canvas-after-scaling

I had:
```
var rect = this.canvas.getBoundingClientRect();
const mouseX = e.clientX - rect.left;
const mouseY = e.clientY - rect.top;
```

fixed with:

```
var rect = this.canvas.getBoundingClientRect();
const mouseX = (e.clientX - rect.left)*(canvas.width/canvas.clientWidth);
const mouseY = (e.clientY - rect.top)*(canvas.height/canvas.clientHeight);
```

## Final version
The above milestone is already fairly close to what I picture as the final outcome. At this stage, I review the plan again, especially the extra tasks and ideas which I noted down while I was working on the earlier milestones. From these, I isolate the 'must-haves'.

Must have:

- homepage:
    - maj/min scales
    - break line before the scale root note
    - settings:
        - settings overlay window
        - show/hide notes from outside scale
        - adjustable card size
    - fix links for prod old_examples, relative links?
- side page: sax-paint
- tidy up blog and examples in the blog
- write a README.md

The remaining tasks will be added to the issues board for the project, to be addressed later. See the 'Followups' at the end of this article for a list.

### Homepage: CSS, navbar menu

First of all, we work on the navigation menu and add some CSS. Currently, it looks a bit boring without any CSS:

<center>
  <img src='/media/com-saxjs/final1.png' style='border:1px solid'></img>
</center>

We're going to aim for a black-gray look, similar to my [zhtools app](https://konradp.gitlab.io/zhtools/):

<center>
  <img src='/media/com-saxjs/zhtools.png' style='border:1px solid'></img>
</center>

And so after some CSS tweaks, the app looks like below.

<center>
  <img src='/media/com-saxjs/final2.png' style='border:1px solid'></img>
</center>
Settings icon: https://iconduck.com/sets/genericons-neue

Things to be adjusted later:

- remove purple bar or choose a different colour
- decide how to handle the settings: which settings should be accessible via the cogwheel button, and which should be on the page front

### Homepage, main app: align cards: flexbox?
There is one thing which bothers me, so I'd like to fix it now. Each new octave starts on a new line, and so it can happen that there is one or more cards on the first or the last line, but they do not form a full octave (see the screenshot below). For example, the lowest note of the Cmaj scale playable on saxophone is the B (I haven't checked this, there might be yet lower notes available), and this means that the B note appears as a single card on the first line.

Now, because the cards live inside a center-ed div, the B card is positioned in the center, but it'd be nicer if it was on the right, above the next octave's B note. Similarly, the last line should be aligned to the left.


<center>
  <img src='/media/com-saxjs/final3.png' style='border:1px solid'></img>
</center>
I think the flexbox layout will have some trick to make this happen. If so, the plan is to make the first line align to the right, and all subsequent lines to align to the left.

I tried flexbox briefly, and the newlines (`<br>`) stopped working, so I tried [a trick from a blog article](https://tobiasahlin.com/blog/flexbox-break-to-new-row/), which helped, but then aligning individual rows wasn't working as I expected.

Another idea would be to insert fake/invisible divs on the left when required, which I think will work. At the same time, I remembered also about the grid layout, and after reading [an MDN article](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Relationship_of_Grid_Layout) describing the difference between grid and flexbox, it looks like grid layout is what I need and so I read about it on [MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Basic_Concepts_of_Grid_Layout).


## Conclusion
- app is available at https://konradp.gitlab.io/saxjs
- I learned new stuff
- sax playing recording here


### What I learned

- canvas Bezier curves
- canvas **`toArc()`**
- TODO: Add the CSS to cheatsheet examples

### Follow-up

TODO: Add these to the GitLab project issue board:

- extra: clean up code, e.g. function naming and order
- extra: synth: play notes as we click on them
- extra: mirror vertically
- extra: API documentation + examples
- extra: is this truly the sax range? or just basic range?
- extra: transparent background for youtube video overlays
- extra: swicher is hard to operate (too small) when cards are small
- extra: homepage: draw a favicon  
  ```
  GET http://localhost:8085/favicon.ico 404 (Not Found)
  ```
- extra: pentatonic
- extra: highlight I, II, III notes etc (colours)
- extra: bug: fix realistic diagram
- extra: colours
- extra: add tests
- extra: better frontF key in realistic diagram  
  <img src="/media/com-saxjs/frontF.png" style="width:40%"></img>
- extra: better pinky keys in realistic diagram
- extra: sheet music
- extra: concert pitch note name
- extra: add to CDNJS


## References

- [ref1: wfg.woodwind.org](https://www.wfg.woodwind.org/sax/sax_fing.html)
- [ref2: wikipedia.org](https://en.wikipedia.org/wiki/Alto_saxophone)
- [ref2: sgasd.org](https://www.sgasd.org/cms/lib/PA01001732/Centricity/Domain/255/Alto%20Saxophone%20Fingering%20Chart.pdf)  
  WARNING: I don't think this is a primary source.  
  TODO: reverse-search this file
- [ref3: yamaha.com](https://www.yamaha.com/en/musical_instrument_guide/saxophone/play/play002.html)
- [MDN: TODO](?)


<!-- SCRIPTS -->
<script src='/media/com-saxjs/sax.js'></script>
<script src='/media/com-saxjs/example1-pressing-keys.js'></script>
<script src='/media/com-saxjs/example2-simplified-diagram.js'></script>
<script src='/media/com-saxjs/sax_notes.js'></script>
<script src='/media/com-saxjs/examples.js'></script>
<script src='/media/com-saxjs/example6-scales.js'></script>
