---
title: "COM: Contributing to an open source project"
subtitle: 
date: 2023-03-21
tags: [ "wip" ]
categories: wip
---

As part of my 2023 goals, I'd like to contribute to at least one open source project. This article documents my journey.

**TABLE OF CONTENTS**
{{<toc>}}


## Plan
### Types of contribution
I can think of three types of contributions:

- bug fixes
- features
- documentation
- testing?

TODO: Write thoughts about each. Think about whether it's a project I'm familiar with, or something I'm completely new to.

### Type of projects
Type of projects:

- something that I am very familiar with already
- a project/tool which I haven't used much

TODO: Note: Both have pros and cons. Contributing might be a great way to get familiar with a new tool, or to improve a tool that I already know and care about. Mention that it might be good for a CV also.

### How to get started
- Read contribution guides
- Check out github issues
- Check out PRs and discussions


### Candidate projects
- ansible
- calf:
    - https://github.com/calf-studio-gear/calf/pulls
- clementine
    - fix spotify integration
- gitlab
    - https://gitlab.com/gitlab-org/gitlab/-/issues/357819
- haiku
- hugo
- puppet
- terraform
- vim

#### git stats
project     | git             | stars | forks | PRs | contributors | issues | last_commit
---         | ---             | ---   | ---   | --- | ---          | ---    | ---
[ansible]   | [ansible-git]   | 56.9k | 23.2k | 334 | 5k+          | 648    | 20230401
[calf]      | [calf-git]      | 584   | 88    | 6   | 45           | 124    | 20211112
[clementine]| [clementine-git]| 3.4k  | 663   | 41  | 200          | 2.3k   | 20230210
[gitlab]    | [gitlab-git]    | 4.3k  | 8.1k  | 1.5k| ?            | 52k    | 20230403
[haiku]     | [haiku-git]     | ?     | ?     | ?   | 9+           | ?      | 20230403
[hugo]      | [hugo-git]      | 66.3k | 7.1k  | 70  | 760          | 653    | 20230330
[puppet]    | [puppet-git]    | 6.9k  | 2.3k  | 32  | 575          | ?      | 20230329
[terraform] | [tf-git]        | 36.8k | 8.5k  | 165 | 1.7k         | 1.6k   | 20230330
[vim]       | [vim-git]       | 29.9k | 4.7k  | 172 | 181          | 1.3k   | 20230403


## Terraform
I've picked Terraform for this study because it's something that comes up very often in DevOps job specs, and I haven't used it much yet. I've read the 'Getting started' guide to get some feel for how the tool works. Now I'll look into the contributor guides.

Reading the main repo README, the main repo (https://github.com/hashicorp/terraform) contains only TF core, which includes the CLI and the main graph engine. The providers are elsewhere, on the TF registry: https://registry.terraform.io/ (see also [the plugin development guide](https://developer.hashicorp.com/terraform/plugin)).

The README provides three resources for contributing:

- [Contributing guide](https://github.com/hashicorp/terraform/blob/main/.github/CONTRIBUTING.md)
- [Bug triage guide](https://github.com/hashicorp/terraform/blob/main/BUGPROCESS.md)
- [TF Documentation README](https://github.com/hashicorp/terraform/blob/main/website/README.md)

and also

- https://www.hashicorp.com/community-guidelines
- https://github.com/hashicorp/terraform/contribute for good first issues (only one)

Following the guide, I notice that the size of the source is quite big:

```
$ du -sh terraform
332M    terraform
```
and the `go install .` gets a lot of libraries:
```
$ du -sh /home/konrad/go/pkg
1.7G    /home/konrad/go/pkg
```

### Picking tasks
As per the 'Bug triage guide', the TF issues are in either of these states:

- [Newly created issues](https://github.com/hashicorp/terraform/issues?q=is%3Aopen+label%3Anew+label%3Abug+-label%3Abackend%2Fk8s+-label%3Abackend%2Foss+-label%3Abackend%2Fazure+-label%3Abackend%2Fs3+-label%3Abackend%2Fgcs+-label%3Abackend%2Fconsul+-label%3Abackend%2Fartifactory+-label%3Aterraform-cloud+-label%3Abackend%2Fremote+-label%3Abackend%2Fswift+-label%3Abackend%2Fpg+-label%3Abackend%2Ftencent++-label%3Abackend%2Fmanta++-label%3Abackend%2Fatlas++-label%3Abackend%2Fetcdv3++-label%3Abackend%2Fetcdv2+-label%3Aconfirmed+-label%3A%22pending+project%22+-label%3A%22waiting+for+reproduction%22+-label%3A%22waiting-response%22+-label%3Aexplained+) (55)
- [Unreproduced issues](https://github.com/hashicorp/terraform/issues?q=is%3Aopen+label%3Abug+created%3A%3E2020-05-01+-label%3Abackend%2Fk8s+-label%3Aprovisioner%2Fsalt-masterless+-label%3Adocumentation+-label%3Aprovider%2Fazuredevops+-label%3Abackend%2Foss+-label%3Abackend%2Fazure+-label%3Abackend%2Fs3+-label%3Abackend%2Fgcs+-label%3Abackend%2Fconsul+-label%3Abackend%2Fartifactory+-label%3Aterraform-cloud+-label%3Abackend%2Fremote+-label%3Abackend%2Fswift+-label%3Abackend%2Fpg+-label%3Abackend%2Ftencent+-label%3Abackend%2Fmanta+-label%3Abackend%2Fatlas+-label%3Abackend%2Fetcdv3+-label%3Abackend%2Fetcdv2+-label%3Aconfirmed+-label%3A%22pending+project%22+-label%3Anew+-label%3A%22waiting+for+reproduction%22+-label%3Awaiting-response+-label%3Aexplained+sort%3Acreated-asc+) (20)
- [Confirmed issues](https://github.com/hashicorp/terraform/issues?q=is%3Aopen+label%3Abug+-label%3Aexplained+-label%3Abackend%2Foss+-label%3Abackend%2Fk8s+-label%3Abackend%2Fazure+-label%3Abackend%2Fs3+-label%3Abackend%2Fgcs+-label%3Abackend%2Fconsul+-label%3Abackend%2Fartifactory+-label%3Aterraform-cloud+-label%3Abackend%2Fremote+-label%3Abackend%2Fswift+-label%3Abackend%2Fpg+-label%3Abackend%2Ftencent++-label%3Abackend%2Fmanta++-label%3Abackend%2Fatlas++-label%3Abackend%2Fetcdv3++-label%3Abackend%2Fetcdv2+label%3Aconfirmed+-label%3A%22pending+project%22+) (28)
- [Explained issues](https://github.com/hashicorp/terraform/issues?q=is%3Aopen+label%3Abug+label%3Aexplained+no%3Amilestone+-label%3Abackend%2Fk8s+-label%3Abackend%2Foss+-label%3Abackend%2Fazure+-label%3Abackend%2Fs3+-label%3Abackend%2Fgcs+-label%3Abackend%2Fconsul+-label%3Abackend%2Fartifactory+-label%3Aterraform-cloud+-label%3Abackend%2Fremote+-label%3Abackend%2Fswift+-label%3Abackend%2Fpg+-label%3Abackend%2Ftencent++-label%3Abackend%2Fmanta++-label%3Abackend%2Fatlas++-label%3Abackend%2Fetcdv3++-label%3Abackend%2Fetcdv2+label%3Aconfirmed+-label%3A%22pending+project%22+) (7)

I'm going to start with the Explained Issues, to see what they're about, see if I can understand and reproduce them.

Explained issues:

contributed | issue | understood? | reproduced? | date_opened | date_last_update | Note
--- | --- | --- | --- | --- | --- | :--
{{<v>}} | [#31102](https://github.com/hashicorp/terraform/issues/31102) | {{<v>}} | {{<v>}} | 20220522 | 20220916 | Already fixed by someone else ([ref](https://github.com/hashicorp/terraform/issues/31102#issuecomment-1500396127))
{{<x>}} | [#31946](https://github.com/hashicorp/terraform/issues/31946) | {{<v>}} | {{<v>}} | 20221005 | 20221011
{{<x>}} | [#30951](https://github.com/hashicorp/terraform/issues/30951) | {{<v>}} | {{<v>}} | 20220428 | 20220928
{{<x>}} | [#30133](https://github.com/hashicorp/terraform/issues/30133) | {{<x>}} | {{<x>}} | 20211210 | 20220928
{{<x>}} | [#29744](https://github.com/hashicorp/terraform/issues/29744) | {{<v>}} | {{<x>}} | 20211012 | 20230314
{{<x>}} | [#28482](https://github.com/hashicorp/terraform/issues/28482) | {{<v>}} | {{<x>}} | 20210422 | 20210726
{{<x>}} | [#27151](https://github.com/hashicorp/terraform/issues/27151) | {{<x>}} | {{<x>}} | 20201204 | 20220811

It looks like the 'explained issues' are quite stale, maybe because they are difficult to solve?
TODO:

- reproduce them all

### Issue #31102
title: Plan is confusing when output value changes between tuple type and list type with the same elements values  
Unable to reproduce with my version
```
$ tf -version
Terraform v1.4.2
on linux_amd64
```
The original issue was raised for 1.2.0, which is now EOL (since 20230308).

For fun, can I get TF 1.2.0 and reproduce it? Can I find the version which fixed it? At this point I discovered a program called **`tfenv`**, below are some resources which I used to learn more about it:

- https://medium.com/@haridevvengateri/how-to-install-an-older-version-of-terraform-on-mac-os-4b1aac04d45e
- https://releases.hashicorp.com/terraform/1.2.0/
- https://stackoverflow.com/questions/56283424/upgrade-terraform-to-specific-version
- https://github.com/tfutils/tfenv
- https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli

#### Reproduce

I take the main.tf from the original GitHub issue, and reproduce the issue on Terraform v1.2.0.

{{<highlight bash>}}
$ tf --version
Terraform v1.2.0
on linux_amd64

$ cat main.tf.from 
output "test" {
  value = ["first", "second"]
}

$ cat main.tf.to 
output "test" {
  value = true ? ["first", "second"] : [] # ["first", "second"]
}

$ diff main.tf.from main.tf.to 
2c2
<   value = ["first", "second"]
---
>   value = true ? ["first", "second"] : [] # ["first", "second"]

$ cp main.tf.from main.tf
{{</highlight>}}

Apply terraform config.

{{<highlight bash>}}
$ tf apply

Changes to Outputs:
  + test = [
      + "first",
      + "second",
    ]

You can apply this plan to save these new output values to the Terraform state,
without changing any real infrastructure.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes


Apply complete! Resources: 0 added, 0 changed, 0 destroyed.

Outputs:

test = [
  "first",
  "second",
]
{{</highlight>}}

Now, update the main.tf, and run `terraform plan`, which shows the bug.

{{<highlight bash>}}
$ cp main.tf.to main.tf
$ tf plan

Changes to Outputs:
  ~ test = [
      - "first",
      - "second",
    ] -> [
      + "first",
      + "second",
    ]
{{</highlight>}}

#### Reproduce with different versions

I try reproducing the issue with various terraform versions:

- 1.2.0: issue occurs
- 1.3.0: issue occurs
- 1.3.9: issue occurs
- 1.4.0-alpha20221207: issue occurs
- **1.4.0-alpha20221109: issue occurs**
- **1.4.0-beta1: issue resolved**
- 1.4.0-beta2: issue resolved
- 1.4.0-rc1: issue resolved
- 1.4.0: issue resolved
- 1.4.4: issue resolved

So we see that the issue was fixed between 1.4.0-alpha20221109 and 1.4.0-beta1.

Now, let's see if we can see the list of commits in git between the two versions (git tags): https://github.com/hashicorp/terraform/compare/v1.4.0-alpha20221109...v1.4.0-beta1

There are 259 commits, from 44 contributors, with 706 files changed. I have no idea which file would have contained the fix, so I first try to roughly read through all the commit messages to see if anything looks related.

Reading the messages, I found a feature which was introduced, called 'Structured Renderer' or 'Structured plan renderer' which sounds like it might be related.

- https://github.com/hashicorp/terraform/commit/aff94591c1383bb9cb412858a8f4d26275110453
- https://github.com/hashicorp/terraform/commit/95782f2491b4b58d8ea9954b444134fda5833c89
- https://github.com/hashicorp/terraform/commit/8d61c5bfc411a97716e4b6f387cf5b6cb1a53cec
- https://github.com/hashicorp/terraform/commit/60d6e52021a6654da3a585c73c6b27936070f2d9
- https://github.com/hashicorp/terraform/commit/d818d7850d92ac6e4024c3a8de6705c32931870d

In a [related PR for the Structured Renderer](https://github.com/hashicorp/terraform/pull/32497), I noticed that there is an option to disable it, with a `structured-renderer=false` flag. So let's try  reproducing the bug with this flag set. Ah, but looking at the discussion, that PR was closed, in favour of [#32520](https://github.com/hashicorp/terraform/pull/32520), so no flag for disabling.

Next idea: Compile terraform from source but remove the commits introduced by the #32520 to confirm if this is what fixed the issue.

#### Make my dev Terraform work with tfenv
So I am using tfenv to manage terraform versions. It stores files in `~/.tfenv`. I also have my git-cloned and compiled terraform repo. I would like to be able to use tfenv to switch to my own compiled version, e.g. with `tfenv use dev` instead of `tfenv 1.4.4`.

So here is how I think `tfenv` works:

- ~/.tfenv/bin/terraform: this is a script which executes the currently-set terraform binary
- ~/.tfenv/versions/1.4.4/terraform: the versions dir has terraform binaries for different tf versions
- ~/.tfenv/version: the currently selected version file, e.g. 1.4.4

And indeed, creating the dir `~/.tfenv/versions/dev` now shows in `tfenv list`:
```
$ tfenv list
* 1.4.4 (set by /home/konrad/.tfenv/version)
  1.4.0
  1.4.0-beta1
  dev
```

Now, when I compiled tf, it outputs the terraform binary to my $GOBIN path, as the binary `~/go/bin/terraform`, so I symlink it:
```
$ ln -s ~/go/bin/terraform ~/.tfenv/versions/dev/terraform
```

and now I can switch to it with `tfenv`:
```
$ tfenv use dev
Switching default version to vdev
Default version (when not overridden by .terraform-version or TFENV_TERRAFORM_VERSION) is now: dev
$ tf --version
Terraform v1.5.0-dev
on linux_amd64
```
Great!

#### Git interlude: Checkout a tag
I now need to do the following:

- git checkout the 1.4.0-beta1 tag: TODO: Tidy this up!
- compile terraform, confirm the issue is resolved
- remove the Structured Plan Renderer commits (#32520)
- compile tf, confirm the issue appears again

It's not immediately obvious how to switch my repo to the state in which it was at a specific tag:
```
$ git checkout 1.4.0-beta1
error: pathspec '1.4.0-beta1' did not match any file(s) known to git
$ git checkout -b konrad-test 1.4.0-beta1
fatal: '1.4.0-beta1' is not a commit and a branch 'konrad-test' cannot be created from it
$ git checkout tags/1.4.0-beta1 -b konrad-test
fatal: 'tags/1.4.0-beta1' is not a commit and a branch 'konrad-test' cannot be created from it
```
No good. Some articles to help me:

- https://stackoverflow.com/questions/35979642/what-is-git-tag-how-to-create-tags-how-to-checkout-git-remote-tags
- https://www.git-tower.com/learn/git/faq/git-checkout-tag
- https://devconnected.com/how-to-checkout-git-tags/

The problem is that I don't have this tag fetched locally. I see that I can fetch all tags with `git fetch --all --tags` or the latest tag with `git fetch --tags`. I'm thinking it would be nice to fetch just the tag that I need, but if I think about it, tags are just pointers to commits, so it shouldn't take too much space to fetch them all. But before I do, how can I see which tags I have fetched? The below shows it but I know I never fetched it.

```
$ git tag | grep 1.4.0-beta1
v1.4.0-beta1
```
What about `git ls-remote --tags origin`?

```
$ g ls-remote --tags origin | grep v1.4.0-beta1
8969cb28fbce0c5d5f9e923b406a010f6ec05211        refs/tags/v1.4.0-beta1
5276747af8fef04525030738886176cb4a3db7cf        refs/tags/v1.4.0-beta1^{}
```

Will the `git fetch --all --tags` pollute my `git branch` output with all the tags out there?

Anyway, a helpful doc for fetching a single tag:

- https://stackoverflow.com/questions/45338495/fetch-a-single-tag-from-remote-repository

Fetch a single tag:
```
git fetch origin tag v1.4.0-beta1 --no-tags
git fetch origin refs/tags/1.4.0-beta1:refs/tags/1.4.0-beta1
```
but
```
$ git fetch origin refs/tags/1.4.0-beta1:refs/tags/1.4.0-beta1
fatal: couldn't find remote ref refs/tags/1.4.0-beta1
```

I give up, I just fetch all tags

```
$ git fetch --all --tags
Fetching origin
remote: Enumerating objects: 75, done.
remote: Counting objects: 100% (75/75), done.
remote: Compressing objects: 100% (21/21), done.
remote: Total 75 (delta 52), reused 72 (delta 51), pack-reused 0
Unpacking objects: 100% (75/75), 17.35 KiB | 111.00 KiB/s, done.
From github.com:hashicorp/terraform
   c960b16e87..a3db16e4ea  main                                     -> origin/main
 * [new branch]            bflad/debugging-link-to-provider-logging -> origin/bflad/debugging-link-to-provider-logging
 * [new branch]            f-cloud-no-intermediate-state-snapshots  -> origin/f-cloud-no-intermediate-state-snapshots
 * [new branch]            jbardin/destroy-plan-null-type           -> origin/jbardin/destroy-plan-null-type
   9b7a2ece16..5359eea47a  liamcervante/checks/filter-diags         -> origin/liamcervante/checks/filter-diags
 + ddae3cdbb9...0f1507b71a liamcervante/plan-timestamp              -> origin/liamcervante/plan-timestamp  (forced update)
 * [new branch]            releng/smoke-test-docker-builds          -> origin/releng/smoke-test-docker-builds
 * [new tag]               v1.5.0-alpha20230405                     -> v1.5.0-alpha20230405
```

but I still can't checkout that tag!

```
$ git checkout 1.4.0-beta1
error: pathspec '1.4.0-beta1' did not match any file(s) known to git
$ git checkout -b konrad-test 1.4.0-beta1
fatal: '1.4.0-beta1' is not a commit and a branch 'konrad-test' cannot be created from it
$ git checkout tags/1.4.0-beta1 -b konrad-test
fatal: 'tags/1.4.0-beta1' is not a commit and a branch 'konrad-test' cannot be created from it
```

At this point, I stop listening to a podcast while trying to do this, I need to focus a bit better. And here it is, courtesy of the devconnected.com article:

```
$ git fetch --all --tags
Fetching origin
$ git checkout tags/v1.4.0-beta1 -b v1.4.0-beta1
Switched to a new branch 'v1.4.0-beta1'
```
Bingo! Now I can recompile terraform as per the `.github/CONTRIBUTING.md` doc.

```
go install .
```
and confirm
```
$ tf --version
Terraform v1.4.0-beta1
on linux_amd64
```

And I've also confirmed that the issue #31102 does not appear, as expected.

#### Find exact commit which fixed the issue
I rolled back to the commit which applied the new structured renderer.
```
git reset --hard 8d61c5bfc411a97716e4b6f387cf5b6cb1a53cec
```
and then recompiled tf and tried again.  
Confirmed: Issue is fixed.
```
$ tf plan

───────────────────────────────────────────────────────────────────────────────────────────────────────────────

Note: You didn't use the -out option to save this plan, so Terraform can't guarantee to take exactly these
actions if you run "terraform apply" now.
```

Then I checked out the commit right before the structured renderer was enabled.
```
git reset --hard 93f739e927e011aaa166f98bdd8a76ec6b029ebe
```
Then, again did `go install .` to recompile tf, and retested.  
Confirmed: Issue exists.


## Learnings and notes

- [goenv](https://github.com/syndbg/goenv)
- [go modules](https://blog.golang.org/using-go-modules)
- What are the three dots in `go test ./...`  ? See `go help packages`
- **`tfenv`**: This is useful for being able to compare behaviour between different terraform versions
- There is more to git than what I know so far
- TODO: Tidy up the git checkout tag section, and add a git cheatsheet


<!-- REFERENCES -->
## References
[ansible]: https://www.ansible.com/
[ansible-git]: https://github.com/ansible/ansible
[calf]: https://calf-studio-gear.org/
[calf-git]: https://github.com/calf-studio-gear/calf
[clementine]: https://www.clementine-player.org/
[clementine-git]: https://github.com/clementine-player/Clementine
[gitlab]: https://gitlab.com/
[gitlab-git]: https://gitlab.com/gitlab-org/gitlab
[haiku]: https://www.haiku-os.org/
[haiku-git]: https://git.haiku-os.org/haiku/
[hugo]: https://gohugo.io/
[hugo-git]: https://github.com/gohugoio/hugo
[puppet]: https://www.puppet.com/
[puppet-git]: https://github.com/puppetlabs/puppet
[terraform]: https://www.terraform.io/
[tf-git]: https://github.com/hashicorp/terraform
[vim]: https://www.vim.org/
[vim-git]: https://github.com/vim/vim
