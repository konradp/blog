---
title: "OTHER: Yearly goals"
date: 2023-01-12
tags: [ 'other' ]
categories: other
---

This is my yearly plan, it lists the things I would like to achieve, and the projects I would like to complete. The full list is on [trello](https://trello.com/b/NllGIvYN/todo) (private board). This article here is for the milestones and overall progress tracking.

{{<toc>}}

## 2025
### Goals
Six goal categories: FIN, MUS, HEALTH, EMPL, GEN


Process improvements:
- The cards which contribute towards advancing the main GOAL have title in capitals, e.g. `GEN: house, pick a country based on mortgage and tax rates`. These which do not are in lower-case, e.g. `mus: convert all projects to ardour`
- Split off the FIN goal into own Trello board

## 2024
### Goals
Six goals: FIN, MUS, HEALTH, EMPL, TIDY, FUN.

- **{{<x>}} FIN: make money on the market**  
  score: 2/10  
  Made some improvements fo finplotjs, implemented neural network for trade classification (but wasn't very good). Studied some (created blog articles: studies). By the end of the year, all-time P/L is -$952 (-63% of 1.5k invested)
  - {{<x>}} $1000 profit on etoro
- **{{<w>}} MUS: musical album**  
  score: 2/10  
  Released 'although' on Spotify (but this was an album with half-finished songs which I've already had mp3s for. Cleaned up hydrogen songs, samples (to an extent), and old albums. Recorded Roses(backing). Various improvements to saxjs and kavaljs. Bought a piano. Bought soldering iron and enough electronics gear but did no electronics. Didn't record 'Do I wanna know' (after a year of practicing). Didn't fully learn BWV 1001 or BWV 812 or Scarlatti K027. Didn't learn all piano scales. Did I fix Alesis power?
  - {{<w>}} album published on spotify
  - {{<x>}} 100 casettes
- **{{<x>}} HEALTH: improve health**  
  score: 1/10  
  No changes to beer and tobacco habits. Did a lot of gardening and walking, so that could be considered as exercise. Still struggling to wake up early and go to bed early. Did not go to a swimming pool a single time.
  - {{<w>}} out of bed at/before 8am
  - {{<x>}} drop beer belly, reduce beer intake
  - {{<x>}} develop muscle
  - {{<x>}} quit smoking
  - {{<x>}} start swimming again: learn butterfly
- **{{<w>}} EMPL: increase employability**
  - {{<x>}} AWS exam
  - {{<v>}} AWS cert: Did some practice papers, and some reading
  - {{<v>}} CVJS updates
  - {{<v>}} tidy gitlab repos
  - {{<v>}} GEN: Standardise font on project pages
- **{{<w>}} TIDY: general life, documents etc**
  - {{<v>}} finished the room: disco ball, guitar hangers, chair, desk
- **{{<x>}} FUN: other, fun things**
  - {{<x>}} build guitar body
  - {{<x>}} perfect Bach piano piece
  - {{<x>}} perfect Bach guitar piece
  - {{<v>}} lots of gardening and garage: learned how to sharpen chisels, prepared wood material for drying for next year
  - {{<v>}} made compost bin
  - {{<v>}} draw easel: waste of time, I drew an easel design in CAD. I should have drawn it on paper

### Review
Overall score: 2/10. Some minor achievements, but none of the goals were achieved.

## 2023
### Goals
- programming
    - {{<x>}} zhtools
        - start using it and improve it enough for use
    - {{<v>}} chords-guitar
        - {{<v>}} rename to guitar-chords
        - {{<v>}} fix display bugs
        - {{<v>}} make a paint-like app
    - {{<v>}} awstools: finish it, abandoned
    - {{<x>}} python: spice up personal projects enough for CVJS
- music
    - {{<x>}} release an album
    - {{<x>}} get better at guitar:
        - get the scales, major/minor, pentatonic
- other
    - {{<x>}} learn Bulgarian: linking words and improve vocab


### Review
**Achieved the year goals? Did these goals change throughout the year?**  
No. The only originally set goal that was achieved was the `guitar-chords` app. Also, learned some more Bulgarian. Also, the goals were changing throughout the year.

**Did I improve my growth processes and planning methods?**

- Redesigned my TODO Trello board from a collection of TODOs into a per-month goals
- Writing each month in a blog post doesn't work
- Using trello, planning for each month works
- Got better at using trello

Per category:

**TIDY**  
Most achievements were in the 'tidy' section. I settled in a different country, got a job, opened a bank account, got an EU health insurance card. Updated my CV, and built up my CVJS. Other than that, tidied up many of my already existing projects, e.g. tidied up spotify playlists, cleaned up my blog. Also, cleaned up some disk space, organised my recordings, organised google photos, my documents, moved passwords to 1pass. Tidied up my git repositories and reduced their number.

**COM**  
A few new music apps, e.g. saxjs, guitar-chords, harmonica-explorer, trumpetjs etc.

**LEARNING**  
Have read the full yaml spec, started reading Atlas Shrugged. Learned more about Web Audio API, js tricks, HTML/CSS flex and grid layout, and more JS Canvas API. Have read the python `stl.os` library documentation, and studied some Terraform.

**MUSIC**  
Tidied up many of the recordings and organised them a bit more. Started the guitar book, cleaned up the Spotify paylists (and reduced their number). Bought BOSS RC-505 mkII, and kaval, also some monitor speakers. My studio is now much more versatile, and I can turn it on with a single switch, but the number of cables and power adapters is now becoming inconvenient.

**OTHER**  
Also, started donating to wikipedia once per month. The 'FIN' project had some revival on the IB side, but not enough.
