---
title: 'MAT: Modern Engineering Mathematics: Book notes'
subtitle: 
date: 2021-07-08
tags: [ "wip" ]
categories: wip
---

{{< toc >}}

# 1. Numbers, algebra, and geometry
## 1.2 Number and arithmetic
Ignore

# 2. Functions

# 3. Complex numbers

# 4. Vector algebra

# 5. Matrix algebra
# 6. Intro to discrete maths
# 7. Sequences, series, limits
# 8. Differentiation/integration
# 9. Further calculus
# 10. ODEs
# 11. Laplace transforms
# 12. Fourier series
# 13. Data handling, probability theory
