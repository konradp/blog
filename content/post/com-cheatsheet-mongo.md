---
title: "mongodb"
date: 2022-01-26
tags: [ 'com', 'cheatsheets' ]
categories: cheatsheets
---

# Client
Connect to localhost or remote, and check connection.
```
mongosh
mongosh "mongodb://mongodb0.example.com:28015"
db.getMongo()
```

# Databases
List
```
test> show dbs
admin     41 kB
config  61.4 kB
local     41 kB
```
**Note:** Will not show a newly created DB, unless we add a collection or a doc.

Use DB (or create new one)
```
use config
```
confirm
```
db
```
# Collections
List
```
show collections;
db.getCollectionNames();
```

Create
```
db.createCollection('myCollection');
```

or create by inserting.
```
db.myCollection.insertOne( { x: 1 } );
```

Insert many.
```
db.myCol.insertMany([
   {
      title: "Title",
      tags: [ "Biography", "Adventure" ],
    },
    {
      title: "Some title",
      tags: [ "Sci-Fi", "Fantasy" ],
    }
])
```
Get all docs in a col.
```
db.myCollection.find()
```

Find docs with filter
```
db.myCollection.find({title: 'Title'})
```

# Documents: update and delete
Update.
```
db.col.updateOne( { title: "Some title" }, { $set: { field: "some value" } })
```
Update by id.
```
db.col.updateOne(
  {_id: ObjectId('61f3ff2f9fd37336ac241bb0')},
  { $set: {name: 'some new name'} }
)
```
Delete fields 'field1' and 'field2' from all documents.
```
db.col.update({}, {$unset: {"field1": 1, "field2": 1}})
```


# Reference
https://docs.mongodb.com/mongodb-shell/
