---
title: "MUS: LV2 plugin hosts"
subtitle: 
date: 2023-01-19
tags: [ 'wip' ]
categories: wip
---
A comparison of the audio plugin hosts out there, for use in my guitar jamming. I also look at plugins themselves. (LV2 plugins only)

{{< toc >}}

**TL;DR:** This document is work in progress. The plugin host I use is ????

TODO:

- clean guitar recording: direct to mixer (no reverb)
- clean guitar recording: direct to mixer (+reverb)
- guitar > amp sim > mixer (no reverb + reverb)
- guitar > amp sim > mixer > plugins (start with reverb)
- guitar > mixer > plugins

## Introduction
### Motivation
Currently, I'm using ardour for applying audio plugins (e.g. reverb, overdrive etc), but ardour takes a while to load and connect to JACK. For situations in which I'd like to just jam, this takes too long to be practican, considering that as part of the setup I first need to plug the guitar in, plug in the amp simulator power, and the mixer power.

This might sound like not that big a deal, but I think that if I reduce the time it takes me to set up for electric guitar playing, it will make it easier for me to practice more frequently.

**Note:** Another option is to get a headphone amp, e.g. a VOX or a Fender, but this wouldn't let me use the PC for the looping.

After a web search, I found some resources which I'll use as a starting point because they contain a list of plugin hosts that I can try:

- https://wiki.linuxaudio.org/apps/categories/lv2
- https://ubuntustudiomusic.wordpress.com/tag/lv2rack/

### Plugin standards: LV2, LADSPA, VST etc
There are a few plugin standards:

- [LV2](https://en.wikipedia.org/wiki/LV2):
    - plugins can have custom GUIs
    - successor to LADSPA and DSSI
- [LADSPA](https://en.wikipedia.org/wiki/LADSPA):
    - no custom GUIs for plugins
    - written in C
- [DSSI](https://en.wikipedia.org/wiki/Disposable_Soft_Synth_Interface):
    - extends LADSPA, focused on instruments/synths
- [VST](https://en.wikipedia.org/wiki/Virtual_Studio_Technology):
    - plugins can have custom GUIs
    - instruments (VSTi) and effects (VSTfx)
    - LXVST: Linux VST

**Note:** I only included the standards which I can use on Linux

In this article, I'm going to **focus only on the LV2** hosts and plugins.

References:
- [Ardour manual: Working with plugins](https://manual.ardour.org/working-with-plugins/)


## Plugin hosts
I'm going to consider:

- Carla
- Element: https://kushview.net/element/
- guitarix
- calfjackhost
- jalv.select
- Synthpod
- from https://jackaudio.org/applications/
    - effects processors:
        - arcangel
        - CP-GFX
        - creox
        - freqtweak
        - guitarix
        - JAMin
        - linuxDSP
        - Louderbox
        - rackarrack
        - redFX
        - tapiir
    - plugin hosts:
        - FSTHOST: VST host, hybrid using winelib
        - jackspa
        - jost
        - ng-jackspa
- https://github.com/lv2/lv2/wiki#lv2-hosts:
- https://lv2plug.in/pages/projects.html:
    - jalv: http://drobilla.net/software/jalv/ page doesn't work
    - [lv2proc](https://naspro.sourceforge.net/applications.html#freeadsp)
- https://github.com/lv2/lv2/wiki#standalone-hosts <- see also audio processing tools

Discarded hosts

host | reason
--- | ---
[JACK Rack (jack-rack)](https://jack-rack.sourceforge.net/) | LADSPA-only
zynjacku/lv2rack | website is down (http://home.gna.org/zynjacku/)

### Carla
homepage: https://kx.studio/Applications:Carla  
source: https://github.com/falkTX/Carla  
licence: GPLv2

This plugin host looks very nice and powerful.

#### Installation
Follow steps from https://kx.studio/Repositories and https://kx.studio/Repositories:Applications to add the ks.studio deb repo, and then install Carla with `sudo apt install carla`.

#### Usage
There is a nice patchbay and an 'Add plugin' view similar to Ardour.

TODO: image of the rack here

patchbay  
![](/media/mus-plugin-hosts/carla-patchbay.png)

add plugin  
![](/media/mus-plugin-hosts/carla-add-plugin.png)

## Plugins
Here I will consider:

- [Calf Studio Gear](https://en.wikipedia.org/wiki/Calf_Studio_Gear)
- guitarix
- plugins from Carla: https://kx.studio/Repositories:Plugins
- lsp-plugins-jack
- zam-plugins: https://github.com/zamaudio/zam-plugins
- https://lsp-plug.in/
- http://linux-sound.org/plugins.html
- https://www.audiopluginsforfree.com/linux/#google_vignette
- http://linux-sound.org/linux-vst-plugins.html
- https://wiki.linuxaudio.org/apps/all/lv2
- https://wiki.linuxaudio.org/apps/effects_apps
- https://github.com/lv2/lv2/wiki#lv2-plugins
- https://manual.ardour.org/working-with-plugins/getting-plugins/
- https://lv2plug.in/pages/projects.html

Not considering:

- LADSPA-only:
    - [CAPS](http://quitte.de/dsp/caps.html): LADSPA-only
    - [CMT](http://www.ladspa.org/cmt/overview.html): LADSPA-only
    - [TAP-plugins](https://tomscii.sig7.se/tap-plugins/)

**Note:** Common paths for LV2 plugin locations are `/usr/lib/lv2`, `/usr/local/lib/lv2`, `~/.lv2`. See the settings in Carla for various paths for different plugin standards.

### Calf Studio Gear
- [wiki](https://en.wikipedia.org/wiki/Calf_Studio_Gear)
- [website](https://calf-studio-gear.org)
- licence: LGPL

see:

- calfjackhost



## Troubleshooting
### Carla
The installation is quite big:
```
$ sudo apt install carla
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following package was automatically installed and is no longer required:
  linux-image-4.19.0-13-amd64
Use 'sudo apt autoremove' to remove it.
The following additional packages will be installed:
  carla-data libqt5designer5 libqt5opengl5 python3-isodate python3-liblo
  python3-pyqt5 python3-pyqt5.qtopengl python3-pyqt5.qtsvg python3-pyqt5.sip
  python3-rdflib python3-sparqlwrapper
Suggested packages:
  pyliblo-utils python-liblo-docs python3-pyqt5-dbg python-rdflib-doc
The following NEW packages will be installed:
  carla carla-data libqt5designer5 libqt5opengl5 python3-isodate python3-liblo
  python3-pyqt5 python3-pyqt5.qtopengl python3-pyqt5.qtsvg python3-pyqt5.sip
  python3-rdflib python3-sparqlwrapper
0 upgraded, 12 newly installed, 0 to remove and 5 not upgraded.
Need to get 73.4 MB of archives.
After this operation, 254 MB of additional disk space will be used.
```


**Problem**
When installing the repo, I saw error.
```
 kxstudio-repos pre-depends on libc6 (>= 2.31~)
  libc6:amd64 is installed, but is version 2.28-10+deb10u1.
```
which means that Carla requires libc6 <= 2.31~ but I only have 2.28.

I first tried `sudo apt-get update; sudo apt-get upgrade`, and that brought the version up to 2.28-10+deb10u2, which is still not good enough so I did `sudo apt-get dist-upgrade`


in repo:

- carla (61mb + 2mb)
- element (3mb)
- MOD LV2 host (157kb)

for plugins:

- kxstudio-meta-audio-plugins-lv2

## References

- https://jackaudio.org/applications/  
  Some more plugin hosts here
- wikis:
    - [LV2](https://en.wikipedia.org/wiki/LV2):
    - [LADSPA](https://en.wikipedia.org/wiki/LADSPA)
    - [DSSI](https://en.wikipedia.org/wiki/Disposable_Soft_Synth_Interface)
    - [VST](https://en.wikipedia.org/wiki/Virtual_Studio_Technology):
- [Ardour manual: Getting plugins](https://manual.ardour.org/working-with-plugins/getting-plugins/)
