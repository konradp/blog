---
title: "gui_tools"
subtitle: 
date: 2023-06-28
tags: [ "com", "cheatsheets" ]
categories: cheatsheets
---
Mostly keyboard shortcuts.

{{<toc>}}

## Ardour
ref: https://manual.ardour.org/appendix/default-keyboard-shortcuts/  
ref: https://manual.ardour.org/setting-up-your-system/keyboard-shortcuts/

- switch mixer/editor
- select tools: hand, cut etc
- play, stop, pause
- go back to beginning

## Postman
- `CTRL + /`: show shortcuts
- `CTRL + \`: show/hide sidebar
- `ALT + E`: environment select

## Thunderbird
ref: https://support.mozilla.org/en-US/kb/keyboard-shortcuts-thunderbird

- `<- / ->`: thread: collapse/expand
- `SPACE`: next unread msg in thread
- `\`: threads: collapse all
- `M`: message: mark as read/unread
- `R`: thread: mark as read
- `T`: thread: go to next unread (and mark current as read)
