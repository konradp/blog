---
title: "COM: LaTex editors"
subtitle: 
date: 2019-12-08
tags: [ "wip" ]
categories: wip
---

Install LaTex distribution:
```
texlive-base
```
Note: Alternatives are `texlive` or `texlive-full`.  
Note: Not strictly required, as installing any of the editors will automatically install the requred texlive distribution.

Options I have tried:

- latexila
- texmaker
- texworks
- vim-latexsuite
- vim-latex-live-preview

Option I settled for:

- vim-latexsuite

# latexila
```
sudo apt-get install latexila
```

# texmaker

# texworks
Small package, quick install.

# vim-latexsuite
```
sudo apt-get install vim-latexsuite
```

In vim, to run
```
:LLPStartPreview
```

in `~/.vimrc`:
```
" LaTeX plugin from
" https://github.com/xuhdev/vim-latex-live-preview
" put plugin/latexlivepreview.vim to ~/.vim/plugin
" compile document with :LLPStartPreview
" ensure installed okular pdf viewer
filetype plugin on
"let g:livepreview_previewer = 'xpdf'
```
