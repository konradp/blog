---
title: "RECIPE: How to boil eggs"
date: 2023-02-06
tags: [ 'other' ]
categories: other
---

I always forget how long it takes to boil eggs and I end up having to look it up online. The websites out there are full of cookies notifications and adverts, which annoys me a lot, so I'm writing this down on my own blog for easier access.  
**Note:** Adapted from https://thestayathomechef.com/how-to-boil-eggs/

Method:

- put eggs in pan of cold water  
  **Note:** One inch/thumbnail of water extra above the eggs.
- boil the water, high heat
- once boiled, remove the pan from heat, and cover
- wait 4-10 min  
    - 4-6 min: soft boiled
    - 8-12 min: hard boiled
- rinse eggs with cold water

Tips:

- add some salt or vinegar to the water to avoid eggs cracking

<hr>
WIP notes:

attempt 1:

- 11:35: started boiling cold water
- 11:40: one egg cracked because forgot to add salt, so added salt
- 11:59: cooled eggs
