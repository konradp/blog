---
title: 'OTHER: Average of averages is not the average'
subtitle: 
date: 2022-11-18
tags: [ 'other' ]
categories: other
---

Consider a list: `[ 1, 2, 3, 4, 5 ]`.  
Their average is: `(1+2+3+4+5)/5 = 3`.  
If grouped into two lists and their respective averages:

- `average([ 1, 2 ]) = 1.5`
- `average([ 3, 4, 5 ]) = 4`

then the average of averages is `(1.5+4)/2 = 2.75`, which is different from 3.

But, consider a list: `[ 1, 2, 3, 4 ]` grouped into `[1, 2]` and `[3, 4]`. Then

- `average([1,2,3,4]) = 2.5`
- `average([1,2]) = 1.5`
- `average([3,4]) = 3.5`

and `(average([1,2])+average([3,4]))/2 = 2.5`, which is the same.

In detail:

```
average(a,b,c,d) = (a+b+c+d)/4
                 = a/4 + b/4 + c/4 + d/4
                 = 1/2(a/2 + b/2 + c/2 + d/2)
                 = ((a+b)/2 + (c+d)/2)/2
                 = (average(a,b) + average(c,d))/2
                 = average( average(a,b), average(c,d) )
```
What makes it work, is that we grouped the items into **groups of equal size (cardinality)**. In these cases, the average of averages is the same as total averages. Otherwise, it doesn't need to be.



# Reference
- https://lemire.me/blog/2005/10/28/average-of-averages-is-not-the-average/
- https://math.stackexchange.com/questions/95909/why-is-an-average-of-an-average-usually-incorrect
- see also: https://en.wikipedia.org/wiki/Simpson%27s_paradox
