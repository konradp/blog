---
title: "MUS: Covers"
subtitle: 
date: 2023-01-18
tags: music
categories: music
---
- [ABRA - Roses](./abra-roses)
- [Arctic Monkeys - Do I wanna know](./arcticmonkeys-doiwannaknow)
- [Bella Poarch - Build a bitch](./bellapoarch-buildabitch)
- [Franz Ferdinand - Ulysses](./franzferdinand-ulysses)
- [John Mayer - Belief](./johnmayer-belief)
- [John Mayer - I don't trust myself](./johnmayer-idonttrustmyself)
- [John Mayer - New light](./johnmayer-newlight)
- [Lianne la Havas - Empty](./lianne-la-havas-empty)
- [Mahalia - Sober](./mahalia-sober)
- [Tom's diner](./tomsdiner)
