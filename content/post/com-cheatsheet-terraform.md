---
title: "terraform"
subtitle: 
date: 2023-03-20
tags: [ "com", "cheatsheets" ]
categories: cheatsheets
---
My Terraform cheatsheet.

{{<toc>}}

## Cheatsheet
Notes:

- `alias tf='terraform'`
- plugins are called **providers**, need to be installed
- modules
- declarative config file language
- uses a state file
- check out Terraform Cloud
- autocomplete: `terraform -install-autocomplete`
- `tf init`  
  Initialise project, downloads providers, creates `.terraform.lock.hcl` (add to git)
- `tf plan`  
  Show changes
- `tf apply`  
  provision
- `tf destroy`
- `tf fmt`: formatting
- `tf validate`
- `terraform.tfstate`
- `tf show`: inspect current state
- `tf state list`
- `tf destroy`
- `tf apply -var "instance_name=AnotherName"`
- `tf output`
- `tf login`

Amazon auth
```
cat ~/.aws/credentials
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
```

shell snippets
```
terraform init
terraform apply
```


## References and resources
- tutorials:
    - {{<v>}} [Get started: AWS](https://developer.hashicorp.com/terraform/tutorials/aws-get-started)
    - {{<x>}} Fundamentals:
        - {{<x>}} CLI
        - {{<x>}} Configuration Language
        - {{<x>}} Modules
        - {{<x>}} Provision
        - {{<x>}} State
        - {{<x>}} Terraform Cloud
- ref: https://developer.hashicorp.com/terraform/language/providers

## Notes, ideas
- bugs in providers?
