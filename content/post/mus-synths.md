---
title: "MUS: Synthesisers"
subtitle: 
date: 2023-06-24
categories: wip
---

I'd like to buy a synth for chords. Here is a list of synths to consider

{{<toc>}}


## ideal spec

- MUST: polysynth: 4 voices or more
- midi in
- portable (battery powered)
- nokbd:
  - {{<v>}} no keyboard
  - {{<w>}} small buttons or pads, keyboard-like
  - {{<x>}} has a keyboard
- price: under £500 <- thomann.de price, in GBP
- list: https://www.thomann.de/gb/topseller_GF_synthesizers.html
- https://www.thomann.de/gb/cat_rank.html?ar=157271&gk=TASYSY

# list
synth                       | price | voices | year | MIDI in | nokbd   | portable | notes
---                         | ---   | --:    | :--  | :-:     | :-:     | :-:      | ---
volca fm2                   | 155   | 6      | 2022 |         | {{<w>}} | {{<v>}}
roland j6                   | 159   | 4      |      | {{<v>}} | {{<w>}} | {{<v>}}
roland s1                   | 175   | 4      |      |         |         |
sonicware LIVEN 8bit warps  | 259   | 6      |      | {{<v>}} | {{<w>}} | {{<v>}}
behringer pro800            | 339   | 8      |      | {{<v>}} | {{<v>}} | {{<x>}}
reface cs                   | 339   | 8      |      |         |         |
novation mininova           | 345   | 18     |      | {{<v>}} | {{<x>}} | {{<x>}}  | nice vocoder
microkorg                   | 349   | 4      |      | {{<v>}} | {{<x>}} |
asm hydrasynth explorer     | 555   | 8      |      | {{<v>}} | {{<x>}} |
dreadbox nymphes            | ?     |        |      |         |         |
modal skulpt                | ?     |        |      |         |         |
volca keys (paraphonic)     | ?     |        |      |         |         |



# photos

# Other notes
- check out dreadbox company, see dreadbox disorder fuzz (£119)
