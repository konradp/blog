---
title: "ardour"
date: 2022-01-19
tags: [ "com", "cheatsheets" ]
categories: cheatsheets
---
{{<toc>}}

link: https://www.youtube.com/watch?v=q8rEMI0n6Qk&list=PL98ZX0JQtKmdmZD4LmFyr7NWiKaSHChot&index=3  

status: tutorial #3

## Setup, general
- monitoring:
  - use mixer (no ardour monitoring)
  - ardour playback during record
- monitor section:
  - use master bus directly
  - use additional monitor bus (master -> monitor -> hardware)
- audio/midi setup:
  - sample rate: 48 kHz <- look up why
  - buffer size: 128 samples, (try 256 first) <- cpu-dependent

to look up:
- punch ranges for recording at points, In, Out buttons
- shift+end
