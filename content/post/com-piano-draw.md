---
title: "COM: piano-draw: Drawing a piano keyboard"
subtitle: 
date: 2023-11-09
categories: wip
math: true
---
TABLE OF CONTENTS:
{{<toc>}}

## Aim
The goal is to draw something like this programmatically.
<center>
<img src="/media/com-piano-draw/piano1.png" width="50%"></img>
</center>

My intention is to make it look similar to a real piano keyboard in which the black keys are not always centered on the dividing lines between the white keys, but slightly offset outwards.

<center>
  <figure style='width:30%'>
    <img src="/media/com-piano-draw/piano6.png" ></img><br>
    <img src="/media/com-piano-draw/piano7.png" ></img>
    <figcaption>src: <a href="http://datagenetics.com/blog/may32016/index.html">datagenetics.com</a></figcaption>
  </figure>
</center>

For reasons why this offset exists, check out these blog articles:
- http://datagenetics.com/blog/may32016/index.html
- http://www.quadibloc.com/other/cnv05.htm

Here, however, I describe my method which just needs to work well enough so that it can be drawn on the screen with javascript.


## Attempt 1: The 7 vs 12 doesn't work
Let's focus on a single octave first (twelve keys in total). We would like the upper part of the keys to be equal width, so we divide the canvas at the top into that many equal parts. The white keys should also have equal width (and there are seven of them), so we divide the canvas into seven equal parts at the bottom.

We use this to try and draw a keyboard. Here, we first draw the white keys (equal width, 7 of them), and then the black keys (divide keyboard into 12, and slot the keys into the resulting positions).

<center>
<canvas id='ex1' style='border:solid 1px;width:100%'></canvas><br>
<script src='/media/com-piano-draw/js/ex1.js'></script>
</center>

We see that this doesn't quite work, because the E key and F key doesn't quite align. This is because we're dividing the octave by 12 and 7. We can represent this as $7W = 12w$, where $W$ is the white key width at the bottom, and $w$ is the width of the keys at the top. From this, $W = (12/7)w$, or $w = (7/12)W$

## Attempt 2: unrealistic symmetric keyboard
Let's forget about the top part of the keys needing to be equal width, and instead draw the black keys centered right in between the white keys.

<center>
<canvas id='ex2' style='border:solid 1px;width:100%'></canvas><br>
<script src='/media/com-piano-draw/js/ex2.js'></script>
</center>

TODO: I like this well-enough, I'll keep it and will return to the article later.

However, for the purposes of my project, piano-draw, the algorithm I used is not pretty, and also need to write an algorithm for full-size keyboard, not just one octave.

<b>TODO</b>
We have a representation like this:
```
const keys = [ 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0 ];
```
where `0` means 'white', and `1` means 'black'. As we see from this, the first key is white, and the second is black. In other words, `keys[0]` is white, `keys[1]` is black. How to relate the index of black keys to the index of white keys? We need this because the black keys position is dependent on the white keys index. For example, key 7 is a black key, and it's between white key 4 and white key 5.

## Attempt 3: symmetric keyboard, multiple octaves

<center>
<canvas id='ex3' style='border:solid 1px;width:100%'></canvas><br>
<script src='/media/com-piano-draw/js/ex3.js'></script>
</center>

## Real piano measurements
### Inspiration
<img src="/media/com-piano-draw/piano2.jpg" width="20%"></img>
<img src="/media/com-piano-draw/piano3.jpg" width="20%"></img>
<img src="/media/com-piano-draw/piano4.jpg" width="20%"></img>
<img src="/media/com-piano-draw/piano5.jpg" width="20%"></img>

TODO: measure my CASIO CTK-3500:
- are white keys equal width (at the bottom)?
- are white keys equal width (at the top)?
- are black keys equal width?

Observations, ideas:
- divide into two sections: C-E and F-B
- C-E section: 3 white keys, 2 black keys; L/3, L/2
- centres:
  - C-E section, white keys:
    - $\frac{1}{3} \div 2 + \frac{1}{3}*0 = \frac{1}{6}$
    - $\frac{1}{3} \div 2 + \frac{1}{3}*1 = \frac{3}{6}$
    - $\frac{1}{3} \div 2 + \frac{1}{3}*2 = \frac{5}{6}$
    - where $\frac{1}{3} \div 2$ is the offset, the middle of the first key, and $\frac{1}{3}$ is the width of the white key



Algorithm from earlier (https://konradp.gitlab.io/piano-practice/)
{{<highlight js>}}
function isWhite(i) {
    let keysWhite = [0,2,4,5,7,9,11];
    if (keysWhite.includes((i) % 12)) {
      return true;
    }
    return false;
}

function drawKey(i, highlight=false) {
  var canvas = document.getElementById('keyboard');
  var ctx = canvas.getContext('2d');
  let width = 12;
  let height = 0;
  let x = 0;
  let keysWhite = [0,2,4,5,7,9,11];
  let j = i-1;
  if (isWhite(j)) {
    // white key
    height = 70;
    x = (keysWhite.indexOf(j%12)*width) + Math.floor(j/12)*7*width;
    ctx.strokeRect(x, 0, width, height);
  } else {
    // black key
    height = 40;
    x = ((width*7)/12)*(i%12-1) + Math.floor(i/12)*width*7;
    width = 8;
    ctx.fillRect(x, 0, width, height);
  }
}

for (let i = 1; i<=88; i++) {
  drawKey(i);
}
{{</highlight>}}
