---
title: "OTHER: Revolut"
date: 2023-01-12
tags: [ 'other' ]
categories: other
---

Bulbank -> Revolut -> Lloyds

Bulbank -> Revolut
- in bank: 7759.03
- 500 GBP = 1140 BGN
- fee: free
- should be after: 7759.03 - 1140 = 6619.03
- but is: 6613.02 BGN (6.01 BGN hidden fee)
- it took 1146.01 BGN
  - currency exchange
- bulbank mobile doesn't update immediately

How much to transfer:
- exchanging on revolut: GBP 499.27
- GBP 3.50 fee for card transfer
- GBP 1.51 fee for bank transfer, 499.27-1.51 = 497.76, 499.27-1.50 = 497.76
- sent on 20230615 14:52

