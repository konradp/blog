---
title: "C++: Application layout"
subtitle: 
date: 2022-11-03
categories: computers
---
While writing the [loop-control](https://gitlab.com/konradp/loop-control) I became dissatisfied with the lack of project structure. In this article I will look at how other C++ programs out there are organised.

**Note:** loop-control is a C++ application for controlling the SooperLooper with a MIDI controller.

{{<toc>}}

## Inspiration: Haiku GUI programs
I really like how Haiku programs are organised, for example the [screenshot app](https://github.com/haiku/haiku/blob/master/src/apps/screenshot/ScreenshotApp.cpp) (simplified):

{{<highlight cpp "linenos=table,hl_lines=4 11 17 23 22-27">}}
#include "ScreenshotApp.h"
#include "ScreenshotWindow.h"

ScreenshotApp::ScreenshotApp()
 :
 BApplication("application/x-vnd.haiku-screenshot")
{
}

void
ScreenshotApp::ArgvReceived(int32 argc, char** argv)
{
  // Parse the arguments
}

void
ScreenshotApp::ReadyToRun()
{
 new ScreenshotWindow(*fUtility, fSilent, fClipboard);
}

int
main()
{
 ScreenshotApp app;
 return app.Run();
}
{{</highlight>}}

I like how simple the `main()` is, and the `ScreenshotApp` class which inherits the `BApplication`. I like how the `BApplication` provides the `ArgvReceived` function which lets us process the arguments, without having to figure out how to pass them through the `int main()`.  

However, I note that this is a GUI application, not a cmd application. I wonder if there are similar patterns for cmd applications. For GUI, I suspect the GUI libraries (Qt or GTK+) will follow a similar pattern to BApplication.

## loop-control overview
My app is:

- commandline
  - so it makes sense to look at other command line apps or daemons
- runs an event loop which waits and processes events, until stopped with CTRL+C
  - so it makes sense to look at daemon apps

## Apps study

### Commandline apps
#### apt and apt-get


## App list and notes

commandline:

- aptitude: https://salsa.debian.org/apt-team/aptitude/-/tree/debian-sid/src
- APT: https://salsa.debian.org/apt-team/apt

daemons:
- incpircd: https://github.com/inspircd/inspircd/blob/insp3/src/inspircd.cpp

Qt based:

- Hydrogen
- Amarok
- Audacious
- Clementine
- MuseScore
- JuK
- LMMS
- Qtractor
- qmmp
- Tomahawk
- MusE
- NoteEdit

GTK+ based:

- XMMS (GTK+)
- Ardour (GTK+)

Other:

- Nightingale (XULRunner)
- Songbird (XULRunner)
- Guayadeque (wxWidgets)
- Audacity (wxWidgets)
- seq24
- DeaDBeeF
- SuperCollider
- Mixxx
- Rosegarden
- MilkyTracker
- amsynth
- Yoshimi
- Zyn-Fusion

Haiku apps:

- cmd: https://github.com/haiku/haiku/blob/master/src/bin/mkfs/main.cpp
- cmd: https://github.com/haiku/haiku/blob/master/src/bin/pkgman/pkgman.cpp
- cmd: https://github.com/haiku/haiku/blob/master/src/bin/keymap/main.cpp


## Reference
- List of software: https://en.wikipedia.org/wiki/List_of_Linux_audio_software
- https://caiorss.github.io/C-Cpp-Notes/
