---
title: "COM: docker-compose: React + NodeJS"
date: 2022-01-10
tags: [ 'wip' ]
categories: wip
---

# Prerequisite: Install NVM
```
VERSION=v0.39.1
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/$VERSION/install.sh | bash
```
**Note:** There might be a newer version than 0.39.1. Find out here:  
https://github.com/nvm-sh/nvm/releases/latest

# Prerequisite: Install latest node
```
nvm list
nvm install node
nvm use node
```
ref: https://github.com/nvm-sh/nvm#usage


# Install create-react-app: OBSOLETE
```
npm install -g create-react-app
```

# Create React App
```
npx create-react-app client
cd app-name
npm start
```

<!--Reference-->
[sudoku wiki]: https://wiki
