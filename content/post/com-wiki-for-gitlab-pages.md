---
title: "COM: Wiki for gitlab pages"
subtitle: 
date: 2021-12-13
tags: [ "wip" ]
categories: wip
---

TODO: GitLab has own wiki for each project. Consider that. Does github have the same?

# Overview
I would like to host a wiki page (browsable and editable documentation). I've worked with Confluence, wiki, and Phabricator wiki frameworks. Can any of these be hosted on gitlab.com? If not, what framework can I use instead? I'd especially like to work with documents organised into a tree-like structure.

# Quickread
My demos:

- [gitbook](https://konradp.gitlab.io/wiki-gitbook/): skipped as can't install it
- [mkdocs](https://konradp.gitlab.io/wiki-mkdocs/)
- ??

Winners:
- ??


{{< toc >}}

# Method
I look at all the examples at https://gitlab.com/pages. It contains gitlab-pages examples for various static-site-serving frameworks (can be a webpage, a blog, a wiki, etc). Out of these, I find out which can be used to serve a wiki page.

Most of these have demo pages which I can visually inspect. If the demo looks like a wiki, I take note of it as a candidate to investigate. If it doesn't, I still check the project's about page.

Wiki framework success criteria:

- organise documents in a **directory-like tree**
- **search**: titles, contents
- easy to **edit documents**: markdown edits, or in-page edits
- extra features: **images**, diagrams, syntax highlighting

# Research: what's available, what is a wiki

{{<table sortable>}}
name/urls                                                           | is a wiki | what is it
---                                                                 | ---       | ---
[brunch](https://gitlab.com/pages/brunch)                           | no        | app writing, pages
[courseware-template](https://gitlab.com/pages/courseware-template) | no        | documents: courses, quizzes
[docusaurus](https://gitlab.com/pages/docusaurus)                   | yes       | wiki
[doxygen](https://gitlab.com/pages/doxygen)                         | no        | documenting c++ programs (python too)
[emacs-reveal](https://gitlab.com/pages/emacs-reveal)               | no        | presentations
[emscripten](https://gitlab.com/pages/emscripten)                   | no        | compile c++ to webassembly
[frozen-flask](https://gitlab.com/pages/frozen-flask)               | no        | static pages in python/flask
[gatsby](https://gitlab.com/pages/gatsby)                           | no        | sites
[gitbook](https://gitlab.com/pages/gitbook)                         | yes       | wiki
[hakyll](https://gitlab.com/pages/hakyll)                           | no        | sites, blogs
[harp](https://gitlab.com/pages/harp)                               | no        | sites
[hexo](https://gitlab.com/pages/hexo)                               | no        | blogs
[hugo](https://gitlab.com/pages/hugo)                               | no        | sites, blogs
[hyde](https://gitlab.com/pages/hyde)                               | no        | sites, blogs
[ikwiki](https://gitlab.com/pages/ikiwiki)                          | maybe     | convert wiki to static pages?
[jekyll](https://gitlab.com/pages/jekyll)                           | no        | sites, blogs
[jigsaw](https://gitlab.com/pages/jigsaw)                           | no        | static sites for PHP/laravel
[jupyterbook](https://gitlab.com/pages/jupyterbook)                 | maybe     | geared towards exe books (python/jupyter), but can do wikis
[lektor](https://gitlab.com/pages/lektor)                           | no        | sites, blogs
[metalsmith](https://gitlab.com/pages/metalsmith)                   | no        | sites, blogs
[middleman](https://gitlab.com/pages/middleman)                     | no        | sites
[mkdocs](https://gitlab.com/pages/mkdocs)                           | yes       | project documentation, wiki
[nanoc](https://gitlab.com/pages/nanoc)                             | no        | sites, blogs
[nikola](https://gitlab.com/pages/nikola)                           | no        | sites, blogs
[nuxt](https://gitlab.com/pages/nuxt)                               | no        | sites, apps using Vue.js
[octopress](https://gitlab.com/pages/octopress)                     | no        | blogs
[org-moge](https://gitlab.com/pages/org-mode)                       | no        | blogs
[pelican](https://gitlab.com/pages/pelican)                         | no        | blogs
[sapper](https://gitlab.com/pages/sapper)                           | no        | apps
[sphinx](https://gitlab.com/pages/sphinx)                           | yes       | wiki
[vuepress](https://gitlab.com/pages/vuepress)                       | maybe     | sites? blogs?
[zim](https://gitlab.com/pages/zim)                                 | maybe     | looks like wiki, and very simple too
{{</table>}}

TODO: readthedocs?

# Four contestants: docusaurus, gitbook, mkdocs, sphinx
I found four frameworks which appear to be wiki pages. We can infer their popularity on gitlab from how many times these gitlab-pages projects have been starred and forked

name       | stars | forks
---        | ---   | ---
[gitbook](https://pages.gitlab.io/gitbook/)       | 139   | 311
[mkdocs](https://pages.gitlab.io/mkdocs/)         | 62    | 160
[sphinx](https://pages.gitlab.io/sphinx/)         | 74    | 131
[docusaurus](https://pages.gitlab.io/docusaurus/) | 25    | 122

We see that in order of pupularity, these are:
- **gitbook: most popular**
- mkdocs
- spkinx
- docusaurus

Here we study each of them. We see that gitbook has most forks and stars, and so we start with gitbook. We expect to do the following:

- setup:
  - fork the gitlab repo, e.g. gitbook -> **wiki-gitbook** (renamed project to wiki-gitbook)
  - run pipeline, inspect results online: https://konradp.gitlab.io/wiki-gitbook
- create wiki:
  - run server locally
  - remove boilerplate (optional)
  - add page tree (many pages organised in a tree)
- publish results:
  - git commit, git push

## gitbook
My demo: https://konradp.gitlab.io/wiki-gitbook

After following the steps from the `README.md` of the forked repo, I saw an error when updating gitbook to the latest version
```
$ gitbook fetch latest
Installing GitBook 2.6.9
/usr/lib/node_modules/gitbook-cli/node_modules/npm/node_modules/graceful-fs/polyfills.js:287
      if (cb) cb.apply(this, arguments)
                 ^

TypeError: cb.apply is not a function
    at /usr/lib/node_modules/gitbook-cli/node_modules/npm/node_modules/graceful-fs/polyfills.js:287:18
    at FSReqCallback.oncomplete (node:fs:199:5)

Node.js v17.3.0
```
similarly when trying to serve it locally
```
$ gitbook serve
Installing GitBook 3.2.3
/usr/lib/node_modules/gitbook-cli/node_modules/npm/node_modules/graceful-fs/polyfills.js:287
      if (cb) cb.apply(this, arguments)
                 ^

TypeError: cb.apply is not a function
    at /usr/lib/node_modules/gitbook-cli/node_modules/npm/node_modules/graceful-fs/polyfills.js:287:18
    at FSReqCallback.oncomplete (node:fs:199:5)

Node.js v17.3.0
```
As this means I can't test my changes locally, I'm going to skip the gitbook for now.

## mkdocs
My demo: https://konradp.gitlab.io/wiki-mkdocs/
### CI/CD problem

After forking the project from gitlab, nothing happened. No CI/CD pipeline was triggered. I had to make a commit to trigger it. TODO: Next time, see in gilab the 'Auto DevOps: Default to Auto DevOps pipeline' option. Tick it, and see if anything happens after forking.  
It worked after this.

### Install
I followed the https://www.mkdocs.org/getting-started/, with some adjustments:
```
sudo -H pip3 install mkdocs
```
and then served the page:
```
mkdocs serve
```
and the wiki was accessible at [http://localhost:8000/mkdocs/](http://localhost:8000/mkdocs/)

### Edits
I edited the `docs/index.md` file, and the browser would immediately refresh and show my edit. Good.

### Folders
Adding folders is really easy

### Search
It works great, but there is one gotcha:

### Themes
As per https://www.mkdocs.org/user-guide/choosing-your-theme/, two themes are available:

- mkdocs (supports all features of mkdocs)
- readthedocs (only two levels of navigation are supported <- this is true, the navbar renders wrong otherwise)

TODO:
- hide sidebar

- search for page title: It will be displayed twice

### Many dirs
Having many directories makes the header navbar overwhelmed really quickly.

![png](/media/com-wiki-for-gitlab-pages/many_folders.png)


# Four other frameworks: ikwiki, jupyterbook, vuepress, zim

## docusaurus
