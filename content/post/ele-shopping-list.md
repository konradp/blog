---
title: "ELEC: Shopping list"
date: 2023-06-01
tags: [ 'other' ]
categories: other
---

Table of contents:
{{<toc>}}

Shop: [elimex.bg](https://elimex.bg)

## Shopping list

Must:
- starter kit
- mono audio cables for CRAVE

Maybe:
- multimeter
- audio jack sockets

Later:
- soldering iron (+accessories: sponge, flux, soldering tin) поялник
  - station: 39.90BGN - 45.90BGN
- veroboard
- how much is an oscilloscope
- project box/metal case

## Search notes
### Starter kit
I want a starter kit with breadboard and:
  - breadboard
  - USB power
  - potentiometer
  - resistors
  - capacitors: 2 types (кондензатори)
  - transistors
  - diodes
  - 555 timer
  - jump cables
Type in search: макетна  

Others to check
Link: https://elimex.bg/category/870-hobi-elektronika

- КИТ - НАБОРИ ЕЛИМЕКС АУДИО          KIT - SETS ELIMEX AUDIO
  - 97 items
- КИТ - СТАРТЕР КИТ MICROCHIP         KIT - STARTER KIT MICROCHIP
  - 34 items
- КИТ - НАБОРИ ЕЛИМЕКС ЗА НАЧИНАЕЩИ   KIT - ELIMEX SETS FOR BEGINNERS
  - 35 items
  - set of resistors
  - nice, but not what I'm looking for

### Soldering iron
Excluding:
- single-cable, no temp control irons
- gun irons
- desoldering kits

Focusing on:
- soldering stations

Link:
- https://elimex.bg/category/1264-poyalni-stantsii




## Project ideas
- bazz fuss
- octavia
- 555 oscillator
- spring reverb
- tremolo
