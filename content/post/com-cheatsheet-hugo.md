---
title: "hugo"
subtitle: 
date: 2019-12-22
tags: [ "cheatsheets" ]
math: true
categories: cheatsheets
---

{{<toc>}}

## Useful links
- To get `<table class="some-class"></table>` in Hugo:  
  https://zwbetz.com/style-a-markdown-table-with-bootstrap-classes-in-hugo/
- Latex in hugo  
  https://mertbakir.gitlab.io/hugo/math-typesetting-in-hugo/  
  `$x = {-b \pm \sqrt{b^2-4ac} \over 2a}$`  
  gives  
  $x = {-b \pm \sqrt{b^2-4ac} \over 2a}$  

## More
```
<\!--more-->
```
**Note:** Remove the backslash

## `config.toml`
Enable javascript and inline HTML  
```
[markup.goldmark.renderer]
unsafe = true
```
Fix `/static` dir
```
canonifyURLs = true
```
TOC all levels  
  ```
[markup]
  [markup.tableOfContents]
    endLevel = 5
    ordered = false
    startLevel = 2
```

## Shortcodes
### table of contents
`layouts/shortcodes/toc.html`
```
<div>
  {{ .Page.TableOfContents }}
</div>
```

### table sortable
`
layouts/shortcodes/table.html`
```
{{ $htmlTable := .Inner | markdownify }}
{{ $class := .Get 0 }}
{{ $old := "<table>" }}
{{ $new := printf "<table class=\"%s\">" $class }}
{{ $htmlTable := replace $htmlTable $old $new }}
{{ $htmlTable | safeHTML }}
```

`static/js/sorttable.js` contents of:

- https://www.kryogenix.org/code/browser/sorttable/sorttable.js

`layouts/partials/head_custom.html`

```
<script src="{{ "/js/sorttable.js" | relURL }}"></script>
```

use as:
**&lbrace;&lbrace;\<table sortable>&rbrace;&rbrace;**


### ticks
images:

- v: {{<v>}} [tick.svg](https://gitlab.com/konradp/blog/-/blob/master/content/media/icons/tick.svg)
- x: {{<x>}} [cross.svg](https://gitlab.com/konradp/blog/-/blob/master/content/media/icons/cross.svg)
- w: {{<w>}} [warning.svg](https://gitlab.com/konradp/blog/-/blob/master/content/media/icons/warning.svg)

`layouts/shortcodes/v.html`
```
<style>
.icon {
  width: 2ch;
  height: 2ch;
}
</style>
<img class="icon" src="/icons/tick.svg" />
```

`layouts/shortcodes/x.html`
```
<style>
  .icon {
      width: 2ch;
      height: 2ch;
  }
</style>
<img class="icon" src="/icons/cross.svg" />
```

`layouts/shortcodes/w.html`
```
<style>
  .icon {
      width: 2ch;
      height: 2ch;
  }
</style>
<img class="icon" src="/icons/warning.svg" />
```



## CSS
In `/static/css/style.css`
https://gitlab.com/konradp/blog-fin/-/blob/main/static/css/style.css


## Posts per category
ref: [austingebauer/devise theme](https://github.com/austingebauer/devise/blob/main/layouts/partials/category-posts.html)

layouts/_default/list.html
```
{{ partial "header.html" . }}

{{if not .IsHome }}
<h1>{{ .Title | markdownify }}</h1>
{{ end }}

{{ .Content }}

{{ range $key, $taxonomy := .Site.Taxonomies.categories.Alphabetical }}
  <h5><span class="category">{{ .Name | humanize }}</span></h5>
  <ul>
    {{ range $taxonomy.Pages }}
      <li>
        {{ .Date.Format "20060102" }}
        <a href="{{ .Permalink }}">{{ .Title }}</a>
      </li>
    {{ end }}
  </ul>
{{ end }}

{{ partial "footer.html" . }}
```
Then, in the blog article have:
```
categories: "some category"
```

## Theme-specific
### xmin
