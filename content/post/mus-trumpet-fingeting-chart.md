---
title: "MUS: Trumpet fingering chart"
date: 2019-05-27
tags: [ 'music' ]
categories: music
---

Trumpet fingering chart including alternative fingerings.
<!--more-->


# Trumpet fingering chart
![png](/media/trumpet-fingering-chart/trumpet-fingering-chart-konrad.png)  
Formats for printing:  
PDF:
[pdf](/media/trumpet-fingering-chart/trumpet-fingering-chart-konrad.pdf)  
PNG: 
[png](/media/trumpet-fingering-chart/trumpet-fingering-chart-konrad-highres.png)
