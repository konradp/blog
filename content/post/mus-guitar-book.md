---
title: "MUS: Guitar book"
subtitle: 
date: 2023-03-13
tags: [ 'wip' ]
categories: wip
---
<script src='https://konradp.gitlab.io/chords-guitar/js/chords-guitar.js'></script>
<script src='/media/mus-guitar-book/basic-chords.js'></script>
<style>
.box    { width: 1em; height: 1em; display: inline-block; }
.black  { background-color: #000000; }
.red    { background-color: #cd001a; }
.orange { background-color: #ef6a00; }
.yellow { background-color: #f2cd00; }
.green  { background-color: #79c300; }
.blue   { background-color: #1961ae; }
.purple { background-color: #61007d; }
</style>

{{<toc>}}

## Notes
- If not specified, 'minor' means 'natural minor'.
- The diagrams here might appear unusual. This is because I play left-handed guitar
- colours, for each scale note:
    - <div class='box black'></div> 1st
    - <div class='box red'></div> 2nd
    - <div class='box orange'></div> 3rd
    - <div class='box yellow'></div> 4th
    - <div class='box green'></div> 5th
    - <div class='box blue'></div> 6th
    - <div class='box purple'></div> 7th

## Guitar setup

<head>
<style>
.small {
  width: 19%;
}
</style>
</head>

### My guitar
I have a Fender Player LH, with fingerboard radius 9.5".

<center>
Action and neck relief

measurement | method     | treble E | bass E
:--         | :--        | :--      | :--
action      | 12th       | 1.5      | 1.75
action      | 17th+capo1 | 1.5      | 1.5
action      | 17th       | ?        | ?
relief      | 8th        | ?        | ?
pickup neck | ?          | ?        | ?
pickup mid  | ?          | ?        | ?
pickup bdg  | ?          | ?        | ?
</center>


### Action
Action is adjusted with saddles. It's measured either at:

- 12th: measure at 12th fret
- 17th+capo1: capo on first fret, measure at 17th fret
- 17th: measure at 17th fret

source      | method            | neck radius | bass E   | treble E
---         | ---               | ---         | ---      | ---
Fender      | 17th              | `7.25"`     | `2.000`| `1.600`
Fender      | 17th              | `9.5"-12"`  | `1.600`| `1.600`
Fender      | 17th              | `15"-17"`   | `1.600`| `1.200`
Paul Davids | 12th or 17th+capo1| ---         | `1.500`| `1.000`
Dunlop      | 12th              | ---         | `1.524`| `1.016`

**Note:** Measurements in mm.


### Neck relief
Fender method:

- capo at first fret
- press on last fret or where the neck joins the body (often the 17th fret)
- measure at 8th fret
- do both E strings

neck radius     | relief
---             | ---
`7.25"`         | 0.30
**`9.5"-12"`**  | **0.25**
`15"-17"`       | 0.20

**Note:** Measurements in mm.  
**Note:** Paul Davids doesn't mention a specific measure, instead check that there is a small gap by tapping the string.


### References
- [Fender action](https://fendercustomersupport.microsoftcrmportals.com/en-us/knowledgebase/article/KA-01901)
- [Fender relief 1](https://fendercustomersupport.microsoftcrmportals.com/en-us/knowledgebase/article/KA-01901)
- [Fender relief 2](https://www.fender.com/articles/how-to/how-to-measure-neck-relief-on-guitar-or-bass)
- [Fender pickups height](https://fendercustomersupport.microsoftcrmportals.com/en-us/knowledgebase/article/KA-01901)
- Dunlop system 64 string action gauge


## Intro

### Sound waves and frequency: Hertz
Sound is a **vibration of air** which our ears can detect. How 'quickly' the air vibrates (oscillation of air pressure) is measured in [Hertz (Hz)](https://en.wikipedia.org/wiki/Hertz). The Hertz is a measure of frequency (how often something happens related to time). We define Hertz as one cycle per second, so an event happening once per second has a frequency of 1Hz. An event happening twice per second has a frequency of 2Hz; if I clap my hands three times per second, I'm clapping at 3Hz; and so on.

We perceive different sound frequencies as **pitch**, and we can typically perceive frequencies between 20Hz to 16,000Hz. To test this, try out any tone generator, e.g. this one https://www.szynalski.com/tone-generator/. You will notice that from around 20-30Hz, you will likely start hearing a musical pitch.

Notes, exercises:
- In the tone generator, try listening to different sound waves, e.g. sine, square, triangle, sawtooth (**Warning:** watch out for the volume, some waves will sound louder than others).
- You will notice that some waves (square, sawtooth) produce a clicking sound, which around 30Hz becomes a pitch. I conclude that if I could clap my hands fast enough, I'd be able to produce a musical note.

### Octave
We're now ready to introduce an **octave**. Take a frequency and double it, e.g. 220Hz and 440Hz. Both notes will have a similar quality, but one is audibly higher than the other (use the tone generator again). We say that such two tones are an octave apart, and we give them the same name (note 'A', in this case).

To see this on the guitar, play an open string, and then play the same string pressed on the 12th fret (this works on any string). By doing this we are halving the length of the string, which doubles the frequency at which it vibrates - it's the same note, but an octave higher.

**TODO:** Discuss harmonics here or not yet?

<center>
Octave: open string vs 12th fret
<img src='/media/mus-guitar-book/octave_12th.png'/><br>
</center>

### 12-TET and chromatic scale
We divide that octave into 12 separate notes, and give them names: A, A#, B, C, C#, D, D#, E, F, F#, G, G# (after G#, the pattern repeats).

**Note:** An octave is not 'equally' divided into twelve notes, but it is rather done according to a formula, which you can see on [wiki](https://en.wikipedia.org/wiki/Piano_key_frequencies). This reflected by the fact that the distance between adjacent frets on the guitar is not the same across the neck - the frets become closer to each other as we move up the guitar neck.

**TODO:** Derive the formula for note frequency

### Standard tuning
Standard guitar tuning is E2, A2, D3, G3, B3, E4 (E A D G B E). You can read more about standard and alternative tunings on [wiki](https://en.wikipedia.org/wiki/Guitar_tunings). Observations which arise from this tuning:

- Fret a string (E, A, D, B) on the fifth (5) fret. This note is the same as the next(thinner) open string
- Fret the G string on the fourth (4) fret. This note is the same as the next open string (B)
- The thinnest and thickest strings are both E, but separated by two octaves (E2 and E4). So thick E on the first fret is going to be the same note (two octaves higher) as the thin E on the first fret, and it's true for all frets
- You can generally play the same note in more than one place (for example the first and second point above)

These are not all, only the most obvious to me. So here are all open strings color-coded, and their corresponding notes and octaves across the entire fretboard:
<center>
  <img src='/media/mus-guitar-book/open_strings_octaves.png'/>
</center>
This is not actually that helpful, and not very easy to remember, so I chunk this data into more memorable patterns.

### Octave patterns
These are the octave patterns (or shapes) which I have memorised. When we move them up and down the neck (left and right really), the notes change, but the interval between the two notes is always an octave.

<center>
  <img src='/media/mus-guitar-book/octave_0_1.png'/>
</center>
<center>
  <img src='/media/mus-guitar-book/octave_1_1.png'/>
  <img src='/media/mus-guitar-book/octave_1_2.png'/>
  <img src='/media/mus-guitar-book/octave_1_3.png'/>
  <img src='/media/mus-guitar-book/octave_1_4.png'/>
</center>
<center>
  <img src='/media/mus-guitar-book/octave_2_1.png'/>
  <img src='/media/mus-guitar-book/octave_2_2.png'/>
  <img src='/media/mus-guitar-book/octave_2_3.png'/>
</center>
<center>
  <img src='/media/mus-guitar-book/octave_3_1.png'/>
</center>

I find these very useful, and they appear in many chords too.

**Note:** For an example of one of these patterns being used in a song, check out:
- Stevie Ray Vaughan - Chitlins con carne, 


## Chords
### Basic chords

<center>
<div id='app-basic-chords'>
<canvas id='canvas-basic-chords'></canvas>
</div>
</center>
<hr>

### Chord composition
TODO: This should be later, as is uses info about scales.

Consider a C chord (C major):  

<center>

![](/media/mus-guitar-chords/c_maj.png)

![](/media/mus-guitar-chords/c_maj_2.png)
</center>

They are both composed of I-III-V notes (C-E-G) (red-green-blue), but in different orders:

one | one | two | two
--- | --- | --- | ---
C   | I   | C   | I
E   | III | G   | V
G   | V   | C   | I
C   | I   | E   | III
E   | III |

The second chord is close to the 2nd inversion:

- 1st inversion: I III V
- 2nd inversion: III V I
- 3rd inversion: V I III


### Movable chord patterns
Notice in the above that the E and F chords have the same shape, just moved up the neck by a fret. There are a few shapes like this which we can memorise.

- black: 1
- green: 3
- blue: 5

#### Major
<center>
  <img src='/media/mus-guitar-book/chord_patterns/maj1.png'/>
  <img src='/media/mus-guitar-book/chord_patterns/maj2.png'/>
  <img src='/media/mus-guitar-book/chord_patterns/maj3.png'/>
  <img src='/media/mus-guitar-book/chord_patterns/maj4.png'/>
  <img src='/media/mus-guitar-book/chord_patterns/maj5.png'/>
</center>

#### Minor

#### Major triads
#### Minor triads


<!------------------ SCALES ------------------>
## Scales
Intervals:

- major: whole, whole, half, whole, whole, whole, half
- minor: whole, half, whole, whole, half, whole, whole

in short:

- major: 2, 2, 1, 2, 2, 2, 1
- minor: 2, 1, 2, 2, 1, 2, 2


<center>
C major scale<br>
<img src='/media/mus-guitar-book/cmaj_all.png'/><br>
</center>

### Pentatonic
major pentatonic: 1, 2, 3, 5, 6

<center>
C major pentatonic<br>
<img src='/media/mus-guitar-book/cmaj_penta.png'/>
</center>


<!--<hr>
C major boxes
<center>
<img src='/media/mus-guitar-book/cmaj_box5.png'/>
<img src='/media/mus-guitar-book/cmaj_box4.png'/>
<img src='/media/mus-guitar-book/cmaj_box3.png'/>
<img src='/media/mus-guitar-book/cmaj_box2.png'/>
<img src='/media/mus-guitar-book/cmaj_box1.png'/>
</center>-->

<hr>
C major pentatonic: positions
<div style='text-align:left;width:100%;'>
<!-- plus penta shapes -->
<img class='small' src='/media/mus-guitar-book/shapes/cmaj_penta_p2.png'/>
<img class='small' src='/media/mus-guitar-book/shapes/cmaj_penta_p1.png'/>
<img class='small' src='/media/mus-guitar-book/shapes/cmaj_penta_0.png' style='border:5px solid'/>
<!-- minus penta shapes -->
<img class='small' src='/media/mus-guitar-book/shapes/cmaj_penta_m1.png'/>
<img class='small' src='/media/mus-guitar-book/shapes/cmaj_penta_m2.png'/>
<br>
<img class='small'/>
<img class='small' src='/media/mus-guitar-book/cmaj_penta9.png'/>
<img class='small' src='/media/mus-guitar-book/cmaj_penta8.png'/>
<img class='small' src='/media/mus-guitar-book/cmaj_penta7.png'/>
<img class='small' src='/media/mus-guitar-book/cmaj_penta6.png'/>
</div>

I notice that there are five pentatonic shapes/positions. I call them:

- 0/main shape/position: the highlighted one
- -1 shape/position: from fret 2 to fret 5
- -2 shape/position: from fret 0 to fret 3
- +1 shape/position: from fret 7 to fret 10
- +2 shape/position: from fret 9 to fret 13
- the higher positions are the same shapes, just an octave higher so I don't name them


### Major vs minor
Recall the major and minor intervals:

- major: 2, 2, 1, 2, 2, 2, 1
- minor: 2, 1, 2, 2, 1, 2, 2

From these, we can construct a major and a minor scale:
<center>
Cmaj & cmin<br>
<img class='small' src='/media/mus-guitar-book/scales/cmaj1.png'/>
<img class='small' src='/media/mus-guitar-book/scales/cmin1.png'/>
</center>

But notice that they are the same intervals, just shifted:
<center>
<img src='/media/mus-guitar-book/scales/maj_min.png'/>
</center>

So, if you start playing C major scale, CDEFGAB, once we get to the sixth note (A), this can be considered as a start of a minor scale (A min). And so the notes of the C major scale (CDEFGAB) will coincide with the notes of the A min scale (ABCDEFG). We say that A min is a relative minor of C major. This means that we only need to learn one scale on the fretboard.


<center>
C major scale<br>
<img src='/media/mus-guitar-book/scales/cmaj_scale.png' style='width:80%'/><br>
C minor scale<br>
<img src='/media/mus-guitar-book/scales/cmin_scale.png' style='width:80%'/><br>
</center>

observations:

- unchanged notes: 1, 2, 4, 5 (black, red, yellow, green)
- changed notes (lowered for minor): 3, 6, 7 (orange, blue, purple)

Another observation is that the shape is the same, just shifted:

<center>
C major scale<br>
<img src='/media/mus-guitar-book/scales/cmaj_scale_box.png' style='width:80%'/><br>
C minor scale<br>
<img src='/media/mus-guitar-book/scales/cmin_scale_box.png' style='width:80%'/><br>
</center>

Notice also that this gives an easy way of remembering the major and minor pentatonic.


#### Pentatonic

- major: 1, 2, 3, 5, 6 (black, red, orange, green, blue)
- minor: 1, 3, 4, 5, 7 (black, orange, yellow, green, purple)

<center>
C major pentatonic<br>
<img src='/media/mus-guitar-book/scales/cmaj_penta.png' style='width:80%'/><br>
C minor pentatonic<br>
<img src='/media/mus-guitar-book/scales/cmin_penta.png' style='width:80%'/><br>
</center>

#### Pentatonic shapes on major/minor scale
You can play the pentatonic 'shape' in various places and it will still coincide with the 8-note major/minor scale.


**Pentatonic shape 0**
<center>
<img class='small' src='/media/mus-guitar-book/shapes/cmaj_penta_0.png' style='border:5px solid red'/><br>
C major scale<br>
<img src='/media/mus-guitar-book/scales/cmaj_scale_penta_box.png' style='width:80%'/><br>
C minor scale<br>
<img src='/media/mus-guitar-book/scales/cmin_scale_penta_box.png' style='width:80%'/><br>
</center>


**Pentatonic shape -1**
<center>
<img class='small' src='/media/mus-guitar-book/shapes/cmaj_penta_m1.png' style='border:5px solid red'/><br>
C major scale<br>
<img src='/media/mus-guitar-book/scales/cmaj_scale_penta_box_m1.png' style='width:80%'/><br>
C minor scale<br>
<img src='/media/mus-guitar-book/scales/cmin_scale_penta_box_m1.png' style='width:80%'/><br>
</center>


**Pentatonic shape -2**
<center>
<img class='small' src='/media/mus-guitar-book/shapes/cmaj_penta_m2.png' style='border:5px solid red'/><br>
C major scale<br>
<img src='/media/mus-guitar-book/scales/cmaj_scale_penta_box_m2.png' style='width:80%'/><br>
C minor scale<br>
<img src='/media/mus-guitar-book/scales/cmin_scale_penta_box_m2.png' style='width:80%'/><br>
</center>


**Pentatonic shape +1**
<center>
<img class='small' src='/media/mus-guitar-book/shapes/cmaj_penta_p1.png' style='border:5px solid red'/><br>
C major scale<br>
<img src='/media/mus-guitar-book/scales/cmaj_scale_penta_box_p1.png' style='width:80%'/><br>
C minor scale<br>
<img src='/media/mus-guitar-book/scales/cmin_scale_penta_box_p1.png' style='width:80%'/><br>
</center>


**Pentatonic shape +2**
<center>
<img class='small' src='/media/mus-guitar-book/shapes/cmaj_penta_p2.png' style='border:5px solid red'/><br>
C major scale<br>
<img src='/media/mus-guitar-book/scales/cmaj_scale_penta_box_p2.png' style='width:80%'/><br>
C minor scale<br>
<img src='/media/mus-guitar-book/scales/cmin_scale_penta_box_p2.png' style='width:80%'/><br>
</center>


### Scale positions
#### C major

<center>
<img class='small' src='/media/mus-guitar-book/scales/cmaj_pos1.png'/>
<img class='small' src='/media/mus-guitar-book/scales/cmaj_pos2.png'/>
<img class='small' src='/media/mus-guitar-book/scales/cmaj_pos3.png'/><br>
<img class='small' src='/media/mus-guitar-book/scales/cmaj_pos4.png'/>
<img class='small' src='/media/mus-guitar-book/scales/cmaj_pos5.png'/>
<img class='small' src='/media/mus-guitar-book/scales/cmaj_pos6.png'/><br>
<img class='small' src='/media/mus-guitar-book/scales/cmaj_pos7.png'/>
<img class='small' src='/media/mus-guitar-book/scales/cmaj_pos8.png' style='border:1px solid'/>
<img class='small' src='/media/mus-guitar-book/scales/cmaj_pos9.png'/><br>
<img class='small' src='/media/mus-guitar-book/scales/cmaj_pos10.png'/>
<img class='small' src='/media/mus-guitar-book/scales/cmaj_pos11.png'/>
<img class='small' src='/media/mus-guitar-book/scales/cmaj_pos12.png'/><br>
<img class='small' src='/media/mus-guitar-book/scales/cmaj_pos13.png'/>
</center>

#### C minor
We already have practically seen all the shapes we need from C major. The relative minor of C major is A minor. C minor is three semitones above A minor. So C minor is the same pattern as above, but three semitones up. I include them here anyway, because the starting note is different than on the major shapes.

<center>
<img class='small' src='/media/mus-guitar-book/scales/cmin_pos1.png'/>
<img class='small' src='/media/mus-guitar-book/scales/cmin_pos2.png'/>
<img class='small' src='/media/mus-guitar-book/scales/cmin_pos3.png'/><br>
<img class='small' src='/media/mus-guitar-book/scales/cmin_pos4.png'/>
<img class='small' src='/media/mus-guitar-book/scales/cmin_pos5.png'/>
<img class='small' src='/media/mus-guitar-book/scales/cmin_pos6.png'/><br>
<img class='small' src='/media/mus-guitar-book/scales/cmin_pos7.png'/>
<img class='small' src='/media/mus-guitar-book/scales/cmin_pos8.png'/>
<img class='small' src='/media/mus-guitar-book/scales/cmin_pos9.png'/><br>
<img class='small' src='/media/mus-guitar-book/scales/cmin_pos10.png'/>
<img class='small' src='/media/mus-guitar-book/scales/cmin_pos11.png'/>
<img class='small' src='/media/mus-guitar-book/scales/cmin_pos12.png'/><br>
<img class='small' src='/media/mus-guitar-book/scales/cmin_pos13.png' style='border:1px solid'/>
</center>

Exercises:

- play major vs minor for each pattern
- derive pentatonic from each pattern

## Modes
Modes:

- ionian (major scale)
- dorian (like natural minor, 6th is major VIth instead of minor VIth)
- phrygian (like natural minor, but 2nd is minor IInd, not major)
- lydian (like ionian, 4th sharp)
- mixolydian (like major, but 7th flattened VIIth, not major VIth)
- aeolian (natural minor scale)
- locrian (five notes flattened half step)

Major/minor

mode | type
--- | ---
ionian     | major
dorian     | minor
phrygian   | minor
lydian     | major
mixolydian | major
aeolian    | minor
locrian    |

### Notes
Modes of C

mode | notes | scale
--- | --- | ---
ionian     | C, D, E, F, G, A, B, C       | C
dorian     | C, D, Eb, F, G, A, Bb, C     | 
phrygian   | C, Db, Eb, F, G, Ab, Bb, C   | 
lydian     | C, D, E, F#, G, A, B, C      | 
mixolydian | C, D, E, F, G, A, Bb, C      | 
aeolian    | C, D, Eb, F, G, Ab, Bb, C    | 
locrian    | C, Db, Eb, F, Gb, Ab, Bb, C  | 

Notes
<div class="equal-table">

mod | C   | Db  | D   | Eb  | E   | F   | Gb  | G   | Ab  | A   | Bb  | B   | C
:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:
io  | X   | -   | X   | -   | X   | X   | -   | X   | -   | X   | -   | X   | X
do  | X   | -   | X   | X   | -   | X   | -   | X   | -   | X   | X   | -   | X
ph  | X   | X   | -   | X   | -   | X   | -   | X   | X   | -   | X   | -   | X
ly  | X   | -   | X   | -   | X   | -   | X   | X   | -   | X   | -   | X   | X
mi  | X   | -   | X   | -   | X   | X   | -   | X   | -   | X   | X   | -   | X
ae  | X   | -   | X   | X   | -   | X   | -   | X   | X   | -   | X   | -   | X
lo  | X   | X   | -   | X   | -   | X   | X   | -   | X   | -   | X   | -   | X
</div>

Looking at ionian intervals:  
2 2 1 2 2 2 1

mod | intervals
--- | ---
io  | `2 2 1 2 2 2 1`
do  | `2 1 2 2 2 1 2`
ph  | `1 2 2 2 1 2 2`
ly  | `2 2 2 1 2 2 1`
mi  | `2 2 1 2 2 1 2`
ae  | `2 1 2 2 1 2 2`
lo  | `1 2 2 1 2 2 2`


```
2 2 1 2 2 2 1  2 2 1 2 2 2 1
2 1 2 2 2 1 2    2 1 2 2 2 1 2
1 2 2 2 1 2 2      1 2 2 2 1 2 2
2 2 2 1 2 2 1        2 2 2 1 2 2 1
2 2 1 2 2 1 2          2 2 1 2 2 1 2
2 1 2 2 1 2 2            2 1 2 2 1 2 2
1 2 2 1 2 2 2              1 2 2 1 2 2 2
```

Reordered
<div class="equal-table">

mod | C   | Db  | D   | Eb  | E   | F   | Gb  | G   | Ab  | A   | Bb  | B   | C
:---|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:
io  | X   | -   | X   | -   | X   | X   | -   | X   | -   | X   | -   | X   | X
do  | X   | -   | X   | X   | -   | X   | -   | X   | -   | X   | X   | -   |
ph  | X   | X   | -   | X   | -   | X   | -   | X   | X   | -   | X   | -   | X
ly  | X   | -   | X   | -   | X   | -   | X   | X   | -   | X   | -   | X   | X
mi  | X   | -   | X   | -   | X   | X   | -   | X   | -   | X   | X   | -   | X
ae  | X   | -   | X   | X   | -   | X   | -   | X   | X   | -   | X   | -   | X
lo  | X   | X   | -   | X   | -   | X   | X   | -   | X   | -   | X   | -   | X
</div>


<br><br><br><br>

## Ideas, notes, TODO
- guitar setup
- tuning
- blue backing track: What is blues in C? minor or major?
- circle of fifths
- harmonics
- modes?
- notes outside of the scale
- blue note
- major vs minor pentatonic
    - +backing example
- chords:
    - major/minor
    - open chords
    - my special chords
- chord progressions


