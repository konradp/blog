---
title: "MUS: Behringer crave notes"
subtitle: 
date: 2023-06-05
categories: wip
---
Notes from Behringer crave tutorials
{{< toc >}}

## Patchbay inputs
- **OSC CV**: control voltage (pitch)
- **OSC FM**: this needs rapidly changing voltage, e.g. sin
- OSC MOD
- VCF CUTOFF
- VCF RES
- MIX1
- MIX1
- VC MIX
  - when nothing patches to VC MIX, then VC MIX knob outputs 0-5V
- MULTIPLE
- MIX CV
- EXT AUDIO
- TEMPO
- PLAY/STOP
- RESET
- HOLD
- ENV GATE
- VCA CV: envelope
- LFO RATE
- VC MIX knob, MIX1, MIX2:



## References
- [Music with Eric](https://www.youtube.com/watch?v=GBWxZ2BqZF8&list=PLUJ4WAICXF1tNKm15WDlrfQecWc6Zzh7K&pp=iAQB)
