---
title: "COM: MERN stack"
date: 2022-02-01
tags: [ 'wip' ]
categories: wip
---
TODO:

- intro: Describe what this actually achieves
- maybe rename

Table of contents
{{<toc>}}

Create project dir.
```
mkdir myapp
cd myapp
git init
echo node_modules > .gitignore
```

## Server
```
mkdir server
cd server
npm init -y
npm install express mongodb dotenv
```

## Client: React
### Init and cleanup
Init.
```
npx create-react-app client
cd client
npm start
```

Remove everything except index.js and App.js (which we will edit).
```
cp src/index.js /tmp/
cp src/App.js /tmp/
rm src/*
mv /tmp/index.js src/
mv /tmp/App.js src/
```

Edit the App.js to no longer include what's been removed. It will look like this.  
```
# client/src/App.js
function App() {
  return (
    <div>
      Hello World.
    </div>
  );
}

export default App;
```

Edit the index.js in a similar way (remove the CSS, and reportWebVitals) to get:
```
# client/src/index.js
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
```

### Routes, components, and navbar
#### Routes
Install react-router-dom.
```
npm install react-router-dom
```

Edit index.js to add BrowserRouter.
```
# client/src/index.js
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);
```

Edit App.js to add some components and routing for them.
```
# client/src/App.js
import { Routes, Route } from 'react-router-dom';
import ServerList from "./components/serverList";

function App() {
  return (
    <div>
      <Routes>
        <Route exact path="/" element={<ServerList />} />
        <Route exact path="/server" element={<ServerList />} />
        <Route exact path="/server/:id" element={<Server />} />
      </Routes>
    </div>
  );
}

export default App;
```

#### Components
The page will crash because the components don't yet exist. Create them.
```
mkdir src/components
touch src/components/vmList.js
touch src/components/serverList.js
touch src/components/server.js
```
with contents
```
$ cat src/components/vmList.js 
export default function VmList() {
  return (
    <div>
      <h3>VMs</h3>
    </div>
  );
}
$ cat src/components/serverList.js 
export default function ServerList() {
  return (
    <div>
      <h3>Servers</h3>
    </div>
  );
}
$ cat src/components/server.js 
export default function Server() {
  return (
    <div>
      <h3>Server</h3>
    </div>
  );
}
```

#### Navbar
Here we finally use the Material UI, the 'App Bar' component (ref: https://mui.com/components/app-bar/).  
Install mui (Material UI).
```
npm install @mui/material @mui/icons-material @emotion/react @emotion/styled
```

## Reference
- https://www.mongodb.com/languages/mern-stack-tutorial  
  Used this for a general idea but using Material UI instead of bootstrap, plus other changes
- https://reactrouter.com/
- https://reactrouter.com/docs/en/v6/getting-started/tutorial
- https://v5.reactrouter.com/web/guides/primary-components
