---
title: "OTHER: Shopping list: things I need + present ideas"
date: 2024-03-20
categories: other
---
This is a list of things that I need.

- Conan book in Polish, from Amber series
- bong
- car
- keyboard: yamaha pss-780E
- synth: roland phantom 9
- casette player/radio: ideally USB-charged
- disk storage: Sone NAS or usb disk for backups and data storage, check: Synology diskstation DSS22+, QNAP TS-251D-4G-US (has HDMI)
- multi track mixer: not clear if I need it, but might be nice to have. A mixer which outputs multiple channels over USB instead of just L/R
