---
title: 'Tech for work'
subtitle: 
date: 2022-08-30
categories: computers
---
IaS (Infrastructure as code):

- Ansible
- Chef
- Puppet
- Terraform

# Cloud
- AWS
- Docker / docker-compose
- GCP
- Kubernetes
- OCI: Oracle Cloud Infrastructure  
  https://www.oracle.com/cloud/

## Oracle cloud
Get free account at https://www.oracle.com/cloud/free/.  
What you get is listed here: https://www.oracle.com/cloud/free/#always-free  
The sign up process fails like below so gave up on Oracle Cloud.

![](/media/com-work-tech/oracle-cloud-verify.png)

# AWS
Connect to EC2 instance.
```
$ ssh -i ./testkeypair.pem ec2-user@ec2-34-230-72-34.compute-1.amazonaws.com

       __|  __|_  )
       _|  (     /   Amazon Linux 2 AMI
      ___|\___|___|

https://aws.amazon.com/amazon-linux-2/
3 package(s) needed for security, out of 7 available
Run "sudo yum update" to apply all updates.
[ec2-user@ip-172-31-88-250 ~]$ 
```


# Other

- FAW: Oracle Function Analytics Warehouse ([link](https://docs.oracle.com/en/cloud/saas/analytics/22r2/fawag/oracle-fusion-analytics-warehouse.html))  
  mostly dashboards, a bit like ServiceNow
- Load balancer
- Packer
- Firewall
- Lumberjack logs

# Projects
- Inject secrets: Compare different IaS
- Orchestrate cloud: Compare different IaS
  - Provision VM and install a package or a service


## Random notes

IBM: Fullstack Developer
https://careers.ibm.com/job/17465658/fullstack-developer-sofia-bg/
- 3+ years of working experience with Java, and Spring
- Exposure to Java SE 11+, Spring framework, Spring Boot and subprojects like: Spring Data, Spring AOC, Spring Security, Spring MVC Rest, Spring Testing, Spring Actuator, Junit 5, Mockito, Gradle/maven, RDMS and NoSQL (document-based, key-value store), Angular 8+, Javascript ES6+
- Exposure to testing frameworks like: Jest, Jasmine, Cypress, RxJS, NgRx (redux pattern), Module bundlers like webpack, HTML5, CSS3
- Professional experience following agile processes such as Scrum
- Fluency in English – both verbal and written
- Experience with Angular would be a plus
- Knowledge of Unix based OSs and networking, working experience in agile and DevOps teams
- Experience with AWS or Azure (edited)

## Tech list
Tech to check out:

- Cloud and virtualization:
  - AWS: RDS, EC2, S3, Redis/Elasticache
  - GCP
  - Azure
  - Docker
  - Kubernetes
- Prog:
  - Algorithms (Big-O etc.)
  - Ruby
  - Rails 6
  - Rust
  - go
  - python
- UI programming:
  - Angular
  - Typescript
  - NgRx
- CICD:
  - Jenkins
  - AWS
  - Nexus
  - Bazel
  - Travis
  - Bamboo
  - TeamCity
  - ArgoCD
  - CircleCI
- Infrastructure as a code:
  - Terraform + Terragrunt (multi-client setup / modules)
  - CloudFormation
- Configuration management:
  - Ansible
  - Chef
  - Puppet
- Containers and container orchestrators:
  - Kubernetes
  - Istio
- UNIX:
  - UNIX IPC

- SRE:
  - load balancing
  - canary, A/B, blue/green, red-line

monitoring/log-aggregation/observability:
- Datadog
- CloudWatch
- Honeycomb
- Splunk
- New Relic

- SLO and SLI
- securityframeworks: OWASP, ISO, CSA, PCI

- Other:
  - AWS Amplify
  - Webpack
  - Boomi
  - Postres cheatsheet
  - CloudWatch
  - Rollbar
  - NewRelic
  - Capistrano/Ansistrano
  - RSpec or Selenium
  - Cloudflare
  - Logz.io
  - Pagerduty
  - Snyk
  - SumoLogic
  - Firebase
  - design patterns (MVC, MVP, MVVM)
  - WebSocket APIs
  - Helm
  - Hashicorp Vault
  - Monitoring/Logging (Grafana, Prometheus)
  - MongoDB Atlas
  - GitHub (Actions, Packages)
  - DigitalOcean
  - RabbitMQ
