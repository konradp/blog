---
title: "COM: Python: asyncio OSC messages and mido callbacks"
subtitle: 
date: 2023-03-24
tags: [ "wip" ]
categories: wip
---

This is a study of python asyncio used for an OSC server and Mido MIDI library threaded callbacks. We compare the variable scope for asyncio callbacks against the Mido callbacks.

Motivation: While writing the python-sooperlooper, I needed to write an OSC server and a MIDI message handler. For the OSC, I use the python-osc library, and for MIDI, I use the Mido library. I need to make them 'work together' and be able to access and update a global state variable.

{{<toc>}}


## Mido

First implementation
{{<highlight python "hl_lines=5 7-9 19">}}
import mido
import sys
import time

state = 'Some state value'

def midi_handle_msg(msg):
  print('msg', msg)
  print('midi_handle_msg: state:', state)

if __name__ == '__main__':
  if len(sys.argv) != 2:
    print('MIDI devices:')
    devices = mido.get_input_names()
    for device in devices:
      print('-', device)
    exit(1)
  dev = sys.argv[1]
  port = mido.open_input(dev, callback=midi_handle_msg)
  while True:
    time.sleep(1)
{{</highlight>}}

and we run it and press any MIDI key on the virtual MIDI keyboard:

```
$ python3 example_mido.py
MIDI devices:
- Midi Through:Midi Through Port-0 14:0
- Virtual Keyboard:Virtual Keyboard 134:0
- Midi Through:Midi Through Port-0 14:0

$ python3 example_mido.py "Virtual Keyboard:Virtual Keyboard 134:0"
msg note_on channel=0 note=48 velocity=127 time=0
midi_handle_msg: state: Some state value
msg note_off channel=0 note=48 velocity=0 time=0
midi_handle_msg: state: Some state value
```

We see that the **`midi_handle_msg()`** is able to access the **`state`** variable for reading.

Now, let's see what happens when we try to set the **`state`** variable:

{{<highlight python "hl_lines=4">}}
def midi_handle_msg(msg):
  print('msg', msg)
  print('midi_handle_msg: state:', state)
  state = 'New value'
{{</highlight>}}

After pressing a MIDI key, we will get an exception:
```
$ python3 example_mido.py "Virtual Keyboard:Virtual Keyboard 134:0"
msg note_on channel=0 note=48 velocity=127 time=0
Traceback (most recent call last):
[...]
UnboundLocalError: local variable 'state' referenced before assignment
```

To fix, we use the 'global' keyword:

{{<highlight python "hl_lines=4">}}
def midi_handle_msg(msg):
  global state
  print('msg', msg)
  print('midi_handle_msg: state:', state)
  state = 'New value'
{{</highlight>}}

and it works:

```
$ python3 example_mido.py "Virtual Keyboard:Virtual Keyboard 134:0"
msg note_on channel=0 note=48 velocity=127 time=0
midi_handle_msg: state: Some state value
msg note_off channel=0 note=48 velocity=0 time=0
midi_handle_msg: state: New value
```

### Observations
- the server (**`mido.open_input(...)`** is non-blocking, and so we have the **`while True`** loop a the end
- the callback is able to update the **`state`** variable once it's made **`global`**

## python-osc
We have an OSC client and a server. First, the client:

{{<highlight python "hl_lines=4">}}
from pythonosc.udp_client import SimpleUDPClient
client = SimpleUDPClient('127.0.0.1', 9000)
client.send_message('/ping', 'Hello')
{{</highlight>}}






{{<highlight python "hl_lines=4">}}
{{</highlight>}}
{{<highlight python "hl_lines=4">}}
{{</highlight>}}
{{<highlight python "hl_lines=4">}}
{{</highlight>}}


## References
https://stackoverflow.com/questions/56277440/how-can-i-integrate-python-mido-and-asyncio
