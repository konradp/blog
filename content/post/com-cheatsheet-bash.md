---
title: "bash"
subtitle: 
date: 2023-03-06
tags: [ "com", "cheatsheets" ]
categories: cheatsheets
---
My bash cheatsheet:

- is not a list of everything I know, but a list of things that I sometimes forget
- might include tools separate from bash

{{<toc>}}

## bash
### navigation
ref: [navigation (readline interaction)](https://www.gnu.org/software/bash/manual/html_node/Readline-Interaction.html)

  - moving:
    - **`C-b`**: back 1 char
    - **`C-f`**: forward 1 char
    - **`C-a`**: start
    - **`C-e`**: end
  - editing: (kill=cut)
    - **`C-k`**: kill to end
    - **`C-y`**: yank at cursor
  - other:
    - **`C-l`**: clear screen
    - **`C-d`**: del under cursor
    - **`C-_`**: undo
    - **`C-r`**: search history
  - TODO: There are meta-keystrokes (ALT), e.g. alt+f moves one word forward, but this doesn't work for me yet


### other
- redirects:
    - **`2>&1`**: stderr to stdout
    - **`&>`**: stderr and stdout to somewhere else
- [file tests](https://tldp.org/LDP/abs/html/fto.html):
    - **`-e`**: file exists
    - **`-d`**: file is a dir
    - **`-s`**: non-zero size
- [comparison ops](https://tldp.org/LDP/abs/html/comparison-ops.html):
    - **`-z`**: string is empty, zero-length
- [special parameters](https://www.gnu.org/software/bash/manual/bash.html#Special-Parameters)
    - **`$?`**: exitcode of last cmd
    - **`$$`**: PID of the shell (parent shell)
    - **`$#`**: number of positional params
- date:
    - ```
      $ date +%s
      1678172258
      ```      
    - ```
      $ date -d@1678172258
      Tue  7 Mar 08:57:38 EET 2023
      ```
    - ```
      $ date +"%Y%m%d %H:%M"
      20230307 09:52
      ```
- file descriptors:
    - 0: stdin
    - 1: stdout
    - 2: stderr
- heredoc var, with variable expansion
  ```
  read -r -d '' VAR <<EOF
  abc'asdf"
  $(execute)
  foo"bar"''
  EOF
  ```
- heredoc var, no variable expansion  
  ```
  read -r -d '' VAR <<'EOF'
  abc'asdf"
  $(execute)
  foo"bar"''
  EOF
  ```
- currency convert: `qalc -t 1 GBP to EUR`. Note: Need `qalc -e` or `qalc -exrates` first to update the exchange rates


## tools
### general
- ```
  nc -lvp 6000
  telnet localhost 6000
  ```
- pdf rotate  
  ```
  pdftk in.pdf cat 1-endeast output out.pdf
  qpdf in.pdf out.pdf --rotate=90
  ```
- pdf split into pages
  ```
  pdfseparate -f 1 -l 5 in.pdf out.pdf
  ```
- http server  
  ```
  python3 -m http.server 9000
  ```
- xterm clipboard:
  ```
  # In ~/.Xresources
  xterm*VT100.Translations: #override \
       Ctrl Shift <Key>V: insert-selection(CLIPBOARD) \n\
       Ctrl Shift <Key>C: copy-selection(CLIPBOARD)
  ```


### vim
- vim clipboard: on Debian, in stall `vim-gtk` pkg to get clipboard and xterm_clipboard:
  ```
  $ vim --version | grep clipboard
  +clipboard         +jumplist          +popupwin          +user_commands
  +ex_extra          -mouse_jsbterm     -sun_workshop      +xterm_clipboard
  ```
- [vim folds](https://vim.fandom.com/wiki/Folding#Opening_and_closing_folds):
  - **`zr / zm`: open/close one fold level**
  - `zR / zM`: open/close all folds
  - `zc / zo / za`: close/open/toggle
  - `zC / zO / zA`: all folds at cursor


### ffmpeg
- append replace audio of .mp4 with a .wav  
  ```
  ffmpeg -i v.mp4 -i a.wav -c:v copy -map 0:v:0 -map 1:a:0 new.mp4
  ```


### tmux
- `tmux ls`
- `Ctrl+b d`: detach
- `tmux a -t mysession`: attach to session named `mysession`


### screen
- `screen`: open session
- `screen -S some_name`: open named session
- `ctrl+a`: list sessions
- `screen -ls`: list sessions
- `screen -r`: reattach
- `ctrl+a ctrl+d`: detach


### Python packages
https://stackoverflow.com/questions/75602063/pip-install-r-requirements-txt-is-failing-this-environment-is-externally-mana/75696359#75696359
```
python3 -m venv .venv
source .venv/bin/activate
python3 -m pip install -r requirements.txt
```


### top alternatives
- htop
- btop
- bpytop
- atop
- nmon
- vtop
- bashtop
- gtop
- glances


## References
- bash manual: https://www.gnu.org/software/bash/manual/bash.html
- someone else's cheatsheet: https://devhints.io/bash
