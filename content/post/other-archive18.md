---
title: 'OTHER: Archive 81 - notes'
subtitle: 
date: 2022-03-17
tags: [ 'wip' ]
categories: wip
---
My notes from the podcast. I mostly listen to the podcast as I'm falling asleep, and so this is to keep track of which episode I'm on.

# Season 1

# Season 2
## 11 (1): Body, Recorder

## 14 (4): Water, Leviathans
- ?
- Rat

## 15 (5): Aurora, Cello
- Clara and Lou
- crossed the river ith Kera/Kara?
- she was tall
- what was the toll to cross the river?
- scene with Rat: mentioning podcasts, mention Leviathans, staring at the sun
- Who is Rat
- tape:
  - guy
  - awkward interviewer

## 16 (6): Vines, Contest
We learn more about Suit, a garden is expored, and a contst begins.

- advert: Atom Tickets
- Dan wakes up and is connects to the team through a new method (broadcasting)
- After visiting the gargoyle building, the team is running away from vines
- Dan points to a service elevator and a ladder, suggests using fire or a flare against the vines, which worked temporarily
- The vines are crashing into the city, concrete is moving
- A voice advises Dan (who is it?): Go into green (towards the vines)
- Dan passes out, to be woken up by Rat, who hints about giving birth, advises to be more careful, warns about the suit walking around by itself
- Dan asks about tapes to do with nature or vines
- Rat used to work in park management in their youth, used to love gardens, before becoming interested in cities and designed objects
- Dan meets the singing Suit, who is scheduled, says 'this place is an abomination', 'Right now I'm walking through the outposts, singing'
- Tape: Dan listens to the tape, topic: nature:
  - A Guided tour of the gardens (with classical music)
  - Venus fly trap vs. Mars fly trap
  - Plants can communicate and have memories, sometimes in dreams I'm transported to a place where this is obvious and true. I'm not sure if this is a nightmare
  - "The bodies of the dead make a wonderful fertiliser"
- chiptune music
- Reconnected to the team

## 17: Reflection, Retaliation
## 18: Collection, Composition
## 19: Escape, Birth
## 20: Portal, Ending

# Reference
