---
title: "Web browsers"
subtitle: 
date: 2020-11-01
categories: computers
---
# Web browsers
Options:

- arora
- dillo
- falkon
- firefox
- midori
- qutebrowser
- surf

# Thoughts
Options:
- (x) arora: Nice, but not present on armbian
- (x) dillo: Didn't like, looks old
- (x) falkon: 246MB of dependencies
- (?) firefox: 192MB of dependencies, slow load time
- (x) midori: 4MB of dependencies: nice, but can't ignore self-signed SSL certs
- (x) qutebrowser: 202MB of dependencies
- (?) surf: very nice but can't ignore self-signed SSL certs

# Notes
Midori is not in Debian packages
```
$ sudo apt-add-repository ppa:midori/ppa
$ sudo apt-get update -qq
$ sudo apt-get install midori
```

# Favourite
?
