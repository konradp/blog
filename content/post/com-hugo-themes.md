---
title: "Hugo themes"
date: 2023-04-24
categories: computers
---
{{<toc>}}

This is my own list of minimal Hugo blog themes, extrapolated from https://themes.gohugo.io/tags/minimal/. I'd like to create a separate blog for one of my projects, and the only theme I've used so far is xmin, and so I thought I'd check out other themes as part of this.

TODO: Add size

## Themes

### Good themes
theme/demo | Notes
--- | ---
[archie](https://themes.gohugo.io/themes/archie/)                                  [demo](https://athul.github.io/archie/)                           | 
[devise](https://themes.gohugo.io/themes/devise/)                                  [demo](https://austingebauer.dev/post/)                           |
[etch](https://themes.gohugo.io/themes/etch/)                                      [demo](https://lukasjoswiak.github.io/etch/)                      |
[lowkey](https://themes.gohugo.io/themes/lowkey-hugo-theme/)                       [demo](https://lowkey-hugo.netlify.app)                           |
[minimal-bootstrap](https://themes.gohugo.io/themes/minimal-bootstrap-hugo-theme/) [demo](https://minimal-bootstrap-hugo-theme.netlify.app/)         |
[nostyleplease](https://themes.gohugo.io/themes/hugo-theme-nostyleplease/)         [demo](https://themes.gohugo.io/themes/hugo-theme-nostyleplease/) | 
[risotto](https://themes.gohugo.io/themes/risotto/)                                [demo](https://risotto.joeroe.io/)                                |
[SK1](https://themes.gohugo.io/themes/hugo-theme-sk1/)                             [demo](https://sk1.jsiu.dev/)                                     |
[techlab](https://themes.gohugo.io/themes/techlab-hugo-theme/)                     [demo](https://techlab.seanfeng.dev/)                             |
[xmin](https://themes.gohugo.io/themes/hugo-xmin/)                                 [demo](https://themes.gohugo.io/themes/hugo-xmin/)                |
[yinyang](https://themes.gohugo.io/themes/hugo-theme-yinyang/)                     [demo](https://blog.joway.io/)                                    |


### Missing link to 'posts'
theme | demo | reason
--- | --- | ---
[accessible-minimalism](https://themes.gohugo.io/themes/accessible-minimalism-hugo-theme/) | https://accessible-minimalism.netlify.app/
[bearblog](https://themes.gohugo.io/themes/hugo-bearblog/) | https://janraasch.github.io/hugo-bearblog/
[coder](https://themes.gohugo.io/themes/hugo-coder/)         | https://hugo-coder.netlify.app/


### I don't like themes
theme | demo | why
--- | --- | ---
[holy](https://themes.gohugo.io/themes/holy/) | https://hugo-holy.pages.dev/ | images in lists
[hugotex](https://themes.gohugo.io/themes/hugotex/) | https://hugotex.vercel.app/ | images in lists, and weird spacing between words
[papermod](https://themes.gohugo.io/themes/hugo-papermod/) | https://adityatelange.github.io/hugo-PaperMod/

### Doesn't work out of the box for some reason
Retry these later
theme | demo
--- | ---
[bearcub](https://themes.gohugo.io/themes/hugo-bearcub/) | https://clente.github.io/hugo-bearcub/
[compost](https://themes.gohugo.io/themes/compost/) | https://canstand.github.io/compost/
[eiio](https://themes.gohugo.io/themes/hugo_eiio/) | https://heyuanfei.com/
[vanilla-bootstrap](https://themes.gohugo.io/themes/vanilla-bootstrap-hugo-theme/) | https://vanilla-bootstrap-hugo-theme.netlify.app/

### Extended hugo is required, or a version that I don't have
My hugo version: 0.80.0  
TODO: What is extended version of hugo?

theme | demo | reason
--- | --- | ---
[anubis](https://themes.gohugo.io/themes/hugo-theme-anubis/) | https://hugo-theme-anubis.netlify.app/ | extended version
[awesome](https://themes.gohugo.io/themes/hugo-blog-awesome/) | https://hba.sid.one/ | 0.86.0
[gokarna](https://themes.gohugo.io/themes/gokarna/) | https://gokarna-hugo.netlify.app/ | 0.81.0

### Other
These are other themes that caught my eye, but I'm probably not going to use them for the project that I have in mind.

theme | demo
--- | ---
[minimo](https://themes.gohugo.io/themes/engimo/) | https://minimo.netlify.app/
[console](https://themes.gohugo.io/themes/hugo-theme-console/) | https://themes.gohugo.io/themes/hugo-theme-console/
[shell](https://themes.gohugo.io/themes/hugo-theme-shell/) | https://themes.gohugo.io/themes/hugo-theme-shell/

## New blog instructions
Inspired by https://gohugo.io/getting-started/quick-start/

Create new git dir and add it to git.  
**Note:** Here, we call it **blog-waterworld**.
```
hugo new site blog-waterworld
cd blog-waterworld
```
Here, create a new repo in GitLab, called **blog-waterworld**.  
Follow the steps from 'Push an existing folder'.

Now, create a `.gitlab-ci.yml` file with the contents below and add/commit them to the repo.
```
# All available Hugo versions are listed here: https://gitlab.com/pages/hugo/container_registry
image: registry.gitlab.com/pages/hugo:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive

test:
  script:
  - hugo
  rules:
  - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  rules:
  - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

Install my themes.
```
curl -O https://gitlab.com/konradp/snippets/-/raw/master/os/blog_themes_install.sh; chmod +x blog_themes_install.sh
chmod +x blog_themes_install.sh
./blog_themes_install.sh
```

Enable theme
```
echo "theme = 'xmin'" >> config.toml
hugo server
```

Add a new post
```
hugo new posts/test-post.md
```
In the file, remove the 'draft' line.

Edit `config.toml`:
  - `baseURL` to the GitLab Page
  - `languageCode` to 'en-gb', or delete entirely
  - `title`

Add and commit, push.

The site will be available at https://konradp.gitlab.io/blog-waterworld

### Theme
- clone theme repo to the `themes` dir of your blog (or use git submodule).
- copy `config.toml` from theme  
  TODO: elaborate on this: https://gohugo.io/installation/, the `theme` var needs to be set

another option
```
$ hugo server -t etch
```

Updating themes
```
git submodule update --remote --merge
```

### Followup steps
- config.toml:
  - title
  - `baseURL = "https://konradp.gitlab.io/blog-waterworld"`
  - [enable HTML/js](post/com-cheatsheet-hugo/#configtoml)
- Custom CSS: [here](/post/com-cheatsheet-hugo/#css)
- shortcode: Table of contents [here](/post/com-cheatsheet-hugo/#shortcodes)
- `/archetypes/default.md`: Remove the draft line
- `/archetypes/default.md`: `date: {{ .Date.Format "2006-01-02" }}`
