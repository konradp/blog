---
title: "Markdown blog frameworks for GitLab Pages"
subtitle: Jekyll, Hugo, or Pelican
date: 2018-05-02
categories: computers
---

In this post I describe why I chose to use [Hugo][hugo] as a framework for my blog hosted on [Gitlab Pages][pages doc], and how I implemented it.

<!--more-->

## Why Hugo
Quickly enough, I discovered that there are overwhelmingly many blogging frameworks available to choose from on the [examples page][pages examples]. I dug into the repositories of each example, whilst bearing in mind that I wanted to use something that is lightweight, allows for writing posts in Markdown, and can be deployed with minimal dependencies. To get a feel for what each framework is about, I looked at its Wikipedia page, the directory structure of its example, and the programming language being used.

Having considered the above, I narrowed down my choice to three frameworks.

- Jekyll (written in Ruby, used by GitHub)
- Hugo (written in golang)
- Pelican (written in Python)

Hugo immediately won due to its clean directory structure. Excluding the `themes/` dir, the example Hugo page contains, essentially:

```text
    ./static/favicon.ico
    ./content/post/2017-03-20-photoswipe-gallery-sample.md
    ./content/post/2017-03-07-bigimg-sample.md
    [...]
    ./content/page/about.md
    ./content/_index.md
    ./config.toml
    ./README.md
    ./LICENSE
    ./.gitlab-ci.yml
```
Nice! To get started, I simply forked the [example Hugo project][hugo example].

## Initial set-up
After forking the project, I needed to change the `baseurl` variable in `config.toml` file to `https://konradp.gitlab.io/blog`. As soon as I committed and pushed this change, the CI/CD pipeline was triggered, i.e. what's defined in the `.gitlab-ci.yml` file kicked in.

Once the pipeline completed (just over 20 sec), my project was indeed available via https://konradp.gitlab.io/blog.
I was then able to add new blog posts by creating new Markdown files in the `content/post/` directory.

**Note:** Have a closer look at the `config.toml` as there are other things which can be safely tweaked/removed.

## Continuous integration
Take a look at the `.gitlab-ci.yml` file. It essentially runs the `hugo` binary, pretty much as described in the below section.

## Development environment
As the CI/CD pipelines in GitLab are so easy, I didn't actually look at this until right before publishing this very post. Otherwise, with the deployment pipeline taking just over 20 seconds, I was thus far simply commiting my tweaks, pushing upstream, and watching the page being uploaded immediately.

Whilst finishing up this post, I realised I didn't have Hugo installed locally so I quickly grabbed it for just over 20MB in size via my distro's package manager. I concluded even this to be an overkill, for Hugo is distributed as a small archive (containing a `LICENCE` text file, `README` file, and a `hugo` binary file).

After running `hugo server` inside my blog's root dir, I was given a `http://localhost:1313/blog` url which, when followed, gave me a live preview of my edits, without the need to refresh my browser.

<!--Reference-->
[pages doc]: https://docs.gitlab.com/ee/user/project/pages/
[pages examples]: https:///gitlab.com/pages
[hugo]: https://gohugo.io/
[hugo example]: https://gitlab.com/pages/hugo

