---
title: "COM: python-sooperlooper: Control Sooperlooper with MIDI interface"
subtitle: 
date: 2022-10-13
tags: [ 'wip' ]
categories: wip
---

{{<toc>}}

## App spec
### UI
Functionality, general requirements:

- select loop
- record
- overdub
- undo
- mute/unmute
- clear loop (or back to record, i.e. re-record)

### Gestures

Pedal:

- single press
- long press
- double press

Long press:

- on note on, trigger timer
- on timer callback, if there was no note off, then this is a long press. Ignore next note off, or undo the action which was started already with the earlier single press
- if there was a note off before the end of timer, cancel the timer

Double press:

- on note-on, trigger timer?
- without timer: on note-on, check when was the last note-on

### Recording a loop
- select a loop
- start record: hit pedal
- stop record: hit pedal
- undo: long press pedal

## Tech stack considerations
- I wrote loop-control in C++ but I'm struggling a bit with timers in libuv
- I like the timers in js
- but browser cannot open a raw socket to the sooperlooper
- however, if I run a server localhost, have a browser UI, the backend can make OSC connection to sooperlooper
- the browser has MIDI API
- now, should the backend be python/flask or nodejs/express?
- also, do I want a web-based GUI at all, or can I just use Python and python GUI lib?
- and if I use python backend and browser-based GUI, should the browser handle MIDI events, or the python backend?
- wild idea: what about golang?
- we'd like websocket for browser-based GUI, as the backend will need to send messages to the frontend

## Other notes
Arturia uses channel 9, note 36 for pad 1

## Design
<img src='/media/com-python-sooperlooper/python-sooperlooper.svg'></img>

**Note:** Android device or a browser on the PC  
**Note:** Browser (web app) is actually better, as no need to learn android dev

python app, libraries:
- `aiohttp`: HTTP server
- `mido`: MIDI server/client
- `pythonosc`: OSC server/client
- `websockets`: WebSockets server

Thoughts:
- this design feels problematic, in that everything is inside `app.py` (OSC server, http server, MIDI server)
- but it makes it simpler, in that there's only one app to run
- would it be better to convert it into a service, with multiple apps/processes?
- but then, all of these components would need to access some shared representation of state. What would that be?
- should there be some message queue? rabbitmq (we don't need flower + celery)
- or maybe UNIX sockets for IPC?

## Milestones
### Milestones: initial

- {{<v>}} backend: MIDI: receive MIDI messages
- {{<v>}} backend: MIDI: capture long-press using a timer
- {{<v>}} backend: MIDI: send MIDI SysEx message to change colours
- {{<v>}} backend: MIDI: receive controller/knob messages
- {{<v>}} backend: OSC: connect to SL, ping
- {{<v>}} backend: OSC: send msg to SL
- {{<v>}} backend: OSC: receive msgs from SL
- {{<v>}} backend: serve frontend
- {{<v>}} backend: WebSocket server
- {{<v>}} backend: event-based: can process WebSocket messages and MIDI messages at the same time
- {{<v>}} frontend: WebSocket client
- {{<v>}} frontend: draw pads/loops
- {{<v>}} frontend: highlight pads from WebSocket messages received

### Milestones: functional product
What does the app need for me to be able to use it to start looping:

- {{<v>}} init loops:
    - {{<v>}} global setting: quantize=loop
    - {{<v>}} init project:
        - {{<v>}} added loops should be synced  
- understanding:
    - {{<x>}} 'sync' option
    - {{<x>}} 'play sync' option
    - {{<x>}} 'play feedback' option
- {{<x>}} undo option?
- {{<x>}} mute functionality
- {{<x>}} check option 'enable feedback during playback'
- {{<x>}} UI: red pad when recording
- {{<x>}} arturia: red pad when recording

### Milestones: first release
- {{<x>}} youtube video


## Choosing the right libraries
The backend will need to handle:

- **MIDI**: send/receive
- **OSC**: send/receive
- **WebSocket**: send/receive
- **web server**: serve the static page


### Python MIDI libraries

lib                            | stars   | forks  | last commit
---                            | ---     | ---    | ---
**[mido] ([git][mido-git])**   | **1.1k**| **177**| **2023.02.17**
[py-midi] ([git][py-midi-git]) | 47      | 7      | 2021.01.15
[python-midi]                  | 1.4k    | 380    | 2015.07.08

The **mido** is the winner here.

### Python OSC libraries
lib                                     | stars   | forks  | last commit | note
---                                     | ---     | ---    | ---         | ---
**[python-osc] ([git][python-osc-git])**| **415** | **97** | **2023.01.15**
[oscpy]                                 | 100     | 25     | 2022.08.03
[pyOSC][pyosc]                          | 91      | 31     | 2011.06.28
[SimpleOSC][simpleosc]                  | 10      | 4      | 2013.11.27  | Relies on pyosc
[aiosc]                                 | 32      | 3      | 2021.07.29
[osc4py3]                               | ?       | ?      | ?           | ?
txosc                                   | ?       | ?      | ?           | Can't find info on this
[pyliblo] ([git][pyliblo-git])          | 38      | 23     | 2015.11.17  | I like liblo
[pyliblo3]                              | 4       | 1      | 2022.05.02

The winner is **python-osc**.

### Python WebSocket, web server
For the web server, we only need minimal functionality: serve a **single page**, and maybe some static assets (images etc maybe). This app is not something that will need to be served in prod/public, but will be run on the user computer. So I think I'll use a websocket-specific library (which has an option to serve static files over HTTP), or even the built-in python HTTP server, or Flask.

WebSocket libraries:


lib                                           | stars | forks | last commit
---                                           | ---   | ---   | ---
[python-socketio] ([git][python-socketio-git])| 3.3k  | 527   | 2023.03.20
[websockets] ([git][websockets-git])          | 4.5k  | 467   | 2023.03.02

**Note:** websockets only does websockets, it won't help to serve the static page. For this, consider Sanic which uses websockets (sanicframework.org).

Web server libraries:

lib                                           | stars | forks | last commit | Note
---                                           | ---   | ---   | ---         | ---
[aiohttp]                                     | 13.4k | 1.9k  | 2023.03.17  | +WebSocket
[flask]                                       | 62.3k | 15.5k | 2023.03.11  | Supports WebSocket through extensions?
[fastapi]                                     | 55.8k | 4.6k  | 2023.03.18  | +WebSocket
[tornado]                                     | 21k   | 5.5k  | 2023.02.21  | +WebSocket
[flask-socketio] ([git][flask-socketio-git])  | 5k    | 890   | 2023.03.23  | +WebSocket
[flask-sock] ([git][flask-sock-git])          | 181   | 14    | 2023.02.08  | +WebSocket
[uvicorn]                                     | 6.3k  | 561   | 2023.03.16  | +WebSocket
[gunicorn]                                    | 8.8k  | 1.6k  | 2023.01.27  | No WebSocket

guides:

- https://flask.palletsprojects.com/en/2.2.x/patterns/singlepageapplications/

## Other TODO
- (x) convince myself that I need this project
- (x) how much is boss RC looper?
  - rc-505mkII: £459
- (x) raspberry pi: where to get, how much is it
- (x) browser: keep screen awake: Screen Wake Lock API - Web APIs | MDN



<br><br><br><br><br>

<!-- JS -->
[osc.js]: https://github.com/colinbdclark/osc.js
[osc-js]: https://github.com/adzialocha/osc-js
<!-- PYTHON: MIDI -->
[py-midi]: https://pypi.org/project/py-midi/
[py-midi-git]: https://github.com/edthrn/py-midi
[mido]: https://mido.readthedocs.io/
[mido-git]: https://github.com/mido/mido
[python-midi]: https://github.com/vishnubob/python-midi
<!-- PYTHON: WEBSOCKETS -->
[websockets]: https://websockets.readthedocs.io/
[websockets-git]: https://github.com/aaugustin/websockets
[python-socketio]: https://python-socketio.readthedocs.io/
[python-socketio-git]: https://github.com/miguelgrinberg/python-socketio
<!-- PYTHON: OSC -->
[python-osc]: https://python-osc.readthedocs.io/
[python-osc-git]: https://github.com/attwad/python-osc
[pyosc]: https://github.com/ptone/pyosc
[simpleosc]: https://github.com/enrike/simpleOSC
[aiosc]: https://github.com/artfwo/aiosc
[osc4py3]: https://osc4py3.readthedocs.io/
[pyliblo]: https://das.nasophon.de/pyliblo/
[pyliblo-git]: https://github.com/dsacre/pyliblo
[pyliblo3]: https://github.com/gesellkammer/pyliblo3
[oscpy]: https://github.com/kivy/oscpy
<!-- PYTHON: WEB SERVER -->
[uvicorn]: https://www.uvicorn.org/
[gunicorn]: https://gunicorn.org/
[cherrypy]: https://cherrypy.dev/
[aiohttp]: https://docs.aiohttp.org/
[tornado]: https://www.tornadoweb.org/
[flask]: https://flask.palletsprojects.com/
[fastapi]: https://fastapi.tiangolo.com/
[flask-sock]: https://flask-sock.readthedocs.io/
[flask-sock-git]: https://github.com/miguelgrinberg/flask-sock
[flask-socketio]: https://flask-socketio.readthedocs.io/
[flask-socketio-git]: https://github.com/miguelgrinberg/flask-socketio
[fastapi]: https://fastapi.tiangolo.com/
