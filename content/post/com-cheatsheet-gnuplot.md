---
title: "gnuplot"
subtitle: 
date: 2022-12-28
tags: [ "com", "cheatsheets" ]
categories: cheatsheets
---

{{<toc>}}

## Example
data:
```
20221222 09:49 38.64
20221222 13:54 38.46
20221222 16:13 39.10
20221222 17:34 38.31
20221222 18:35 38.76
20221222 18:46 38.61
20221222 19:44 38.22
20221222 20:32 38.70
```

## timefmt
symbol | meaning
--- | ---
%d | day of month
%m | month
%Y | year
%H | hour
%M | minute
%S | seconds
%b | month, three char, jan, feb, mar

## Reference
- https://gnuplot.sourceforge.net/demo_5.4/
- https://gnuplot.sourceforge.net/docs_4.2/node274.html
- http://www.gnuplot.info/docs_5.2/Gnuplot_5.2.pdf
