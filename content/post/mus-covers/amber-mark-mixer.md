---
title: "John Mayer - Neon"
---
- back to [covers](../)
- key: ?  
- tuning: ? lower E to ?
- tuning: ?

```
--------------------------------------------------
--------------------------------------------------
--------------------------------------------------
--------------------------------------------------
--------------------------------------------------
--------------------------------------------------
```

```
--------------------------------------------------
--------------------------------------------------
--------------------------------------------------
--------------------------------------------------
--------------------------------------------------
--------------------------------------------------
```

Lyrics
```
Gonna get in your mind one fine day
Gonna figure you out one fine day
Dear Lord, maybe I should stay away, stay away
Little lost, little found, blind with temptation

Tell me somethin', do I make you feel the way that I do?
I been uptight, you got me right, I'm back into my groove
Keep me pumpin', gimme somethin' that I could get used to
I'm on fire, gotta keep me lit all night, all night, they say

And they say my heart will bleed
But it doesn't gotta do it
'Cause your love is so nasty, boy, yeah, yeah, yeah
In the dark I'm livin' the dream
When you pull up behind me
Like a mixer up in that green, boy, yeah, yeah, yeah

Every night, cut lights, touch your face
Every night I try to find out what's on your brain
Dear Mama, help me know just what to say
'Cause I don't wanna be blind with temptation

Tell me somethin', do I make you feel the way that I do?
I been uptight, you got me right, I'm back into my groove
Keep me pumpin', gimme somethin' that I could get used to
I'm on fire, gotta keep me lit all night, all night, they say

And they say my heart will bleed
But it doesn't gotta do it
'Cause your love is so nasty, boy, yeah, yeah, yeah
In the dark I'm livin' the dream
When you pull up behind me
Like a mixer up in that green, boy, yeah, yeah, yeah

You hit me late night, "Miss, can I come over?"
Put you on replay in my mind all summer
I'm wide awake, my body shakes and shivers
I'm a fool, I guess, 'cause I text you, "Yes"
Even though my friends, they say

And they say my heart will bleed
But it doesn't gotta do it
'Cause your love is so nasty, boy, yeah, yeah, yeah
In the dark I'm livin' the dream
When you pull up behind me
Like a mixer up in that green, boy, yeah, yeah, yeah
```
