---
title: "Arctic Monkeys - Do I wanna know"
---
- back to [covers](../)
- note: Dua Lipa cover
- tonation: ? G min?


# Tab
```
[INTRO x 2]
-------------------------------------------------------
----------3------6--4--3-----------3-------------------
----------3---------3-----5--3--5-----5-----3h5-----3--
----------5--5/8----5-----5-----5-----5-----5-------5--
-------1--5---------6-----6-----3-----3--3----------5--
--1h3-----3--------------------------------------3--3--
```

```
[VERSE/BRIDGE]
-------------------------------|---------
-----3-----4-----3----------3--|-----3---
-----3-----5-----5-----3h5-----|-----3---
-----5-----5-----5-----5-------|-----5---
-----5--6--6--3--3--3----------|----(5)--
--3--3-------------------------|--3--3---

-------------------------------|---------
-------------------------------|-----7---
-----THE-SAME------------------|----(7)--
-------------------------------|-----7---
-------------------------------|--5------
-------------------------------|---------
alternative last chord
-------------------------------|---------
-------------------------------|-----3---
-----THE-SAME------------------|-----2---
-------------------------------|-----4---
-------------------------------|--5------
-------------------------------|---------

[CHORUS]
-----------------|-------------------------|--------------------------
-----4-----4-----|-----3-----3----------3--|-----3----3-----3-----1---
-----3-----3-----|-----5-----5-----3h5-----|-----3----3-----3-----2---
-----5-----5-----|-----5-----5-----5-------|-----5----5-----5-----3---
--6-----6-----6--|--3-----3-----3----------|--------------------------
-----------------|-------------------------|--3-----3----3-----1------
```


# lyrics
```
[R: chord ring out]
[INTRO]

[VERSE 1]
Have you got colour in your cheeks?
Do you ever get that fear that you can't shift the tide
That sticks around like summat in your teeth?
Are there some aces up your sleeve?
Have you no idea that you're in deep?
I've dreamt about you nearly every night this week
How many secrets can you keep?
'Cause there's this tune I found
That makes me think of you somehow an' I play it on repeat
Until I fall asleep, spillin' drinks on my settee

[CHORUS]
[guitar lively]
(Do I wanna know?) If this feelin' flows both ways?
(Sad to see you go) Was sorta hopin' that you'd stay
(Baby, we both know) That the nights were mainly made
For sayin' things that you can't say tomorrow day

[BRIDGE]
Crawlin' back to you
                        [R]
Ever thought of callin' when
             [R]
You've had a few?
'Cause I always do

Maybe I'm too
           [R]
Busy bein' yours
                [R]
To fall for somebody new
Now, I've thought it through
Crawlin' back to you

[VERSE 2]
So have you got the guts?
Been wonderin' if your heart's still open
And if so, I wanna know what time it shuts
Simmer down an' pucker up, I'm sorry to interrupt
It's just I'm constantly on the cusp of tryin' (to kiss you)
But I don't know if you feel the same as I do
But we could be together if you wanted to

[CHORUS]
[guitar lively]
(Do I wanna know?) If this feelin' flows both ways?
(Sad to see you go) Was sorta hopin' that you'd stay
(Baby, we both know) That the nights were mainly made
For sayin' things that you can't say tomorrow day

[BRIDGE]
Crawlin' back to you (Crawlin' back to you)
Ever thought of callin' when
You've had a few? (Had a few)
'Cause I always do ('Cause I always do)
Maybe I'm too (Maybe I'm too busy)
Busy bein' yours (Bein' yours)
To fall for somebody new
Now, I've thought it through
Crawlin' back to you
[END]


[OUTRO CHORUS: not used]
(Do I wanna know?) If this feelin' flows both ways?
(Sad to see you go) Was sorta hopin' that you'd stay
(Baby, we both know) That the nights were mainly made
For sayin' things that you can't say tomorrow day
(Do I wanna know?) Too busy bein' yours to fall
(Sad to see you go) Ever thought of callin', darlin'?
(Do I wanna know?) Do you want me crawlin' back to you?
```
