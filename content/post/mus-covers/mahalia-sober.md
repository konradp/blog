---
title: "Mahalia - Sober"
---
- back to [covers](../)
- capo: 2nd fret
- `~~~~` ring out


## Tab
chords:
```
----------------------------------------
----------------------------------------
----------------------------------------
-7---6---9---2--------------------------
-7---7---9---4--------------------------
-9---7---11--4--------------------------
-7---5---9---2--------------------------
Bm7  A C#m7 F#m
```

## Lyrics
```
[ CH 1 ]
Now that I am sober                  I take back what I said
I'm sittin' with this love hangover  And boy it's hurtin' my head
It's the middle of October           And we just came to an end
I'm just sorry that there ain't no   time left

You and I we're over                 Me and you are done
When I wanted to be closer           You just wanted to run
Though I know it's no ones fault     If I've learned anything at all
With us, no matter how I add it up   ~~~~One plus one is none

[ VE 1 ]: [ F#m here ]
You've been on my mind for a while now     Tryna get you off 'cause this ain't right now
You said it was a vibe for the time being  It was nothing more but I just couldn't see it
This is not a song for the whole world     Felt like it could've been us against the whole world
Maybe you were mine and I was your girl    ~~~~Whatever happened to the time that we spent

[ CH 2 ]

[ VE 2 ]: [ F#m here ]
I've been at this party for some time now               Sippin' on Bacardi 'til it runs out
You are in the corner with your head down               I don't even wanna know what that's about
'Cause you're the one that's always gettin' too wasted  And I'm the one that's never got intoxicated
Tonight I'm in my feelings, oh I'm faded                But I don't wanna think about the complicated

[ BRIDGE ]
Oh your love got me feeling like I can't
Get up, wishing for it but it never lasts
I thought we'd be together
Oh it ended so ~~~~~fast

[ CH 3 ]:
[forte on "cos you and I we're over"]
[Change on 'one plus one' three times]
```

