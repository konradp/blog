---
title: "ABRA - Roses"
---
- back to [covers](../)

## ABRA - Roses
### bass
Notes: G#1 C#1 E1 G#0 G0
<center>
  <img src='/media/mus-guitar-covers/roses-1.png' style='width:50%'></img><br>
  <img src='/media/mus-guitar-covers/roses-2.png' style='width:50%'></img><br>
  <img src='/media/mus-guitar-covers/roses-3.png' style='width:50%'></img><br>
  <img src='/media/mus-guitar-covers/roses-4.png' style='width:50%'></img><br>
</center>
TODO: Is it really 4 patterns or  actually fewer? Experiment with different pattern lengths

### drums
kick, snare, hat, clap
<center>
  <img src='/media/mus-guitar-covers/roses-drums1.png' style='width:50%'></img><br>
  <img src='/media/mus-guitar-covers/roses-drums2.png' style='width:50%'></img><br>
</center>

### lyrics / structure
structure:
```
B1 B2 B3 B4
D1 D2 D1 D2
```
lyrics
```
[INTRO]
B1 B2 B3 B4
-- -- D1 D2

[VERSE 1]
                               To...
night...                      Petals
fall from pink roses, and everything
I thought I knew I suppose is a lie
Everything dies and everything changes

  Iiiiii... was never one to have a gree...
een thumb oh oh, and I'm
green with envy of the me that was young and un-
wise and unknowing. The thorns are showing, 

aaaaa aaa, The thorns are showing
aaaaa aaa, The thorns are showing
aaaaa aaa, The thorns are...
aaaaa aaa, ...showing..... but

[CHORUS: B1 B2 B3 B4]
...you taste best when you're in full bloom
Lay in my teeth, my sweet love trophy
You can’t be killed if I rip from the roots
Take it with grace, I’m dumb and I’ll chase. I'm...

...young and I’ll waste you away (a ay). I'm...
...young and I'll waste you away (a ay). I'm...
...young and I'll waste you away (a ay). I'm...
[no drums]
...young and I'll waste you away (ay ay ay ay)

aaaa
aaaa
aaaa
aaaa. To...

[VERSE 2]
  [no bass]
...night      If you leave me I’ll
flood out your fire. Why don’t you
love me like I love you? Like I want you, like
  [unmute bass mid-way, on 'dies']
I need you (Everything dies)

It’s always too soon to lose to the moon, And you’re
freaking them out, And you look like a fool, And you’re
starting to know it ...Your....
...thorns are showing.... But...

[CHORUS]
...you taste best when you’re in full bloom
Lay in in my teeth, my sweet love trophy
You can’t be killed if I rip from the roots
Take it with grace, I’m dumb and I’ll chase. I'm...

[no drums]
...young and I’ll waste you away (a ay). I'm...
...young and I'll waste you away (a ay). I'm...
...young and I'll waste you away (a ay). I'm...
                                [mute bass]
...young and I'll waste you away (ay ay ay ay)

[drums back]
aaaa
aaaa
aaaa
aaaa

B1
B2
B3
[no drums]
B4/D-
```
