---
title: "Franz Ferdinand - Ulysses"
---
- back to [covers](../)

## Lyrics
```
[VERSE 1]
Well I sit and hear sentimental footsteps   Then a voice say, "Hi, so
So whatcha got, whatcha got this time?      C'mon, let's get high
C'mon Lexo, whatcha got next-o?             Walk 25 miles, oh
Well I'm bored, I'm bored                   C'mon, let's get high
[SYNTH]
C'mon, let's get high
C'mon, let's get high, high

[CHORUS 1]
Well, I found a new way    I found a new way
C'mon, don't amuse me      I don't need your sympathy
La, la-la-la-la            Ulysses
I found a new way          I found a new way, baby

[SYNTH]

[VERSE 2]
Am I Ulysses? Am I Ulysses?         No, but you are now, boy
So sinister, so sinister            But last night was wild
What's the matter, there?           Feelin' kind of anxious
That hot blood grew cold            Yeah, everyone, everybody knows it
Yeah, everyone, everybody knows it  Everybody knows ah...

[CHORUS 2]
La, la-la-la-la    Ulysses
I found a new way  I found a new way, baby
La, la-la-la-la    Ulysses
I found a new way  I found a new way, baby

[BRIDGE]
Oh... Oh, then suddenly you know  You're never goin' home
You're never, you're never        You're never, you're never
You're never, you're never        You're never goin' home

[OUTRO]
You're not Ulysses, oh baby, no    La-la-la-la
You're not Ulysses                 La-la-la-la
```

## Drums
instr | voice                     | tune
--- | --- | --:
KICK       | 01 MONSTER KICK      | +7
SNARE      | 12 RAP SNARE         | +1
CLOSED HAT | 23 R&B CLOSED HAT    | -1
MID HAT    | 34 FAST CABASA       | -2
--- | 
OPEN HAT   | 22 OPEN HI HAT       | +8
CLAPS      | 36 GATED CLAPS       | +8
PERC 3     | 47 JAM BLOCK         | -9
PERC 4     | 35 TAMBOURINE        | -3
--- |
TOM 1      | 21 MID HAT           | -3
TOM 2      | 47 JAM BLOCK         | -16
TOM 3      | 11 MID PICOLO SNARE  | +1
TOM 4      | 02 TILED ROOM KICK   | +3
--- |
RIDE       | 03 BIG FOOT          | -5
CRASH      | 04 RAP KICK          | +7
PERC 1     | 18 SIDESTICK         | -7
PERC 2     | 47 JAM BLOCK         | -9
