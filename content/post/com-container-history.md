---
title: "COM: History of containers"
subtitle: 
date: 2023-03-03
tags: [ "wip" ]
categories: wip
---

**TABLE OF CONTENTS**
{{<toc>}}

## Timeline

- 1973: 'nice' program released
- 1979: The chroot was introduced as a system call in Version 7 Unix [1]. See https://minnie.tuhs.org/cgi-bin/utree.pl. There is also a mention of it in 1982 for building the 4.2BSD system.
- 1991: chroot used to honeypot a hacker [1]
- 2000: Freebsd introduces the 'jail' command [1]
- 2002: Linux namespaces [2]
- 2007: cgroups [3]
- 2008: LXC
- 2013: Docker [4]
- 2015: runc, a container runtime, a CLI tool for spawning and running containers on Linux according to the OCI specification
- 2016: cgroups v2 [3]
- 2017: containerd, a container runtime
- 2019: containerd graduated within CNCF [7]

## cgroups vs. namespaces
cgroups: Linux control groups

- organise proceses into hierarchical groups, whose usage of resources can be limited and monitored
- resource usage (CPU, memory, disk I/O, network, etc.) for processes:
    - limits
    - accounts for
    - isolates
- doc: https://www.kernel.org/doc/Documentation/cgroup-v2.txt
- man: https://man7.org/linux/man-pages/man7/cgroups.7.html

namespaces:

- partition kernel resources so that different processes see different sets of resources:
    - process IDs
    - host names
    - user IDs
    - file names
    - etc
- man: https://man7.org/linux/man-pages/man7/namespaces.7.html

Despite these, I'm not clear what the difference is between cgroup and namespaces. I see that there is a cgroup namespace, which makes it even more confusing.

## cgroups

## namespaces
Namespace types:

- mount (mnt)
- process id (pid)
- network (net)
- inter-process comms (ipc)
- UTS (UNIX time-sharing)
- user id (user)
- cgroup
- time namespace

## References
- https://en.wikipedia.org/wiki/Chroot
- [1](https://en.wikipedia.org/wiki/Chroot)
- [2](https://en.wikipedia.org/wiki/Linux_namespaces)
- [3](https://en.wikipedia.org/wiki/Cgroups)
- [4](https://en.wikipedia.org/wiki/Docker_(software))
- [5](https://en.wikipedia.org/wiki/Open_Container_Initiative)
- [6](https://en.wikipedia.org/wiki/Cloud_Native_Computing_Foundation#containerd)
- [7](https://containerd.io/)
- [8](https://github.com/opencontainers/runc)
