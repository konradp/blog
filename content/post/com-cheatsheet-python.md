---
title: "python"
date: 2022-11-08
tags: [ "com", "cheatsheets" ]
categories: cheatsheets
---
A reference of resources, ideas and words to do with python.

{{<toc>}}

## By PEP
- [PEP-0202]: list comprehension
  {{<highlight python>}}
    >>> print [i for i in range(20) if i%2 == 0]
    [0, 2, 4, 6, 8, 10, 12, 14, 16, 18]
  {{</highlight>}}
- [PEP-0557]: [dataclasses]  
  c-like structs with named fields
  {{<highlight python>}}
    from dataclasses import dataclass

    @dataclass
    class Point:
      """Class for keeping track of an item in inventory."""
      name: int
      unit_price: int = 2

      def total_cost(self) -> float:
        return self.name * self.unit_price
    p = Point(name=1, unit_price=3)
  {{</highlight>}}
- [PEP-3107]: function annotation
  {{<highlight python>}}
    def func(self, param: str='value') -> list:
  {{</highlight>}}
- [PEP-0584](https://peps.python.org/pep-0584/): Merge and update operators: `|` and `|=`

## Other
- snippets: https://github.com/konradp/snippets/python
- function decorators
- exceptions: catch and raise
{{<highlight python>}}
try:
  // do something
  if something:
    raise Exception()
except Exception as e:
  print('Exception', e)
{{</highlight>}}
- python mysql ORM:
  - Django:
      - https://docs.djangoproject.com/en/4.1/topics/db/queries/
      - https://python.plainenglish.io/using-djangos-orm-as-a-stand-alone-project-58576a5516e4
      - https://stackoverflow.com/questions/937742/use-django-orm-as-standalone
  - SQLAlchemy
  - peewee
  - pony
- STL:
    - [os](https://docs.python.org/3/library/os.html)
- [file open modes](https://docs.python.org/3/library/functions.html#open):
    - r: reading (default)
    - w: write, truncate file first
    - x: exclusive creation, failing if the file already exists
    - a: write, append to file if exists
    - b: binary mode
    - t: text mode (default)
    - +: updating (reading and writing)
- What is USGI, UWSGI, WSGI, ASGI?
- **`python -m websockets ws://localhost:9001/`**  
  test websocket


<!-- REFERENCES -->
[dataclasses]: https://docs.python.org/3/library/dataclasses.html)  
[PEP-0557]: https://peps.python.org/pep-0557/
[PEP-3107]: https://peps.python.org/pep-3107/
[PEP-0202]: https://peps.python.org/pep-0202/
