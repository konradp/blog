---
title: "nodejs"
subtitle: 
date: 2022-12-06
tags: [ "com", "cheatsheets" ]
categories: cheatsheets
---

{{<toc>}}

## Build new app

### Install nvm and node
Install nvm.
```
VERSION=v0.39.1
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/$VERSION/install.sh | bash
```
and then
```
nvm -v
0.39.2
```
**Note:** There might be a newer version than 0.39.1. Find out here:  
https://github.com/nvm-sh/nvm/releases/latest

Install and use latest node
```
nvm list
nvm install node
nvm use node
```
and
```
$ node -v
v19.2.0
```

### Use different node versions
```
nvm list
nvm ls-remote --lts
nvm install v18.18.0
nvm use v18.18.0
```

### Init npm package and add express server
Inside project dir.
```
npm init
```
**Note:** Change the entry point from `index.js` to e.g. `server.js`.
```
  "scripts": {
    "start": "node server.js"
  }
```

Then, have a look in the **snippets** repo, e.g.  
https://gitlab.com/konradp/snippets/-/blob/master/js/node-ui-template/server.js


Also,
```
npm install --save express
```
and in `.gitignore`:
```
node_modules
```
then, in `public/index.html`:
```
<html>
<head>
  <script src='js/app.js'></script>
</head>
</html>
```
in `public/js/app.js`:
```
window.onload = function() {
  // stuff here
}
```

and run as:
```
npm start
```

### ES modules vs. CommonJS modules
I will use the ES modules in new projects.

<table style='font-size: 0.8em'>
<th>ES</th>
<th>CommonJS</th>
<tr>
<td><pre><code>
// addTwo.mjs
function addTwo(num) {
  return num + 2;
}
export { addTwo };

// app.mjs
import { addTwo } from './addTwo.mjs';
</code></pre>
</td>
<td><pre><code>
module.exports = ...

const circle = require('./circle.js');
</code></pre></td>
</tr>
</table>

Error examples
```
(node:13930) Warning: To load an ES module,
set "type": "module" in the package. or use the .mjs extension.
import { readdir } from 'node:fs/promises';
SyntaxError: Cannot use import statement outside a module
```

#### The package.json "type": "module"
In `package.json`, we can have either:

- type: **commonjs** (default, treat `.js` files as CommonJS modules)
- type: **module** (treat `.js` files as ES modules)

Apart from that, the special file extensions will be treated as expected:

- `.cjs`: CommonJS modules
- `.mjs`: ES modules


#### ES module imports
Examples
{{<highlight js "linenos=table">}}
import { addTwo } from './addTwo.mjs';
import showdown from 'showdown';
import * as showdown from 'showdown'; // not sure on this
{{</highlight>}}

TODO:

- Read up on imports, what's difference between 1 and 2?

## References
- nvm: https://github.com/nvm-sh/nvm#usage
- [node: ES modules](https://nodejs.org/api/esm.html#modules-ecmascript-modules)
- [node: CommonJS modules](https://nodejs.org/api/modules.html#modules-commonjs-modules)
- [blog: CommonJS modules vs. ES6](https://pencilflip.medium.com/using-es-modules-with-commonjs-modules-in-node-js-1015786dab03)
