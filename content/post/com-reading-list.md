---
title: "Reading list"
subtitle: 
date: 2023-03-06
categories: computers
---
List of things that I'm reading or want to read.  
Legend: {{<w>}} in progress, {{<x>}} want to read, {{<v>}} finished

Computers:
- AWS
- HTML/CSS
    - {{<w>}} [MDN: Grid layout](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Basic_Concepts_of_Grid_Layout)
        - [my notes](https://konradp.gitlab.io/snippets/grid/index.html)
    - {{<w>}} [MDN: Flexbox layout](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox)
        - [my notes](https://konradp.gitlab.io/snippets/flex/index.html)
    - {{<x>}} [MDN: Web Audio API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API)
- bash
    - {{<x>}} [bash manual](https://www.gnu.org/software/bash/manual/bash.html)
- linux:
    - containers:
        - {{<x>}} [cgroup doc](https://www.kernel.org/doc/Documentation/cgroup-v2.txt)
        - {{<x>}} [cgroup man](manhttps://man7.org/linux/man-pages/man7/cgroups.7.html)
    - {{<x>}} man tar
- python:
    - {{<x>}} [python stl](https://docs.python.org/3/library/):
      - {{<v>}} [stl.os](https://docs.python.org/3/library/os.html)
    - libraries:
      - {{<x>}} [asyncio](https://docs.python.org/3/library/asyncio.html)
- DevOps:
    - {{<x>}} [Google SRE books](https://sre.google/books/)
    - {{<x>}} [ref: Jenkins pipelines, Jenkinsfile](https://www.jenkins.io/doc/book/pipeline/jenkinsfile/)
    - {{<x>}} GitLab Pages
- HTML/CSS:
    - {{<x>}} [grid by example](https://gridbyexample.com/)
- other:
    - {{<x>}} yaml spec

Maths:
- principia:
  - about: https://en.wikipedia.org/wiki/Philosophi%C3%A6_Naturalis_Principia_Mathematica
  - translation: https://www.17centurymaths.com/contents/newtoncontents.html
  - translation: https://en.wikisource.org/wiki/The_Mathematical_Principles_of_Natural_Philosophy_(1846)
  - translation: https://books.google.bg/books?id=Tm0FAAAAQAAJ&pg=PA1&redir_esc=y#v=onepage&q&f=false
