class Diagram {
  constructor() {
    console.log('constructor');
    this.canvas = document.querySelector('#ex1');
    this.c = this.canvas.getContext('2d');
    this.w = this.canvas.width;
    this.h = this.canvas.height;
    //this.margin = this.
    this.Draw();
  };

  Draw() {
    const c = this.c;
    c.lineWidth = 5;
    c.arc(this.w/2, this.h/2, this.h/2, 0, 2*Math.PI);
    c.stroke();
  };
} // class Diagram

function Run() {
  console.log('test');
  let d = new Diagram();
}


Run();
//document.addEventListener('load', Run());
