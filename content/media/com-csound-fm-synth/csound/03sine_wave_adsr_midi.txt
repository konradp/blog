<CsoundSynthesizer>
<CsOptions>
-Ma -odac -+rtmidi=portmidi --realtime
</CsOptions>
<CsInstruments>

sr = 44100
ksmps = 32
nchnls = 2
0dbfs = 1

; all MIDI channels to instrument 1
massign 0, 1

instr 1
  ; Constants
  imin  = 0.01
  imax  = 4

  ; Volume
  kmvol midic7 74, 30, 100
  initc7 1, 74, 1

  ; ADSR envelope
  imatt midic7 71, imin, imax 
  iat expcurve imatt, 1
  imdec midic7 76, imin, imax 
  imlev midic7 77, imin, 1
  imrel midic7 93, imin, imax 
  initc7 1, 77, 0.5
  aenv mxadsr imatt, imdec, imlev, imrel

  ; Oscillator and out
  icps cpsmidi
  aout oscil aenv, icps
  kout gainslider kmvol
  aout = aout*kout
    outs aout, aout
endin

instr 99
  allL, allR monitor
  fout "3sine_adsr.ogg", 50, allL, allR
endin

</CsInstruments>
<CsScore>
i 99 0 1000
</CsScore>
</CsoundSynthesizer>
