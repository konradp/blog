window.addEventListener('load', main);
const chords = {
  'E':  { 'maj': '022100', 'min': '022000', 'frets':{'maj':[1,3], 'min':[1,3]}},
  'F':  { 'maj': '133211', 'min': '133111', 'frets':{'maj':[1,3], 'min':[1,3]}},
  'F#': { 'maj': '244322', 'min': '244222', 'frets':{'maj':[2,4], 'min':[2,4]}},
  'G':  { 'maj': '321113', 'min': '355333', 'frets':{'maj':[1,3], 'min':[3,5]}},
  'G#': { 'maj': 'XX1114', 'min': '466444', 'frets':{'maj':[1,4], 'min':[4,6]}},
  'A':  { 'maj': 'X02220', 'min': 'X02210', 'frets':{'maj':[1,3], 'min':[1,3]}},
  'A#': { 'maj': 'X13331', 'min': 'X13321', 'frets':{'maj':[1,3], 'min':[1,3]}},
  'B':  { 'maj': 'X24442', 'min': 'X24432', 'frets':{'maj':[2,4], 'min':[2,4]}},
  'C':  { 'maj': 'X32010', 'min': 'X31013', 'frets':{'maj':[1,3], 'min':[1,3]}},
  'C#': { 'maj': 'X43121', 'min': 'X46654', 'frets':{'maj':[1,4], 'min':[4,6]}},
  'D':  { 'maj': 'XX0232', 'min': 'XX0231', 'frets':{'maj':[1,3], 'min':[1,3]}},
  'D#': { 'maj': 'XX1343', 'min': 'XX1342', 'frets':{'maj':[1,4], 'min':[1,4]}},
}
let fb = null;

function main() {
  fb = new Fingerboard('canvas-basic-chords');
  const div = document.querySelector('#app-basic-chords');
  // Controls
  const selectKey = createDropdown('key', Object.keys(chords));
  const selectType = createDropdown('type', ['maj','min']);
  div.appendChild(document.createElement('br'));
  div.appendChild(selectKey);
  div.appendChild(selectType);
  selectKey.addEventListener('change', onUpdateChord);
  selectType.addEventListener('change', onUpdateChord);

  // Draw chord
  let c = new Chord(chords['E']['maj']);
  fb.DrawChord(c);
  fb.SetFrets(1, 3);
  fb.FlipOrientation();
  fb.FlipMirror();
}


function onUpdateChord() {
  const key = document.querySelector('#key').value;
  const type = document.querySelector('#type').value;
  let chordInfo = chords[key];
  const c = new Chord(chordInfo[type]);
  fb.DrawChord(c);
  fb.SetFrets(chordInfo['frets'][type][0], chordInfo['frets'][type][1]);
  

}


function createDropdown(id, options) {
  const select = document.createElement('select');
  select.id = id;
  for (let i of options) {
    let opt = document.createElement('option');
    opt.innerHTML = i;
    select.appendChild(opt);
  }
  return select;
}
