class Piano1 {
  // Load and play sounds
  sound = null;
  constructor() {
    try {
      // Init AudioContext
      this.context = new AudioContext();
      if(!this.context)
        this.context = new webkitAudioContext();
      if(!this.context)
        this.context = new window.webkitAudioContext();
    } catch(e) {
      throw 'Web Audio API is not supported in this browser';
    }
  }


  play(name) {
    // Play note
    console.log('this', this);
    var audioBuffer = this.sound;
    if(!audioBuffer) {
      console.log('Could not play', name);
      return false;
    }
    if (this.debug) console.log('Playing', name);
    var source = this.context.createBufferSource();
    source.buffer = audioBuffer;
    source.connect(this.context.destination); // speakers
    source.start(0);
  }

  //////////////// PRIVATE ////////////////////
  loadSound(name, fileName) {
    // Load sound
    // Example: load('H', 'sounds/sound1.ogg');
    var self = this
    fileName = '/blog/com-app-piano-sampler-' + fileName;
    fetch(fileName)
      .then((response => response.arrayBuffer()))
      .then(data => {
      console.log('data', fileName, data); // DEBUG
      this.context.decodeAudioData(
        data,
        (audio) => {
          this.sound = audio;
        },
        (err) => { console.log('Loading failed: ', err); }
      )
    })
  }

}; // class Player
