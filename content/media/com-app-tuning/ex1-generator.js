window.addEventListener('load', () => {
  document.querySelector('#example1')
    .querySelector('#generate').addEventListener('click', () => {
      let root = document.querySelector('#example1');
      let f = parseFloat(root.querySelector('#fundamental').value);
      let n = parseInt(root.querySelector('#notes').value);
      let out = generate(f, n);
      root.querySelector('#output').innerHTML = JSON.stringify(out, null, 2);
    });

  // Create table
  let root = document.querySelector('#example1');
  let f = parseFloat(root.querySelector('#fundamental').value);
  let n = parseInt(root.querySelector('#notes').value);
  let t = document.querySelector('#ex1-tbl');
  for (let note of generate(f, 50)) {
    let tr = _new('tr');
    // values
    let td1 = _new('td')
    td1.innerHTML = note.k;
    tr.appendChild(td1);
    let td2 = _new('td')
    td2.innerHTML = note.i;
    tr.appendChild(td2);
    let td3 = _new('td')
    td3.innerHTML = note.j;
    tr.appendChild(td3);
    let td4 = _new('td')
    td4.innerHTML = note.freq.toFixed(2);
    tr.appendChild(td4);
    t.appendChild(tr);
  }
});

function generate(f, n) {
  console.log('N', n);
  let out = [];
  let i = 1;
  let k = 1;
  let nextOctave = f*2;
  while (out.length < n) {
    console.log('OUT', out.length);
    for (let j = 1; j <= i; j++) {
      let freq = f*i/j;
      if (freq > nextOctave) {
        // Only interested in notes within a single octave
        continue;
      }
      if (out.filter((e) => e.freq == freq).length) {
        // This freq is already present, skip it
        continue;
      }
      if (out.length == n) {
        continue;
      }
      out.push({
        i: i,
        j: j,
        k: k,
        freq,
      });
      k++;
    }
    i++;
  }
  return out;
}

function _new(name) {
  return document.createElement(name);
}
