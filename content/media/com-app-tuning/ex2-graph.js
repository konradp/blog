let graph = null;
let dataOverlays = [
  [
    // This is a regular scale in concert pitch
    { 'name': 'A4',  'freq': 440.00, 'color': 'red' },
    { 'name': 'A#4', 'freq': 466.16, 'color': 'red' },
    { 'name': 'B4',  'freq': 493.88, 'color': 'red' },
    { 'name': 'C5',  'freq': 523.25, 'color': 'red' },
    { 'name': 'C#5', 'freq': 554.37, 'color': 'red' },
    { 'name': 'D5',  'freq': 587.33, 'color': 'red' },
    { 'name': 'D#5', 'freq': 622.25, 'color': 'red' },
    { 'name': 'E5',  'freq': 659.26, 'color': 'red' },
    { 'name': 'F5',  'freq': 698.46, 'color': 'red' },
    { 'name': 'F#5', 'freq': 739.99, 'color': 'red' },
    { 'name': 'G5',  'freq': 783.99, 'color': 'red' },
    { 'name': 'G#5', 'freq': 830.61, 'color': 'red' },
    { 'name': 'A5', 'freq': 880, 'color': 'red' },

  ],
];


window.addEventListener('load', () => {
  let root = document.querySelector('#example2');
  console.log('test'); // DEBUG

  // DEBUG
  let data = [
    { 'name': 'A', 'freq': '400', 'osc': null },
    { 'name': 'B', 'freq': '600', 'osc': null },
    { 'name': 'C4', 'freq': '800', 'osc': null },
  ];
  graph = new Graph('ex2-canvas');
  graph.min = 420;
  graph.max = 900;
  graph.setData(data);
  graph.setOverlays(dataOverlays);
  updateData();

  root.querySelector('#ex2-notes').addEventListener('change', (e) => {
    updateData();
  });


});

function updateData() {
  let root = document.querySelector('#example2');
  let notes = parseInt(root.querySelector('#ex2-notes').value);
  let freqs = generate(440, notes);
  let data = [];
  for (let freq of freqs) {
    console.log('freq', freq.freq.toFixed(2));
    data.push({
      'name': freq.freq.toFixed(2),
      'freq': freq.freq,
    });
  }

  graph.setData(data);
  console.log(freqs);


}
