let graph = null;
window.addEventListener('load', () => {
  // Slider actions
  let sliders = document.querySelectorAll('.freq');
  for (slider of sliders) {
    slider.addEventListener('input', (e) => {
      // Update label on slider move
      let id = e.target.id.replace('freq', '');
      document.querySelector('#label' + id).value = e.target.value;
      updateGraph();
    });
  }

  // Draw graph
  graph = new Graph('canvas');
  updateGraph();
});


function updateGraph() {
  // Get all frequency values and update graph
  let freqs = document.querySelectorAll('.label');
  let names = document.querySelectorAll('.name');
  let data = [];
  for (let i = 0; i < freqs.length; i++) {
    let freq = freqs[i].value;
    let name = names[i].value;
    data.push({
      'name': names[i].value,
      'freq': parseFloat(freqs[i].value),
    });
  }
  // DEBUG, TODO: Extra static scale notes
  data.push({ 'name': 'C4', 'freq': 500, 'color': 'red' });
  graph.setData(data);
}


class Graph {
  min = 30; // TODO: hardcoded
  max = 800; // TODO: hardcoded
  margin = null;
  c = null;
  canvas = null;
  data = [];

  constructor() {
    const canvas = document.querySelector('.canvas');
    this.canvas = canvas;
    const c = canvas.getContext('2d');
    this.c = c;
    canvas.width = 100;
    canvas.height = 800; // TODO: Work out the pixel height/width from the canvas style size ratio
      // to make the circles round
    this.margin = canvas.height*(2/100);
    this.draw();
  };

  setData(data) {
    // data = [
    //   { freq: 220, name: 'A3' },
    //   { freq: 400, name: 'C3' },
    //   { freq: 440, name: 'A4' },
    // ]
    this.data = data;
    this.draw();
  };


  draw() {
    const c = this.c;
    c.clearRect(0, 0, this.canvas.width, this.canvas.height);

    // Draw axis
    c.beginPath();
    c.moveTo(10, 0);
    c.lineTo(10, canvas.height); // TODO: 10 hardcoded
    c.stroke();

    const data = this.data;
    for (let point of data) {
      this.drawBar(point.freq, point.name, point.color);
    }
  };


  drawBar(freq, name, color=null) {
    const pos = this.freqToPos(freq);
    const c = this.c;

    // circle
    c.beginPath();
    if (color != null) {
      c.globalAlpha = 0.5;
      c.fillStyle = color;
    }
    const rad = 10; // TODO: rad hardcoded
    c.arc(10, pos, rad, 0, 2*Math.PI); // TODO: 10 hardcoded
    c.fill();
    c.fillStyle = 'black';
    c.globalAlpha = 1;

    // label
    c.beginPath();
    let text = c.measureText(name);
    c.font = "1em sans";
    c.fillText(name, 15+rad, pos+text.width/2); // TODO x hardcoded, TODO: should be text height

  };


  freqToPos(freq) {
    // If min=10 and max=90, map these to 0% and 100%
    const k = (freq - this.min) / (this.max-this.min);
    const pos = this.canvas.height
      - this.margin
      - k * (this.canvas.height - 2*this.margin);
    return pos;
  };

}; // class Graph
