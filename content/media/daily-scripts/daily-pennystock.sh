#!/bin/bash

## FUNCTIONS
# Usage PrepTable LIST
function PrepTable() {
  [[ -z "$1" ]] && echo Empty list && exit 1
  echo "Symbol | Img365 | Img60"
  echo "--- | --- | ---"
  VAR=$(echo "$1" | tail +3)
  while read -r LINE; do
    IFS=" | " read -r SYMBOL PRICE CHANGE SECTOR <<< "$LINE"
    I="<table>"
    I="${I}<tr><td>$SYMBOL</td><td></tr>"
    I="${I}<tr><td>price:</td><td align="right">$PRICE</td></tr>"
    I="${I}<tr><td>chg:</td><td align="right">$CHANGE</td></tr>"
    I="${I}</table>"
    printf "%-6s | %-43s | %-45s \n" \
      "$I" \
      "![$SYMBOL](/media/$DATE-pstock/img/$SYMBOL.png)" \
      "![$SYMBOL](/media/$DATE-pstock/img/$SYMBOL-1.png)"
  done <<< "$VAR"
}

# Usage: ReadTemplate TEMPLATEFILE
function ReadTemplate() {
  m4 \
    -DDATE="$DATE" \
    -DTABLE="$TABLE" \
    "$1"
}

function repad:list() {
  while read -r LINE; do
    read -r SYMBOL _ _ <<< "$LINE"
    echo $SYMBOL
  done <<< "$1"
}

###
### MAIN
###
# Prepare vars
DATE=$(date +%Y-%m-%d)
LIST=$(curl -s http://5.152.176.107:5000/p)
QRY='.[] | [.symbol, .price, .changePerc] | join(" ")'
LIST=$(echo "$LIST" | jq -r "$QRY")
TABLE=$(PrepTable "$LIST")

[[ -z $LIST ]] && echo "Curl returned no data" && exit 1

# Read template
echo Create blog post
DOC=$(ReadTemplate daily-pennystock-template.m4)
echo "$DOC" > post/$DATE-daily-pennystock.md

# Generate graphs
LIST=$(repad:list "$LIST")
ARR=($LIST)
echo Got a list of ${#ARR[@]} elements
echo Generate graphs
mkdir -p media/$DATE-pstock
cp plot* media/$DATE-pstock/
cd media/$DATE-pstock
./plot.sh "$LIST"
