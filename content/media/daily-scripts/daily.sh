#!/bin/bash

## FUNCTIONS
# Usage PrepTable LIST
function PrepTable() {
  [[ -z "$1" ]] && echo Empty list && exit 1
  echo "Info | Img365 | Img60"
  echo "--- | --- | ---"
  VAR=$(echo "$1" | tail +3)
  while read -r LINE; do
    IFS=" | " read -r SYMBOL PRICE CHANGE SECTOR <<< "$LINE"
    I="<table>"
    I="${I}<tr><td>$SYMBOL</td><td></tr>"
    I="${I}<tr><td>price:</td><td align="right">$PRICE</td></tr>"
    I="${I}<tr><td>chg:</td><td align="right">$CHANGE</td></tr>"
    I="${I}<tr><td>sect:</td><td align="right">$SECTOR</td></tr>"
    I="${I}</table>"
    printf "%-6s | %-43s | %-45s \n" \
      "$I" \
      "![$SYMBOL](/media/$DATE/img/$SYMBOL.png)" \
      "![$SYMBOL](/media/$DATE/img/$SYMBOL-1.png)"
  done <<< "$VAR"
}

# Usage: ReadTemplate TEMPLATEFILE
function ReadTemplate() {
  m4 \
    -DDATE="$DATE" \
    -DLIST="$LIST" \
    -DTABLE="$TABLE" \
    "$1"
}

###
### MAIN
###
# Prepare vars
DATE=$(date +%Y-%m-%d)
TABLETMP=$(curl -s http://5.152.176.107:5000/list/winners/table)
LIST=$(curl -s http://5.152.176.107:5000/list/winners/list)
TABLE=$(PrepTable "$TABLETMP")

[[ -z $LIST ]] && echo "Curl returned no data" && exit 1

# Read template
echo Create blog post
DOC=$(ReadTemplate daily-template.m4)
echo "$DOC" > post/$DATE-daily.md

# Generate graphs
echo Generate graphs
mkdir -p media/$DATE
cp plot* media/$DATE/
cd media/$DATE
./plot.sh "$LIST"
