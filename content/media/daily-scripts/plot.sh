#!/bin/bash
# Usage: plot.sh LIST
# E.g.:  plot.sh "EARS AAPL"

# Config
DFILE="data.json"
DATE=$(date +%Y-%m-%d)

# helpers
die() {
  echo $@
  exit 1
}

clean:data() {
  echo Clean data
  cat data.json | grep -Ev 'Volume|change"|changePer|vwap|label' > tmp.json
  mv tmp.json data.json

  for SYMBOL in $1; do
    printf .
    QRY="del(.$SYMBOL.chart[].changeOverTime)"
    jq "$QRY" data.json > tmp.json
    mv tmp.json data.json
  done
  echo
}

download:symbols() {
  SYMBOLS=$(echo "$1" | tr ' ' ',')
  SYMBOLS=$(curl -s \
    "https://api.iextrading.com/1.0/stock/market/batch?symbols=$SYMBOLS&types=chart&range=1y")
  [[ -z $SYMBOLS ]] && echo No data received && exit
  echo "$SYMBOLS"
}

# Usage: download:data SYMBOLS
# E.g.:  download:data "EARS AAPL"
download:data() {
  echo Download data for $1
  [[ -z "$1" ]] && echo Empty list && exit 1
  local I=1 SYMBOLS=$1 TMPJSON="{}" TMPSYMBOLS=

  for SYMBOL in $SYMBOLS; do
    printf .
    let 'I +=1'
    TMPSYMBOLS="$TMPSYMBOLS $SYMBOL"
    if [[ $I == 100 ]]; then
      QUOTES=$(download:symbols "$TMPSYMBOLS")
      TMPJSON=$(echo "$TMPJSON$QUOTES" | jq -s '.[0] * .[1]')
      I=0
      TMPSYMBOLS=
    fi
  done
  echo

  # There lies a bug here if #SYMBOL%200 = 0
  QUOTES=$(download:symbols "$TMPSYMBOLS")
  TMPJSON=$(echo "$TMPJSON$QUOTES" | jq -s '.[0] * .[1]')
  echo "$TMPJSON" > data.json
}

# Usage: $0 SYMBOLS
# E.g.   $0 "EARS AAPL"
download:today:symbol() {
  curl -s -o "$2" "https://api.iextrading.com/1.0/stock/$1/quote"
  printf .
}

# Usage: download:today SYMBOLS
# E.g.   download:today "EARS AAPL"
download:today() {
  echo Download today for $1
  [[ -z "$1" ]] && echo Empty list && exit 1

  DATE=$(date +%Y-%m-%d)
  TMPDIR=$(mktemp -d)

  echo Downloading
  for SYMBOL in $1; do
    download:today:symbol $SYMBOL $TMPDIR/$SYMBOL &
  done
  wait
  echo

  echo Processing
  TMPJSON="$(cat data.json)"
  for SYMBOL in $1; do
    printf .
    QUOTE="$(cat $TMPDIR/$SYMBOL)"
    OPEN=$(echo "$QUOTE" | jq '.open')
    HIGH=$(echo "$QUOTE" | jq '.high')
    LOW=$(echo "$QUOTE" | jq '.low')
    CLOSE=$(echo "$QUOTE" | jq '.close')
    VOL=$(echo "$QUOTE" | jq '.avgTotalVolume')
    OBJ=$(cat <<EOL
[
  {
    "date": "$DATE",
    "open": $OPEN,
    "high": $HIGH,
    "low": $LOW,
    "close": $CLOSE,
    "volume": $VOL
  }
]
EOL
)
    QRY=".$SYMBOL.chart += $OBJ"
    TMPJSON="$(echo "$TMPJSON" | jq -e "$QRY")"
    #mv tmp.json data.json
  done
  echo "$TMPJSON" > data.json
  echo
  rm -rf $TMPDIR
}

getfield() {
  echo "$1" | jq -r ".$2"
}

#### MAIN
# Checks
if ! which gnuplot > /dev/null; then
  die Gnuplot is not present on this system
fi

[[ -z "$1" ]] && echo Empty list && exit 1
LIST="$1"

# Make data and img dirs
[[ -d data ]] && rm -rf data
mkdir data
[[ -d img ]] && rm -rf img
mkdir img

download:data "$LIST"
download:today "$LIST"
clean:data

echo Preparing data
SYMBOLS=$(jq -r '. | keys[]' $DFILE)
MIN=
for SYMBOL in $SYMBOLS; do
  printf .
  CHART=$(jq ".$SYMBOL.chart[]" $DFILE)
  for I in "$(echo "$CHART" | jq '.')"; do
    QRY='[
      .date,
      .open // "-",
      .high // "-",
      .low // "-",
      .close // "-",
      .volume // "-"
    ] | @tsv'
    DATA=$(echo "$I" | jq -r "$QRY")
    echo "$DATA" >> data/$SYMBOL.dat
  done
  TMP=$(head -n1 data/$SYMBOL.dat | cut -d$'\t' -f1)
  TMP=$(date -d $TMP +%s)
  if [ -z $MIN ] || [ $TMP -le $MIN ]; then
    MIN=$TMP
  fi
  MIN2=$(tail -n40 data/$SYMBOL.dat | head -n1 | cut -d$'\t' -f1)
done
echo
MIN=$(date -d @$MIN +%Y-%m-%d)

echo Drawing graphs
for SYMBOL in $SYMBOLS; do
  printf .
  filings $SYMBOL > data/$SYMBOL.filings
  gnuplot -c plot1.tmpl $SYMBOL $MIN $DATE
  gnuplot -c plot2.tmpl $SYMBOL $MIN2 $DATE
done
echo
