#!/bin/bash

## FUNCTIONS
die() {
  cat << EOF
Usage: $0 LIST
Example $0 "AAPL AMZN"
EOF
  exit 1
}

# Usage: get:today SYMBOLS
# Example SYMBOLS="EARS AAPL"
CleanData() {
  echo Clean data
  cat data.json | grep -Ev 'Volume|change"|changePer|vwap|label' > tmp.json
  mv tmp.json data.json

  for SYMBOL in $1; do
    printf .
    QRY="del(.$SYMBOL.chart[].changeOverTime)"
    jq "$QRY" data.json > tmp.json
    mv tmp.json data.json
  done
  echo
}

GetToday() {
  echo Get today
  DATE=$(date +%Y-%m-%d)
  for SYMBOL in $1; do
    printf .
    URL="https://api.iextrading.com/1.0/stock/$SYMBOL/quote"
    QUOTE=$(curl -s $URL)
    OPEN=$(echo "$QUOTE" | jq '.open')
    HIGH=$(echo "$QUOTE" | jq '.high')
    LOW=$(echo "$QUOTE" | jq '.low')
    CLOSE=$(echo "$QUOTE" | jq '.close')
    VOL=$(echo "$QUOTE" | jq '.avgTotalVolume')
    OBJ=$(cat <<EOL
[
  {
    "date": "$DATE",
    "open": $OPEN,
    "high": $HIGH,
    "low": $LOW,
    "close": $CLOSE,
    "volume": $VOL
  }
]
EOL
)
    QRY=".$SYMBOL.chart += $OBJ"
    jq -e "$QRY" data.json > tmp.json
    mv tmp.json data.json
  done
  echo
}

# Usage PrepImages LIST
function PrepImages() {
  [[ -z "$1" ]] && echo Empty list && exit 1
  echo "Info | Img365 | Img60"
  echo "--- | --- | ---"
  for SYMBOL in $1; do
    printf "%-6s | %s | %s \n" \
    $SYMBOL \
    "![$SYMBOL](/media/$DATE-jrn/img/$SYMBOL.png)" \
    "![$SYMBOL](/media/$DATE-jrn/img/$SYMBOL-1.png)"
  done
}

# Usage PrepInfo LIST
PrepInfo() {
  printf "Type: (MO=MarketOrder)\n\n\n"
  TR="%-8s | %-7s | %-6s | %-7s | %-7s | %-5s | %-6s"
  for SYMBOL in $1; do
    printf "$TR\n" \
      Symbol Trans Type Price Price Fee Total
    printf "$TR\n" \
      --- --- --- --- --- --- ---
    printf "$TR\n" \
      $SYMBOL buy
    printf "$TR\n\n" \
      $SYMBOL sell
    echo
  done
}

# Usage: ReadTemplate TEMPLATEFILE
function ReadTemplate() {
  m4 \
    -DDATE="$DATE" \
    -DINFO="$INFO" \
    -DLIST="$LIST" \
    -DIMAGES="$IMAGES" \
    "$1"
}

###
### MAIN
# Param check
[[ $1 ]] || die

# Prepare vars
DATE=$(date +%Y-%m-%d)
LIST=$1
LIST2=$(echo $LIST | tr " " ",")
IMAGES=$(PrepImages "$LIST")
INFO=$(PrepInfo "$LIST")

# Read template
echo Run daily
DOC=$(ReadTemplate daily-trade-journal-template.m4)
echo "$DOC" > post/$DATE-daily-trade-journal.md

# Generate graphs
GDIR="media/$DATE-jrn"
mkdir -p $GDIR/img
cp plot* $GDIR/
cd $GDIR
echo Download data
curl -s -o data.json \
  "https://api.iextrading.com/1.0/stock/market/batch?symbols=$LIST2&types=chart&range=1y"
[[ ! -s data.json ]] && echo No data received && exit
GetToday "$LIST"
CleanData "$LIST"
./plot.sh "$LIST"
