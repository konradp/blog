class Sax {
  constructor(canvas_id, settings={}) {
    // User-settable general settings
    this.settings = {
      isSimplified: true, // simplified diagram
      isStrokeAll: false, // whether to draw all keys even if not pressed,
      isHidePicker: false, // hide multiple fingerings picker
    };
    this.settings = { ...this.settings, ...settings };
    this.canvas = document.getElementById(canvas_id);
    this.context = this.canvas.getContext('2d');
    this.canvas.width = 140;
    this.canvas.height = 435;
    this.context.lineWidth = 3;
    this.context.strokeStyle = 'black';
    this.context.fillStyle = 'black';
    this.fingering = [];
    this.label = null;
    this.isDrawLabel = false;
    this.unitX = this.canvas.width/100;
    this.unitY = this.canvas.height/100;
    this.coords_data = {
      realistic: {
        octave:      { x: 14, y: 22 },
        frontF:      { x: 40, y:  2 },
        lh1:         { x: 41, y: 17 },
        lhBb:        { x: 40, y: 24 },
        lh2:         { x: 42, y: 32 },
        lh3:         { x: 50, y: 40 },
        palm1:       { x: 70, y:  8 },
        palm2:       { x: 86, y: 16 },
        palm3:       { x: 66, y: 20 },
        lhPinky:     { x: 84, y: 46 },
        rh1:         { x: 55, y: 60 },
        rh2:         { x: 55, y: 73 },
        rh3:         { x: 55, y: 86 },
        side:        { x: 14, y: 48 },
        highFs:      { x: 36, y: 67 },
        fsAlternate: { x: 36, y: 79 },
        rhPinky:     { x: 16, y: 94 },
      },
      simplified: {
        octave:      { x: 20, y: 10 },
        frontF:      { x: 50, y:  5 },
        lh1:         { x: 50, y: 15 },
        lhBb:        { x: 64, y: 22 },
        lh2:         { x: 50, y: 29 },
        lh3:         { x: 50, y: 43 },
        palm1:       { x: 80, y: 15 },
        palm2:       { x: 92, y: 22 },
        palm3:       { x: 80, y: 29 },
        lhPinky:     { x: 81, y: 50 },
        rh1:         { x: 50, y: 57 },
        rh2:         { x: 50, y: 71 },
        rh3:         { x: 50, y: 85 },
        side:        { x: 19, y: 48 },
        highFs:      { x: 36, y: 64 },
        fsAlternate: { x: 36, y: 78 },
        rhPinky:     { x: 19, y: 94 },
      },
    };
    this.coords = {};
    this.switcher = {
      enabled: false,
      pos: { x: 0, y: 0 },
      idx: 1,
      paths: {
        left: null,
        right: null,
      },
    };
    this.state = [
      {
        id: 0,
        name: 'octave',
        longName: 'octave',
        path: null, // the canvas path
        isDown: false,
      },
      { name: 'frontF', longName: 'F front' },
      { name: 'lh1', longName: 'B first' },
      { name: 'lhBb', longName: 'Bb' },
      { name: 'lh2', longName: 'A/C second' },
      { name: 'lh3', longName: 'G third'  },
      { name: 'palm1', longName: 'Eb palm' },
      { name: 'palm2', longName: 'D palm' },
      { name: 'palm3', longName: 'F palm' },
      { name: 'lhPinkyTop', longName: 'G#' },
      { name: 'lhPinkyLeft', longName: 'B low' }, // inner
      { name: 'lhPinkyRight', longName: 'C# low' }, // outer
      { name: 'lhPinkyBottom', longName: 'Bb low' },
      { name: 'rh1', longName: 'F first' },
      { name: 'rh2', longName: 'E second' },
      { name: 'rh3', longName: 'D third' },
      { name: 'side1', longName: 'E side' },
      { name: 'side2', longName: 'C side' },
      { name: 'side3', longName: 'Bb side' },
      { name: 'highFs', longName: 'F# high' },
      { name: 'fsAlternate', longName: 'F# alternate' },
      { name: 'rhPinky1', longName: 'Eb low' },
      { name: 'rhPinky2', longName: 'C low' },
    ];
    this.Init();
    this.DrawSax();
  }; // end constructor


  DrawSwitcher() {
    if (!this.switcher.enabled) {
      return;
    }
    const c = this.context;
    // TODO: move pos and size to 'this'
    const pos = this.Transform({x: 80, y: 98});
    const size = this.Transform({x: 40, y: 5});
    const x = pos.x;
    const y = pos.y;
    const sx = size.x;
    const sy = size.y;
    const as = 1*sx/5;

    // right arrow
    let pr = new Path2D;
    pr.moveTo(x+as,   y+sy/2);
    pr.lineTo(x+as,   y-sy/2);
    pr.lineTo(x+sx/2, y);
    pr.lineTo(x+as,   y+sy/2);
    c.fill(pr);
    this.switcher.right = pr;

    // left arrow
    let pl = new Path2D;
    pl.moveTo(x-as,   y+sy/2);
    pl.lineTo(x-as,   y-sy/2);
    pl.lineTo(x-sx/2, y);
    pl.lineTo(x-as,   y+sy/2);
    c.fill(pl);
    this.switcher.left = pl;

    // Text
    let text = this.switcher.idx;
    c.font = "16px sans bold";
    let t = c.measureText(text);
    c.fillText(text, x-t.width/2, y+t.width/2);
  }


  Init() {
    // Things which require calculation
    if (this.settings.isSimplified) {
      this.key_size = 14*this.unitX;
      this.coords = this.coords_data.simplified;
    } else {
      this.key_size = 7*this.unitX;
      this.coords = this.coords_data.realistic;
    }
    for (let i = 0; i<23; i++) {
      this.fingering.push(false);
      this.state[i].isDown = false; // DEBUG
    }
    // Transform coords from 0-100 x 0-100 to actual canvas coordinates
    for (let field in this.coords) {
      this.coords[field] = this.Transform(this.coords[field]);
    }
    this.canvas.addEventListener('mousedown', (e) => {
      // Switcher mouse actions
      if (!this.switcher.enabled) return;
      const c = this.context;
      const ca = this.canvas;
      const state = this.state;
      // Mouse position relative to canvas
      var rect = this.canvas.getBoundingClientRect();
      //const mouseX = e.clientX - rect.left;
      //const mouseY = e.clientY - rect.top;
      const mouseX = ca.width*(e.clientX - rect.left)/ca.clientWidth;
      const mouseY = ca.height*(e.clientY - rect.top)/ca.clientHeight;
      if (c.isPointInPath(this.switcher.right, mouseX, mouseY)) {
      console.log('mousedown'); // DEBUG
        if (this.switcher.idx < this.fingering.length) {
          this.switcher.idx++;
          this._setFingering(this.fingering[this.switcher.idx-1]);
        }
      } else if (c.isPointInPath(this.switcher.left, mouseX, mouseY)) {
        if (this.switcher.idx > 1) {
          this.switcher.idx--;
          this._setFingering(this.fingering[this.switcher.idx-1]);
        }
      }
    });
  };


  GetKeyByName(name) {
    return this.state.filter(e => e.name==name)[0];
  };
  isKeyDown(name) {
    return this.GetKeyByName(name).isDown;
  };


  setFingering(fingering, multiple=false) {
    // TODO: Multiple fingerings
    // Data is an array of 23 booleans
    // representing each key:
    // true=pressed, false: depressed
    // [
    //   false, // 00 LH: octave
    //   false, // 01 LH: frontF
    //   false, // 02 LH: 1
    //   false, // 03 LH: Bb
    //   false, // 04 LH: 2
    //   false, // 05 LH: 3
    //   false, // 06 LH: palm 1
    //   false, // 07 LH: palm 2
    //   false, // 08 LH: palm 3
    //   false, // 09 LH: pinky top
    //   false, // 10 LH: pinky left
    //   false, // 11 LH: pinky right
    //   false, // 12 LH: pinky bottom
    //   false, // 13 RH: 1
    //   false, // 14 RH: 2
    //   false, // 15 RH: 3
    //   false, // 16 RH: side 1
    //   false, // 17 RH: side 2
    //   false, // 18 RH: side 3
    //   false, // 19 RH: highfs
    //   false, // 20 RH: fsAlternate
    //   false, // 21 RH: pinky top
    //   false, // 22 RH: pinky bottom
    // ]
    // This does:
    // - decide if switcher should be on
    // - set fingering to the first fingering
    this.fingering = fingering;
    if (fingering.every((e) => Array.isArray(e))) {
      // multiple fingerings
      this.switcher.enabled = true;
      fingering = fingering[0];
    } else {
      // single fingering
      this.switcher.enabled = false;
    }
    this._setFingering(fingering);
  };

  _setFingering(fingering) {
    for (let i in fingering) {
      this.state[i].isDown = false;
      if (fingering[i] == true || fingering[i] == 1) {
        this.state[i].isDown = true;
      }
    }
    this.DrawSax();
  };


  setLabel(label) {
    this.label = label;
    this.isDrawLabel = true;
    this.DrawSax();
  };
  drawLabel() {
    if (this.isDrawLabel) {
      const c = this.context;
      c.font = "30px sans bold";
      //let t = c.measureText(text);
      let pos = this.Transform({x: 74, y: 6});
      c.fillText(this.label, pos.x, pos.y);
    }
  }


  addCanvasEventListener(type, func) {
    // User-provided callback for the canvas
    this.canvas.addEventListener(type, func.bind(this));
  }


  Transform(coords) {
    let a = {...coords};
    a.x = this.unitX*coords.x;
    a.y = this.unitY*coords.y;
    return a;
  }


  ////////////////// DRAWING FUNCTIONS /////////////
  DrawSax() {
    const c = this.context;
    c.clearRect(0, 0, this.canvas.width, this.canvas.height);
    const coords = this.coords;
    // Left hand
    this.DrawOctave();
    this.DrawFrontF(); // above 1
    this.GetKeyByName('lh1').path = this.DrawCircle(coords.lh1, this.isKeyDown('lh1'));  // 1
    this.DrawBb(); // Bb, between 1 and 2
    this.GetKeyByName('lh2').path = this.DrawCircle(coords.lh2, this.isKeyDown('lh2'));  // 2
    this.GetKeyByName('lh3').path = this.DrawCircle(coords.lh3, this.isKeyDown('lh3'));        // 3
    this.DrawPalmKeys();
    this.DrawPinkyLH(coords.lhPinky);

    // Horizontal line
    if (this.settings.isSimplified) {
      let a = this.Transform({x: 40, y: 50});
      let b = this.Transform({x: 60, y: 50});
      c.beginPath();
      c.moveTo(a.x, b.y);
      c.lineTo(b.x, b.y);
      c.stroke();
    }

    // Right hand
    this.GetKeyByName('rh1').path = this.DrawCircle(coords.rh1, this.isKeyDown('rh1'));    // 1
    this.DrawHighFs(coords.highFs); // between 1 and 2
    this.GetKeyByName('rh2').path = this.DrawCircle(coords.rh2, this.isKeyDown('rh2'));    // 2
    this.GetKeyByName('rh3').path = this.DrawCircle(coords.rh3, this.isKeyDown('rh3'));    // 3
    this.DrawSideKeys(coords.side);
    this.DrawFsAlternate(coords.fsAlternate); // between 2 and 3
    this.DrawPinkyRH(coords.rhPinky);
    this.DrawSwitcher(); // DEBUG
    this.drawLabel();
  }; // DrawSax()


  DrawOctave() {
    const c = this.context;
    const co = this.coords.octave;
    const isDown = this.GetKeyByName('octave').isDown;
    const size = (4/3)*this.key_size;
    let path = null;
    if (this.settings.isSimplified) {
      path = this.DrawEllipse(co, isDown);
    } else {
      c.beginPath();
      c.ellipse(
        co.x, co.y,
        (60/100)*size, // radX
        size,          // radY
        0, 0, 2*Math.PI);
      c.stroke();
      if (isDown) c.fill();
    }
    this.GetKeyByName('octave').path = path;
  }


  DrawFrontF() {
    const c = this.context;
    const co = this.coords.frontF;
    const isDown = this.GetKeyByName('frontF').isDown;
    let size = this.key_size;
    let path = null;
    if (this.settings.isSimplified) {
      if (!isDown && !this.settings.isStrokeAll) return;
      path = this.DrawSmallCircle(co, isDown);
    } else {
      size = (120/100)*size;
      let w = (150/100)*size;
      let h = (380/100)*size;
      c.beginPath();
      c.moveTo(co.x, co.y);
      c.lineTo(co.x, co.y + h);
      c.lineTo(co.x-w, co.y + h);
      c.lineTo(co.x, co.y);
      c.stroke();
      if (isDown) c.fill();
    }
    let a = this.GetKeyByName('frontF');
    a.path = path;
  } // DrawFrontF


  DrawBb() {
    // Bb: between 1 and 2
    const key = this.GetKeyByName('lhBb');
    const isDown = key.isDown;
    if (this.settings.isSimplified
      && !isDown
      && !this.settings.isStrokeAll
    ) {
      return;
    }
    key.path = this.DrawSmallCircle(this.coords.lhBb, isDown);
  } // DrawBb


  DrawCircle(coords, isDown, rad=this.key_size) {
    const c = this.context;
    let path = new Path2D();
    //path.arc(d.x, d.y, rad, 0, 2*Math.PI);
    path.arc(coords.x, coords.y, rad, 0, 2*Math.PI);
    c.stroke(path);
    if (isDown) c.fill(path);
    return path;
  }
  DrawSmallCircle(coords, isDown) {
    const c = this.context;
    let path = new Path2D();
    path.arc(coords.x, coords.y, (3/5)*this.key_size, 0, 2*Math.PI);
    c.stroke(path);
    if (isDown) c.fill(path);
    return path;
  }


  DrawPalmKeys() {
    const c = this.context;
    const co = this.coords;
    const isDown = [
      this.GetKeyByName('palm1').isDown,
      this.GetKeyByName('palm2').isDown,
      this.GetKeyByName('palm3').isDown,
    ];
    if (this.settings.isSimplified
      && !isDown.includes(true)
      && !this.settings.isStrokeAll
    ) {
      // Do not draw keys unless one of them is pressed
      // or stroking all keys is enforced
      return;
    }
    this.GetKeyByName('palm1').path = this.DrawPalmKey(co.palm1, isDown[0]);
    this.GetKeyByName('palm2').path = this.DrawPalmKey(co.palm2, isDown[1]);
    this.GetKeyByName('palm3').path = this.DrawPalmKey(co.palm3, isDown[2]);
  }
  DrawPalmKey(coords, isDown) {
    // Cubic Bézier curve
    const c = this.context;
    let path = new Path2D();
    if (this.settings.isSimplified) {
      path = this.DrawEllipse(coords, isDown);
    } else {
      c.moveTo(x, y);
      path.bezierCurveTo(
        coords.x-20, coords.y+54, // cp1
        coords.x+30, coords.y+64, // cp2
        coords.x,    coords.y,    // end
      );
    }
    c.stroke(path);
    if (isDown) c.fill(path);
    return path;
  }


  DrawPinkyLH(coords) {
    const c = this.context;
    const x = coords.x;
    const y = coords.y;
    const s = (5/4)*this.key_size;
    const isDown = [
      this.GetKeyByName('lhPinkyTop').isDown,
      this.GetKeyByName('lhPinkyLeft').isDown,
      this.GetKeyByName('lhPinkyRight').isDown,
      this.GetKeyByName('lhPinkyBottom').isDown,
    ];
    if (this.settings.isSimplified) {
      if (!isDown.includes(true)) {
        return;
      }
      // top
      let p = new Path2D;
      p.arc(x, y, s, (-1/6)*Math.PI, (7/6)*Math.PI, true);
      p.moveTo(x-s+2, y-s/2);
      p.lineTo(x+s-2, y-s/2);
      c.stroke(p);
      if (isDown[0]) c.fill(p);
      this.GetKeyByName('lhPinkyTop').path = p;

      // left
      p = new Path2D();
      p.arc(x, y, s, (5/6)*Math.PI, (7/6)*Math.PI, false);
      p.lineTo(x, y-s/2);
      p.lineTo(x, y+s/2);
      p.lineTo(x-s+s/10, y+s/2);
      c.stroke(p);
      if (isDown[1]) c.fill(p);
      this.GetKeyByName('lhPinkyLeft').path = p;

      // right
      p = new Path2D();
      p.arc(x, y, s, (1/6)*Math.PI, (11/6)*Math.PI, true);
      p.lineTo(x, y-s/2);
      p.lineTo(x, y+s/2);
      p.lineTo(x+s-s/10, y+s/2);
      c.stroke(p);
      if (isDown[2]) c.fill(p);
      this.GetKeyByName('lhPinkyRight').path = p;

      // bottom
      p = new Path2D();
      p.arc(x, y, s, (1/6)*Math.PI, (5/6)*Math.PI, false);
      c.stroke(p);
      if (isDown[3]) c.fill(p);
      this.GetKeyByName('lhPinkyBottom').path = p;
    } else {
      s = (7/2)*this.key_size;
      c.lineWidth = c.lineWidth - 1;
      const x = x - s/2;
      const y = y - s/2;
      c.strokeRect(x, y, s, s); // contour

      // top, left, right, bottom
      c.strokeRect(x, y, s, s/3);
      if (isDown[0])
        c.fillRect(x, y, s, s/3);
      c.strokeRect(x, y+s/3, s/2, s/3);
      if (isDown[1])
        c.fillRect(x, y+s/3, s/2, s/3);
      c.strokeRect(x+s/2, y+s/3, s/2, s/3);
      if (isDown[2])
        c.fillRect(x+s/2, y+s/3, s/2, s/3);
      c.strokeRect(x, y+2*s/3, s, s/3);
      if (isDown[3])
        c.fillRect(x, y+2*s/3, s, s/3);

      c.lineWidth = c.lineWidth + 1;
    }
  }


  //////////////// RH //////////////
  DrawSideKeys(coords) {
    const c = this.context;
    const x = coords.x;
    const y = coords.y;
    const isDown = [
      this.GetKeyByName('side1').isDown,
      this.GetKeyByName('side2').isDown,
      this.GetKeyByName('side3').isDown,
    ];
    if (this.settings.isSimplified) {
      // Simplified diagram
      if (!isDown.includes(true)
        && !this.settings.isStrokeAll
      ) {
        // Do not draw keys unless one of them is pressed
        // or stroking all keys is enforced
        return;
      }
      this.GetKeyByName('side1').path = this.DrawEllipse(coords, isDown[0]);
      this.GetKeyByName('side2').path = this.DrawEllipse({
        x: x,
        y: y+this.Transform({x:0,y:12}).y,
      }, isDown[1]);
      this.GetKeyByName('side3').path = this.DrawEllipse({
        x: x,
        y: y+this.Transform({x:0,y:24}).y,
      }, isDown[2]);
    } else {
      // Realistic diagram
      const size = 8*this.key_size;
      const h_divider = size/14;
      const w = size/10;
      const h_height = (size - 2*h_divider)/3
      // 1, 2, 3
      c.strokeRect(x-w/2, y, w, h_height);
      if (isDown[0])
        c.fillRect(x-w/2, y, w, h_height);
      c.strokeRect(x-w/2, y+h_height+h_divider, w, h_height);
      if (isDown[1])
        c.fillRect(x-w/2, y+h_height+h_divider, w, h_height);
      c.strokeRect(x-w/2, y+2*h_height+2*h_divider, w, h_height);
      if (isDown[2])
        c.fillRect(x-w/2, y+2*h_height+2*h_divider, w, h_height);
    }
  }


  DrawPinkyRH(coords) {
    const c = this.context;
    const s = (5/4)*this.key_size;
    const isDown = [
      this.GetKeyByName('rhPinky1').isDown,
      this.GetKeyByName('rhPinky2').isDown,
    ];
    let x = coords.x;
    let y = coords.y;
    if (this.settings.isSimplified) {
      this.DrawCircle(coords, false, s); // contour
      // top
      const x = coords.x;
      const y = coords.y;
      let p = new Path2D();
      p.arc(x, y, s, 0, Math.PI, true);
      p.moveTo(x-s+2, y);
      p.lineTo(x+s-2, y);
      c.stroke(p);
      if (isDown[0]) c.fill(p);
      this.GetKeyByName('rhPinky1').path = p;

      // bottom
      p = new Path2D();
      p.arc(x, y, s, 0, Math.PI);
      p.moveTo(x-s+2, y);
      p.lineTo(x+s-3, y);
      c.stroke(p);
      if (isDown[1]) c.fill(p);
      this.GetKeyByName('rhPinky2').path = p;
    } else {
      const s = (7/2)*this.key_size;
      c.lineWidth--;
      x = x - s/2;
      y = y - s/2;
      // contour
      c.strokeRect(x, y, s, s);
      // top
      c.strokeRect(x, y, s, s/2);
      if (isDown[0]) c.fillRect(x, y, s, s/2);
      // bottom
      c.strokeRect(x, y+s/2, s, s/2);
      if (isDown[1]) c.fillRect(x, y+s/2, s, s/2);
      c.lineWidth++;
    }
  }


  DrawHighFs(coords) {
    const c = this.context;
    const isDown = this.GetKeyByName('highFs').isDown;
    const x = coords.x;
    const y = coords.y;
    if (this.settings.isSimplified) {
      if (!isDown && !this.settings.isStrokeAll) {
        // Do not draw keys unless one of them is pressed
        // or stroking all keys is enforced
        return;
      }
      this.GetKeyByName('highFs').path = this.DrawSmallCircle(coords, isDown);
    } else {
      const h = this.key_size*4;
      c.beginPath();
      c.moveTo(x + h/4,       y);
      c.lineTo(x - h/20,      y + h/2);
      c.lineTo(x - h/4,       y + h/2 - h/10);
      c.lineTo(x - h/4 + h/8, y);
      c.lineTo(x - h/4,       y - h/2 + h/10);
      c.lineTo(x - h/20,      y - h/2);
      c.lineTo(x + h/4,       y);
      c.stroke();
      if (isDown) c.fill();
    }
  } // DrawHighFs


  DrawFsAlternate(coords) {
    const c = this.context;
    const isDown = this.GetKeyByName('fsAlternate').isDown;
    const size = this.key_size;
    const x = coords.x;
    const y = coords.y;
    if (this.settings.isSimplified) {
      if (!isDown && !this.settings.isStrokeAll) {
        // Do not draw keys unless one of them is pressed
        // or stroking all keys is enforced
        return;
      }
      this.GetKeyByName('fsAlternate').path = this.DrawSmallCircle(coords, isDown);
    } else {
      c.beginPath();
      c.ellipse(x, y,
        (60/100)*size, // radX
        (120/100)*size, // radY
        13*Math.PI/20, // rot
        0, 2*Math.PI);
      c.stroke();
      if (isDown) c.fill();
    }
  }


  DrawEllipse(coords, isDown, size=this.key_size) {
    const c = this.context;
    let path = new Path2D();
    path.ellipse(
      coords.x, coords.y,
      (40/100)*size, // radX
      size,          // radY
      0, 0, 2*Math.PI);
    c.stroke(path);
    if (isDown) c.fill(path);
    return path;
  }
}; // class Sax
