class Example1PressingKeys {
  constructor(canvas_id) {
    this.canvas = document.getElementById(canvas_id);
    this.context = this.canvas.getContext('2d');
    //this.canvas.width = window.innerWidth/2;
    //this.canvas.height = window.innerHeight - 20;
    //this.canvas.width = 187;
    this.canvas.width = 140;
    this.canvas.height = 435;
    this.context.lineWidth = 3;
    this.context.strokeStyle = 'black';
    this.context.fillStyle = 'black';
    this.fingering = [];
    for (let i = 0; i<23; i++) { this.fingering.push(false); }
    this.unitX = this.canvas.width/100;
    this.unitY = this.canvas.height/100;
    this.key_size = 7*this.unitX;
    this.key_coordinates = {
      LH: {
        octave: { x: 14, y: 22 },
        frontF: { x: 40, y: 2 },
        1: { x: 41, y: 17 },
        Bb: { x: 40, y: 24 },
        2: { x: 42, y: 32 },
        3: { x: 50, y: 40 },
        palm: {
          1: { x: 70, y: 8 },
          2: { x: 86, y: 16 },
          3: { x: 66, y: 20 },
        },
        pinky: { x: 84, y: 46 },
      },
      RH: {
        1:           { x: 55, y: 60 },
        2:           { x: 55, y: 73 },
        3:           { x: 55, y: 86 },
        side:        { x: 14, y: 48 },
        highFs:      { x: 36, y: 67 },
        fsAlternate: { x: 36, y: 79 },
        pinky:       { x: 16, y: 94 },
      },
    };
    this.DrawSax();
  };


  SetFingering() {
    // Data is an array of 23 booleans
    // representing each key:
    // true=pressed, false: depressed
    // [
    //   false, // 00 LH: octave
    //   false, // 01 LH: frontF
    //   false, // 02 LH: 1
    //   false, // 03 LH: Bb
    //   false, // 04 LH: 2
    //   false, // 05 LH: 3
    //   false, // 06 LH: palm 1
    //   false, // 07 LH: palm 2
    //   false, // 08 LH: palm 3
    //   false, // 09 LH: pinky top
    //   false, // 10 LH: pinky left
    //   false, // 11 LH: pinky right
    //   false, // 12 LH: pinky bottom
    //   false, // 13 RH: 1
    //   false, // 14 RH: 2
    //   false, // 15 RH: 3
    //   false, // 16 RH: side 1
    //   false, // 17 RH: side 2
    //   false, // 18 RH: side 3
    //   false, // 19 RH: highfs
    //   false, // 20 RH: fsAlternate
    //   false, // 21 RH: pinky top
    //   false, // 22 RH: pinky bottom
    // ]
    this.fingering = fingering;
    this.DrawSax();
  };


  Transform(coords) {
    let a = {...coords};
    a.x = this.unitX*coords.x;
    a.y = this.unitY*coords.y;
    return a;
  }

  ////////////////// DRAWING FUNCTIONS /////////////
  DrawSax() {
    const c = this.context;
    c.clearRect(0, 0, this.canvas.width, this.canvas.height);
    const size = this.key_size;
    const f = this.fingering;
    // Left hand
    let coords = this.key_coordinates.LH;
    this.DrawOctave(coords.octave, (4/3)*size, f[0]);
    this.DrawFrontF(coords.frontF, (120/100)*size, f[1]); // above 1
    this.DrawCircle(coords['1'], size, f[2]);  // 1
    this.DrawCircle(coords.Bb, (60/100)*size, f[3]); // Bb, between 1 and 2
    this.DrawCircle(coords['2'], size, f[4]);  // 2
    this.DrawCircle(coords['3'], size, f[5]);        // 3
    this.DrawPalmKeys(coords.palm, [f[6], f[7], f[8]]);
    this.DrawPinkyLH(coords.pinky, (7/2)*size, [f[9], f[10], f[11], f[12]]);

    // Right hand
    coords = this.key_coordinates.RH;
    this.DrawCircle(coords['1'], size, f[13]); // 1
    this.DrawCircle(coords['2'], size, f[14]); // 2
    this.DrawCircle(coords['3'], size, f[15]); // 3
    this.DrawSideKeys(coords.side, 8*size, [f[16], f[17], f[18]]);
    this.DrawHighFSharp(coords.highFs, size, f[19]);       // between 1 and 2
    this.DrawFsAlternate(coords.fsAlternate, size, f[20]); // between 2 and 3
    this.DrawPinkyRH(coords.pinky, (7/2)*size, [f[21], f[22]]);
  }; // DrawSax()


  DrawOctave(coords, size, is_down) {
    const c = this.context;
    const x = this.Transform(coords).x;
    const y = this.Transform(coords).y;
    c.beginPath();
    c.ellipse(
      x, y,
      (60/100)*size, // radX
      size,          // radY
      0, 0, 2*Math.PI);
    c.stroke();
    if (is_down) c.fill();
  }


  DrawFrontF(coords, size) {
    const c = this.context;
    let d = this.Transform(coords);
    let w = (150/100)*size;
    let h = (380/100)*size;
    c.beginPath();
    c.moveTo(d.x, d.y);
    c.lineTo(d.x, d.y + h);
    c.lineTo(d.x-w, d.y + h);
    c.lineTo(d.x, d.y);
    c.stroke();
    if (this.fingering[1]) c.fill();
  } // DrawFrontF


  DrawCircle(coords, rad, is_pressed) {
    const c = this.context;
    const d = this.Transform(coords);
    c.beginPath();
    c.arc(d.x, d.y, rad, 0, 2*Math.PI);
    c.stroke();
    if (is_pressed) c.fill();
  }


  DrawPalmKeys(coords, is_down) {
    const c = this.context;
    this.DrawPalmKey(coords['1'], is_down[0]);
    this.DrawPalmKey(coords['2'], is_down[1]);
    this.DrawPalmKey(coords['3'], is_down[2]);
  }
  DrawPalmKey(coords, is_down) {
    // TODO: Remove hardcoded, use size
    // Cubic Bézier curve
    const c = this.context;
    const x = this.Transform(coords).x;
    const y = this.Transform(coords).y;
    c.beginPath();
    c.moveTo(x, y);
    c.bezierCurveTo(
      x-20, y+54, // cp1
      x+30, y+64, // cp2
      x, y,       // end
    );
    c.stroke();
    if (is_down) c.fill();
  }


  DrawPinkyLH(coords, size, is_down) {
    const c = this.context;
    c.lineWidth = c.lineWidth - 1;
    const x = this.Transform(coords).x - size/2;
    const y = this.Transform(coords).y - size/2;
    c.strokeRect(x, y, size, size); // contour

    // top
    c.strokeRect(x, y, size, size/3);
    if (is_down[0])
      c.fillRect(x, y, size, size/3);

    // left
    c.strokeRect(x, y+size/3, size/2, size/3);
    if (is_down[1])
      c.fillRect(x, y+size/3, size/2, size/3);

    // right
    c.strokeRect(x+size/2, y+size/3, size/2, size/3);
    if (is_down[2])
      c.fillRect(x+size/2, y+size/3, size/2, size/3);

    // bottom
    c.strokeRect(x, y+2*size/3, size, size/3);
    if (is_down[3])
      c.fillRect(x, y+2*size/3, size, size/3);

    c.lineWidth = c.lineWidth + 1;
  }


  //////////////// RH //////////////
  DrawSideKeys(coords, size, is_down) {
    const c = this.context;
    const h_divider = size/14;
    const w = size/10;
    const x = this.Transform(coords).x;
    const y = this.Transform(coords).y;
    const h_height = (size - 2*h_divider)/3
    c.strokeRect(x-w/2, y, w, h_height);
    if (is_down[0])
      c.fillRect(x-w/2, y, w, h_height);

    c.strokeRect(x-w/2, y+h_height+h_divider, w, h_height);
    if (is_down[1])
      c.fillRect(x-w/2, y+h_height+h_divider, w, h_height);

    c.strokeRect(x-w/2, y+2*h_height+2*h_divider, w, h_height);
    if (is_down[2])
      c.fillRect(x-w/2, y+2*h_height+2*h_divider, w, h_height);
  }


  DrawPinkyRH(coords, size, is_down) {
    const c = this.context;
    c.lineWidth--;
    const x = this.Transform(coords).x - size/2;
    const y = this.Transform(coords).y - size/2;
    c.strokeRect(x, y, size, size); // contour

    c.strokeRect(x, y, size, size/2); // top
    if (is_down[0])
      c.fillRect(x, y, size, size/2); // top

    c.strokeRect(x, y+size/2, size, size/2); // bottom
    if (is_down[1])
      c.fillRect(x, y+size/2, size, size/2); // bottom
    c.lineWidth++;
  }


  DrawHighFSharp(coords, size, is_down) {
    const c = this.context;
    const x = this.Transform(coords).x;
    const y = this.Transform(coords).y;
    const h = size*4;
    c.beginPath();
    c.moveTo(x + h/4,       y);
    c.lineTo(x - h/20,      y + h/2);
    c.lineTo(x - h/4,       y + h/2 - h/10);
    c.lineTo(x - h/4 + h/8, y);
    c.lineTo(x - h/4,       y - h/2 + h/10);
    c.lineTo(x - h/20,      y - h/2);
    c.lineTo(x + h/4,       y);
    c.stroke();
    if (is_down) c.fill();
  } // DrawHighFSharp


  DrawFsAlternate(coords, size, is_down) {
    const c = this.context;
    const x = this.Transform(coords).x;
    const y = this.Transform(coords).y;
    c.beginPath();
    c.ellipse(x, y,
      (60/100)*size, // radX
      (120/100)*size, // radY
      13*Math.PI/20, // rot
      0, 2*Math.PI);
    c.stroke();
    if (is_down) c.fill();
  }


}; // class Sax
