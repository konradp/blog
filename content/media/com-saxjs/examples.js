// Example 1: auto pressing keys
// Example 2: simplified diagram, auto pressing keys
const example1 = new Example1PressingKeys('example1-pressing-keys');
const example2 = new Example2SimplifiedDiagram('example2-simplified-diagram');
let fingering = [];
let idx = 0;
setInterval(function() {
  fingering = [];
  for (let i = 0; i<23; i++) {
    fingering.push(false);
  }
  fingering[idx] = true;
  example1.SetFingering(fingering);
  example2.SetFingering(fingering);
  idx++;
  if (idx == 23) idx = 0;
}, 100);


// Example 3: interactive: press keys
let example3 = new Sax('example3-interactive-press', {
  isSimplified: true,
  isStrokeAll: true,
});
example3.addCanvasEventListener('mousedown', function(e) {
  // Open or close pads on click
  const c = this.context;
  const state = this.state;
  // Mouse position relative to canvas
  var rect = this.canvas.getBoundingClientRect();
  const mouseX = e.clientX - rect.left;
  const mouseY = e.clientY - rect.top;
  for (i in state) {
    if (state[i].hasOwnProperty('path')) {
      if (c.isPointInPath(state[i].path, mouseX, mouseY)) {
        state[i].isDown = !state[i].isDown;
        this.DrawSax();
      }
    }
  }
});


// Example 4: interactive: key label on hover
const example4 = new Sax('example4-interactive-hover', {
  isSimplified: true,
  isStrokeAll: true,
});
example4.addCanvasEventListener('mousemove', function(e) {
  const c = this.context;
  const state = this.state;
  // Mouse position relative to canvas
  var rect = this.canvas.getBoundingClientRect();
  const mouseX = e.clientX - rect.left;
  const mouseY = e.clientY - rect.top;
  const div = document.getElementById('name');
  for (i in state) {
    if (state[i].hasOwnProperty('path')) {
      if (c.isPointInPath(state[i].path, mouseX, mouseY)) {
        div.innerHTML = state[i].longName;
        state[i].isDown = true;
        this.DrawSax();
      } else {
        // Unpress all other keys
        state[i].isDown = false;
      }
    }
  }
});


// Example 5: interactive: picker
const sax = new Sax('example5-interactive-picker', {
  isSimplified: true,
  isStrokeAll: false,
});
sax.setFingering([
  [
    true,  false, true , false, true,
    false, false, false, false, false,
    false, false, false, false, false,
    false, false, false, true,  false,
    false, false, false,
  ], [
    true,  false, true,  false, false,
    false, false, false, false, false,
    false, false, false, true,  false,
    false, false, false, false, false,
    false, false, false,
  ], [
    true,  false, true,  true,  false,
    false, false, false, false, false,
    false, false, false, false, false,
    false, false, false, false, false,
    false, false, false,
  ],
]);

// Example 6: multiple cards
function Example6() {
  function _new(tag) {
    return document.createElement(tag);
  }
  function _getId(id) {
    return document.getElementById(id);
  }
  const div = _getId('example6-multiple-cards');
  for (let i = 0; i <=3; i++) {
    let note = SAX_NOTES[i];
    const canvasId = 'canvas' + i;
    let canvas = _new('canvas');
    canvas.id = canvasId;
    canvas.style = 'border:1px solid';
    div.appendChild(canvas);
    let sax = new Sax(canvasId);
    sax.setFingering(note);
  }
}
Example6();
