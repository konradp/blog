#!/bin/bash
# Comment

# Config
DFILE="data.json"
DATE=$(date +%Y-%m-%d)

# helpers
die() {
  echo $@
  exit 1
}

getfield() {
  echo "$1" | jq -r ".$2"
}

# Checks
if ! which gnuplot > /dev/null; then
  die Gnuplot is not present on this system
fi

# Make data and img dirs
[[ -d data ]] && rm -rf data
mkdir data
[[ -d img ]] && rm -rf img
mkdir img

echo Preparing data
SYMBOLS=$(jq -r '. | keys[]' $DFILE)
MIN=
for SYMBOL in $SYMBOLS; do
  printf .
  CHART=$(jq ".$SYMBOL.chart[]" $DFILE)
  for I in "$(echo "$CHART" | jq '.')"; do
    QRY='[
      .date,
      .open // "-",
      .high // "-",
      .low // "-",
      .close // "-",
      .volume // "-"
    ] | @tsv'
    DATA=$(echo "$I" | jq -r "$QRY")
    echo "$DATA" >> data/$SYMBOL.dat
  done
  TMP=$(head -n1 data/$SYMBOL.dat | cut -d$'\t' -f1)
  TMP=$(date -d $TMP +%s)
  if [ -z $MIN ] || [ $TMP -le $MIN ]; then
    MIN=$TMP
  fi
  MIN2=$(tail -n40 data/$SYMBOL.dat | head -n1 | cut -d$'\t' -f1)
done
echo
MIN=$(date -d @$MIN +%Y-%m-%d)

echo Drawing graphs
for SYMBOL in $SYMBOLS; do
  printf .
  gnuplot -c plot1.tmpl $SYMBOL $MIN $DATE
  gnuplot -c plot2.tmpl $SYMBOL $MIN2 $DATE
done
echo
