#!/usr/bin/env python3
import json

with open('data.js', 'r') as f:
  data = json.loads(f.read())
  print(data)
  print(json.dumps(data, indent=2))
