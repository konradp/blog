window.addEventListener('load', () => {
  let countOctaves = 2;
  const countKeys = countOctaves*12;
  const keys = [ 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0 ];
  const countWhites = countOctaves*7;
  const canvas = document.getElementById('ex3');
  const c = canvas.getContext('2d');
  const margin = canvas.width/20;
  const boxW = canvas.width-2*margin;
  const boxH = canvas.height - 2*margin;
  const whiteW = boxW/countWhites;
  const blackW = whiteW/2;
  draw();

  function isWhite(key) {
    //return (keys[(key)%12] == 0)? true : false;
    return [0,2,4,5,7,9,11].includes(key%12);
  }

  function drawKey(i) {
    let reminder = i%12;
    let k = Math.floor(i/12);
    // TODO: instead of slice/filter, find the index of reminder here (+1), use 0,2,4,5,7,8,11 whiteKeys
    let n = keys.slice(0, reminder).filter(x=>x==0).length;
    let x = margin + (k*7 + n)*whiteW;
    if (isWhite(i)) {
      c.strokeRect(x, margin, whiteW, boxH);
    } else {
      // offset black keys
      x -= (1/2)*blackW;
      c.fillRect(x, margin, blackW, (3/5)*boxH);
    }
  }

  function draw() {
    c.clearRect(0, 0, canvas.width, canvas.height);
    c.strokeRect(margin, margin, boxW, boxH);

    // Draw white keys, then black keys
    for (let i = 0; i<countKeys; i++) {
      if (isWhite(i)) {
        drawKey(i);
      }
    }
    for (let i = 0; i<=countKeys; i++) {
      if (!isWhite(i)) {
        drawKey(i);
      }
    }
  }
});
