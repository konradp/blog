window.addEventListener('load', () => {
  const keys = [ 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0 ];

  // functions
  function isWhite(key) {
    return (keys[key-1] == 0)? true : false;
  }

  function draw() {
    const canvas = document.getElementById('ex2');
    const c = canvas.getContext('2d');
    const margin = canvas.width/20;
    //c.clearRect(0, 0, w, h);
    const boxW = canvas.width-2*margin;
    const boxH = canvas.height - 2*margin;
    const whiteW = boxW/7;
    const blackW = boxW/12;
    const w = boxW/7;
    c.strokeRect(margin, margin, boxW, boxH);

    for (let i = 1; i<=12; i++) {
      // centres
      let x = margin + (i)*(boxW/12);
      c.beginPath();
      c.moveTo(x - (1/2)*(boxW/12), margin/2);
      c.lineTo(x - (1/2)*(boxW/12), 2/3*margin);
      c.stroke();

      // key boundaries
      if (i == 1) {
        // extra boundary on the left
        let x = margin;
        c.beginPath();
        c.moveTo(margin, margin/2);
        c.lineTo(margin, margin);
        c.stroke();
      }
      c.beginPath();
      c.moveTo(x, margin/2);
      c.lineTo(x, margin);
      c.stroke();
    }

    for (let i = 1; i<=12; i++) {
      // white keys
      if (isWhite(i)) {
        let n = keys.slice(0, i).filter(x=>x==0).length;
        let x = margin + (n-1)*(whiteW);
        c.strokeRect(x, margin, whiteW, boxH);
      }
    }

    for (let i = 1; i<=12; i++) {
      // black keys
      if (!isWhite(i)) {
        let n = keys.slice(0, i).filter(x=>x==0).length;
        let x = margin + (n)*whiteW - (1/2)*blackW;
        c.fillRect(x, margin, blackW, boxH/2);
      }
    }
  }
  draw();
});
