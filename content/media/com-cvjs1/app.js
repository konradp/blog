let structure = null;
const d = document;
let body = null;
let div = null;
let first = true;
let content = null;


function OnLoad() {
  body = document.body;
  // Table
  div = document.getElementById('content');
  div.setAttribute('border', 'true');

  fetch('/blog/media/com-cvjs1/structure.yaml')
    .then((response => response.text()))
    .then(data => {
      structure = jsyaml.load(data);
      Draw();
    })

};

function _new(tag) {
  return document.createElement(tag);
}

function FlipVisible(el) {
  let cur = el.style.getPropertyValue('display');
  if (cur == 'block') {
    // hide
    el.style.setProperty('display', 'none');
    el.style.setProperty('height', 0);
  } else if (cur == 'none') {
    // show
    el.style.setProperty('display', 'block');
    el.style.removeProperty('height');
  }
};

function FlipButton(btn) {
  let val = btn.innerHTML;
  if (val == '+') {
    btn.innerHTML = '-';
  } else {
    btn.innerHTML = '+';
  }
};

function WalkStructure(root, parent_div) {
  let ul = d.createElement('ul');
  ul.className = 'cv-ul';
  ul.style.setProperty('height', 0);
  if (!first) {
    ul.style.setProperty('display', 'none');
  }
  first = false;
  parent_div.appendChild(ul);
  for (key in root) {
    let li = d.createElement('li'),
        li_header = d.createElement('div'),
        li_title = d.createElement('span'),
        li_button = d.createElement('span');
      li_title.className = 'section_title';
      li_button.className = 'button';
      li_button.innerHTML = '+';
    if (typeof(root[key]) != 'string') {
      li_title.addEventListener('click', (e) => {
        e.stopPropagation(); // Avoid parent onclick
        let t = e.target;
        let child = t.parentNode.querySelector('ul');
        let btn = t.parentNode.querySelector('.button');
        FlipButton(btn);
        FlipVisible(child);
      });
    } else {
      li_button.innerHTML = '-';
    }
    if (!Array.isArray(root)) {
      li_title.innerHTML = key;
      if (typeof(root[key]) == 'string') {
        // No children, just a string
        li_title.innerHTML = key + ': ' + root[key];
        // Different behaviour
        li_button.innerHTML = '-';
      }
    } else {
      li_title.innerHTML = root[key];
    }
    li_header.appendChild(li_button);
    li_header.appendChild(li_title);
    li_header.appendChild(li);
    ul.appendChild(li_header);
    if (typeof(root[key]) == 'object') {
      WalkStructure(root[key], li);
    }
  }
}

function Draw() {
  WalkStructure(structure, div);
};
