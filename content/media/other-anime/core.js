var show_columns = {
    'title': true,
    'score': true,
    //
    'audience-age': null,
    'available': null,
    'episodes': null,
    'fanservice': null,
    'genre': null,
    'gore': null,
    'aesthetics': null,
    'music': null,
    'music-theme': null,
    'released': null,
    'series': null,
};
var show_watched = true;
var watched = [];

//var watched = get('other-anime/data.js');

function main() {
  get('other-anime/data.js')
  .then((d) => {
    processData(d);
  });

}

function processData(watched) {
  // Draw show/hide cols drop-down
  var cols = document.getElementById('cols');
  for (var field in show_columns) {
    if (field == 'title') {
      continue;
    }
    const i = document.createElement('option');
    i.innerHTML = field;
    cols.appendChild(i);
  }

  // Draw table header
  var t = document.getElementById('t-watched-head');
  for (var field in show_columns) {
    var th = document.createElement('th');
    if (show_columns[field]) {
      th.style = 'display: table-cell';
    } else {
      th.style = 'display: none';
    }
    th.classList.add('td-' + field);
    th.innerHTML = field;
    t.appendChild(th);
  }

  // Draw table body
  var t = document.getElementById('t-watched-body');
  for (var entry of watched) {
    var tr = document.createElement('tr');
    tr.classList.add('tr-entry');
    for (var field in show_columns) {
      var td = document.createElement('td');

      // Not all fields are displayed
      if (show_columns[field]) {
        td.style = 'display: table-cell;';
      } else {
        td.style = 'display: none;';
      }
      td.classList.add('td-' + field);
      td.innerHTML = entry[field];

      // If title has a score, it has been watched
      if (entry['score'] === '') tr.watched = false;
      else tr.watched = true;
      tr.appendChild(td);
    }
    t.appendChild(tr);
  }
  var t = document.getElementById('t-watched');
  sorttable.makeSortable(t);
  showHideWatched();
}


function showHideCol(col_name) {
  // Flip shown value
  show_columns[col_name] = !show_columns[col_name];
  var is_shown = show_columns[col_name];

  // Show/hide table content
  var items = document.getElementsByClassName('td-' + col_name);
  for (i in items) {
    if (is_shown) {
      items[i].style = 'display: table-cell';
    } else {
      items[i].style = 'display: none';
    }
  }
}


function showHideWatched() {
  // Show or hide titles, depending it they have been watched or unwatched
  // global watched_unwatched = 'watched' or else
  var rows = document.getElementsByClassName('tr-entry');
  for (let i = 0; i < rows.length; i++) {
    if (show_watched == rows[i].watched) {
      rows[i].style = 'display: table-row';
    } else {
      rows[i].style = 'display: none';
    }
  }
}


function flipShowHideWatched() {
  // Flip ShowHideWatched
  // global watched_unwatched = 'watched' or else
  show_watched = !show_watched;
  showHideWatched();
}

function get(url) {
  let req = new XMLHttpRequest();
  url = '../../media/' + url;
  return new Promise((resolve, reject) => {
    req.open('GET', url, true);

    // Handler
    req.onreadystatechange = function() {
      if(req.readyState == 4 && req.status == 200) {
        resolve(JSON.parse(req.responseText));
      }
    }
  req.send();
  })
}
