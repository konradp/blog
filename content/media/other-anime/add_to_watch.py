#!/usr/bin/env python3
import json
import sys

def usage():
  print('Usage: ./add_to_watch.py NAME')
  print('Example ./add_to_watch.py "Knights of Sidonia"')
  exit(1)

def main():
  if len(sys.argv) != 2:
    usage()
  with open('data.js', 'r') as f:
    data = json.loads(f.read())
    entry = {
      'title': sys.argv[1],
      'score': '',
      'audience-age': None,
      'available': 'no',
      'episodes': None,
      'fanservice': None,
      'genre': None,
      'gore': None,
      'music': None,
      'music-theme': None,
      'released': None,
      'series': None,
    }
    data.append(entry)
    print(json.dumps(entry, indent=2))
    confirm = input('Will add the above, is this correct? (y/n): ')
    if confirm == 'y':
      with open('data.js', 'w') as f:
        f.write(json.dumps(data, indent=2))

      
if __name__ == '__main__':
  sys.exit(main())
