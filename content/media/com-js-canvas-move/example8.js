class Example8 {
  constructor(canvas_id, control_id, label_id) {
    // canvas_id
    // control_id: an input type=number for the radius control
    // label_id: a label or text field to display current radius number
    this.canvas = document.getElementById(canvas_id);
    this.div_radius = document.getElementById(control_id);
    this.div_radius_label = document.getElementById(label_id);
    this.context = this.canvas.getContext('2d');
    this.isDragging = false;
    this.currentDragging = null;
    this.mousePos = { x: 100, y: 100, };
    this.coords = {
      start: { x: 200, y: 20 },
      ctrl1: { x: 200, y: 130 },
      ctrl2: { x: 50, y: 20 },
      radius: 40,
    };
    this.points = {
      start: null,
      ctrl1: null,
      ctrl2: null,
    };
    this.point_size = 5;

    // Callbacks
    this.canvas.addEventListener('mousedown', (e) => {
      const mouse = this.GetMousePos(e);
      const c = this.context;
      const points = this.points;

      for (let point in points) {
        if (c.isPointInPath(points[point], mouse.x, mouse.y)) {
          this.isDragging = true;
          this.currentDragging = point;
        }
      }
    });
    this.canvas.addEventListener('mouseup', () => {
      this.isDragging = false;
    });
    this.canvas.addEventListener('mousemove', (e) => {
      if (this.isDragging) {
        const mousePos = this.GetMousePos(e);
        this.mousePos = this.GetMousePos(e);
        this.coords[this.currentDragging] = {
          x: mousePos.x,
          y: mousePos.y,
        };
        this.Draw();
      }
    });
    this.div_radius.addEventListener('input', (e) => {
      this.div_radius_label.innerHTML = e.target.value;
      this.coords.radius = e.target.value;
      e.target.innerHTML = e.target.value;
      this.Draw();
    });

    // MAIN
    this.Draw();
  }

  GetMousePos(e) {
    var rect = this.canvas.getBoundingClientRect();
    return {
      x: e.clientX - rect.left,
      y: e.clientY - rect.top
    };
  }

  Draw() {
    const c = this.context;
    const coords = this.coords;
    let points = this.points;
    c.clearRect(0, 0, this.canvas.width, this.canvas.height);

    // Adapted from
    // https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/arcTo
    // Tangential lines
    c.beginPath();
    c.strokeStyle = 'gray';
    c.lineWidth = 1;
    c.moveTo(coords.start.x, coords.start.y);
    c.lineTo(coords.ctrl1.x, coords.ctrl1.y);
    c.lineTo(coords.ctrl2.x, coords.ctrl2.y);
    c.stroke();

    // Arc
    c.beginPath();
    c.strokeStyle = 'black';
    c.lineWidth = 5;
    c.moveTo(this.coords.start.x, this.coords.start.y);
    c.arcTo(
      this.coords.ctrl1.x,
      this.coords.ctrl1.y,
      this.coords.ctrl2.x,
      this.coords.ctrl2.y,
      this.coords.radius,
    );
    c.stroke();

    // Start point
    points.start = new Path2D();
    //c.beginPath();
    c.fillStyle = 'blue';
    points.start.arc(coords.start.x, coords.start.y, this.point_size, 0, 2 * Math.PI);
    c.fill(points.start);

    // Control points
    //c.beginPath();
    c.fillStyle = 'red';
    points.ctrl1 = new Path2D();
    points.ctrl2 = new Path2D();
    //points.ctrl1.name = 'ctrl1';
    //points.ctrl2.name = 'ctrl2';
    points.ctrl1.arc(coords.ctrl1.x, coords.ctrl1.y, this.point_size, 0, 2 * Math.PI);
    points.ctrl2.arc(coords.ctrl2.x, coords.ctrl2.y, this.point_size, 0, 2 * Math.PI);
    c.fill(points.ctrl1);
    c.fill(points.ctrl2);
  }
}; // class Example7
